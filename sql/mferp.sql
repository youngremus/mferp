/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50721
Source Host           : localhost:3306
Source Database       : guns

Target Server Type    : MYSQL
Target Server Version : 50721
File Encoding         : 65001

Date: 2018-06-26 23:10:40
*/

DROP DATABASE IF EXISTS mferp;
CREATE DATABASE IF NOT EXISTS mferp DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

USE mferp;

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `num` int(11) DEFAULT NULL COMMENT '排序',
  `pid` int(11) DEFAULT NULL COMMENT '父部门id',
  `pids` varchar(255) DEFAULT NULL COMMENT '父级ids',
  `simplename` varchar(45) DEFAULT NULL COMMENT '简称',
  `fullname` varchar(255) DEFAULT NULL COMMENT '全称',
  `tips` varchar(255) DEFAULT NULL COMMENT '提示',
  `version` int(11) DEFAULT NULL COMMENT '版本（乐观锁保留字段）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='部门表';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('24', '1', '0', '[0],', '总公司', '总公司', '', null);
INSERT INTO `sys_dept` VALUES ('25', '2', '24', '[0],[24],', '开发部', '开发部', '', null);
INSERT INTO `sys_dept` VALUES ('26', '3', '24', '[0],[24],', '运营部', '运营部', '', null);
INSERT INTO `sys_dept` VALUES ('27', '4', '24', '[0],[24],', '战略部', '战略部', '', null);

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `num` int(11) DEFAULT NULL COMMENT '排序',
  `pid` int(11) DEFAULT NULL COMMENT '父级字典',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `tips` varchar(255) DEFAULT NULL COMMENT '提示',
  `code` varchar(255) DEFAULT NULL COMMENT '值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='字典表';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('50', '0', '0', '性别', null, 'sys_sex');
INSERT INTO `sys_dict` VALUES ('51', '1', '50', '男', null, '1');
INSERT INTO `sys_dict` VALUES ('52', '2', '50', '女', null, '2');
INSERT INTO `sys_dict` VALUES ('53', '0', '0', '状态', null, 'sys_state');
INSERT INTO `sys_dict` VALUES ('54', '1', '53', '启用', null, '1');
INSERT INTO `sys_dict` VALUES ('55', '2', '53', '禁用', null, '2');
INSERT INTO `sys_dict` VALUES ('56', '0', '0', '账号状态', null, 'account_state');
INSERT INTO `sys_dict` VALUES ('57', '1', '56', '启用', null, '1');
INSERT INTO `sys_dict` VALUES ('58', '2', '56', '冻结', null, '2');
INSERT INTO `sys_dict` VALUES ('59', '3', '56', '已删除', null, '3');

-- ----------------------------
-- Table structure for sys_expense
-- ----------------------------
DROP TABLE IF EXISTS `sys_expense`;
CREATE TABLE `sys_expense` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `money` decimal(20,2) DEFAULT NULL COMMENT '报销金额',
  `desc` varchar(255) DEFAULT '' COMMENT '描述',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `state` int(11) DEFAULT NULL COMMENT '状态: 1.待提交  2:待审核   3.审核通过 4:驳回',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `processId` varchar(255) DEFAULT NULL COMMENT '流程定义id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='报销表';

-- ----------------------------
-- Records of sys_expense
-- ----------------------------

-- ----------------------------
-- Table structure for sys_login_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_log`;
CREATE TABLE `sys_login_log` (
  `id` int(65) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `logname` varchar(255) DEFAULT NULL COMMENT '日志名称',
  `userid` int(65) DEFAULT NULL COMMENT '管理员id',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `succeed` varchar(255) DEFAULT NULL COMMENT '是否执行成功',
  `message` text COMMENT '具体消息',
  `ip` varchar(255) DEFAULT NULL COMMENT '登录ip',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=217 DEFAULT CHARSET=utf8 COMMENT='登录记录';

-- ----------------------------
-- Records of sys_login_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `code` varchar(255) DEFAULT NULL COMMENT '菜单编号',
  `pcode` varchar(255) DEFAULT NULL COMMENT '菜单父编号',
  `pcodes` varchar(255) DEFAULT NULL COMMENT '当前菜单的所有父菜单编号',
  `name` varchar(255) DEFAULT NULL COMMENT '菜单名称',
  `icon` varchar(255) DEFAULT NULL COMMENT '菜单图标',
  `url` varchar(255) DEFAULT NULL COMMENT 'url地址',
  `num` int(65) DEFAULT NULL COMMENT '菜单排序号',
  `levels` int(65) DEFAULT NULL COMMENT '菜单层级',
  `ismenu` int(11) DEFAULT NULL COMMENT '是否是菜单（1：是  0：不是）',
  `tips` varchar(255) DEFAULT NULL COMMENT '备注',
  `status` int(65) DEFAULT NULL COMMENT '菜单状态 :  1:启用   0:不启用',
  `isopen` int(11) DEFAULT NULL COMMENT '是否打开:    1:打开   0:不打开',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('105', 'system', '0', '[0],', '系统管理', 'fa-user', '#', '4', '1', '1', null, '1', '1');
INSERT INTO `sys_menu` VALUES ('106', 'mgr', 'system', '[0],[system],', '用户管理', '', '/mgr', '1', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('107', 'mgr_add', 'mgr', '[0],[system],[mgr],', '添加用户', null, '/mgr/add', '1', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('108', 'mgr_edit', 'mgr', '[0],[system],[mgr],', '修改用户', null, '/mgr/edit', '2', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('109', 'mgr_delete', 'mgr', '[0],[system],[mgr],', '删除用户', null, '/mgr/delete', '3', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('110', 'mgr_reset', 'mgr', '[0],[system],[mgr],', '重置密码', null, '/mgr/reset', '4', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('111', 'mgr_freeze', 'mgr', '[0],[system],[mgr],', '冻结用户', null, '/mgr/freeze', '5', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('112', 'mgr_unfreeze', 'mgr', '[0],[system],[mgr],', '解除冻结用户', null, '/mgr/unfreeze', '6', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('113', 'mgr_setRole', 'mgr', '[0],[system],[mgr],', '分配角色', null, '/mgr/setRole', '7', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('114', 'role', 'system', '[0],[system],', '角色管理', null, '/role', '2', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('115', 'role_add', 'role', '[0],[system],[role],', '添加角色', null, '/role/add', '1', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('116', 'role_edit', 'role', '[0],[system],[role],', '修改角色', null, '/role/edit', '2', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('117', 'role_remove', 'role', '[0],[system],[role],', '删除角色', null, '/role/remove', '3', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('118', 'role_setAuthority', 'role', '[0],[system],[role],', '配置权限', null, '/role/setAuthority', '4', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('119', 'menu', 'system', '[0],[system],', '菜单管理', null, '/menu', '4', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('120', 'menu_add', 'menu', '[0],[system],[menu],', '添加菜单', null, '/menu/add', '1', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('121', 'menu_edit', 'menu', '[0],[system],[menu],', '修改菜单', null, '/menu/edit', '2', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('122', 'menu_remove', 'menu', '[0],[system],[menu],', '删除菜单', null, '/menu/remove', '3', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('128', 'log', 'system', '[0],[system],', '业务日志', null, '/log', '6', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('130', 'druid', 'system', '[0],[system],', '监控管理', null, '/druid', '7', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('131', 'dept', 'system', '[0],[system],', '部门管理', null, '/dept', '3', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('132', 'dict', 'system', '[0],[system],', '字典管理', null, '/dict', '4', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('133', 'loginLog', 'system', '[0],[system],', '登录日志', null, '/loginLog', '6', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('134', 'log_clean', 'log', '[0],[system],[log],', '清空日志', null, '/log/delLog', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('135', 'dept_add', 'dept', '[0],[system],[dept],', '添加部门', null, '/dept/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('136', 'dept_update', 'dept', '[0],[system],[dept],', '修改部门', null, '/dept/update', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('137', 'dept_delete', 'dept', '[0],[system],[dept],', '删除部门', null, '/dept/delete', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('138', 'dict_add', 'dict', '[0],[system],[dict],', '添加字典', null, '/dict/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('139', 'dict_update', 'dict', '[0],[system],[dict],', '修改字典', null, '/dict/update', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('140', 'dict_delete', 'dict', '[0],[system],[dict],', '删除字典', null, '/dict/delete', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('141', 'notice', 'system', '[0],[system],', '通知管理', null, '/notice', '9', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('142', 'notice_add', 'notice', '[0],[system],[notice],', '添加通知', null, '/notice/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('143', 'notice_update', 'notice', '[0],[system],[notice],', '修改通知', null, '/notice/update', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('144', 'notice_delete', 'notice', '[0],[system],[notice],', '删除通知', null, '/notice/delete', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('145', 'hello', '0', '[0],', '通知', 'fa-rocket', '/notice/hello', '1', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('148', 'code', '0', '[0],', '代码生成', 'fa-code', '/code', '3', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('149', 'api_mgr', '0', '[0],', '接口文档', 'fa-leaf', '/swagger-ui.html', '2', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('150', 'to_menu_edit', 'menu', '[0],[system],[menu],', '菜单编辑跳转', '', '/menu/menu_edit', '4', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('151', 'menu_list', 'menu', '[0],[system],[menu],', '菜单列表', '', '/menu/list', '5', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('152', 'to_dept_update', 'dept', '[0],[system],[dept],', '修改部门跳转', '', '/dept/dept_update', '4', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('153', 'dept_list', 'dept', '[0],[system],[dept],', '部门列表', '', '/dept/list', '5', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('154', 'dept_detail', 'dept', '[0],[system],[dept],', '部门详情', '', '/dept/detail', '6', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('155', 'to_dict_edit', 'dict', '[0],[system],[dict],', '修改菜单跳转', '', '/dict/dict_edit', '4', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('156', 'dict_list', 'dict', '[0],[system],[dict],', '字典列表', '', '/dict/list', '5', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('157', 'dict_detail', 'dict', '[0],[system],[dict],', '字典详情', '', '/dict/detail', '6', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('158', 'log_list', 'log', '[0],[system],[log],', '日志列表', '', '/log/list', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('159', 'log_detail', 'log', '[0],[system],[log],', '日志详情', '', '/log/detail', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('160', 'del_login_log', 'loginLog', '[0],[system],[loginLog],', '清空登录日志', '', '/loginLog/delLoginLog', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('161', 'login_log_list', 'loginLog', '[0],[system],[loginLog],', '登录日志列表', '', '/loginLog/list', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('162', 'to_role_edit', 'role', '[0],[system],[role],', '修改角色跳转', '', '/role/role_edit', '5', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('163', 'to_role_assign', 'role', '[0],[system],[role],', '角色分配跳转', '', '/role/role_assign', '6', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('164', 'role_list', 'role', '[0],[system],[role],', '角色列表', '', '/role/list', '7', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('165', 'to_assign_role', 'mgr', '[0],[system],[mgr],', '分配角色跳转', '', '/mgr/role_assign', '8', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('166', 'to_user_edit', 'mgr', '[0],[system],[mgr],', '编辑用户跳转', '', '/mgr/user_edit', '9', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('167', 'mgr_list', 'mgr', '[0],[system],[mgr],', '用户列表', '', '/mgr/list', '10', '3', '0', null, '1', null);

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `type` int(11) DEFAULT NULL COMMENT '类型',
  `content` text COMMENT '内容',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `creater` int(11) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='通知表';

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES ('6', '世界', '10', '欢迎使用Guns管理系统', '2017-01-11 08:53:20', '1');
INSERT INTO `sys_notice` VALUES ('8', '你好', null, '你好', '2017-05-10 19:28:57', '1');

-- ----------------------------
-- Table structure for sys_operation_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_operation_log`;
CREATE TABLE `sys_operation_log` (
  `id` int(65) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `logtype` varchar(255) DEFAULT NULL COMMENT '日志类型',
  `logname` varchar(255) DEFAULT NULL COMMENT '日志名称',
  `userid` int(65) DEFAULT NULL COMMENT '用户id',
  `classname` varchar(255) DEFAULT NULL COMMENT '类名称',
  `method` text COMMENT '方法名称',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `succeed` varchar(255) DEFAULT NULL COMMENT '是否成功',
  `message` text COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=554 DEFAULT CHARSET=utf8 COMMENT='操作日志';

-- ----------------------------
-- Records of sys_operation_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_relation
-- ----------------------------
DROP TABLE IF EXISTS `sys_relation`;
CREATE TABLE `sys_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `menuid` bigint(11) DEFAULT NULL COMMENT '菜单id',
  `roleid` int(11) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3792 DEFAULT CHARSET=utf8 COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of sys_relation
-- ----------------------------
INSERT INTO `sys_relation` VALUES ('3377', '105', '5');
INSERT INTO `sys_relation` VALUES ('3378', '106', '5');
INSERT INTO `sys_relation` VALUES ('3379', '107', '5');
INSERT INTO `sys_relation` VALUES ('3380', '108', '5');
INSERT INTO `sys_relation` VALUES ('3381', '109', '5');
INSERT INTO `sys_relation` VALUES ('3382', '110', '5');
INSERT INTO `sys_relation` VALUES ('3383', '111', '5');
INSERT INTO `sys_relation` VALUES ('3384', '112', '5');
INSERT INTO `sys_relation` VALUES ('3385', '113', '5');
INSERT INTO `sys_relation` VALUES ('3386', '114', '5');
INSERT INTO `sys_relation` VALUES ('3387', '115', '5');
INSERT INTO `sys_relation` VALUES ('3388', '116', '5');
INSERT INTO `sys_relation` VALUES ('3389', '117', '5');
INSERT INTO `sys_relation` VALUES ('3390', '118', '5');
INSERT INTO `sys_relation` VALUES ('3391', '119', '5');
INSERT INTO `sys_relation` VALUES ('3392', '120', '5');
INSERT INTO `sys_relation` VALUES ('3393', '121', '5');
INSERT INTO `sys_relation` VALUES ('3394', '122', '5');
INSERT INTO `sys_relation` VALUES ('3395', '150', '5');
INSERT INTO `sys_relation` VALUES ('3396', '151', '5');
INSERT INTO `sys_relation` VALUES ('3737', '105', '1');
INSERT INTO `sys_relation` VALUES ('3738', '106', '1');
INSERT INTO `sys_relation` VALUES ('3739', '107', '1');
INSERT INTO `sys_relation` VALUES ('3740', '108', '1');
INSERT INTO `sys_relation` VALUES ('3741', '109', '1');
INSERT INTO `sys_relation` VALUES ('3742', '110', '1');
INSERT INTO `sys_relation` VALUES ('3743', '111', '1');
INSERT INTO `sys_relation` VALUES ('3744', '112', '1');
INSERT INTO `sys_relation` VALUES ('3745', '113', '1');
INSERT INTO `sys_relation` VALUES ('3746', '165', '1');
INSERT INTO `sys_relation` VALUES ('3747', '166', '1');
INSERT INTO `sys_relation` VALUES ('3748', '167', '1');
INSERT INTO `sys_relation` VALUES ('3749', '114', '1');
INSERT INTO `sys_relation` VALUES ('3750', '115', '1');
INSERT INTO `sys_relation` VALUES ('3751', '116', '1');
INSERT INTO `sys_relation` VALUES ('3752', '117', '1');
INSERT INTO `sys_relation` VALUES ('3753', '118', '1');
INSERT INTO `sys_relation` VALUES ('3754', '162', '1');
INSERT INTO `sys_relation` VALUES ('3755', '163', '1');
INSERT INTO `sys_relation` VALUES ('3756', '164', '1');
INSERT INTO `sys_relation` VALUES ('3757', '119', '1');
INSERT INTO `sys_relation` VALUES ('3758', '120', '1');
INSERT INTO `sys_relation` VALUES ('3759', '121', '1');
INSERT INTO `sys_relation` VALUES ('3760', '122', '1');
INSERT INTO `sys_relation` VALUES ('3761', '150', '1');
INSERT INTO `sys_relation` VALUES ('3762', '151', '1');
INSERT INTO `sys_relation` VALUES ('3763', '128', '1');
INSERT INTO `sys_relation` VALUES ('3764', '134', '1');
INSERT INTO `sys_relation` VALUES ('3765', '158', '1');
INSERT INTO `sys_relation` VALUES ('3766', '159', '1');
INSERT INTO `sys_relation` VALUES ('3767', '130', '1');
INSERT INTO `sys_relation` VALUES ('3768', '131', '1');
INSERT INTO `sys_relation` VALUES ('3769', '135', '1');
INSERT INTO `sys_relation` VALUES ('3770', '136', '1');
INSERT INTO `sys_relation` VALUES ('3771', '137', '1');
INSERT INTO `sys_relation` VALUES ('3772', '152', '1');
INSERT INTO `sys_relation` VALUES ('3773', '153', '1');
INSERT INTO `sys_relation` VALUES ('3774', '154', '1');
INSERT INTO `sys_relation` VALUES ('3775', '132', '1');
INSERT INTO `sys_relation` VALUES ('3776', '138', '1');
INSERT INTO `sys_relation` VALUES ('3777', '139', '1');
INSERT INTO `sys_relation` VALUES ('3778', '140', '1');
INSERT INTO `sys_relation` VALUES ('3779', '155', '1');
INSERT INTO `sys_relation` VALUES ('3780', '156', '1');
INSERT INTO `sys_relation` VALUES ('3781', '157', '1');
INSERT INTO `sys_relation` VALUES ('3782', '133', '1');
INSERT INTO `sys_relation` VALUES ('3783', '160', '1');
INSERT INTO `sys_relation` VALUES ('3784', '161', '1');
INSERT INTO `sys_relation` VALUES ('3785', '141', '1');
INSERT INTO `sys_relation` VALUES ('3786', '142', '1');
INSERT INTO `sys_relation` VALUES ('3787', '143', '1');
INSERT INTO `sys_relation` VALUES ('3788', '144', '1');
INSERT INTO `sys_relation` VALUES ('3789', '145', '1');
INSERT INTO `sys_relation` VALUES ('3790', '148', '1');
INSERT INTO `sys_relation` VALUES ('3791', '149', '1');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `num` int(11) DEFAULT NULL COMMENT '序号',
  `pid` int(11) DEFAULT NULL COMMENT '父角色id',
  `name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `deptid` int(11) DEFAULT NULL COMMENT '部门名称',
  `tips` varchar(255) DEFAULT NULL COMMENT '提示',
  `version` int(11) DEFAULT NULL COMMENT '保留字段(暂时没用）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '1', '0', '超级管理员', '24', 'administrator', '1');
INSERT INTO `sys_role` VALUES ('5', '2', '1', '临时', '26', 'temp', null);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `account` varchar(45) DEFAULT NULL COMMENT '账号',
  `password` varchar(45) DEFAULT NULL COMMENT '密码',
  `salt` varchar(45) DEFAULT NULL COMMENT 'md5密码盐',
  `name` varchar(45) DEFAULT NULL COMMENT '名字',
  `birthday` datetime DEFAULT NULL COMMENT '生日',
  `sex` int(11) DEFAULT NULL COMMENT '性别（1：男 2：女）',
  `email` varchar(45) DEFAULT NULL COMMENT '电子邮件',
  `phone` varchar(45) DEFAULT NULL COMMENT '电话',
  `roleid` varchar(255) DEFAULT NULL COMMENT '角色id',
  `deptid` int(11) DEFAULT NULL COMMENT '部门id',
  `status` int(11) DEFAULT NULL COMMENT '状态(1：启用  2：冻结  3：删除）',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `version` int(11) DEFAULT NULL COMMENT '保留字段',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'timg.jpg', 'admin', 'ecfadcde9305f8891bcfe5a1e28c253e', '8pgby', '张三', '2017-05-05 00:00:00', '2', 'sn93@qq.com', '18200000000', '1', '27', '1', '2016-01-29 08:49:53', '25');
INSERT INTO `sys_user` VALUES ('44', null, 'test', '45abb7879f6a8268f1ef600e6038ac73', 'ssts3', 'test', '2017-05-01 00:00:00', '1', 'abc@123.com', '', '5', '26', '3', '2017-05-16 20:33:37', null);
INSERT INTO `sys_user` VALUES ('45', null, 'boss', '71887a5ad666a18f709e1d4e693d5a35', '1f7bf', '老板', '2017-12-04 00:00:00', '1', '', '', '1', '24', '1', '2017-12-04 22:24:02', null);
INSERT INTO `sys_user` VALUES ('46', null, 'manager', 'b53cac62e7175637d4beb3b16b2f7915', 'j3cs9', '经理', '2017-12-04 00:00:00', '1', '', '', '1', '24', '1', '2017-12-04 22:24:24', null);

insert into `sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) values('168','db','0','[0],','数据管理','fa-database','/#','10','1','1',NULL,'1',NULL);

DROP TABLE IF EXISTS `mf_db_source`;
CREATE TABLE `mf_db_source` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `remark` VARCHAR(500) DEFAULT NULL COMMENT '备注',
  `create_time` DATETIME DEFAULT NULL COMMENT '创建时间',
  `update_time` DATETIME DEFAULT NULL COMMENT '更新时间',
  `source_name` VARCHAR(255) DEFAULT NULL COMMENT '数据源名称',
  `db_name` VARCHAR(100) DEFAULT NULL COMMENT '数据库名称',
  `db_type` VARCHAR(32) DEFAULT NULL COMMENT '数据库类型',
  `db_url` VARCHAR(500) DEFAULT NULL COMMENT '链接地址',
  `db_driver` VARCHAR(100) DEFAULT NULL COMMENT '数据库驱动',
  `username` VARCHAR(100) DEFAULT NULL COMMENT '账号',
  `password` VARCHAR(100) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='数据源信息';

insert into `sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) values('1262051110444920833','mfDbSource','db','[0],[db],','数据源管理','fa-arrows-v','/mfDbSource','99','2','1',NULL,'1','0');
insert into `sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) values('1262051110444920834','mfDbSource_list','mfDbSource','[0],[db],[mfDbSource],','列表','','/mfDbSource/list','99','3','0',NULL,'1','0');
insert into `sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) values('1262051110444920835','mfDbSource_add','mfDbSource','[0],[db],[mfDbSource],','添加','','/mfDbSource/add','99','3','0',NULL,'1','0');
insert into `sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) values('1262051110444920836','mfDbSource_update','mfDbSource','[0],[db],[mfDbSource],','更新','','/mfDbSource/update','99','3','0',NULL,'1','0');
insert into `sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) values('1262051110444920837','mfDbSource_delete','mfDbSource','[0],[db],[mfDbSource],','删除','','/mfDbSource/delete','99','3','0',NULL,'1','0');
insert into `sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) values('1262051110444920838','mfDbSource_detail','mfDbSource','[0],[db],[mfDbSource],','详情','','/mfDbSource/detail','99','3','0',NULL,'1','0');

DROP TABLE IF EXISTS `mf_sql_command`;
CREATE TABLE `mf_sql_command` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `db_source_id` int(11) DEFAULT NULL COMMENT '数据源编号',
  `db_source_name` varchar(255) DEFAULT NULL COMMENT '数据源名称',
  `cmd_text` varchar(1000) DEFAULT NULL COMMENT '命令文本',
  `cmd_type` varchar(32) DEFAULT NULL COMMENT '命令类型',
  `enabled_status` varchar(32) DEFAULT NULL COMMENT '启用状态',
  `is_show` varchar(100) DEFAULT NULL COMMENT '是否显示',
  `cmd_name` varchar(100) DEFAULT NULL COMMENT '命令名称',
  `cmd_cname` varchar(100) DEFAULT NULL COMMENT '命令中文名称',
  `verify_pwd` varchar(100) DEFAULT NULL COMMENT '验证密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='命令信息';

insert into `sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) values('1262053259891556354','sqlCommand','db','[0],[db],','命令管理','fa-bell','/sqlCommand','99','2','1',NULL,'1','0');
insert into `sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) values('1262053259891556355','sqlCommand_list','sqlCommand','[0],[db],[sqlCommand],','列表','','/sqlCommand/list','99','3','0',NULL,'1','0');
insert into `sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) values('1262053259891556356','sqlCommand_add','sqlCommand','[0],[db],[sqlCommand],','添加','','/sqlCommand/add','99','3','0',NULL,'1','0');
insert into `sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) values('1262053259891556357','sqlCommand_update','sqlCommand','[0],[db],[sqlCommand],','更新','','/sqlCommand/update','99','3','0',NULL,'1','0');
insert into `sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) values('1262053259891556358','sqlCommand_delete','sqlCommand','[0],[db],[sqlCommand],','删除','','/sqlCommand/delete','99','3','0',NULL,'1','0');
insert into `sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) values('1262053259891556359','sqlCommand_detail','sqlCommand','[0],[db],[sqlCommand],','详情','','/sqlCommand/detail','99','3','0',NULL,'1','0');

/*生产记录表*/
CREATE TABLE `tjlb` (
  `FNo` INT(11) NOT NULL AUTO_INCREMENT COMMENT '小票编号',
  `Fguid` VARCHAR(64) DEFAULT NULL COMMENT 'Guid值，插入新纪录时写入。根据此值可查出小票号fno',
  `FZt` VARCHAR(8) DEFAULT NULL COMMENT '状态',
  `FScrq` VARCHAR(10) DEFAULT NULL COMMENT '生产日期',
  `FScbt` VARCHAR(1) DEFAULT NULL COMMENT '生产拌台',
  `FDcsj` VARCHAR(10) DEFAULT NULL COMMENT '到厂时间',
  `FCcsj` VARCHAR(10) DEFAULT NULL COMMENT '出厂时间',
  `FRwdh` INT(11) DEFAULT NULL COMMENT '任务单号',
  `FHtbh` VARCHAR(20) DEFAULT '0' COMMENT '合同编号',
  `FKhbh` INT(11) DEFAULT NULL,
  `FHtdw` VARCHAR(128) DEFAULT NULL COMMENT '单位名称',
  `FGcmc` VARCHAR(128) DEFAULT NULL COMMENT '工程名称',
  `FGcdz` VARCHAR(128) DEFAULT NULL COMMENT '工程地址',
  `FJzbw` VARCHAR(128) DEFAULT NULL COMMENT '施工部位',
  `FJzfs` VARCHAR(30) DEFAULT NULL COMMENT '施工方法',
  `FGls` DOUBLE DEFAULT NULL COMMENT '公里数',
  `FTpz` VARCHAR(32) DEFAULT NULL COMMENT '砼品种',
  `FTld` VARCHAR(8) DEFAULT NULL COMMENT '坍落度',
  `FSgpb` INT(11) DEFAULT NULL COMMENT '配合比号',
  `FSjpb` INT(11) DEFAULT NULL COMMENT '砂浆配比',
  `FShch` VARCHAR(8) DEFAULT NULL COMMENT '车号',
  `FSjxm` VARCHAR(8) DEFAULT NULL COMMENT '司机',
  `FBcps` INT(11) DEFAULT NULL COMMENT '盘数',
  `FBcfs` DOUBLE DEFAULT NULL COMMENT '本车方数',
  `FZzl` DOUBLE DEFAULT NULL COMMENT '总重量',
  `FBsfs` DOUBLE DEFAULT NULL COMMENT '泵送数量',
  `FLjfs` DOUBLE DEFAULT NULL COMMENT '累计方数',
  `FLjcs` INT(11) DEFAULT NULL COMMENT '累计车数',
  `FCcqf` VARCHAR(8) DEFAULT NULL COMMENT '出厂签发',
  `FYhqs` VARCHAR(8) DEFAULT NULL COMMENT '用户签收',
  `FCzy` VARCHAR(8) DEFAULT NULL COMMENT '操作员',
  `FBz` VARCHAR(255) DEFAULT NULL COMMENT '备注',
  `FRwno` VARCHAR(20) DEFAULT NULL,
  `FHtno` VARCHAR(20) DEFAULT NULL COMMENT '合同编号',
  `FGpfs` VARCHAR(32) DEFAULT NULL,
  `FSpsj` VARCHAR(10) DEFAULT NULL,
  `FJbsj` INT(11) DEFAULT NULL COMMENT '搅拌时间',
  `FBranchId` VARCHAR(2) DEFAULT NULL,
  `FHdDateTime` VARCHAR(17) DEFAULT NULL,
  `FHdOperator` VARCHAR(8) DEFAULT NULL,
  `FMzKg` INT(11) DEFAULT NULL,
  `FPzKg` INT(11) DEFAULT NULL,
  `FJzKg` INT(11) DEFAULT NULL,
  `FRz` DOUBLE DEFAULT NULL,
  `FFlNum` DOUBLE DEFAULT NULL,
  `FClsj` VARCHAR(20) DEFAULT NULL,
  `FSby` VARCHAR(10) DEFAULT NULL,
  `FJby` VARCHAR(10) DEFAULT NULL,
  `FGpsj` VARCHAR(200) DEFAULT NULL,
  `FRecId` VARCHAR(16) DEFAULT NULL,
  `FPhbNo` VARCHAR(20) DEFAULT NULL COMMENT '配合比号',
  `FGlsID` INT(11) DEFAULT NULL,
  `FTlb` VARCHAR(8) DEFAULT NULL,
  `FBcfsC` DOUBLE DEFAULT NULL,
  `FBcfsM` DOUBLE DEFAULT NULL,
  `FPcbID` VARCHAR(32) DEFAULT NULL,
  `FPhbSJ` VARCHAR(20) DEFAULT NULL,
  `FA1` VARCHAR(8) DEFAULT NULL,
  `FA2` VARCHAR(16) DEFAULT NULL,
  `FA3` VARCHAR(24) DEFAULT NULL COMMENT '委托方任务编号',
  `FA4` VARCHAR(32) DEFAULT NULL,
  `FC1` INT(11) DEFAULT NULL,
  `FC2` INT(11) DEFAULT NULL,
  `FC3` INT(11) DEFAULT NULL,
  `FRvA` DOUBLE DEFAULT NULL COMMENT '余方量',
  `FRvB` DOUBLE DEFAULT NULL COMMENT '拌方量',
  `FRvC` DOUBLE DEFAULT NULL COMMENT '票方量',
  `FQvA` DOUBLE DEFAULT NULL COMMENT '退方量',
  `FQvB` DOUBLE DEFAULT NULL,
  `FQvC` DOUBLE DEFAULT NULL,
  `FIv0` INT(11) DEFAULT NULL,
  `FCcsjEx` VARCHAR(10) DEFAULT NULL,
  `FTmA` VARCHAR(8) DEFAULT NULL,
  `FTmB` VARCHAR(8) DEFAULT NULL,
  `Fdywc` INT(2) DEFAULT '0' COMMENT '小票是否打印完成',
  `Fqrscwc` INT(2) DEFAULT '1' COMMENT '确认生产完成',
  `Fdysj` DATETIME DEFAULT '0000-00-00 00:00:00' COMMENT '第一次小票打印的时间',
  `FVersion` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FXpKey` BIGINT(20) NOT NULL DEFAULT '0',
  `FXpNo` INT(11) DEFAULT NULL,
  PRIMARY KEY (`FNo`),
  KEY `FCcsj` (`FCcsj`),
  KEY `FRwdh` (`FRwdh`),
  KEY `FScbt` (`FScbt`),
  KEY `FScrq` (`FScrq`),
  KEY `FGcmc` (`FGcmc`,`FTpz`)
) ENGINE=INNODB AUTO_INCREMENT=1769 DEFAULT CHARSET=utf8 COMMENT '生产记录表';
INSERT INTO `sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1262754150944669697', 'tjlb', 'db', '[0],[db],', '生产记录', '', '/tjlb', '99', '2', '1', NULL, '1', '0');
INSERT INTO `sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1262754150953058306', 'tjlb_list', 'tjlb', '[0],[db],[tjlb],', '生产记录列表', '', '/tjlb/list', '99', '3', '0', NULL, '1', '0');
INSERT INTO `sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1262754150953058307', 'tjlb_add', 'tjlb', '[0],[db],[tjlb],', '生产记录添加', '', '/tjlb/add', '99', '3', '0', NULL, '1', '0');
INSERT INTO `sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1262754150953058308', 'tjlb_update', 'tjlb', '[0],[db],[tjlb],', '生产记录更新', '', '/tjlb/update', '99', '3', '0', NULL, '1', '0');
INSERT INTO `sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1262754150953058309', 'tjlb_delete', 'tjlb', '[0],[db],[tjlb],', '生产记录删除', '', '/tjlb/delete', '99', '3', '0', NULL, '1', '0');
INSERT INTO `sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1262754150953058310', 'tjlb_detail', 'tjlb', '[0],[db],[tjlb],', '生产记录详情', '', '/tjlb/detail', '99', '3', '0', NULL, '1', '0');

CREATE TABLE `trwd` (
  `FRwdh` int(11) NOT NULL AUTO_INCREMENT COMMENT '任务单号',
  `FHtbh` varchar(20) DEFAULT '0' COMMENT '合同编号',
  `FRwxz` varchar(8) DEFAULT NULL COMMENT '任务性质',
  `FRwly` varchar(8) DEFAULT NULL COMMENT '任务来源',
  `FZt` varchar(16) DEFAULT NULL COMMENT '状态',
  `FHtdw` varchar(128) DEFAULT NULL COMMENT '单位名称',
  `FGcmc` varchar(128) DEFAULT NULL COMMENT '工程名称',
  `FGcjb` varchar(8) DEFAULT NULL COMMENT '工程级别',
  `FGclb` varchar(8) DEFAULT NULL COMMENT '工程类别',
  `FJzbw` varchar(128) DEFAULT NULL COMMENT '施工部位',
  `FJzfs` varchar(30) DEFAULT NULL COMMENT '泵送',
  `FBclb` varchar(8) DEFAULT NULL,
  `FGcdz` varchar(128) DEFAULT NULL COMMENT '施工地点',
  `FGls` double DEFAULT NULL COMMENT '运距',
  `FJhrq` varchar(64) DEFAULT NULL COMMENT '计划日期',
  `FTpz` varchar(32) DEFAULT NULL COMMENT '砼品种',
  `FTld` varchar(8) DEFAULT NULL COMMENT '坍落度',
  `FSnbh` varchar(8) DEFAULT NULL COMMENT '水泥品种',
  `FSzgg` varchar(8) DEFAULT NULL COMMENT '石子规格',
  `FTbj` varchar(255) DEFAULT NULL COMMENT '备注|渗料规格|外加剂规格|抗渗等级|其他要求|施工单位联系人|工程编号|营销部门联系人|监督号码|优先采用的仓库编号|监控系统工地号|质检员',
  `FJhsl` double DEFAULT NULL COMMENT '计划方量',
  `FScbt` varchar(8) DEFAULT NULL COMMENT '生产拌台',
  `FWcsl` double DEFAULT NULL COMMENT '完成方量/砼数量',
  `FLjcs` int(11) DEFAULT NULL COMMENT '累计车数',
  `FXdrw` varchar(8) DEFAULT NULL COMMENT '下达任务',
  `FCzy` varchar(8) DEFAULT NULL COMMENT '操作员',
  `FDlrq` varchar(10) DEFAULT NULL COMMENT '登录日期',
  `FSgpb` int(11) DEFAULT NULL COMMENT '施工配合比号',
  `FSyy1` varchar(8) DEFAULT NULL COMMENT '试验员',
  `FSjpb` int(11) DEFAULT NULL COMMENT '砂浆配合比号',
  `FSyy2` varchar(8) DEFAULT NULL COMMENT '试验员',
  `FZdpb` varchar(8) DEFAULT NULL,
  `FTjsj` varchar(17) DEFAULT NULL,
  `FTjr` varchar(8) DEFAULT NULL,
  `FKsrq` varchar(20) DEFAULT NULL,
  `FWcrq` varchar(20) DEFAULT NULL,
  `FXph1` int(11) DEFAULT NULL,
  `FXph2` int(11) DEFAULT NULL,
  `FKzsl` double DEFAULT NULL,
  `FKzdj` double DEFAULT NULL,
  `FKzje` double DEFAULT NULL,
  `FQrTsl` double DEFAULT NULL,
  `FQrTdj` double DEFAULT NULL,
  `FQrTje` double DEFAULT NULL,
  `FQrBssl` double DEFAULT NULL,
  `FQrBsdj` double DEFAULT NULL,
  `FQrBsje` double DEFAULT NULL,
  `FQrKzsl` double DEFAULT NULL,
  `FQrKzdj` double DEFAULT NULL,
  `FQrKzje` double DEFAULT NULL,
  `FQrZjje` double DEFAULT NULL,
  `FQrBz` varchar(32) DEFAULT NULL,
  `FQrFzr` varchar(8) DEFAULT NULL,
  `FQrrq` varchar(10) DEFAULT NULL,
  `FTdj` double DEFAULT NULL COMMENT '砼单价',
  `FTje` double DEFAULT NULL COMMENT '砼金额',
  `FBssl` double DEFAULT NULL COMMENT '泵送数量',
  `FBsdj` double DEFAULT NULL COMMENT '泵送单价',
  `FBsje` double DEFAULT NULL COMMENT '泵送金额',
  `FZje` double DEFAULT NULL,
  `FRwno` varchar(20) DEFAULT NULL,
  `FHtno` varchar(20) DEFAULT NULL,
  `FJbsj` int(11) DEFAULT NULL COMMENT '搅拌时间',
  `FClsjNo` int(11) DEFAULT NULL COMMENT '出砼门控制参数',
  `FJsdNo` varchar(20) DEFAULT NULL,
  `FBranchId` varchar(2) DEFAULT NULL,
  `FHdDateTime` varchar(17) DEFAULT NULL,
  `FHdOperator` varchar(8) DEFAULT NULL,
  `FZzl` double DEFAULT NULL,
  `FByDateTime` varchar(17) DEFAULT NULL,
  `FByOperator` varchar(8) DEFAULT NULL,
  `FByBz` varchar(255) DEFAULT NULL,
  `FC1` int(11) DEFAULT NULL,
  `FC2` int(11) DEFAULT NULL,
  `FC3` int(11) DEFAULT NULL,
  `FOperator` varchar(8) DEFAULT NULL,
  `FTimeStamp` varchar(19) DEFAULT NULL,
  `FCheckSum` int(11) DEFAULT NULL,
  `FCheckKey` int(11) DEFAULT NULL,
  `FMpfsP` int(11) DEFAULT NULL,
  `FPzzt` varchar(10) DEFAULT NULL,
  `FPzrq` varchar(20) DEFAULT NULL,
  `FPzr` varchar(10) DEFAULT NULL,
  `FMemo` varchar(100) DEFAULT NULL,
  `FPhfh` varchar(20) DEFAULT NULL,
  `FPhfhsj` varchar(20) DEFAULT NULL,
  `FPhbNo` varchar(20) DEFAULT NULL COMMENT '配合比号',
  `FBSfs` varchar(10) DEFAULT NULL,
  `FShb` varchar(10) DEFAULT NULL,
  `FPhbId` varchar(20) DEFAULT NULL,
  `FRz` varchar(10) DEFAULT NULL COMMENT '容重',
  `FQrZzl` double DEFAULT NULL,
  `FQrRz` varchar(10) DEFAULT NULL,
  `FA1` varchar(8) DEFAULT NULL,
  `FA2` varchar(16) DEFAULT NULL,
  `FA3` varchar(24) DEFAULT NULL,
  `FA4` varchar(32) DEFAULT NULL,
  `FSjb` varchar(8) DEFAULT NULL,
  `FSlPt` varchar(8) DEFAULT NULL,
  `FJnclKg` varchar(8) DEFAULT NULL,
  `FRvA` double DEFAULT NULL,
  `FRvB` double DEFAULT NULL,
  `FRvC` double DEFAULT NULL,
  `FQvA` double DEFAULT NULL,
  `FQvB` double DEFAULT NULL,
  `FQvC` double DEFAULT NULL,
  `FIv0` int(11) DEFAULT NULL,
  `Fwyzfs` double NOT NULL DEFAULT '0' COMMENT '外援总方数',
  `Fwyzcs` int(11) NOT NULL DEFAULT '0' COMMENT '外援总车数',
  `FVersion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FXpNo` int(11) DEFAULT NULL,
  `FXpKey` bigint(20) DEFAULT NULL,
  `FXgsj` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`FRwdh`),
  KEY `FZt` (`FZt`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2367 DEFAULT CHARSET=utf8 COMMENT='任务单';
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1265665553871532034', 'trwd', 'pfgl', '[0],[pfgl],', '任务单', '', '/trwd', '99', '2', '1', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1265665553871532035', 'trwd_list', 'trwd', '[0],[pfgl],[trwd],', '任务单列表', '', '/trwd/list', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1265665553871532036', 'trwd_add', 'trwd', '[0],[pfgl],[trwd],', '任务单添加', '', '/trwd/add', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1265665553871532037', 'trwd_update', 'trwd', '[0],[pfgl],[trwd],', '任务单更新', '', '/trwd/update', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1265665553871532038', 'trwd_delete', 'trwd', '[0],[pfgl],[trwd],', '任务单删除', '', '/trwd/delete', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1265665553871532039', 'trwd_detail', 'trwd', '[0],[pfgl],[trwd],', '任务单详情', '', '/trwd/detail', '99', '3', '0', NULL, '1', '0');


CREATE TABLE `trwdphb` (
  `FId` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `FRwdh` int(11) DEFAULT NULL COMMENT '任务单号',
  `FTzsj` varchar(20) DEFAULT NULL COMMENT '调整时间',
  `FPblx` varchar(4) DEFAULT NULL COMMENT '类别',
  `FPbh1` int(11) DEFAULT NULL COMMENT '调整前配比编号',
  `FPbh2` int(11) DEFAULT NULL COMMENT '调整后配比编号',
  `FXpbh` int(11) DEFAULT NULL,
  `FLjcs` int(11) DEFAULT NULL COMMENT '累计车数',
  `FCzy` varchar(8) DEFAULT NULL COMMENT '操作员',
  `FSyy` varchar(8) DEFAULT NULL COMMENT '试验员',
  `FBz` varchar(5000) DEFAULT NULL COMMENT '备注',
  `FNo` int(11) DEFAULT NULL,
  PRIMARY KEY (`FId`),
  KEY `FRwdh` (`FRwdh`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=327 DEFAULT CHARSET=utf8 COMMENT='任务单施工配比';
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1265665695253131265', 'trwdphb', 'pfgl', '[0],[pfgl],', '任务单施工配比', '', '/trwdphb', '99', '2', '1', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1265665695257325570', 'trwdphb_list', 'trwdphb', '[0],[pfgl],[trwdphb],', '任务单施工配比列表', '', '/trwdphb/list', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1265665695257325571', 'trwdphb_add', 'trwdphb', '[0],[pfgl],[trwdphb],', '任务单施工配比添加', '', '/trwdphb/add', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1265665695257325572', 'trwdphb_update', 'trwdphb', '[0],[pfgl],[trwdphb],', '任务单施工配比更新', '', '/trwdphb/update', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1265665695257325573', 'trwdphb_delete', 'trwdphb', '[0],[pfgl],[trwdphb],', '任务单施工配比删除', '', '/trwdphb/delete', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1265665695257325574', 'trwdphb_detail', 'trwdphb', '[0],[pfgl],[trwdphb],', '任务单施工配比详情', '', '/trwdphb/detail', '99', '3', '0', NULL, '1', '0');

CREATE TABLE `trwdphbycl` (
  `FId` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `FRwdh` int(11) DEFAULT NULL COMMENT '任务单号',
  `FPblb` int(11) DEFAULT NULL COMMENT '类别',
  `FYlmc` varchar(24) DEFAULT NULL COMMENT '材料名称',
  `FPzgg` varchar(24) DEFAULT NULL COMMENT '材料规格',
  `FSysl` double DEFAULT NULL COMMENT '用量',
  `FPlcw` int(11) DEFAULT NULL COMMENT '仓位',
  `FNdPt` double DEFAULT NULL,
  `FClPt` double DEFAULT NULL,
  `FAddnum` double DEFAULT NULL,
  `FPbsl` double DEFAULT NULL,
  `FSl` double DEFAULT NULL,
  `FPb_Num` double DEFAULT NULL,
  `FBtId` int(2) NOT NULL DEFAULT '0',
  `FCkno` varchar(16) DEFAULT NULL COMMENT '仓库编号',
  `FHsl` double NOT NULL DEFAULT '0' COMMENT '含水率',
  `FXpKey` bigint(20) DEFAULT NULL,
  `FVersion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FBigSandAdd` float DEFAULT NULL,
  `FProValue` float DEFAULT NULL,
  `FSandRate` float DEFAULT NULL,
  `FStoneRate` float DEFAULT NULL,
  `FWaterRate` float DEFAULT NULL,
  `FOrder` int(11) DEFAULT NULL,
  PRIMARY KEY (`FId`),
  KEY `FRwdh` (`FRwdh`,`FPblb`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=685750 DEFAULT CHARSET=utf8 COMMENT='任务单施工配比原材料';
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1265665777771868161', 'trwdphbycl', 'pfgl', '[0],[pfgl],', '任务单施工配比原材料', '', '/trwdphbycl', '99', '2', '1', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1265665777771868162', 'trwdphbycl_list', 'trwdphbycl', '[0],[pfgl],[trwdphbycl],', '任务单施工配比原材料列表', '', '/trwdphbycl/list', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1265665777771868163', 'trwdphbycl_add', 'trwdphbycl', '[0],[pfgl],[trwdphbycl],', '任务单施工配比原材料添加', '', '/trwdphbycl/add', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1265665777771868164', 'trwdphbycl_update', 'trwdphbycl', '[0],[pfgl],[trwdphbycl],', '任务单施工配比原材料更新', '', '/trwdphbycl/update', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1265665777771868165', 'trwdphbycl_delete', 'trwdphbycl', '[0],[pfgl],[trwdphbycl],', '任务单施工配比原材料删除', '', '/trwdphbycl/delete', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1265665777776062466', 'trwdphbycl_detail', 'trwdphbycl', '[0],[pfgl],[trwdphbycl],', '任务单施工配比原材料详情', '', '/trwdphbycl/detail', '99', '3', '0', NULL, '1', '0');

/*剩料记录表*/
create table offcut_log
(
   id                   int(11) not null comment '编号',
   origin_no            varchar(32) comment '原小票号',
   origin_order_no      varchar(32) comment '原单号',
   origin_concrete_type varchar(16) comment '原砼品种',
   origin_project_name  varchar(200) comment '原工程名称',
   origin_pour_position varchar(200) comment '原浇筑部位',
   origin_production_date varchar(32) comment '原出厂日期',
   origin_production_time varchar(32) comment '原出厂时间',
   origin_construction_method varchar(100) comment '原施工方法',
   car_number           varchar(32) comment '车号',
   current_no           varchar(32) comment '当前小票号',
   current_order_no     varchar(32) comment '当前单号',
   current_concrete_type varchar(16) comment '当前砼品种',
   current_project_name varchar(32) comment '当前工程名称',
   current_pour_position varchar(200) comment '当前浇筑部位',
   current_production_date varchar(32) comment '当前出厂日期',
   current_production_time varchar(32) comment '当前出厂时间',
   current_construction_method varchar(100) comment '当前施工方法',
   remark               varchar(200) comment '备注',
   primary key (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 comment '剩料记录表';

/*以下表配合Quartz使用*/
SET GLOBAL innodb_file_format = BARRACUDA;
SET GLOBAL innodb_large_prefix = ON;
SET GLOBAL innodb_file_per_table=1;

DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
SCHED_NAME VARCHAR(120) NOT NULL,
TRIGGER_NAME VARCHAR(200) NOT NULL,
TRIGGER_GROUP VARCHAR(200) NOT NULL,
JOB_NAME VARCHAR(200) NOT NULL,
JOB_GROUP VARCHAR(200) NOT NULL,
DESCRIPTION VARCHAR(250) NULL,
NEXT_FIRE_TIME BIGINT(13) NULL,
PREV_FIRE_TIME BIGINT(13) NULL,
PRIORITY INTEGER NULL,
TRIGGER_STATE VARCHAR(16) NOT NULL,
TRIGGER_TYPE VARCHAR(8) NOT NULL,
START_TIME BIGINT(13) NOT NULL,
END_TIME BIGINT(13) NULL,
CALENDAR_NAME VARCHAR(200) NULL,
MISFIRE_INSTR SMALLINT(2) NULL,
JOB_DATA BLOB NULL,
PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
FOREIGN KEY (SCHED_NAME,JOB_NAME,JOB_GROUP)
REFERENCES QRTZ_JOB_DETAILS(SCHED_NAME,JOB_NAME,JOB_GROUP)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
SCHED_NAME VARCHAR(120) NOT NULL,
ENTRY_ID VARCHAR(95) NOT NULL,
TRIGGER_NAME VARCHAR(200) NOT NULL,
TRIGGER_GROUP VARCHAR(200) NOT NULL,
INSTANCE_NAME VARCHAR(200) NOT NULL,
FIRED_TIME BIGINT(13) NOT NULL,
SCHED_TIME BIGINT(13) NOT NULL,
PRIORITY INTEGER NOT NULL,
STATE VARCHAR(16) NOT NULL,
JOB_NAME VARCHAR(200) NULL,
JOB_GROUP VARCHAR(200) NULL,
IS_NONCONCURRENT VARCHAR(1) NULL,
REQUESTS_RECOVERY VARCHAR(1) NULL,
PRIMARY KEY (SCHED_NAME,ENTRY_ID)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `SCHED_NAME` VARCHAR(120) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_NAME` VARCHAR(200) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` VARCHAR(200) COLLATE utf8mb4_bin NOT NULL,
  `BLOB_DATA` BLOB,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
)  ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `SCHED_NAME` VARCHAR(120) COLLATE utf8mb4_bin NOT NULL,
  `CALENDAR_NAME` VARCHAR(200) COLLATE utf8mb4_bin NOT NULL,
  `CALENDAR` BLOB NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `SCHED_NAME` VARCHAR(120) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_NAME` VARCHAR(200) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` VARCHAR(200) COLLATE utf8mb4_bin NOT NULL,
  `CRON_EXPRESSION` VARCHAR(200) COLLATE utf8mb4_bin NOT NULL,
  `TIME_ZONE_ID` VARCHAR(80) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `SCHED_NAME` VARCHAR(120) COLLATE utf8mb4_bin NOT NULL,
  `JOB_NAME` VARCHAR(200) COLLATE utf8mb4_bin NOT NULL,
  `JOB_GROUP` VARCHAR(200) COLLATE utf8mb4_bin NOT NULL,
  `DESCRIPTION` VARCHAR(250) COLLATE utf8mb4_bin DEFAULT NULL,
  `JOB_CLASS_NAME` VARCHAR(250) COLLATE utf8mb4_bin NOT NULL,
  `IS_DURABLE` VARCHAR(1) COLLATE utf8mb4_bin NOT NULL,
  `IS_NONCONCURRENT` VARCHAR(1) COLLATE utf8mb4_bin NOT NULL,
  `IS_UPDATE_DATA` VARCHAR(1) COLLATE utf8mb4_bin NOT NULL,
  `REQUESTS_RECOVERY` VARCHAR(1) COLLATE utf8mb4_bin NOT NULL,
  `JOB_DATA` BLOB,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `SCHED_NAME` VARCHAR(120) COLLATE utf8mb4_bin NOT NULL,
  `LOCK_NAME` VARCHAR(40) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `SCHED_NAME` VARCHAR(120) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` VARCHAR(200) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `SCHED_NAME` VARCHAR(120) COLLATE utf8mb4_bin NOT NULL,
  `INSTANCE_NAME` VARCHAR(200) COLLATE utf8mb4_bin NOT NULL,
  `LAST_CHECKIN_TIME` BIGINT(13) NOT NULL,
  `CHECKIN_INTERVAL` BIGINT(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `SCHED_NAME` VARCHAR(120) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_NAME` VARCHAR(200) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` VARCHAR(200) COLLATE utf8mb4_bin NOT NULL,
  `REPEAT_COUNT` BIGINT(7) NOT NULL,
  `REPEAT_INTERVAL` BIGINT(12) NOT NULL,
  `TIMES_TRIGGERED` BIGINT(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;


DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `SCHED_NAME` VARCHAR(120) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_NAME` VARCHAR(200) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` VARCHAR(200) COLLATE utf8mb4_bin NOT NULL,
  `STR_PROP_1` VARCHAR(512) COLLATE utf8mb4_bin DEFAULT NULL,
  `STR_PROP_2` VARCHAR(512) COLLATE utf8mb4_bin DEFAULT NULL,
  `STR_PROP_3` VARCHAR(512) COLLATE utf8mb4_bin DEFAULT NULL,
  `INT_PROP_1` INT(11) DEFAULT NULL,
  `INT_PROP_2` INT(11) DEFAULT NULL,
  `LONG_PROP_1` BIGINT(20) DEFAULT NULL,
  `LONG_PROP_2` BIGINT(20) DEFAULT NULL,
  `DEC_PROP_1` DECIMAL(13,4) DEFAULT NULL,
  `DEC_PROP_2` DECIMAL(13,4) DEFAULT NULL,
  `BOOL_PROP_1` VARCHAR(1) COLLATE utf8mb4_bin DEFAULT NULL,
  `BOOL_PROP_2` VARCHAR(1) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1270139534359973889', 'offcutLog', 'db', '[0],[db],', '剩料记录', '', '/offcutLog', '99', '2', '1', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1270139534359973890', 'offcutLog_list', 'offcutLog', '[0],[db],[offcutLog],', '剩料记录列表', '', '/offcutLog/list', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1270139534359973891', 'offcutLog_add', 'offcutLog', '[0],[db],[offcutLog],', '剩料记录添加', '', '/offcutLog/add', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1270139534359973892', 'offcutLog_update', 'offcutLog', '[0],[db],[offcutLog],', '剩料记录更新', '', '/offcutLog/update', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1270139534359973893', 'offcutLog_delete', 'offcutLog', '[0],[db],[offcutLog],', '剩料记录删除', '', '/offcutLog/delete', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1270139534359973894', 'offcutLog_detail', 'offcutLog', '[0],[db],[offcutLog],', '剩料记录详情', '', '/offcutLog/detail', '99', '3', '0', NULL, '1', '0');

INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1272137048600616961', 'tphb', 'pfgl', '[0],[pfgl],', '生产配比', '', '/tphb', '99', '2', '1', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1272137048600616962', 'tphb_list', 'tphb', '[0],[pfgl],[tphb],', '生产配比列表', '', '/tphb/list', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1272137048600616963', 'tphb_add', 'tphb', '[0],[pfgl],[tphb],', '生产配比添加', '', '/tphb/add', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1272137048600616964', 'tphb_update', 'tphb', '[0],[pfgl],[tphb],', '生产配比更新', '', '/tphb/update', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1272137048600616965', 'tphb_delete', 'tphb', '[0],[pfgl],[tphb],', '生产配比删除', '', '/tphb/delete', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1272137048600616966', 'tphb_detail', 'tphb', '[0],[pfgl],[tphb],', '生产配比详情', '', '/tphb/detail', '99', '3', '0', NULL, '1', '0');

INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1272137127189291010', 'tphbycl', 'pfgl', '[0],[pfgl],', '生产配比原料', '', '/tphbycl', '99', '2', '1', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1272137127189291011', 'tphbycl_list', 'tphbycl', '[0],[pfgl],[tphbycl],', '生产配比原料列表', '', '/tphbycl/list', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1272137127189291012', 'tphbycl_add', 'tphbycl', '[0],[pfgl],[tphbycl],', '生产配比原料添加', '', '/tphbycl/add', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1272137127189291013', 'tphbycl_update', 'tphbycl', '[0],[pfgl],[tphbycl],', '生产配比原料更新', '', '/tphbycl/update', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1272137127189291014', 'tphbycl_delete', 'tphbycl', '[0],[pfgl],[tphbycl],', '生产配比原料删除', '', '/tphbycl/delete', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1272137127189291015', 'tphbycl_detail', 'tphbycl', '[0],[pfgl],[tphbycl],', '生产配比原料详情', '', '/tphbycl/detail', '99', '3', '0', NULL, '1', '0');

INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1374368985695649793', 'exception', 'system', '[0],[system],', '异常信息', '', '/exception', '99', '2', '1', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1374368985699844097', 'exception_list', 'exception', '[0],[system],[exception],', '异常信息列表', '', '/exception/list', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1374368985699844098', 'exception_add', 'exception', '[0],[system],[exception],', '异常信息添加', '', '/exception/add', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1374368985699844099', 'exception_update', 'exception', '[0],[system],[exception],', '异常信息更新', '', '/exception/update', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1374368985699844100', 'exception_delete', 'exception', '[0],[system],[exception],', '异常信息删除', '', '/exception/delete', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1374368985699844101', 'exception_detail', 'exception', '[0],[system],[exception],', '异常信息详情', '', '/exception/detail', '99', '3', '0', NULL, '1', '0');

CREATE TABLE `template_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `template_name` varchar(200) DEFAULT NULL COMMENT '模板名称',
  `template_type` varchar(32) DEFAULT NULL COMMENT '模板类型',
  `template_num` varchar(32) DEFAULT NULL COMMENT '模板编号',
  `is_default` varchar(32) DEFAULT NULL COMMENT '是否默认',
  `enabled_status` varchar(32) DEFAULT NULL COMMENT '启用状态',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='模板信息';

CREATE TABLE `template_data_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `data_name` varchar(100) DEFAULT NULL COMMENT '数据名称',
  `data_type` varchar(32) DEFAULT NULL COMMENT '数据类型',
  `data_value` varchar(100) DEFAULT NULL COMMENT '数据信息',
  `template_num` varchar(32) DEFAULT NULL COMMENT '模板编号',
  `data_cn_name` varchar(200) DEFAULT NULL COMMENT '中文名称',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='模板数据信息';

INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1277247604982644738', 'templateInfo', 'pfgl', '[0],[pfgl],', '模板信息', '', '/templateInfo', '99', '2', '1', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1277247604982644739', 'templateInfo_list', 'templateInfo', '[0],[pfgl],[templateInfo],', '模板信息列表', '', '/templateInfo/list', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1277247604982644740', 'templateInfo_add', 'templateInfo', '[0],[pfgl],[templateInfo],', '模板信息添加', '', '/templateInfo/add', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1277247604982644741', 'templateInfo_update', 'templateInfo', '[0],[pfgl],[templateInfo],', '模板信息更新', '', '/templateInfo/update', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1277247604982644742', 'templateInfo_delete', 'templateInfo', '[0],[pfgl],[templateInfo],', '模板信息删除', '', '/templateInfo/delete', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1277247604982644743', 'templateInfo_detail', 'templateInfo', '[0],[pfgl],[templateInfo],', '模板信息详情', '', '/templateInfo/detail', '99', '3', '0', NULL, '1', '0');

CREATE TABLE `bulk_density_standard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `min_bulk_density` decimal(10,0) DEFAULT NULL COMMENT '最小容重',
  `max_bulk_density` decimal(10,0) DEFAULT NULL COMMENT '最大容重',
  `strength_value` int(3) DEFAULT NULL COMMENT '强度值',
  `strength` varchar(32) DEFAULT NULL COMMENT '强度等级',
  `is_default` varchar(32) DEFAULT NULL COMMENT '是否默认',
  `enabled_status` varchar(32) DEFAULT NULL COMMENT '是否启用',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='容重标准';

INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1287311453442048002', 'bulkDensityStandard', 'pfgl', '[0],[pfgl],', '容重标准', '', '/bulkDensityStandard', '99', '2', '1', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1287311453442048003', 'bulkDensityStandard_list', 'bulkDensityStandard', '[0],[pfgl],[bulkDensityStandard],', '容重标准列表', '', '/bulkDensityStandard/list', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1287311453442048004', 'bulkDensityStandard_add', 'bulkDensityStandard', '[0],[pfgl],[bulkDensityStandard],', '容重标准添加', '', '/bulkDensityStandard/add', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1287311453442048005', 'bulkDensityStandard_update', 'bulkDensityStandard', '[0],[pfgl],[bulkDensityStandard],', '容重标准更新', '', '/bulkDensityStandard/update', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1287311453442048006', 'bulkDensityStandard_delete', 'bulkDensityStandard', '[0],[pfgl],[bulkDensityStandard],', '容重标准删除', '', '/bulkDensityStandard/delete', '99', '3', '0', NULL, '1', '0');
INSERT INTO `mferp`.`sys_menu` (`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES ('1287311453442048007', 'bulkDensityStandard_detail', 'bulkDensityStandard', '[0],[pfgl],[bulkDensityStandard],', '容重标准详情', '', '/bulkDensityStandard/detail', '99', '3', '0', NULL, '1', '0');

