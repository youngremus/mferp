/**
 * 初始化下拉框信息
 */
function initDictToSelect(code, defaultVal) {
    var html = '<option value="">请选择</option>';
    if (code != "" && code != undefined) {
        var ajax = new $ax(Feng.ctxPath + "/dict/selectListByParentCode", function (data) {
            for (var i = 0; i < data.length; i++) {
                if(defaultVal && defaultVal == data[i].code) {
                    html += '<option value="' + data[i].code + '" selected>' + data[i].name + '</option>';
                } else {
                    html += '<option value="' + data[i].code + '">' + data[i].name + '</option>';
                }
            }
        }, function (data) {
            Feng.error("初始化数据失败!" + data.responseJSON.message + "!");
        });
        ajax.set("code", code);
        ajax.start();
    }
    return html;
}

/**
 * 获取字典返回数组
 */
function arrayFromDict(code) {
    var array = new Array();
    if (code != "" && code != undefined) {
        var ajax = new $ax(Feng.ctxPath + "/dict/selectListByParentCode", function (data) {
            for (var i = 0; i < data.length; i++) {
                array[i] = data[i].name;
            }
        }, function (data) {
            Feng.error("初始化数据失败!" + data.responseJSON.message + "!");
        });
        ajax.set("code", code);
        ajax.start();
    }
    return array;
}

// true:数值型的，false：非数值型
// 判断一个值是数字
function myIsNaN(value) {
    return typeof value === 'number' && !isNaN(value);
}

//判断字符串如果是整数不能以0开头后面加正整数，如果是浮点数整数部分不能为两个0：如00.00，如果是整数，  
function isNumeric(str){
    var re = /^((-[1-9][0-9]*\.[0-9][0-9]*)|(-[0]\.[0-9][0-9]*)|(-[1-9][0-9]*)|(-[0]{1}))|(([1-9][0-9]*\.[0-9][0-9]*)|([0]\.[0-9][0-9]*)|([1-9][0-9]*)|([0]{1}))$/;   
    var b;
    if (re.exec(str)==null){
        var re1
        b= false;
    } else{
        b= true;
    }
    return b;
}

function isEmpty(str){
    return str == null || str == undefined || str == "" || str == "null";
}

Date.prototype.format = function(fmt) {
    var o = {
        "M+" : this.getMonth()+1,                 //月份
        "d+" : this.getDate(),                    //日
        "h+" : this.getHours(),                   //小时
        "m+" : this.getMinutes(),                 //分
        "s+" : this.getSeconds(),                 //秒
        "q+" : Math.floor((this.getMonth()+3)/3), //季度
        "S"  : this.getMilliseconds()             //毫秒
    };
    if(/(y+)/.test(fmt)) {
        fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
    }
    for(var k in o) {
        if(new RegExp("("+ k +")").test(fmt)){
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
        }
    }
    return fmt;
};


