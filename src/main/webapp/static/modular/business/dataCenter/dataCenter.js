/**
 * 管理初始化
 */
var DataCenter = {
    id: "DataCenterTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
DataCenter.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'remark', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '更新时间', field: 'updateTime', visible: true, align: 'center', valign: 'middle'},
            {title: '数据源名称', field: 'sourceName', visible: true, align: 'center', valign: 'middle'},
            {title: '数据库名称', field: 'dbName', visible: true, align: 'center', valign: 'middle'},
            {title: '数据库类型', field: 'dbType', visible: true, align: 'center', valign: 'middle'},
            {title: '链接地址', field: 'dbUrl', visible: true, align: 'center', valign: 'middle'},
            {title: '数据库驱动', field: 'dbDriver', visible: true, align: 'center', valign: 'middle'},
            {title: '账号', field: 'username', visible: true, align: 'center', valign: 'middle'},
            {title: '密码', field: 'password', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
DataCenter.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        DataCenter.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
DataCenter.openAddDataCenter = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/DataCenter/dataCenter_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看详情
 */
DataCenter.openDataCenterDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/DataCenter/dataCenter_update/' + DataCenter.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除
 */
DataCenter.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/DataCenter/delete", function (data) {
            Feng.success("删除成功!");
            DataCenter.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("dataCenterId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询列表
 */
DataCenter.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    DataCenter.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = DataCenter.initColumn();
    var table = new BSTable(DataCenter.id, "/DataCenter/list", defaultColunms);
    table.setPaginationType("client");
    DataCenter.table = table.init();
});
