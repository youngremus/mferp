/**
 * 初始化命令信息详情对话框
 */
var MfSqlCommandInfoDlg = {
    mfSqlCommandInfoData : {}
};

/**
 * 清除数据
 */
MfSqlCommandInfoDlg.clearData = function() {
    this.mfSqlCommandInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
MfSqlCommandInfoDlg.set = function(key, val) {
    this.mfSqlCommandInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
MfSqlCommandInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
MfSqlCommandInfoDlg.close = function() {
    parent.layer.close(window.parent.MfSqlCommand.layerIndex);
}

/**
 * 收集数据
 */
MfSqlCommandInfoDlg.collectData = function() {
    this
    .set('id')
    .set('remark')
    .set('createTime')
    .set('updateTime')
    .set('dbSourceId')
    .set('dbSourceName')
    .set('cmdText')
    .set('cmdType')
    .set('enabledStatus')
    .set('isShow')
    .set('cmdName')
    .set('cmdCname')
    .set('verifyPwd');
}

/**
 * 提交添加
 */
MfSqlCommandInfoDlg.addSubmit = function() {

    this.clearData();

    if($("#dbSource").val()) {
        $("#dbSourceId").val($("#dbSource").val());
    }
    var dbSourceArray = arrayFromDict("dbType");
    for(var o in dbSourceArray) {

    }

    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/mfSqlCommand/add", function(data){
        Feng.success("添加成功!");
        window.parent.MfSqlCommand.table.refresh();
        MfSqlCommandInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.mfSqlCommandInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
MfSqlCommandInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/mfSqlCommand/update", function(data){
        Feng.success("修改成功!");
        window.parent.MfSqlCommand.table.refresh();
        MfSqlCommandInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.mfSqlCommandInfoData);
    ajax.start();
}

$(function() {
    $("#dbSource").html(initDictToSelect("dbType"));

    var dbSourceId = $("#dbSourceId").val();
    if(dbSourceId) {
        $("#dbSource").val(dbSourceId);
    }

    $("#cmdType").html(initDictToSelect("cmdType"));

    var cmdTypeValue = $("#cmdTypeValue").val();
    if(cmdTypeValue) {
        $("#cmdType").val(cmdTypeValue);
    }

    $("#enabledStatus").html(initDictToSelect("enabledStatus"));

    var enabledStatusValue = $("#enabledStatusValue").val();
    if(enabledStatusValue) {
        $("#enabledStatus").val(enabledStatusValue);
    }

    $("#isShow").html(initDictToSelect("isShow"));

    var isShowValue = $("#isShowValue").val();
    if(isShowValue) {
        $("#isShow").val(isShowValue);
    }
});
