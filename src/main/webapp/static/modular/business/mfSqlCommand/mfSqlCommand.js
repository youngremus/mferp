/**
 * 命令信息管理初始化
 */
var MfSqlCommand = {
    id: "MfSqlCommandTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
MfSqlCommand.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '编号', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '数据源编号', field: 'dbSourceId', visible: false, align: 'center', valign: 'middle'},
        {title: '数据源名称', field: 'dbSourceName', visible: true, align: 'center', valign: 'middle'},
        {title: '命令类型', field: 'cmdType', visible: true, align: 'center', valign: 'middle'},
        {title: '启用状态', field: 'enabledStatus', visible: true, align: 'center', valign: 'middle'},
        {title: '是否显示', field: 'isShow', visible: true, align: 'center', valign: 'middle'},
        {title: '命令名称', field: 'cmdName', visible: true, align: 'center', valign: 'middle'},
        {title: '命令中文名称', field: 'cmdCname', visible: true, align: 'center', valign: 'middle'},
        {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
MfSqlCommand.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        MfSqlCommand.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加命令信息
 */
MfSqlCommand.openAddMfSqlCommand = function () {
    if(this.layerIndex) {
        layer.close(this.layerIndex);
    }
    var index = layer.open({
        type: 2,
        title: '添加命令信息',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/mfSqlCommand/mfSqlCommand_add'
    });
    layer.full(index);
    this.layerIndex = index;
};

/**
 * 打开查看命令信息详情
 */
MfSqlCommand.openMfSqlCommandDetail = function () {
    if(this.layerIndex) {
        layer.close(this.layerIndex);
    }
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '命令信息详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/mfSqlCommand/mfSqlCommand_update/' + MfSqlCommand.seItem.id
        });
        layer.full(index);
        this.layerIndex = index;
    }
};

/**
 * 删除命令信息
 */
MfSqlCommand.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/mfSqlCommand/delete", function (data) {
            Feng.success("删除成功!");
            MfSqlCommand.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("mfSqlCommandId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询命令信息列表
 */
MfSqlCommand.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    MfSqlCommand.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = MfSqlCommand.initColumn();
    var table = new BSTable(MfSqlCommand.id, "/mfSqlCommand/list", defaultColunms);
    table.setPaginationType("client");
    MfSqlCommand.table = table.init();
});
