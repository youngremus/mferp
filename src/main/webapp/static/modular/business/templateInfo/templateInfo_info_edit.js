/**
 * 初始化模板信息详情对话框
 */
var TemplateInfoInfoDlg = {
    templateInfoInfoData : {}
};

/**
 * 清除数据
 */
TemplateInfoInfoDlg.clearData = function() {
    this.templateInfoInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TemplateInfoInfoDlg.set = function(key, val) {
    this.templateInfoInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TemplateInfoInfoDlg.get = function(key) {
    return $("#" + key).val();
};

/**
 * 关闭此对话框
 */
TemplateInfoInfoDlg.close = function() {
    parent.layer.close(window.parent.TemplateInfo.layerIndex);
};

/**
 * 收集数据
 */
TemplateInfoInfoDlg.collectData = function() {
    this
    .set('templateName')
    .set('templateType')
    .set('templateNum')
    .set('isDefault')
    .set('enabledStatus')
    .set('remark')
};

/**
 * 提交添加
 */
TemplateInfoInfoDlg.editSubmit = function() {
    layui.use(['transfer', 'layer', 'util'], function() {
        var transfer = layui.transfer;
        var getData = transfer.getData('ycl');
        var queryData = {
            templateName : $("#templateName").val(),
            templateNum : $("#templateNum").val(),
            templateType : $("#templateType").val(),
            enabledStatus:$("#enabledStatus").val(),
            isDefault :  $("#isDefault").val(),
            remark : $("#remark").val(),
            list : getData
        };
        $news.post('/templateInfo/update', JSON.stringify(queryData), function (data) {
            console.log(data);
            $news.success(data.message);
            parent.TemplateInfo.table.refresh();
        });
    });

    //提交信息
    /*var ajax = new $ax(Feng.ctxPath + "/templateInfo/add", function(data){
        Feng.success("添加成功!");
        window.parent.TemplateInfo.table.refresh();
        TemplateInfoInfoDlg.close();
    }, function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.templateInfoInfoData);
    ajax.start();*/
};
Array.prototype.maps = function(fun /*, thisp*/) {
    var len = this.length;
    if (typeof fun != "function")
        throw new TypeError();
    var res = new Array(len);
    var thisp = arguments[1];
    for (var i = 0; i < len; i++) {
        if (i in this)
            res[i] = fun.call(thisp, this[i], i, this);
    }
    return res;
};
var maps = function(array, func) {
    var res = new Array(array.length);
    for (var i = 0; i < array.length; i++) {
            res[i] = func.call(array[i]);
    }
    return res;
};
$(function() {
    $("#templateType").html(initDictToSelect("template_type"));
    $("#enabledStatus").html(initDictToSelect("sys_state"));
    $("#isDefault").html(initDictToSelect("is_default"));

    var templateTypeValue = $("#templateTypeValue").val();
    var enabledStatusValue = $("#enabledStatusValue").val();
    var isDefaultValue = $("#isDefaultValue").val();
    var chooseItem = $("#chooseItem").val();
    $("#templateType").val(templateTypeValue);
    $("#enabledStatus").val(enabledStatusValue);
    $("#isDefault").val(isDefaultValue);
    console.log("chooseItem：", chooseItem);
    var temp = chooseItem.split(",");
    console.log("temp：", temp);

    layui.use(['transfer', 'layer', 'util', 'form'], function(){
        var $ = layui.$
            ,transfer = layui.transfer
            ,layer = layui.layer
            ,util = layui.util,
            form = layui.form;

        $news.postX('/tycl/getYclTransferList', {}, function (data) {
            //显示搜索框
            transfer.render({
                elem: '#test1'
                ,data: data
                ,title: ['可选材料', '已选材料']
                ,showSearch: true
                ,id: 'ycl'
                ,value: temp
                ,onchange: function(obj, index) {
                    if(index == 1) {
                        $('.videoMoveBtn').addClass("layui-btn-disabled");
                    }
                    var arr = ['左边', '右边'];
                    console.log('来自 <strong>'+ arr[index] + '</strong> 的数据：'+ JSON.stringify(obj)); //获得被穿梭时的数据
                }
            });

            $news.transferBtn({
                elem: '#test1',
                val: temp
            });


        });

    });
});
