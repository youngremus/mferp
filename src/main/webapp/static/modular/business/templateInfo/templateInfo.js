/**
 * 模板信息管理初始化
 */
var TemplateInfo = {
    id: "TemplateInfoTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
TemplateInfo.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '序号', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '模板名称', field: 'templateName', visible: true, align: 'center', valign: 'middle'},
            {title: '模板类型', field: 'templateType', visible: true, align: 'center', valign: 'middle'},
            {title: '模板编号', field: 'templateNum', visible: true, align: 'center', valign: 'middle'},
            {title: '是否默认', field: 'isDefault', visible: true, align: 'center', valign: 'middle'},
            {title: '启用状态', field: 'enabledStatus', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
TemplateInfo.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        $news.info("请先选中表格中的某一记录！");
        return false;
    }else{
        TemplateInfo.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加模板信息
 */
TemplateInfo.openAddTemplateInfo = function () {
    var index = layer.open({
        type: 2,
        title: '添加模板信息',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/templateInfo/templateInfo_add',
        end: function () {
            window.parent.TemplateInfo.table.refresh();
        }
    });
    this.layerIndex = index;
    layer.full(index);
};

/**
 * 打开查看模板信息详情
 */
TemplateInfo.openTemplateInfoDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '模板信息详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/templateInfo/templateInfo_update/' + TemplateInfo.seItem.id,
            end: function () {
                window.parent.TemplateInfo.table.refresh();
            }
        });
        this.layerIndex = index;
        layer.full(index);
    }
};

/**
 * 删除模板信息
 */
TemplateInfo.delete = function () {
    if (TemplateInfo.check()) {
        $news.postX('/templateInfo/delete', {"templateInfoId":TemplateInfo.seItem.id}, function (data) {
            if(data.code != 200) {
                $news.fail("删除失败!" + data.message + "!");
            } else {
                $news.success("删除成功!");
            }
            TemplateInfo.table.refresh();
        });
    }
};

/**
 * 查询模板信息列表
 */
TemplateInfo.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    TemplateInfo.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = TemplateInfo.initColumn();
    var table = new BSTable(TemplateInfo.id, "/templateInfo/list", defaultColunms);
    table.setPaginationType("client");
    TemplateInfo.table = table.init();
});
