/**
 * 初始化模板信息详情对话框
 */
var TemplateInfoInfoDlg = {
    templateInfoInfoData : {}
};

/**
 * 清除数据
 */
TemplateInfoInfoDlg.clearData = function() {
    this.templateInfoInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TemplateInfoInfoDlg.set = function(key, val) {
    this.templateInfoInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TemplateInfoInfoDlg.get = function(key) {
    return $("#" + key).val();
};

/**
 * 关闭此对话框
 */
TemplateInfoInfoDlg.close = function() {
    parent.layer.close(window.parent.TemplateInfo.layerIndex);
};

/**
 * 收集数据
 */
TemplateInfoInfoDlg.collectData = function() {
    this
    .set('templateName')
    .set('templateType')
    .set('templateNum')
    .set('isDefault')
    .set('enabledStatus')
    .set('remark')
};

/**
 * 提交添加
 */
TemplateInfoInfoDlg.addSubmit = function() {
    layui.use(['transfer', 'layer', 'util'], function() {
        var transfer = layui.transfer;
        var getData = transfer.getData('ycl');
        var queryData = {
            templateName : $("#templateName").val(),
            templateType : $("#templateType").val(),
            enabledStatus:$("#enabledStatus").val(),
            isDefault :  $("#isDefault").val(),
            remark : $("#remark").val(),
            list : getData
        };
        $news.post('/templateInfo/add', JSON.stringify(queryData), function (data) {
            console.log(data);
            $news.success(data.message);
            parent.TemplateInfo.table.refresh();
        });
    });

    //提交信息
    /*var ajax = new $ax(Feng.ctxPath + "/templateInfo/add", function(data){
        Feng.success("添加成功!");
        window.parent.TemplateInfo.table.refresh();
        TemplateInfoInfoDlg.close();
    }, function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.templateInfoInfoData);
    ajax.start();*/
};

/**
 * 提交修改
 */
TemplateInfoInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/templateInfo/update", function(data){
        Feng.success("修改成功!");
        window.parent.TemplateInfo.table.refresh();
        TemplateInfoInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.templateInfoInfoData);
    ajax.start();
};

$(function() {
    var templateTypeValue = $("#templateTypeValue").val();
    var enabledStatusValue = $("#enabledStatusValue").val();
    var isDefaultValue = $("#isDefaultValue").val();

    $("#enabledStatus").html(initDictToSelect("sys_state"), enabledStatusValue);
    $("#isDefault").html(initDictToSelect("is_default"), isDefaultValue);
    $("#templateType").html(initDictToSelect("template_type"), templateTypeValue);

    layui.use(['transfer', 'layer', 'util', 'form'], function(){
        var $ = layui.$
            ,transfer = layui.transfer
            ,layer = layui.layer
            ,util = layui.util,
            form = layui.form;

        $news.postX('/tycl/getYclTransferList', {}, function (data) {
            console.log(data);
            //显示搜索框
            transfer.render({
                elem: '#test1'
                ,data: data
                ,title: ['可选材料', '已选材料']
                ,showSearch: true
                ,id: 'ycl'
                ,onchange: function(obj, index) {
                    //var arr = ['左边', '右边'];
                    //layer.alert('来自 <strong>'+ arr[index] + '</strong> 的数据：'+ JSON.stringify(obj)); //获得被穿梭时的数据
                }
            });

            $news.transferBtn({
                elem: '#test1',
                btn: '.videoMoveBtn'
            });
        });

        //实例调用
        /*transfer.render({
            elem: '#test7'
            ,data: data1
            ,id: 'key123' //定义唯一索引
        })
        //批量办法定事件
        util.event('lay-demoTransferActive', {
            getData: function(othis){
                var getData = transfer.getData('key123'); //获取右侧数据
                layer.alert(JSON.stringify(getData));
            }
            ,reload:function(){
                //实例重载
                transfer.reload('key123', {
                    title: ['文人', '喜欢的文人']
                    ,value: ['2', '5', '9']
                    ,showSearch: true
                })
            }
        });*/

        /*form.on('checkbox(layTransferCheckbox)', function (data) {
            $news.transferBtn({
                elem:'#test1',
                btn: '.videoMoveBtn'
            });
            changeBtnDisplay({
                elem:'#test1'
            });
        });*/
    });
});
