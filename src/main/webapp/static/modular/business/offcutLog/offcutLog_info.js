/**
 * 初始化剩料记录详情对话框
 */
var OffcutLogInfoDlg = {
    offcutLogInfoData : {}
};

/**
 * 清除数据
 */
OffcutLogInfoDlg.clearData = function() {
    this.offcutLogInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
OffcutLogInfoDlg.set = function(key, val) {
    this.offcutLogInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
OffcutLogInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
OffcutLogInfoDlg.close = function() {
    parent.layer.close(window.parent.OffcutLog.layerIndex);
}

/**
 * 收集数据
 */
OffcutLogInfoDlg.collectData = function() {
    this
    .set('id')
    .set('num')
    .set('originNo')
    .set('originOrderNo')
    .set('originConcreteType')
    .set('originProjectName')
    .set('originPourPosition')
    .set('originProductionDate')
    .set('originProductionTime')
    .set('originConstructionMethod')
    .set('carNumber')
    .set('currentNo')
    .set('currentOrderNo')
    .set('currentConcreteType')
    .set('currentProjectName')
    .set('currentPourPosition')
    .set('currentProductionDate')
    .set('currentProductionTime')
    .set('currentConstructionMethod')
    .set('remark')
    .set('offcutType');
}

/**
 * 提交添加
 */
OffcutLogInfoDlg.addSubmit = function() {
    if(!isNumeric($("#num").val())) {
        Feng.error("剩料请输入数字");
        return false;
    }
    if(isEmpty($("#originOrderNo").val())) {
        $news.fail("原小票未输入");
        return false;
    }
    if(isEmpty($("#currentNo").val())) {
        $news.fail("现小票未输入");
    }
    this.clearData();
    this.collectData();

    $news.postX("/offcutLog/add", OffcutLogInfoDlg.offcutLogInfoData, function (data) {
        console.log(JSON.stringify(data));
        if(data.code == '200') {
            $news.success("添加成功!");
        } else {
            $news.fail(data.message);
        }
        window.parent.OffcutLog.table.refresh();
        OffcutLogInfoDlg.close();
    });
};

/**
 * 提交修改
 */
OffcutLogInfoDlg.editSubmit = function() {
    if(!isNumeric($("#num").val())) {
        Feng.error("剩料请输入数字");
        return false;
    }
    if(isEmpty($("#currentNo").val())) {
        Feng.info("现小票未输入");
    }
    this.clearData();
    this.collectData();

    //提交信息
    $news.postX("/offcutLog/update", OffcutLogInfoDlg.offcutLogInfoData, function () {
        $news.success("修改成功!");
        window.parent.OffcutLog.table.refresh();
        OffcutLogInfoDlg.close();
    });
};

$(function() {
    $("#offcutType").html(initDictToSelect("offcutType"));

    var offcutTypeValue = $("#offcutTypeValue").val();
    if(offcutTypeValue) {
        $("#offcutType").val(offcutTypeValue);
    }

    $("#originNo").blur(function () {
        var no = $(this).val();
        if(isEmpty(no)) {
            $("#originOrderNo").val("");
            $("#originProjectName").val("");
            $("#originPourPosition").val("");
            $("#originProductionDate").val("");
            $("#originConcreteType").val("");
            $("#originConstructionMethod").val("");
            $("#originProductionTime").val("");
        } else {
            $news.postX("/tjlb/get_tjlb", {"no" : $("#originNo").val()}, function (data) {
                $("#originOrderNo").val(data.frwdh);
                $("#carNumber").val(data.fshch);
                $("#originProjectName").val(data.fgcmc);
                $("#originPourPosition").val(data.fjzbw);
                $("#originProductionDate").val(data.fscrq);
                $("#originConcreteType").val(data.ftpz);
                $("#originConstructionMethod").val(data.fjzfs);
                $("#originProductionTime").val(data.fccsj);
            });
        }
    });

    $("#currentNo").blur(function () {
        var no = $(this).val();
        if(isEmpty(no)) {
            $("#currentOrderNo").val("");
            $("#currentProjectName").val("");
            $("#currentPourPosition").val("");
            $("#currentProductionDate").val("");
            $("#currentConcreteType").val("");
            $("#currentConstructionMethod").val("");
            $("#currentProductionTime").val("");
        } else {
            $news.postX("/tjlb/get_tjlb", {"no" : $("#currentNo").val()}, function (data) {
                var carNo = data.fshch;
                var oCarNo = $("#carNumber").val();
                if(!isEmpty(oCarNo) && carNo != oCarNo) {
                    $news.fail("现小票的车号与原小票车号不一致！");
                    return false;
                }
                $("#currentOrderNo").val(data.frwdh);
                $("#currentProjectName").val(data.fgcmc);
                $("#currentPourPosition").val(data.fjzbw);
                $("#currentProductionDate").val(data.fscrq);
                $("#currentConcreteType").val(data.ftpz);
                $("#currentConstructionMethod").val(data.fjzfs);
                $("#currentProductionTime").val(data.fccsj);
            });
        }

    });
});
