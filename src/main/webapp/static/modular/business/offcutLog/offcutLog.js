/**
 * 剩料记录管理初始化
 */
var OffcutLog = {
    id: "OffcutLogTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
OffcutLog.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '编号', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '车号', field: 'carNumber', visible: true, align: 'center', valign: 'middle', width: 60},
        {title: '剩料', field: 'num', visible: true, align: 'center', valign: 'middle', width: 60},
        {title: '类型', field: 'offcutType', visible: true, align: 'center', valign: 'middle', width: 70},
        {title: '原小票', field: 'originNo', visible: true, align: 'center', valign: 'middle', width: 90},
        {title: '原工地', field: 'originProjectName', visible: true, align: 'center', valign: 'middle'},
        {title: '原品种', field: 'originConcreteType', visible: true, align: 'center', valign: 'middle'},
        {title: '原部位', field: 'originPourPosition', visible: true, align: 'center', valign: 'middle'},
        {title: '现小票', field: 'currentNo', visible: true, align: 'center', valign: 'middle', width: 90},
        {title: '现工地', field: 'currentProjectName', visible: true, align: 'center', valign: 'middle'},
        {title: '现品种', field: 'currentConcreteType', visible: true, align: 'center', valign: 'middle'},
        {title: '现部位', field: 'currentPourPosition', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
OffcutLog.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        $news.info("请先选中表格中的某一记录！");
        return false;
    }else{
        OffcutLog.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加剩料记录
 */
OffcutLog.openAddOffcutLog = function () {
    if(this.layerIndex) {
        layer.close(this.layerIndex);
    }
    var index = layer.open({
        type: 2,
        title: '添加剩料记录',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/offcutLog/offcutLog_add'
    });
    layer.full(index);
    this.layerIndex = index;
};

/**
 * 打开查看剩料记录详情
 */
OffcutLog.openOffcutLogDetail = function () {
    if(this.layerIndex) {
        layer.close(this.layerIndex);
    }
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '剩料记录详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/offcutLog/offcutLog_update/' + OffcutLog.seItem.id
        });
        layer.full(index);
        this.layerIndex = index;
    }
};

/**
 * 删除剩料记录
 */
OffcutLog.delete = function () {
    if (this.check()) {
        $news.confirm("是否删除选中的剩料记录？", function () {
            $news.postX("/offcutLog/delete", {"offcutLogId" : OffcutLog.seItem.id},function () {
                $news.success("删除成功!");
                OffcutLog.table.refresh();
            });
        });
    }
};

/**
 * 导出剩料记录
 */
OffcutLog.exportMsg = function() {
    $news.confirm("是否导出查询到的所有数据?", function () {
        var url = "/offcutLog/download" + "?jstime=" + new Date().getTime();
        if($("#carNumber").val() != '') url = url + "&carNumber=" + $("#carNumber").val();
        if($("#originNo").val() != '') url = url + "&originNo=" + $("#originNo").val();
        if($("#currentNo").val() != '') url = url + "&currentNo=" + $("#currentNo").val();
        if($("#start").val() != '') url = url + "&start=" + $("#start").val();
        if($("#end").val() != '') url = url + "&end=" + $("#end").val();
        if($("#offcutType").val() != '') url = url + "&offcutType=" + $("#offcutType").val();
        url = url + "&excelType=03";
        $news.download(url);
    });
};

/**
 * 报表
 */
OffcutLog.report = function() {
    if (this.check()) {
        var carNumber = OffcutLog.seItem.carNumber;
        window.open(ctxPath + "/ureport/preview?_u=file:offcutLog.ureport.xml&carNumber=" + carNumber, "_blank");
    }
};

/**
 * 查询剩料记录列表
 */
OffcutLog.search = function () {
    var queryData = {};
    queryData['carNumber'] = $("#carNumber").val();
    queryData['originNo'] = $("#originNo").val();
    queryData['currentNo'] = $("#currentNo").val();
    queryData['start'] = $("#start").val();
    queryData['end'] = $("#end").val();
    queryData['offcutType'] = $("#offcutType").val();
    OffcutLog.table.refresh({query: queryData});
    $news.postX("/offcutLog/count", queryData, function (data) {
        console.log(data);
        var result = data.data;
        var m = result.monthCount;
        var y = result.yearCount;
        var c = result.counts;
        if(m && isNumeric(m)) {
            m = Number(m).toFixed(2);
        } else {
            m = 0;
        }
        if(y && isNumeric(y)) {
            y = Number(y).toFixed(2);
        } else {
            y = 0;
        }
        if(c && isNumeric(c)) {
            c = Number(c).toFixed(2);
        } else {
            c = 0;
        }
        $("blockquote").html("本月总方数："+String(m)+"；本年总方数：" + String(y)+"；当前统计方数：" + String(c));
    });
};

$(function () {
    $("#offcutType").html(initDictToSelect("offcutType"));
    var defaultColunms = OffcutLog.initColumn();
    var table = new BSTable(OffcutLog.id, "/offcutLog/list", defaultColunms);
    table.setPaginationType("client");
    OffcutLog.table = table.init();
});
