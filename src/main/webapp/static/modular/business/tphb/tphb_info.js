/**
 * 初始化生产配合比详情对话框
 */
var TphbInfoDlg = {
    tphbInfoData : {}
};

/**
 * 清除数据
 */
TphbInfoDlg.clearData = function() {
    this.tphbInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TphbInfoDlg.set = function(key, val) {
    this.tphbInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TphbInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
TphbInfoDlg.close = function() {
    parent.layer.close(window.parent.Tphb.layerIndex);
}

/**
 * 收集数据
 */
TphbInfoDlg.collectData = function() {
    this
    .set('FPhbh')
    .set('FZt')
    .set('FTpz')
    .set('FYt')
    .set('FTld')
    .set('FSnpz')
    .set('FSzgg')
    .set('FTbj')
    .set('FTlq')
    .set('FBz')
    .set('FSy')
    .set('FSh')
    .set('FJsfz')
    .set('FCzy')
    .set('FDlrq')
    .set('FJbsj')
    .set('FClsjNo')
    .set('FPhbNo')
    .set('FPhbNoA')
    .set('FPhbNoB')
    .set('FPhbNoC')
    .set('FQddj')
    .set('FKsdj')
    .set('FKddj')
    .set('FKzdj')
    .set('FQtdj')
    .set('FCd')
    .set('FCnsj')
    .set('FZnsj')
    .set('FNjsj')
    .set('fc1')
    .set('fc2')
    .set('fc3')
    .set('FOperator')
    .set('FTimeStamp')
    .set('FCheckSum')
    .set('FCheckKey')
    .set('fa1')
    .set('fa2')
    .set('fa3')
    .set('fa4')
    .set('FRz')
    .set('FSjb')
    .set('FSlPt')
    .set('FJnclKg')
    .set('FIv0')
    .set('FBsfs')
    .set('FVersion')
    .set('FXpNo')
    .set('FXpKey');
}

/**
 * 提交添加
 */
TphbInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/tphb/add", function(data){
        Feng.success("添加成功!");
        window.parent.Tphb.table.refresh();
        TphbInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.tphbInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
TphbInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/tphb/update", function(data){
        Feng.success("修改成功!");
        window.parent.Tphb.table.refresh();
        TphbInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.tphbInfoData);
    ajax.start();
}

$(function() {

});
