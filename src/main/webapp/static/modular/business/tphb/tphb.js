/**
 * 生产配合比管理初始化
 */
var Tphb = {
    id: "TphbTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Tphb.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '编号', field: 'fphbh', visible: true, align: 'center', valign: 'middle'},
        {title: '配合比号', field: 'fphbno', visible: true, align: 'center', valign: 'middle'},
        {title: '砼品种', field: 'ftpz', visible: true, align: 'center', valign: 'middle'},
        {title: '状态', field: 'fzt', visible: true, align: 'center', valign: 'middle'},
        {title: '用途', field: 'fyt', visible: true, align: 'center', valign: 'middle'},
        {title: '坍落度', field: 'ftld', visible: true, align: 'center', valign: 'middle'},
        {title: '水泥品种', field: 'fsnpz', visible: true, align: 'center', valign: 'middle'},
        {title: '备注', field: 'fbz', visible: true, align: 'center', valign: 'middle'},
        {title: '登录日期', field: 'fdlrq', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Tphb.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Tphb.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加生产配合比
 */
Tphb.openAddTphb = function () {
    var index = layer.open({
        type: 2,
        title: '添加生产配合比',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/tphb/tphb_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看生产配合比详情
 */
Tphb.openTphbDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '生产配合比详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/tphb/tphb_update/' + Tphb.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除生产配合比
 */
Tphb.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/tphb/delete", function (data) {
            Feng.success("删除成功!");
            Tphb.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("tphbId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 导入生产配合比
 */
Tphb.import = function () {

};

/**
 * 查询生产配合比列表
 */
Tphb.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Tphb.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Tphb.initColumn();
    var table = new BSTable(Tphb.id, "/tphb/list", defaultColunms);
    table.setPaginationType("client");
    Tphb.table = table.init();

    /*$("#import").fileinput({
        language: 'zh', //设置语言
        textEncoding : "UTF-8",//文本编码
        uploadUrl: Feng.ctxPath + "/tphb/import", //上传的地址
        allowedFileExtensions: ['xls', 'xlsx'], //接收的文件后缀
        uploadAsync: true, //默认异步上传
        showUpload: false, //是否显示上传按钮
        showRemove : false, //显示移除按钮
        showPreview : true, //是否显示预览
        showCaption: false,//是否显示标题
        browseClass: "btn btn-primary", //按钮样式
        dropZoneEnabled: true,//是否显示拖拽区域
        maxFileCount: 1, //表示允许同时上传的最大文件个数
        enctype: 'multipart/form-data',
        validateInitialCount:false
    }).on("fileuploaded", function (event, data, previewId, index) {

    });*/
});
