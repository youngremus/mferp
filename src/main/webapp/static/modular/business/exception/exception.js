/**
 * 异常信息管理初始化
 */
var Exception = {
    id: "ExceptionTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Exception.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '序号', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'memo', visible: true, align: 'center', valign: 'middle'},
            {title: 'ip地址', field: 'ip', visible: true, align: 'center', valign: 'middle'},
            {title: '类名', field: 'className', visible: true, align: 'center', valign: 'middle'},
            {title: '方法名', field: 'methodName', visible: true, align: 'center', valign: 'middle'},
            {title: '异常类型', field: 'exceptionType', visible: true, align: 'center', valign: 'middle'},
            {title: '异常详情', field: 'exceptionDetail', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '更新时间', field: 'updateTime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Exception.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Exception.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加异常信息
 */
Exception.openAddException = function () {
    var index = layer.open({
        type: 2,
        title: '添加异常信息',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/exception/exception_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看异常信息详情
 */
Exception.openExceptionDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '异常信息详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/exception/exception_update/' + Exception.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除异常信息
 */
Exception.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/exception/delete", function (data) {
            Feng.success("删除成功!");
            Exception.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("exceptionId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询异常信息列表
 */
Exception.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Exception.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Exception.initColumn();
    var table = new BSTable(Exception.id, "/exception/list", defaultColunms);
    table.setPaginationType("client");
    Exception.table = table.init();
});
