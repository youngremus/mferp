/**
 * 初始化异常信息详情对话框
 */
var ExceptionInfoDlg = {
    exceptionInfoData : {}
};

/**
 * 清除数据
 */
ExceptionInfoDlg.clearData = function() {
    this.exceptionInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ExceptionInfoDlg.set = function(key, val) {
    this.exceptionInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ExceptionInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
ExceptionInfoDlg.close = function() {
    parent.layer.close(window.parent.Exception.layerIndex);
}

/**
 * 收集数据
 */
ExceptionInfoDlg.collectData = function() {
    this
    .set('id')
    .set('memo')
    .set('ip')
    .set('className')
    .set('methodName')
    .set('exceptionType')
    .set('exceptionDetail')
    .set('createTime')
    .set('updateTime');
}

/**
 * 提交添加
 */
ExceptionInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/exception/add", function(data){
        Feng.success("添加成功!");
        window.parent.Exception.table.refresh();
        ExceptionInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.exceptionInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
ExceptionInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/exception/update", function(data){
        Feng.success("修改成功!");
        window.parent.Exception.table.refresh();
        ExceptionInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.exceptionInfoData);
    ajax.start();
}

$(function() {

});
