/**
 * 初始化数据源信息详情对话框
 */
var MfDbSourceInfoDlg = {
    mfDbSourceInfoData : {}
};

/**
 * 清除数据
 */
MfDbSourceInfoDlg.clearData = function() {
    this.mfDbSourceInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
MfDbSourceInfoDlg.set = function(key, val) {
    this.mfDbSourceInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
MfDbSourceInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
MfDbSourceInfoDlg.close = function() {
    parent.layer.close(window.parent.MfDbSource.layerIndex);
}

/**
 * 收集数据
 */
MfDbSourceInfoDlg.collectData = function() {
    this
    .set('id')
    .set('remark')
    .set('createTime')
    .set('updateTime')
    .set('sourceName')
    .set('dbName')
    .set('dbType')
    .set('dbUrl')
    .set('dbDriver')
    .set('username')
    .set('password');
}

/**
 * 提交添加
 */
MfDbSourceInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/mfDbSource/add", function(data){
        Feng.success("添加成功!");
        window.parent.MfDbSource.table.refresh();
        MfDbSourceInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.mfDbSourceInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
MfDbSourceInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/mfDbSource/update", function(data){
        Feng.success("修改成功!");
        window.parent.MfDbSource.table.refresh();
        MfDbSourceInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.mfDbSourceInfoData);
    ajax.start();
}

$(function() {
    $("#dbType").html(initDictToSelect("dbType"));

    var dbTypeValue = $("#dbTypeValue").val();
    if(dbTypeValue) {
        $("#dbType").val(dbTypeValue);
    }
});
