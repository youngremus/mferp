/**
 * 数据源信息管理初始化
 */
var MfDbSource = {
    id: "MfDbSourceTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
MfDbSource.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '编号', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '数据源名称', field: 'sourceName', visible: true, align: 'center', valign: 'middle'},
        {title: '数据库名称', field: 'dbName', visible: true, align: 'center', valign: 'middle'},
        {title: '数据库类型', field: 'dbType', visible: true, align: 'center', valign: 'middle'},
        {title: '数据库驱动', field: 'dbDriver', visible: true, align: 'center', valign: 'middle'},
        {title: '账号', field: 'username', visible: true, align: 'center', valign: 'middle'},
        {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
MfDbSource.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        MfDbSource.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加数据源信息
 */
MfDbSource.openAddMfDbSource = function () {
    if(this.layerIndex) {
        layer.close(this.layerIndex);
    }
    var index = layer.open({
        type: 2,
        title: '添加数据源信息',
        area: ['860px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/mfDbSource/mfDbSource_add'
    });
    layer.full(index);
    this.layerIndex = index;
};

/**
 * 打开查看数据源信息详情
 */
MfDbSource.openMfDbSourceDetail = function () {
    if(this.layerIndex) {
        layer.close(this.layerIndex);
    }
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '数据源信息详情',
            area: ['860px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/mfDbSource/mfDbSource_update/' + MfDbSource.seItem.id
        });
        layer.full(index);
        this.layerIndex = index;
    }
};

/**
 * 删除数据源信息
 */
MfDbSource.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/mfDbSource/delete", function (data) {
            Feng.success("删除成功!");
            MfDbSource.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("mfDbSourceId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询数据源信息列表
 */
MfDbSource.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    MfDbSource.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = MfDbSource.initColumn();
    var table = new BSTable(MfDbSource.id, "/mfDbSource/list", defaultColunms);
    table.setPaginationType("client");
    MfDbSource.table = table.init();
});
