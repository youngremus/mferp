/**
 * 初始化任务单施工配比详情对话框
 */
var TrwdphbInfoDlg = {
    trwdphbInfoData : {}
};

/**
 * 清除数据
 */
TrwdphbInfoDlg.clearData = function() {
    this.trwdphbInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TrwdphbInfoDlg.set = function(key, val) {
    this.trwdphbInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TrwdphbInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
TrwdphbInfoDlg.close = function() {
    parent.layer.close(window.parent.Trwdphb.layerIndex);
}

/**
 * 收集数据
 */
TrwdphbInfoDlg.collectData = function() {
    this
    .set('FId')
    .set('FRwdh')
    .set('FTzsj')
    .set('FPblx')
    .set('FPbh1')
    .set('FPbh2')
    .set('FXpbh')
    .set('FLjcs')
    .set('FCzy')
    .set('FSyy')
    .set('FBz')
    .set('FNo');
}

/**
 * 提交添加
 */
TrwdphbInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/trwdphb/add", function(data){
        Feng.success("添加成功!");
        window.parent.Trwdphb.table.refresh();
        TrwdphbInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.trwdphbInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
TrwdphbInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/trwdphb/update", function(data){
        Feng.success("修改成功!");
        window.parent.Trwdphb.table.refresh();
        TrwdphbInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.trwdphbInfoData);
    ajax.start();
}

$(function() {

});
