/**
 * 任务单施工配比管理初始化
 */
var Trwdphb = {
    id: "TrwdphbTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Trwdphb.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '编号', field: 'fid', visible: false, align: 'center', valign: 'middle'},
            {title: '任务单号', field: 'frwdh', visible: true, align: 'center', valign: 'middle'},
            {title: '调整时间', field: 'ftzsj', visible: true, align: 'center', valign: 'middle'},
            {title: '生产线', field: 'fpblx', visible: true, align: 'center', valign: 'middle'},
            {title: '前配比号', field: 'fpbh1', visible: true, align: 'center', valign: 'middle'},
            {title: '后配比号', field: 'fpbh2', visible: true, align: 'center', valign: 'middle'},
            {title: '累计车数', field: 'fljcs', visible: true, align: 'center', valign: 'middle'},
            {title: '操作员', field: 'fczy', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'fbz', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Trwdphb.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Trwdphb.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加任务单施工配比
 */
Trwdphb.openAddTrwdphb = function () {
    var index = layer.open({
        type: 2,
        title: '添加任务单施工配比',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/trwdphb/trwdphb_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看任务单施工配比详情
 */
Trwdphb.openTrwdphbDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '任务单施工配比详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/trwdphb/trwdphb_update/' + Trwdphb.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除任务单施工配比
 */
Trwdphb.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/trwdphb/delete", function (data) {
            Feng.success("删除成功!");
            Trwdphb.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("trwdphbId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询任务单施工配比列表
 */
Trwdphb.search = function () {
    var queryData = {};
    queryData['frwdh'] = $("#frwdh").val();
    queryData['fscbt'] = $("#fscbt").val();
    Trwdphb.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Trwdphb.initColumn();
    var table = new BSTable(Trwdphb.id, "/trwdphb/list", defaultColunms);
    table.setPaginationType("client");
    var queryData = {};
    queryData['frwdh'] = $("#frwdh").val();
    queryData['fscbt'] = $("#fscbt").val();
    table.setQueryParams(queryData)
    Trwdphb.table = table.init();
});
