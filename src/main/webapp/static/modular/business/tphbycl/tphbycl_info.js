/**
 * 初始化详情对话框
 */
var TphbyclInfoDlg = {
    tphbyclInfoData : {}
};

/**
 * 清除数据
 */
TphbyclInfoDlg.clearData = function() {
    this.tphbyclInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TphbyclInfoDlg.set = function(key, val) {
    this.tphbyclInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TphbyclInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
TphbyclInfoDlg.close = function() {
    parent.layer.close(window.parent.Tphbycl.layerIndex);
}

/**
 * 收集数据
 */
TphbyclInfoDlg.collectData = function() {
    this
    .set('FPhbh')
    .set('FYlmc')
    .set('FPzgg')
    .set('FSysl')
    .set('FPlcw')
    .set('FNdPt')
    .set('FClPt')
    .set('FCkno')
    .set('FHsl')
    .set('FXpKey')
    .set('FVersion')
    .set('FXpNo');
}

/**
 * 提交添加
 */
TphbyclInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/tphbycl/add", function(data){
        Feng.success("添加成功!");
        window.parent.Tphbycl.table.refresh();
        TphbyclInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.tphbyclInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
TphbyclInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/tphbycl/update", function(data){
        Feng.success("修改成功!");
        window.parent.Tphbycl.table.refresh();
        TphbyclInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.tphbyclInfoData);
    ajax.start();
}

$(function() {

});
