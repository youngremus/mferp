/**
 * 管理初始化
 */
var Tphbycl = {
    id: "TphbyclTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Tphbycl.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '配合比号', field: 'FPhbh', visible: true, align: 'center', valign: 'middle'},
            {title: '原材料名称', field: 'FYlmc', visible: true, align: 'center', valign: 'middle'},
            {title: '品种规格', field: 'FPzgg', visible: true, align: 'center', valign: 'middle'},
            {title: '用量（kg）', field: 'FSysl', visible: true, align: 'center', valign: 'middle'},
            {title: '仓位', field: 'FPlcw', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FNdPt', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FClPt', visible: true, align: 'center', valign: 'middle'},
            {title: '仓库编号', field: 'FCkno', visible: true, align: 'center', valign: 'middle'},
            {title: '含水率', field: 'FHsl', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FXpKey', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FVersion', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FXpNo', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Tphbycl.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Tphbycl.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
Tphbycl.openAddTphbycl = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/tphbycl/tphbycl_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看详情
 */
Tphbycl.openTphbyclDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/tphbycl/tphbycl_update/' + Tphbycl.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除
 */
Tphbycl.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/tphbycl/delete", function (data) {
            Feng.success("删除成功!");
            Tphbycl.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("tphbyclId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询列表
 */
Tphbycl.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Tphbycl.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Tphbycl.initColumn();
    var table = new BSTable(Tphbycl.id, "/tphbycl/list", defaultColunms);
    table.setPaginationType("client");
    Tphbycl.table = table.init();
});
