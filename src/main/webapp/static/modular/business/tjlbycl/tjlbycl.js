/**
 * 生产记录原材料管理初始化
 */
var Tjlbycl = {
    id: "TjlbyclTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Tjlbycl.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '编号', field: 'fid', visible: true, align: 'center', valign: 'middle'},
            {title: '小票号', field: 'FNo', visible: true, align: 'center', valign: 'middle'},
            {title: '盘号', field: 'FPanNo', visible: true, align: 'center', valign: 'middle'},
            {title: '原料编号', field: 'FBh', visible: true, align: 'center', valign: 'middle'},
            {title: '原料名称', field: 'FYlmc', visible: true, align: 'center', valign: 'middle'},
            {title: '品种规格', field: 'FPzgg', visible: true, align: 'center', valign: 'middle'},
            {title: '配比数量', field: 'FPbsl', visible: true, align: 'center', valign: 'middle'},
            {title: '实用数量', field: 'FSysl', visible: true, align: 'center', valign: 'middle'},
            {title: '含水量', field: 'FHsl', visible: true, align: 'center', valign: 'middle'},
            {title: '管理用量', field: 'FGlyl', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Tjlbycl.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Tjlbycl.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加生产记录原材料
 */
Tjlbycl.openAddTjlbycl = function () {
    var index = layer.open({
        type: 2,
        title: '添加生产记录原材料',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/tjlbycl/tjlbycl_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看生产记录原材料详情
 */
Tjlbycl.openTjlbyclDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '生产记录原材料详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/tjlbycl/tjlbycl_update/' + Tjlbycl.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除生产记录原材料
 */
Tjlbycl.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/tjlbycl/delete", function (data) {
            Feng.success("删除成功!");
            Tjlbycl.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("tjlbyclId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询生产记录原材料列表
 */
Tjlbycl.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Tjlbycl.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Tjlbycl.initColumn();
    var table = new BSTable(Tjlbycl.id, "/tjlbycl/list", defaultColunms);
    table.setPaginationType("client");
    Tjlbycl.table = table.init();
});
