/**
 * 初始化生产记录原材料详情对话框
 */
var TjlbyclInfoDlg = {
    tjlbyclInfoData : {}
};

/**
 * 清除数据
 */
TjlbyclInfoDlg.clearData = function() {
    this.tjlbyclInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TjlbyclInfoDlg.set = function(key, val) {
    this.tjlbyclInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TjlbyclInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
TjlbyclInfoDlg.close = function() {
    parent.layer.close(window.parent.Tjlbycl.layerIndex);
}

/**
 * 收集数据
 */
TjlbyclInfoDlg.collectData = function() {
    this
    .set('fid')
    .set('FNo')
    .set('FPanNo')
    .set('FBh')
    .set('FYlmc')
    .set('FPzgg')
    .set('FPbsl')
    .set('FSysl')
    .set('FHsl')
    .set('FGlyl');
}

/**
 * 提交添加
 */
TjlbyclInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/tjlbycl/add", function(data){
        Feng.success("添加成功!");
        window.parent.Tjlbycl.table.refresh();
        TjlbyclInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.tjlbyclInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
TjlbyclInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/tjlbycl/update", function(data){
        Feng.success("修改成功!");
        window.parent.Tjlbycl.table.refresh();
        TjlbyclInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.tjlbyclInfoData);
    ajax.start();
}

$(function() {

});
