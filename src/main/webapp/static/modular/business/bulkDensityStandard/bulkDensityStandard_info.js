/**
 * 初始化容重标准详情对话框
 */
var BulkDensityStandardInfoDlg = {
    bulkDensityStandardInfoData : {}
};

/**
 * 清除数据
 */
BulkDensityStandardInfoDlg.clearData = function() {
    this.bulkDensityStandardInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
BulkDensityStandardInfoDlg.set = function(key, val) {
    this.bulkDensityStandardInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
BulkDensityStandardInfoDlg.get = function(key) {
    return $("#" + key).val();
};

/**
 * 关闭此对话框
 */
BulkDensityStandardInfoDlg.close = function() {
    parent.layer.close(window.parent.BulkDensityStandard.layerIndex);
};

/**
 * 收集数据
 */
BulkDensityStandardInfoDlg.collectData = function() {
    this
    .set('id')
    .set('minBulkDensity')
    .set('maxBulkDensity')
    .set('strengthValue')
    .set('strength')
    .set('isDefault')
    .set('enabledStatus')
    .set('remark')
    .set('createTime')
    .set('updateTime');
};

/**
 * 提交添加
 */
BulkDensityStandardInfoDlg.addSubmit = function() {
    var queryData = {
        strength : $("#strength").val(),
        strengthValue : $("#strengthValue").val(),
        enabledStatus:$("#enabledStatus").val(),
        isDefault :  $("#isDefault").val(),
        remark : $("#remark").val(),
        minBulkDensity : $("#minBulkDensity").val(),
        maxBulkDensity : $("#maxBulkDensity").val()
    };
    $news.post('/bulkDensityStandard/add', JSON.stringify(queryData), function (data) {
        console.log(data);
        $news.success(data.message);
        parent.BulkDensityStandard.table.refresh();
    });
};

/**
 * 提交修改
 */
BulkDensityStandardInfoDlg.editSubmit = function() {
    var queryData = {
        id:$("#idValue").val(),
        strength : $("#strength").val(),
        strengthValue : $("#strengthValue").val(),
        enabledStatus:$("#enabledStatus").val(),
        isDefault :  $("#isDefault").val(),
        remark : $("#remark").val(),
        minBulkDensity : $("#minBulkDensity").val(),
        maxBulkDensity : $("#maxBulkDensity").val()
    };
    $news.post('/bulkDensityStandard/update', JSON.stringify(queryData), function (data) {
        console.log(data);
        $news.success(data.message);
        parent.BulkDensityStandard.table.refresh();
    });

};

$(function() {
    var enabledStatusValue = $("#enabledStatusValue").val();
    var isDefaultValue = $("#isDefaultValue").val();
    var strengthsValue = $("#strengthsValue").val();
    $("#enabledStatus").html(initDictToSelect("sys_state"), enabledStatusValue);
    $("#isDefault").html(initDictToSelect("is_default"), isDefaultValue);
    $("#strength").html(initDictToSelect("strength"), strengthsValue);

    if(enabledStatusValue) {
        $("#enabledStatus").val(enabledStatusValue);
    }
    if(isDefaultValue) {
        $("#isDefault").val(isDefaultValue);
    }
    if(strengthsValue) {
        $("#strength").val(strengthsValue);
    }

    $("#strength").on('change', function () {
        var strength = $(this).val();
        $("#strengthValue").val(strength.substring(1));
    });
    console.log($("#createTime").val());
    console.log($("#updateTime").val());
    $("#createTime").val($("#createTime").val() ? new Date($("#createTime").val()).format("yyyy-MM-dd hh:mm:ss") : "");
    $("#updateTime").val($("#updateTime").val() ? new Date($("#updateTime").val()).format("yyyy-MM-dd hh:mm:ss") : "");
});
