/**
 * 容重标准管理初始化
 */
var BulkDensityStandard = {
    id: "BulkDensityStandardTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
BulkDensityStandard.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '强度等级', field: 'strength', visible: true, align: 'center', valign: 'middle'},
        {title: '强度值', field: 'strengthValue', visible: true, align: 'center', valign: 'middle'},
        {title: '最小容重', field: 'minBulkDensity', visible: true, align: 'center', valign: 'middle'},
        {title: '最大容重', field: 'maxBulkDensity', visible: true, align: 'center', valign: 'middle'},
        {title: '是否默认', field: 'isDefault', visible: true, align: 'center', valign: 'middle'},
        {title: '是否启用', field: 'enabledStatus', visible: true, align: 'center', valign: 'middle'},
        {title: '备注', field: 'remark', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
BulkDensityStandard.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        BulkDensityStandard.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加容重标准
 */
BulkDensityStandard.openAddBulkDensityStandard = function () {
    var index = layer.open({
        type: 2,
        title: '添加容重标准',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/bulkDensityStandard/bulkDensityStandard_add'
    });
    layer.full(index);
    this.layerIndex = index;
};

/**
 * 打开查看容重标准详情
 */
BulkDensityStandard.openBulkDensityStandardDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '容重标准详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/bulkDensityStandard/bulkDensityStandard_update/' + BulkDensityStandard.seItem.id
        });
        layer.full(index);
        this.layerIndex = index;
    }
};

/**
 * 删除容重标准
 */
BulkDensityStandard.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/bulkDensityStandard/delete", function (data) {
            Feng.success("删除成功!");
            BulkDensityStandard.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("bulkDensityStandardId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询容重标准列表
 */
BulkDensityStandard.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    BulkDensityStandard.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = BulkDensityStandard.initColumn();
    var table = new BSTable(BulkDensityStandard.id, "/bulkDensityStandard/list", defaultColunms);
    table.setPaginationType("client");
    BulkDensityStandard.table = table.init();
});
