/**
 * 初始化生产记录详情对话框
 */
var TjlbInfoDlg = {
    tjlbInfoData : {}
};

/**
 * 清除数据
 */
TjlbInfoDlg.clearData = function() {
    this.tjlbInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TjlbInfoDlg.set = function(key, val) {
    this.tjlbInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TjlbInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
TjlbInfoDlg.close = function() {
    parent.layer.close(window.parent.Tjlb.layerIndex);
}

/**
 * 收集数据
 */
TjlbInfoDlg.collectData = function() {
    this
    .set('FNo')
    .set('FZt')
    .set('FScrq')
    .set('FScbt')
    .set('FDcsj')
    .set('FCcsj')
    .set('FRwdh')
    .set('FHtbh')
    .set('FKhbh')
    .set('FHtdw')
    .set('FGcmc')
    .set('FGcdz')
    .set('FJzbw')
    .set('FJzfs')
    .set('FGls')
    .set('FTpz')
    .set('FTld')
    .set('FSgpb')
    .set('FSjpb')
    .set('FShch')
    .set('FSjxm')
    .set('FBcps')
    .set('FBcfs')
    .set('FZzl')
    .set('FBsfs')
    .set('FLjfs')
    .set('FLjcs')
    .set('FCcqf')
    .set('FYhqs')
    .set('FCzy')
    .set('FBz')
    .set('FRwno')
    .set('FPhbNo');
}

/**
 * 提交添加
 */
TjlbInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/tjlb/add", function(data){
        Feng.success("添加成功!");
        window.parent.Tjlb.table.refresh();
        TjlbInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.tjlbInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
TjlbInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/tjlb/update", function(data){
        Feng.success("修改成功!");
        window.parent.Tjlb.table.refresh();
        TjlbInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.tjlbInfoData);
    ajax.start();
}

$(function() {

});
