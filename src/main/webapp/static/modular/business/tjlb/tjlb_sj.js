/**
 * 砂浆生产记录管理初始化
 */
var Tjlb = {
    id: "TjlbTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    selectedItems: []      //选中项
};

/**
 * 初始化表格的列
 */
Tjlb.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true},
        {title: '日期', field: 'Fscrq', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '工地名称', field: 'Fgcmc', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '部位', field: 'Fjzbw', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '砼品种', field: 'Ftpz', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '出厂时间', field: 'Fccsj', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '车号', field: 'Fshch', visible: true, align: 'center', valign: 'middle', width: 80},
        {title: '方量', field: 'Fbcfs', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '代码', field: 'Fphbno', visible: true, align: 'center', valign: 'middle', width: 110},
        {title: '任务单号', field: 'Frwdh', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '小票号', field: 'Fno', visible: true, align: 'center', valign: 'middle', width: 80},
        {title: '施工方式', field: 'Fjzfs', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '到厂时间', field: 'Fdcsj', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '施工单位', field: 'Fhtdw', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '生产拌台', field: 'Fscbt', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '状态', field: 'Fzt', visible: true, align: 'center', valign: 'middle', width: 80}
        /* {title: '坍落度', field: 'FTld', visible: true, align: 'center', valign: 'middle'},
        {title: '备注', field: 'Fbz', visible: true, align: 'center', valign: 'middle', width: 60},
         {title: '司机', field: 'FSjxm', visible: true, align: 'center', valign: 'middle'},
         {title: '盘数', field: 'FBcps', visible: true, align: 'center', valign: 'middle'},
         {title: '总重量', field: 'FZzl', visible: true, align: 'center', valign: 'middle'},
         {title: '泵送数量', field: 'FBsfs', visible: true, align: 'center', valign: 'middle'},
         {title: '累计方数', field: 'FLjfs', visible: true, align: 'center', valign: 'middle'},
         {title: '累计车数', field: 'FLjcs', visible: true, align: 'center', valign: 'middle'},
         {title: '操作员', field: 'FCzy', visible: true, align: 'center', valign: 'middle'}*/
    ];
};

/**
 * 检查是否选中
 */
Tjlb.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        $news.info("请先选中表格中的记录！");
        return false;
    }else{
        Tjlb.seItem = selected[0];
        return true;
    }
};

Tjlb.getAllSelections = function () {
    var allSelected = $('#' + this.id).bootstrapTable('getAllSelections');
    //console.log("allSelected", allSelected);
    if(allSelected.length > 0) {
        for(var o in allSelected) {
            Tjlb.selectedItems.push(allSelected[o].Fno);
        }
    }
    //console.log(JSON.stringify(Tjlb.selectedItems));
};

/**
 * 打开查看生产记录详情
 */
Tjlb.openTjlbDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '生产记录详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/tjlb/detail_sj/' + Tjlb.seItem.Fno
        });
        this.layerIndex = index;
        layer.full(index);
    }
};

/**
 * 导出生产记录
 */
Tjlb.exportTjlbSjMsg = function () {
    var operation = function() {
        var url = "/tjlb/download_sj" + "?jstime=" + new Date().getTime();
        if($("#gcmc").val() != '') url = url + "&gcmc=" + $("#gcmc").val();
        if($("#beginTime").val() != '') url = url + "&beginTime=" + $("#beginTime").val();
        if($("#endTime").val() != '') url = url + "&endTime=" + $("#endTime").val();
        url = url + "&excelType=03";
        $news.download(url);
    };

    $news.confirm("是否导出查询到的所有砂浆数据?", operation);
};

Tjlb.exportTjlbSjMsgChecked = function () {
    if(Tjlb.selectedItems.length == 0) {
        $news.info("请至少选择一项！")
        return false;
    }
    var operation = function(){
        // var ajax = new $ax(Feng.ctxPath + "/tjlb/download_checked", function () {
        //     Feng.success("导出成功!");
        //     Tjlb.selectedItems = [];
        // }, function (data) {
        //     Feng.error("导出失败!" + data.responseJSON.message + "!");
        //     Tjlb.selectedItems = [];
        // });
        // console.log(JSON.stringify(Tjlb.selectedItems));
        // ajax.set("selected", JSON.stringify(Tjlb.selectedItems));
        // ajax.set("excelType", "07");
        // ajax.setType("GET");
        // ajax.start();
        var url = "/tjlb/download_checked_sj" + "?jstime=" + new Date().getTime();
        if(Tjlb.selectedItems.length > 0) url = url + "&selected=" + JSON.stringify(Tjlb.selectedItems);
        url = url + "&excelType=03";
        $news.download(url);
    };

    $news.confirm("是否导出选择的数据?", operation);
};

/**
 * 查询生产记录列表
 */
Tjlb.search = function () {
    var queryData = {};
    queryData['gcmc'] = $("#gcmc").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    Tjlb.table.refresh({query: queryData});
    Tjlb.selectedItems = [];
};

$(function () {
    var defaultColunms = Tjlb.initColumn();
    var table = new BSTable(Tjlb.id, "/tjlb/list_sj", defaultColunms);
    table.setPaginationType("client");
    table.setPageSize(100);
    Tjlb.table = table.init(function (r, e) {
        Tjlb.selectedItems.push(r.Fno);
    }, function (r, e) {
        Tjlb.selectedItems = Tjlb.selectedItems.filter(function (item) {
            return item != r.Fno
        });
    }, function (rs) {
        for(var o in rs) {
            Tjlb.selectedItems.push(rs[o].Fno);
        }
    }, function (rs) {
        for(var o in rs) {
            Tjlb.selectedItems = Tjlb.selectedItems.filter(function (item) {
                return item != rs[o].Fno
            });
        }
    });
});
