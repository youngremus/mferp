/**
 * 回弹管理初始化
 */
var Tjlb = {
    id: "TjlbTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Tjlb.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '任务单号', field: 'Frwno', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '拌台', field: 'Fscbt', visible: true, align: 'center', valign: 'middle', width: 70},
        {title: '本车', field: 'Fbcfs', visible: true, align: 'center', valign: 'middle', width: 70},
        {title: '累计', field: 'Fljfs', visible: true, align: 'center', valign: 'middle', width: 70},
        {title: '车号', field: 'Fshch', visible: true, align: 'center', valign: 'middle', width: 70},
        {title: '车次', field: 'Fljcs', visible: true, align: 'center', valign: 'middle', width: 70},
        {title: '工程名称', field: 'Fgcmc', visible: true, align: 'center', valign: 'middle', width: 280},
        {title: '施工部位', field: 'Fjzbw', visible: true, align: 'center', valign: 'middle', width: 280},
        {title: '生产日期', field: 'Fscrq', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '出厂时间', field: 'Fccsj', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '到厂时间', field: 'Fdcsj', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '砼品种', field: 'Ftpz', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '施工方式', field: 'Fjzfs', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '施工单位', field: 'Fhtdw', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '小票号', field: 'Fno', visible: true, align: 'center', valign: 'middle', width: 80},
        {title: '状态', field: 'Fzt', visible: true, align: 'center', valign: 'middle', width: 80}
        /* {title: '坍落度', field: 'FTld', visible: true, align: 'center', valign: 'middle'},
        {title: '代码', field: 'Fphbno', visible: true, align: 'center', valign: 'middle', width: 110},
         {title: '司机', field: 'FSjxm', visible: true, align: 'center', valign: 'middle'},
         {title: '盘数', field: 'FBcps', visible: true, align: 'center', valign: 'middle'},
         {title: '总重量', field: 'FZzl', visible: true, align: 'center', valign: 'middle'},
         {title: '泵送数量', field: 'FBsfs', visible: true, align: 'center', valign: 'middle'},
         {title: '操作员', field: 'FCzy', visible: true, align: 'center', valign: 'middle'},
        {title: '备注', field: 'Fbz', visible: true, align: 'center', valign: 'middle', width: 60}*/
    ];
};

/**
 * 检查是否选中
 */
Tjlb.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        $news.info("请先选中表格中的记录！");
        return false;
    }else{
        Tjlb.seItem = selected[0];
        return true;
    }
};

/**
 * 打开查看生产记录详情
 */
Tjlb.openTjlbDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '生产记录详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/tjlb/detail/' + Tjlb.seItem.Fno
        });
        layer.full(index);
        this.layerIndex = index;
    }
};

/**
 * 大屏展示
 */
Tjlb.showFullScreen = function() {
    var index = parent.layer.open({
        type: 2,
        area: ['800px', '420px'], //宽高
        fix: true, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/tjlb/ht?fs=1'
    });
    parent.layer.full(index);
    this.layerIndex = index;
};

/**
 * 查询生产记录列表
 */
Tjlb.search = function () {
    var queryData = {};
    queryData['gcmc'] = $("#gcmc").val();
    queryData['jzbw'] = $("#jzbw").val();
    queryData['scbt'] = $("#scbt").val();
    queryData['rwdh'] = $("#rwdh").val();
    queryData['tpz'] = $("#tpz").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    Tjlb.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Tjlb.initColumn();
    var table = new BSTable(Tjlb.id, "/tjlb/list_ht", defaultColunms);
    table.setPaginationType("client");
    table.setPageSize(100);
    Tjlb.table = table.init();

    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if(pair[0] == "fs"){
            $("#fullScreen").hide();
        }
    }
});
