/**
 * 生产记录管理初始化
 */
var Tjlb = {
    id: "TjlbTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    selectedItems: []      //选中项
};

/**
 * 初始化表格的列
 */
Tjlb.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true},
        {title: '小票号', field: 'Fno', visible: true, align: 'center', valign: 'middle', width: 80},
        {title: '生产日期', field: 'Fscrq', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '任务单号', field: 'Frwdh', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '工程名称', field: 'Fgcmc', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '施工部位', field: 'Fjzbw', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '施工方式', field: 'Fjzfs', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '强度等级', field: 'Ftpz', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '配合比号', field: 'Fphbno', visible: true, align: 'center', valign: 'middle', width: 110},
        {title: '到厂时间', field: 'Fdcsj', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '出厂时间', field: 'Fccsj', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '车号', field: 'Fshch', visible: true, align: 'center', valign: 'middle', width: 80},
        {title: '施工单位', field: 'Fhtdw', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '备注', field: 'Fbz', visible: true, align: 'center', valign: 'middle', width: 60},
        {title: '本车方数', field: 'Fbcfs', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '生产拌台', field: 'Fscbt', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '状态', field: 'Fzt', visible: true, align: 'center', valign: 'middle', width: 80}
       /* {title: '坍落度', field: 'FTld', visible: true, align: 'center', valign: 'middle'},
        {title: '司机', field: 'FSjxm', visible: true, align: 'center', valign: 'middle'},
        {title: '盘数', field: 'FBcps', visible: true, align: 'center', valign: 'middle'},
        {title: '总重量', field: 'FZzl', visible: true, align: 'center', valign: 'middle'},
        {title: '泵送数量', field: 'FBsfs', visible: true, align: 'center', valign: 'middle'},
        {title: '累计方数', field: 'FLjfs', visible: true, align: 'center', valign: 'middle'},
        {title: '累计车数', field: 'FLjcs', visible: true, align: 'center', valign: 'middle'},
        {title: '操作员', field: 'FCzy', visible: true, align: 'center', valign: 'middle'}*/
    ];
};

/**
 * 检查是否选中
 */
Tjlb.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    //console.log("selected", selected);
    if(selected.length == 0){
        $news.info("请先选中表格中的记录！");
        return false;
    }else{
        Tjlb.seItem = selected[0];
        return true;
    }
};

Tjlb.getAllSelections = function () {
    var allSelected = $('#' + this.id).bootstrapTable('getAllSelections');
    console.log("allSelected", allSelected);
    if(allSelected.length > 0) {
        for(var o in allSelected) {
            Tjlb.selectedItems.push(allSelected[o].Fno);
        }
    }
    console.log(JSON.stringify(Tjlb.selectedItems));
};

/**
 * 打开查看生产记录详情
 */
Tjlb.openTjlbDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '生产记录详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/tjlb/detail/' + Tjlb.seItem.Fno
        });
        this.layerIndex = index;
        layer.full(index);
    }
};

/**
 * 导出生产记录
 */
Tjlb.exportTjlbMsg = function () {
    var operation = function(){
        var url = "/tjlb/download" + "?jstime=" + new Date().getTime();
        if($("#gcmc").val() != '') url = url + "&gcmc=" + $("#gcmc").val();
        if($("#beginTime").val() != '') url = url + "&beginTime=" + $("#beginTime").val();
        if($("#endTime").val() != '') url = url + "&endTime=" + $("#endTime").val();
        if($("#outBeginTime").val() != '') url = url + "&outBeginTime=" + $("#outBeginTime").val();
        if($("#outEndTime").val() != '') url = url + "&outEndTime=" + $("#outEndTime").val();
        if($("#inBeginTime").val() != '') url = url + "&inBeginTime=" + $("#inBeginTime").val();
        if($("#inEndTime").val() != '') url = url + "&inEndTime=" + $("#inEndTime").val();
        url = url + "&excelType=03";
        $news.download(url);
    };

    $news.confirm("是否导出查询到的所有数据?", operation);
};

/**
 * 导出对外生产记录
 */
Tjlb.exportTjlbMsgOut = function () {
    var operation = function(){
        var url = "/tjlb/download_out" + "?jstime=" + new Date().getTime();
        if($("#gcmc").val() != '') url = url + "&gcmc=" + $("#gcmc").val();
        if($("#beginTime").val() != '') url = url + "&beginTime=" + $("#beginTime").val();
        if($("#endTime").val() != '') url = url + "&endTime=" + $("#endTime").val();
        if($("#outBeginTime").val() != '') url = url + "&outBeginTime=" + $("#outBeginTime").val();
        if($("#outEndTime").val() != '') url = url + "&outEndTime=" + $("#outEndTime").val();
        if($("#inBeginTime").val() != '') url = url + "&inBeginTime=" + $("#inBeginTime").val();
        if($("#inEndTime").val() != '') url = url + "&inEndTime=" + $("#inEndTime").val();
        url = url + "&excelType=03";
        $news.download(url);
    };

    $news.confirm("是否导出查询到的所有数据?", operation);
};

/**
 * 导出选中的生产记录
 */
Tjlb.exportTjlbMsgChecked = function () {
    if(Tjlb.selectedItems.length == 0) {
        $news.info("请至少选择一项！")
        return false;
    }
    var operation = function(){
        var url = "/tjlb/download_checked" + "?jstime=" + new Date().getTime();
        if(Tjlb.selectedItems.length > 0) url = url + "&selected=" + JSON.stringify(Tjlb.selectedItems);
        url = url + "&excelType=03";
        $news.download(url);
    };

    $news.confirm("是否导出选择的数据?", operation);
};

/**
 * 查询生产记录列表
 */
Tjlb.search = function () {
    var queryData = {};
    queryData['gcmc'] = $("#gcmc").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['outBeginTime'] = $("#outBeginTime").val();
    queryData['outEndTime'] = $("#outEndTime").val();
    queryData['inBeginTime'] = $("#inBeginTime").val();
    queryData['inEndTime'] = $("#inEndTime").val();
    Tjlb.table.refresh({query: queryData});
    Tjlb.selectedItems = [];
};

/*
* 日统计
*/
Tjlb.countByDay = function () {
    var url = "/tjlb/download_rtj" + "?jstime=" + new Date().getTime();
    var start = $("#beginTime").val();
    if(start) url = url + "&start=" + start;
    var end = $("#endTime").val();
    if(end) url = url + "&end=" + end;
    url = url + "&excelType=03";
    $news.download(url);
};
/**
 * 送检统计
 */
Tjlb.submission = function() {
    var url = "/tjlb/download_sub" + "?jstime=" + new Date().getTime();
    var start = $("#beginTime").val();
    if(start) url = url + "&start=" + start;
    var end = $("#endTime").val();
    if(end) url = url + "&end=" + end;
    url = url + "&excelType=03";
    $news.download(url);
};
/**
 * 耗材统计
 */
Tjlb.consumableMaterial = function() {
    var url = "/tjlb/download_cm" + "?jstime=" + new Date().getTime();
    var start = $("#beginTime").val();
    if(start) url = url + "&start=" + start;
    var end = $("#endTime").val();
    if(end) url = url + "&end=" + end;
    url = url + "&excelType=03";
    $news.download(url);
};
/**
 * 对外耗材统计
 */
Tjlb.consumableMaterialOut = function() {
    var url = "/tjlb/download_cm_out" + "?jstime=" + new Date().getTime();
    var start = $("#beginTime").val();
    if(start) url = url + "&start=" + start;
    var end = $("#endTime").val();
    if(end) url = url + "&end=" + end;
    url = url + "&excelType=03";
    $news.download(url);
};
$(function () {
    $("#beginTime").val(new Date().format("yyyy-MM-dd"));
    $("#endTime").val(new Date().format("yyyy-MM-dd"));
    $("#outBeginTime").val("00:00:00");
    $("#outEndTime").val("23:59:59");
    $("#inBeginTime").val("00:00:00");
    $("#inEndTime").val("23:59:59");
    var defaultColunms = Tjlb.initColumn();
    var table = new BSTable(Tjlb.id, "/tjlb/list", defaultColunms);
    table.setPaginationType("client");
    table.setPageSize(100);
    var queryData = {};
    queryData['gcmc'] = $("#gcmc").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['outBeginTime'] = $("#outBeginTime").val();
    queryData['outEndTime'] = $("#outEndTime").val();
    queryData['inBeginTime'] = $("#inBeginTime").val();
    queryData['inEndTime'] = $("#inEndTime").val();
    table.setQueryParams(queryData);
    Tjlb.table = table.init(function (r, e) {
        Tjlb.selectedItems.push(r.Fno);
    }, function (r, e) {
        Tjlb.selectedItems = Tjlb.selectedItems.filter(function (item) {
            return item != r.Fno
        });
    }, function (rs) {
        for(var o in rs) {
            Tjlb.selectedItems.push(rs[o].Fno);
        }
    }, function (rs) {
        for(var o in rs) {
            Tjlb.selectedItems = Tjlb.selectedItems.filter(function (item) {
                return item != rs[o].Fno
            });
        }
    });
});
