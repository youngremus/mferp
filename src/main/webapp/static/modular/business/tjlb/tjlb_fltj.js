/**
 * 方量统计初始化
 */
var Tjlb = {
    id: "TjlbTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Tjlb.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '日期', field: 'fscrq', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '拌台', field: 'fscbt', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '首车时间', field: 'fccsjMin', visible: true, align: 'center', valign: 'middle', width: 130},
        {title: '尾车时间', field: 'fccsjMax', visible: true, align: 'center', valign: 'middle', width: 130},
        {title: '方量', field: 'zfl', visible: true, align: 'center', valign: 'middle', width: 120}
    ];
};

/**
 * 检查是否选中
 */
Tjlb.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        $news.info("请先选中表格中的记录！");
        return false;
    }else{
        Tjlb.seItem = selected[0];
        return true;
    }
};

/**
 * 查询生产记录列表
 */
Tjlb.search = function () {
    var queryData = {};
    queryData['gcmc'] = $("#gcmc").val();
    queryData['jzbw'] = $("#jzbw").val();
    queryData['scbt'] = $("#scbt").val();
    queryData['rwdh'] = $("#rwdh").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['fltjType'] = $("#fltjType").val();
    Tjlb.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Tjlb.initColumn();
    var table = new BSTable(Tjlb.id, "/tjlb/list_fltj", defaultColunms);
    table.setPaginationType("client");
    table.setPageSize(100);
    Tjlb.table = table.init();
});
