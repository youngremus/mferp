/**
 * 初始化任务单施工配比原材料详情对话框
 */
var TrwdphbyclInfoDlg = {
    trwdphbyclInfoData : {}
};

/**
 * 清除数据
 */
TrwdphbyclInfoDlg.clearData = function() {
    this.trwdphbyclInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TrwdphbyclInfoDlg.set = function(key, val) {
    this.trwdphbyclInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TrwdphbyclInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
TrwdphbyclInfoDlg.close = function() {
    parent.layer.close(window.parent.Trwdphbycl.layerIndex);
}

/**
 * 收集数据
 */
TrwdphbyclInfoDlg.collectData = function() {
    this
    .set('FId')
    .set('FRwdh')
    .set('FPblb')
    .set('FYlmc')
    .set('FPzgg')
    .set('FSysl')
    .set('FPlcw')
    .set('FNdPt')
    .set('FClPt')
    .set('FAddnum')
    .set('FPbsl')
    .set('FSl')
    .set('fpbNum')
    .set('FBtId')
    .set('FCkno')
    .set('FHsl')
    .set('FXpKey')
    .set('FVersion')
    .set('FBigSandAdd')
    .set('FProValue')
    .set('FSandRate')
    .set('FStoneRate')
    .set('FWaterRate')
    .set('FOrder');
}

/**
 * 提交添加
 */
TrwdphbyclInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/trwdphbycl/add", function(data){
        Feng.success("添加成功!");
        window.parent.Trwdphbycl.table.refresh();
        TrwdphbyclInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.trwdphbyclInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
TrwdphbyclInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/trwdphbycl/update", function(data){
        Feng.success("修改成功!");
        window.parent.Trwdphbycl.table.refresh();
        TrwdphbyclInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.trwdphbyclInfoData);
    ajax.start();
}

$(function() {

});
