/**
 * 任务单施工配比原材料管理初始化
 */
var Trwdphbycl = {
    id: "TrwdphbyclTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Trwdphbycl.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '编号', field: 'FId', visible: true, align: 'center', valign: 'middle'},
            {title: '任务单号', field: 'FRwdh', visible: true, align: 'center', valign: 'middle'},
            {title: '类别', field: 'FPblb', visible: true, align: 'center', valign: 'middle'},
            {title: '材料名称', field: 'FYlmc', visible: true, align: 'center', valign: 'middle'},
            {title: '材料规格', field: 'FPzgg', visible: true, align: 'center', valign: 'middle'},
            {title: '用量', field: 'FSysl', visible: true, align: 'center', valign: 'middle'},
            {title: '仓位', field: 'FPlcw', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FNdPt', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FClPt', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FAddnum', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FPbsl', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FSl', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'fpbNum', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FBtId', visible: true, align: 'center', valign: 'middle'},
            {title: '仓库编号', field: 'FCkno', visible: true, align: 'center', valign: 'middle'},
            {title: '含水率', field: 'FHsl', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FXpKey', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FVersion', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FBigSandAdd', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FProValue', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FSandRate', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FStoneRate', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FWaterRate', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FOrder', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Trwdphbycl.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Trwdphbycl.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加任务单施工配比原材料
 */
Trwdphbycl.openAddTrwdphbycl = function () {
    var index = layer.open({
        type: 2,
        title: '添加任务单施工配比原材料',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/trwdphbycl/trwdphbycl_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看任务单施工配比原材料详情
 */
Trwdphbycl.openTrwdphbyclDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '任务单施工配比原材料详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/trwdphbycl/trwdphbycl_update/' + Trwdphbycl.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除任务单施工配比原材料
 */
Trwdphbycl.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/trwdphbycl/delete", function (data) {
            Feng.success("删除成功!");
            Trwdphbycl.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("trwdphbyclId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询任务单施工配比原材料列表
 */
Trwdphbycl.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Trwdphbycl.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Trwdphbycl.initColumn();
    var table = new BSTable(Trwdphbycl.id, "/trwdphbycl/list", defaultColunms);
    table.setPaginationType("client");
    Trwdphbycl.table = table.init();
});
