/**
 * 初始化模板数据信息详情对话框
 */
var TemplateDataInfoInfoDlg = {
    templateDataInfoInfoData : {}
};

/**
 * 清除数据
 */
TemplateDataInfoInfoDlg.clearData = function() {
    this.templateDataInfoInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TemplateDataInfoInfoDlg.set = function(key, val) {
    this.templateDataInfoInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TemplateDataInfoInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
TemplateDataInfoInfoDlg.close = function() {
    parent.layer.close(window.parent.TemplateDataInfo.layerIndex);
}

/**
 * 收集数据
 */
TemplateDataInfoInfoDlg.collectData = function() {
    this
    .set('id')
    .set('dataName')
    .set('dataType')
    .set('dataValue')
    .set('templateNum')
    .set('dataCnName')
    .set('sort');
}

/**
 * 提交添加
 */
TemplateDataInfoInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/templateDataInfo/add", function(data){
        Feng.success("添加成功!");
        window.parent.TemplateDataInfo.table.refresh();
        TemplateDataInfoInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.templateDataInfoInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
TemplateDataInfoInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/templateDataInfo/update", function(data){
        Feng.success("修改成功!");
        window.parent.TemplateDataInfo.table.refresh();
        TemplateDataInfoInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.templateDataInfoInfoData);
    ajax.start();
}

$(function() {

});
