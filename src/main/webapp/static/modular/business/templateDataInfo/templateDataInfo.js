/**
 * 模板数据信息管理初始化
 */
var TemplateDataInfo = {
    id: "TemplateDataInfoTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
TemplateDataInfo.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '序号', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '数据名称', field: 'dataName', visible: true, align: 'center', valign: 'middle'},
            {title: '数据类型', field: 'dataType', visible: true, align: 'center', valign: 'middle'},
            {title: '数据信息', field: 'dataValue', visible: true, align: 'center', valign: 'middle'},
            {title: '模板编号', field: 'templateNum', visible: true, align: 'center', valign: 'middle'},
            {title: '中文名称', field: 'dataCnName', visible: true, align: 'center', valign: 'middle'},
            {title: '排序', field: 'sort', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
TemplateDataInfo.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        TemplateDataInfo.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加模板数据信息
 */
TemplateDataInfo.openAddTemplateDataInfo = function () {
    var index = layer.open({
        type: 2,
        title: '添加模板数据信息',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/templateDataInfo/templateDataInfo_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看模板数据信息详情
 */
TemplateDataInfo.openTemplateDataInfoDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '模板数据信息详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/templateDataInfo/templateDataInfo_update/' + TemplateDataInfo.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除模板数据信息
 */
TemplateDataInfo.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/templateDataInfo/delete", function (data) {
            Feng.success("删除成功!");
            TemplateDataInfo.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("templateDataInfoId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询模板数据信息列表
 */
TemplateDataInfo.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    TemplateDataInfo.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = TemplateDataInfo.initColumn();
    var table = new BSTable(TemplateDataInfo.id, "/templateDataInfo/list", defaultColunms);
    table.setPaginationType("client");
    TemplateDataInfo.table = table.init();
});
