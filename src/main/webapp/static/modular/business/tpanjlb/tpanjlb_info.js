/**
 * 初始化每盘生产信息详情对话框
 */
var TpanjlbInfoDlg = {
    tpanjlbInfoData : {}
};

/**
 * 清除数据
 */
TpanjlbInfoDlg.clearData = function() {
    this.tpanjlbInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TpanjlbInfoDlg.set = function(key, val) {
    this.tpanjlbInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TpanjlbInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
TpanjlbInfoDlg.close = function() {
    parent.layer.close(window.parent.Tpanjlb.layerIndex);
}

/**
 * 收集数据
 */
TpanjlbInfoDlg.collectData = function() {
    this
    .set('FNo')
    .set('FZt')
    .set('FPanNo')
    .set('FScrq')
    .set('FScbt')
    .set('FDcsj')
    .set('FCcsj')
    .set('FRwdh')
    .set('FHtbh')
    .set('FKhbh')
    .set('FHtdw')
    .set('FGcmc')
    .set('FGcdz')
    .set('FJzbw')
    .set('FJzfs')
    .set('FGls')
    .set('FTpz')
    .set('FTld')
    .set('FSgpb')
    .set('FSjpb')
    .set('FShch')
    .set('FSjxm')
    .set('FBcps')
    .set('FBcfs')
    .set('FZzl')
    .set('FBsfs')
    .set('FLjfs')
    .set('FLjcs')
    .set('FCcqf')
    .set('FYhqs')
    .set('FCzy')
    .set('FBz')
    .set('FRwno')
    .set('FHtno')
    .set('FGpfs')
    .set('FSpsj')
    .set('FJbsj')
    .set('FBranchId')
    .set('FHdDateTime')
    .set('FHdOperator')
    .set('FMzKg')
    .set('FPzKg')
    .set('FJzKg')
    .set('frz')
    .set('FFlNum')
    .set('FClsj')
    .set('FSby')
    .set('FJby')
    .set('FGpsj')
    .set('FRecId')
    .set('FPhbNo')
    .set('FGlsID')
    .set('FTlb')
    .set('FBcfsC')
    .set('FBcfsM')
    .set('FPcbID')
    .set('FPhbSJ')
    .set('fa1')
    .set('fa2')
    .set('fa3')
    .set('fa4')
    .set('fc1')
    .set('fc2')
    .set('fc3')
    .set('FRvA')
    .set('FRvB')
    .set('FRvC')
    .set('FQvA')
    .set('FQvB')
    .set('FQvC')
    .set('FIv0')
    .set('FCcsjEx')
    .set('FTmA')
    .set('FTmB')
    .set('FBpfs')
    .set('FKsplsj')
    .set('FPljssj')
    .set('Fksjlsj')
    .set('Fksjbsj')
    .set('Fjbjkmsj')
    .set('Fjbjgmsj')
    .set('FGcbh')
    .set('FJdhm')
    .set('FSczt')
    .set('FXpNo');
}

/**
 * 提交添加
 */
TpanjlbInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/tpanjlb/add", function(data){
        Feng.success("添加成功!");
        window.parent.Tpanjlb.table.refresh();
        TpanjlbInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.tpanjlbInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
TpanjlbInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/tpanjlb/update", function(data){
        Feng.success("修改成功!");
        window.parent.Tpanjlb.table.refresh();
        TpanjlbInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.tpanjlbInfoData);
    ajax.start();
}

$(function() {

});
