/**
 * 每盘生产信息管理初始化
 */
var Tpanjlb = {
    id: "TpanjlbTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Tpanjlb.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '小票号', field: 'FNo', visible: true, align: 'center', valign: 'middle'},
            {title: '状态', field: 'FZt', visible: true, align: 'center', valign: 'middle'},
            {title: '盘号', field: 'FPanNo', visible: true, align: 'center', valign: 'middle'},
            {title: '生产日期', field: 'FScrq', visible: true, align: 'center', valign: 'middle'},
            {title: '生产拌台', field: 'FScbt', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FDcsj', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FCcsj', visible: true, align: 'center', valign: 'middle'},
            {title: '任务单号', field: 'FRwdh', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FHtbh', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FKhbh', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FHtdw', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FGcmc', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FGcdz', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FJzbw', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FJzfs', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FGls', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FTpz', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FTld', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FSgpb', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FSjpb', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FShch', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FSjxm', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FBcps', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FBcfs', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FZzl', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FBsfs', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FLjfs', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FLjcs', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FCcqf', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FYhqs', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FCzy', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FBz', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FRwno', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FHtno', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FGpfs', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FSpsj', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FJbsj', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FBranchId', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FHdDateTime', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FHdOperator', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FMzKg', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FPzKg', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FJzKg', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'frz', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FFlNum', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FClsj', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FSby', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FJby', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FGpsj', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FRecId', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FPhbNo', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FGlsID', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FTlb', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FBcfsC', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FBcfsM', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FPcbID', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FPhbSJ', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'fa1', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'fa2', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'fa3', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'fa4', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'fc1', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'fc2', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'fc3', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FRvA', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FRvB', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FRvC', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FQvA', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FQvB', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FQvC', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FIv0', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FCcsjEx', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FTmA', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FTmB', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FBpfs', visible: true, align: 'center', valign: 'middle'},
            {title: '开始配料时间', field: 'FKsplsj', visible: true, align: 'center', valign: 'middle'},
            {title: '配料结束时间', field: 'FPljssj', visible: true, align: 'center', valign: 'middle'},
            {title: '开始计量时间：第一次投料时间', field: 'Fksjlsj', visible: true, align: 'center', valign: 'middle'},
            {title: '开始搅拌时间', field: 'Fksjbsj', visible: true, align: 'center', valign: 'middle'},
            {title: '搅拌机开门时间', field: 'Fjbjkmsj', visible: true, align: 'center', valign: 'middle'},
            {title: '搅拌机关门时间', field: 'Fjbjgmsj', visible: true, align: 'center', valign: 'middle'},
            {title: '工程编号', field: 'FGcbh', visible: true, align: 'center', valign: 'middle'},
            {title: '监督号码', field: 'FJdhm', visible: true, align: 'center', valign: 'middle'},
            {title: '上传状态', field: 'FSczt', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'FXpNo', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Tpanjlb.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Tpanjlb.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加每盘生产信息
 */
Tpanjlb.openAddTpanjlb = function () {
    var index = layer.open({
        type: 2,
        title: '添加每盘生产信息',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/tpanjlb/tpanjlb_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看每盘生产信息详情
 */
Tpanjlb.openTpanjlbDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '每盘生产信息详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/tpanjlb/tpanjlb_update/' + Tpanjlb.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除每盘生产信息
 */
Tpanjlb.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/tpanjlb/delete", function (data) {
            Feng.success("删除成功!");
            Tpanjlb.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("tpanjlbId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询每盘生产信息列表
 */
Tpanjlb.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Tpanjlb.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Tpanjlb.initColumn();
    var table = new BSTable(Tpanjlb.id, "/tpanjlb/list", defaultColunms);
    table.setPaginationType("client");
    Tpanjlb.table = table.init();
});
