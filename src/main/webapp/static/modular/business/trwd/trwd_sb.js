/**
 * 初始化任务单详情对话框
 */
var TrwdSbDlg = {
    id: "rwdTable",	//表格id
    trwdInfoData : {},
    table: null,
    editor: null,
    isAnalyse: false,
    stage: null,
    validateFields: {
        FGcmc: {
            validators: {
                notEmpty: {
                    message: '工程名称不能为空'
                }
            }
        },
        FHtdw: {
            validators: {
                notEmpty: {
                    message: '施工单位不能为空'
                }
            }
        },
        FGcdz: {
            validators: {
                notEmpty: {
                    message: '工地地址不能为空'
                }
            }
        },
        FJzbw: {
            validators: {
                notEmpty: {
                    message: '浇筑部位不能为空'
                }
            }
        },
        FTpz: {
            validators: {
                notEmpty: {
                    message: '强度等级不能为空'
                }
            }
        },
        FJzfs: {
            validators: {
                notEmpty: {
                    message: '卸料方式不能为空'
                }
            }
        },
        Ftld: {
            validators: {
                notEmpty: {
                    message: '塌落度不能为空'
                }
            }
        },
        FJhsl: {
            validators: {
                notEmpty: {
                    message: '预计方量不能为空'
                }
            }
        },
        FJhrq: {
            validators: {
                notEmpty: {
                    message: '浇筑时间不能为空'
                }
            }
        },
        gddh: {
            validators: {
                notEmpty: {
                    message: '工地电话不能为空'
                }
            }
        },
        zlyq: {
            validators: {
                notEmpty: {
                    message: '资料要求不能为空'
                }
            }
        }
    },
    printData: {}
};

/**
 * 清除数据
 */
TrwdSbDlg.clearData = function() {
    this.trwdInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TrwdSbDlg.set = function(key, val) {
    this.trwdInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TrwdSbDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
TrwdSbDlg.close = function() {
    parent.layer.close(window.parent.Trwd.layerIndex);
}

/**
 * 收集数据
 */
TrwdSbDlg.collectData = function() {
    this
    .set('FGcmc')
    .set('FHtdw')
    .set('FGcdz')
    .set('FJzbw')
    .set('FTpz')
    .set('FJzfs')
    .set('Ftld')
    .set('FJhsl')
    .set('FJhrq')
    .set('zlyq')
    .set('gddh')
    .set('FRwxz');
}

var clearAllData = function() {
    this.trwdInfoData = {};
    $("#FGcmc").val("");
    $("#FHtdw").val("");
    $("#FGcdz").val("");
    $("#FJzbw").val("");

    $("#FTpz").val("");
    $("#FJzfs").val("");
    $("#Ftld").val("");
    $("#FJhsl").val("");

    $("#FJhrq").val("");
    $("#gddh").val("");
    $("#zlyq").val("");
    $("#FRwxz").val("");

    $("#FZt").val("");
    $("#Fdlrq").val("");
    $("#Fczy").val("");
};

/**
 * 验证数据是否为空
 */
TrwdSbDlg.validate = function () {
    $('#trwdSbForm').data("bootstrapValidator").resetForm();
    $('#trwdSbForm').bootstrapValidator('validate');
    return $("#trwdSbForm").data('bootstrapValidator').isValid();
};

/**
 * 提交添加
 */
TrwdSbDlg.analyse = function() {
    //var content = TrwdSbDlg.editor.txt.html();
    var content = $("#editor1").val();
    if (!content) {
        Feng.error("请复制报单内容并粘贴！");
        return;
    }
    $("#FRwdh").val("");
    var param = {
        content: content
    };
    this.isAnalyse = true;
    $("#saveBtn").focus();
    $news.postX("/trwd_old/analyse_content", param, function (data) {
        console.log(data.data);
        if (data.code === 200) {
            clearAllData();
            let data1 = data.data;
            $("#FGcmc").val(data1.fgcmc);
            $("#FTpz").val(data1.ftpz);
            $("#FJzfs").val(data1.fjzfs);
            $("#zlyq").val(data1.zlyq);

            $("#FHtdw").val(data1.fhtdw);
            $("#FJhsl").val(data1.fjhsl);
            $("#gddh").val(data1.gddh);
            $("#Ftld").val(data1.ftld);

            $("#FJzbw").val(data1.fjzbw);
            $("#FJhrq").val(data1.fjhrq);
            $("#FGcdz").val(data1.fgcdz);
            $news.success("识别成功", 1, function () {
                TrwdSbDlg.getGddh();
                TrwdSbDlg.getZlyq(data1.zlyq);
            });
            $('#trwdSbForm').data("bootstrapValidator").resetForm();
        } else {
            Feng.error(data.message)
        }
    });
}

/**
 * 提交添加或者修改
 */
TrwdSbDlg.addSubmit = function() {
    let frwdh = $("#FRwdh").val();
    if (frwdh) {
        TrwdSbDlg.editSubmit();
        return;
    }
    this.clearData();
    this.collectData();

    if (!this.validate()) {
        return;
    }

    // 默认点击确认
    if (parent.jQuery('.layui-layer-btn0').length > 0) {
        var $confirmButton = parent.jQuery('.layui-layer-btn0');
        console.log('确认保存！');
        $confirmButton.click();
        $('#trwdSbForm').data("bootstrapValidator").resetForm();
        return;
    }
    console.log('是否保存？')
    var trwd = {
        fgcmc : $("#FGcmc").val(),
        ftpz : $("#FTpz").val(),
        fjzfs : $("#FJzfs").val(),
        zlyq : $("#zlyq").val(),
        fhtdw : $("#FHtdw").val(),
        fjhsl : $("#FJhsl").val(),
        gddh : $("#gddh").val(),
        ftld : $("#Ftld").val(),
        fjzbw : $("#FJzbw").val(),
        fjhrq : $("#FJhrq").val(),
        fgcdz : $("#FGcdz").val(),
        frwxz : $("#FRwxz").val()
    };
    var operation = function() {
        $news.post("/trwd_old/save_trwd", JSON.stringify(trwd), function (data) {
            if(data.code === 200) {
                let data1 = data.data;
                this.printData = data.data;
                $("#FRwdh").val(data1.frwdh);
                $("#Fczy").val(data1.fczy);
                $("#Fdlrq").val(data1.fdlrq);
                $("#FZt").val(data1.fzt);
                $news.success("保存任务单成功", 1, function () {
                    TrwdSbDlg.search();
                    $("#printBtn2").focus();
                });
            } else {
                Feng.error(data.message);
            }
        }, function(data) {
            Feng.error(data.message);
        });
    };
    Feng.confirm("确定数据正确识别并保存当前任务单?", operation);
    /*parent.jQuery(".layui-layer-shade").click();
    document.addEventListener('keydown', function (e) {
        if (e.keyCode === 32) {
            console.log("confirmButton！");
            //e.preventDefault();
            //operation();
            // 获取确认按钮的DOM元素
            var $confirmButton = parent.jQuery('.layui-layer-btn0');
            console.log($confirmButton.html());
            $confirmButton.click();
            return false;
        }
    });*/

    /*this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/trwd/save_trwd", function(data){
        Feng.success("添加成功!");
        window.parent.Trwd.table.refresh();
        TrwdSbDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.trwdInfoData);
    ajax.start();*/
}

/**
 * 提交修改
 */
TrwdSbDlg.editSubmit = function() {
    let frwdh = $("#FRwdh").val();
    console.log(frwdh)
    if (!frwdh) {
        Feng.error("请先填写任务单号", 3);
        return false;
    }
    if(!isNumeric(frwdh)) {
        Feng.error("任务单号请输入数字");
        return false;
    }
    if (!this.validate()) {
        return;
    }
    this.clearData();
    this.collectData();

    // console.log(this.trwdInfoData);

    // 默认点击确认
    if (parent.jQuery('.layui-layer-btn0').length > 0) {
        var $confirmButton = parent.jQuery('.layui-layer-btn0');
        console.log('确认修改！');
        $confirmButton.click();
        $('#trwdSbForm').data("bootstrapValidator").resetForm();
        return;
    }
    console.log('是否修改？');
    var trwd = {
        frwdh : $("#FRwdh").val(),
        fgcmc : $("#FGcmc").val(),
        ftpz : $("#FTpz").val(),
        fjzfs : $("#FJzfs").val(),
        zlyq : $("#zlyq").val(),
        fhtdw : $("#FHtdw").val(),
        fjhsl : $("#FJhsl").val(),
        gddh : $("#gddh").val(),
        ftld : $("#Ftld").val(),
        fjzbw : $("#FJzbw").val(),
        fjhrq : $("#FJhrq").val(),
        fgcdz : $("#FGcdz").val(),
        frwxz : $("#FRwxz").val()
    };
    var operation = function() {
        $news.post("/trwd_old/update_trwd", JSON.stringify(trwd), function (data) {
            if(data.code === 200) {
                let data1 = data.data;
                this.printData = data.data;
                $("#Fczy").val(data1.fczy);
                $("#Fdlrq").val(data1.fdlrq);
                $("#FZt").val(data1.fzt);
                Feng.success("修改任务单成功");
                $("#printBtn").focus();
            } else {
                Feng.error(data.message);
            }
        }, function(data) {
            Feng.error(data.message);
        });
    };
    Feng.confirm("确定数据正确并修改当前任务单?", operation);

    /*this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/trwd/update", function(data){
        Feng.success("修改成功!");
        window.parent.Trwd.table.refresh();
        TrwdSbDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.trwdInfoData);
    ajax.start();*/
}

/**
 * 查找任务单
 */
TrwdSbDlg.getRwd = function() {
    var rwdh = $("#FRwdh").val();
    if (!rwdh) {
        Feng.error("请先填写任务单号", 3);
        return false;
    }
    if(!isNumeric(rwdh)) {
        Feng.error("任务单号请输入数字");
        return false;
    }
    $news.postX("/trwd_old/get_trwd_by_frwdh/" + rwdh, {},function (data) {
        console.log(data.data);
        if (data.code === 200) {
            clearAllData();
            let data1 = data.data;
            this.printData = data.data;
            console.log(data1.ftbj);
            console.log(data1.fssyq);
            $("#FGcmc").val(data1.fgcmc);
            $("#FTpz").val(data1.ftpz);
            $("#FJzfs").val(data1.fjzfs);
            $("#zlyq").val(data1.zlyq);

            $("#FHtdw").val(data1.fhtdw);
            $("#FJhsl").val(data1.fjhsl);
            $("#gddh").val(data1.gddh);
            $("#Ftld").val(data1.ftld);

            $("#FJzbw").val(data1.fjzbw);
            $("#FJhrq").val(data1.fjhrq);
            $("#FGcdz").val(data1.fgcdz);
            $("#FZt").val(data1.fzt);

            $("#FRwxz").val(data1.frwxz);
            $("#Fczy").val(data1.fczy);
            $("#Fdlrq").val(data1.fdlrq);
            TrwdSbDlg.getGddh();
        } else {
            Feng.error(data.message);
        }
    });
}

/**
 * 查找上一个任务单
 */
TrwdSbDlg.lastRwd = function() {
    var rwdh = $("#FRwdh").val();
    if (!rwdh) {
        rwdh = "0";
    }
    $news.postX("/trwd_old/get_last_trwd_by_frwdh/" + rwdh, {},function (data) {
        console.log(data.data);
        if (data.code === 200) {
            clearAllData();
            let data1 = data.data;
            console.log(data1.ftbj);
            console.log(data1.fssyq);
            $("#FGcmc").val(data1.fgcmc);
            $("#FTpz").val(data1.ftpz);
            $("#FJzfs").val(data1.fjzfs);
            $("#zlyq").val(data1.zlyq);

            $("#FHtdw").val(data1.fhtdw);
            $("#FJhsl").val(data1.fjhsl);
            $("#gddh").val(data1.gddh);
            $("#Ftld").val(data1.ftld);

            $("#FJzbw").val(data1.fjzbw);
            $("#FJhrq").val(data1.fjhrq);
            $("#FGcdz").val(data1.fgcdz);
            $("#FZt").val(data1.fzt);
            $("#FRwdh").val(data1.frwdh);

            $("#FRwxz").val(data1.frwxz);
            $("#Fczy").val(data1.fczy);
            $("#Fdlrq").val(data1.fdlrq);
            TrwdSbDlg.getGddh();
        } else {
            Feng.error(data.message);
        }
    });
}

/**
 * 查找任务单
 */
TrwdSbDlg.nextRwd = function() {
    var rwdh = $("#FRwdh").val();
    if (!rwdh) {
        rwdh = "0";
    }
    $news.postX("/trwd_old/get_next_trwd_by_frwdh/" + rwdh, {},function (data) {
        console.log(data.data);
        if (data.code === 200) {
            clearAllData();
            let data1 = data.data;
            this.printData = data.data;
            console.log(data1.ftbj);
            console.log(data1.fssyq);
            $("#FGcmc").val(data1.fgcmc);
            $("#FTpz").val(data1.ftpz);
            $("#FJzfs").val(data1.fjzfs);
            $("#zlyq").val(data1.zlyq);

            $("#FHtdw").val(data1.fhtdw);
            $("#FJhsl").val(data1.fjhsl);
            $("#gddh").val(data1.gddh);
            $("#Ftld").val(data1.ftld);

            $("#FJzbw").val(data1.fjzbw);
            $("#FJhrq").val(data1.fjhrq);
            $("#FGcdz").val(data1.fgcdz);
            $("#FZt").val(data1.fzt);
            $("#FRwdh").val(data1.frwdh);

            $("#FRwxz").val(data1.frwxz);
            $("#Fczy").val(data1.fczy);
            $("#Fdlrq").val(data1.fdlrq);
            TrwdSbDlg.getGddh();
        } else {
            Feng.error(data.message);
        }
    });
}

/**
 * 初始化表格的列
 */
TrwdSbDlg.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true, visible: false},
        {title: '单号', field: 'frwdh', visible: true, align: 'center', valign: 'middle', width: 80},
        {title: '计划日期', field: 'fjhrq', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '施工单位', field: 'fhtdw', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '工程名称', field: 'fgcmc', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '施工部位', field: 'fjzbw', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '浇筑方式', field: 'fjzfs', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '砼品种', field: 'ftpz', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '生产拌台', field: 'fscbt', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '计划方量', field: 'fjhsl', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '任务性质', field: 'frwxz', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '状态', field: 'fzt', visible: true, align: 'center', valign: 'middle', width: 80}
    ];
};

/**
 * 初始化表格
 */
TrwdSbDlg.initTable = function (queryData) {
    var defaultColunms = TrwdSbDlg.initColumn();
    var table = new BSTable(TrwdSbDlg.id, "/trwd_old/get_trwd_list_today", defaultColunms);
    table.setPaginationType("client");
    table.setQueryParams(queryData);
    TrwdSbDlg.table = table.init(function (r, e) {
    }, function (r, e) {
    }, function (rs) {
    }, function (rs) {
    });
};

/**
 * 查询当天新建任务单
 */
TrwdSbDlg.search = function () {
    var queryData = {};
    if(TrwdSbDlg.table == null) {
        TrwdSbDlg.initTable(queryData);
    } else {
        TrwdSbDlg.table.refresh({query: queryData});
    }
};

TrwdSbDlg.setV = function () {
    this.printData = {
        frwdh : $("#FRwdh").val(),
        fgcmc : $("#FGcmc").val(),
        ftpz : $("#FTpz").val(),
        fjzfs : $("#FJzfs").val(),
        zlyq : $("#zlyq").val(),
        fhtdw : $("#FHtdw").val(),
        fjhsl : $("#FJhsl").val(),
        gddh : $("#gddh").val(),
        ftld : $("#Ftld").val(),
        fjzbw : $("#FJzbw").val(),
        fjhrq : $("#FJhrq").val(),
        fgcdz : $("#FGcdz").val(),
        frwxz : $("#FRwxz").val(),
        fczy :  $("#Fczy").val(),
        fdlrq: $("#Fdlrq").val()
    };
}

TrwdSbDlg.createPrintPage = function () {
    TrwdSbDlg.setV();
    //var LODOP=getLodop(document.getElementById('LODOP_OB'),document.getElementById('LODOP_EM'));
    var rwdh = $("#FRwdh").val();
    if (!rwdh) {
        Feng.error("请先保存任务单！");
        return;
    }
    var fgcmc = this.printData["fgcmc"];
    if (!fgcmc) {
        Feng.error("请先保存任务单！");
        return;
    }
    var frwdh = this.printData["frwdh"];
    var fjzbw = this.printData["fjzbw"];
    var fhtdw = this.printData["fhtdw"];
    var fjhsl = this.printData["fjhsl"];
    var ftpz = this.printData["ftpz"];
    var fgcdz = this.printData["fgcdz"];
    var gddh = this.printData["gddh"];
    var zlyq = this.printData["zlyq"];
    var ftld = this.printData["ftld"];
    var fjzfs = this.printData["fjzfs"];
    var fjhrq = this.printData["fjhrq"];
    var fdlrq = this.printData["fdlrq"];
    var fczy = this.printData["fczy"];
    LODOP = getLodop();

    LODOP.PRINT_INIT("打印控件功能演示_Lodop功能_空白练习");
    LODOP.ADD_PRINT_TEXT("2.5cm","4.606cm","13.549cm","1cm",fgcmc);
    LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
    LODOP.SET_PRINT_STYLEA(0,"LineSpacing",6);
    LODOP.ADD_PRINT_TEXT("3.519cm","4.604cm","13.549cm","1cm",fjzbw);
    LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
    LODOP.ADD_PRINT_TEXT(172,174,"6.3cm","1cm",fhtdw);
    LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
    LODOP.ADD_PRINT_TEXT(171,"12.184cm","6.001cm",38,fjhsl);
    LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
    LODOP.ADD_PRINT_TEXT(209,"12.158cm","6.001cm",38,ftpz);
    LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
    LODOP.ADD_PRINT_TEXT(210,174,"6.3cm","1cm",fgcdz);
    LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
    LODOP.ADD_PRINT_TEXT(247,174,"6.3cm","1cm","/");
    LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
    LODOP.ADD_PRINT_TEXT(284,174,"6.3cm","1cm",gddh);
    LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
    LODOP.ADD_PRINT_TEXT(322,174,"6.3cm","1cm","/");
    LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
    LODOP.ADD_PRINT_TEXT(359,"1.931cm","9.001cm","2.199cm","");
    LODOP.SET_PRINT_STYLEA(0,"FontSize",10);
    LODOP.ADD_PRINT_TEXT(365,"3.5cm","6.3cm","1cm",zlyq);
    LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
    LODOP.ADD_PRINT_TEXT(402,"3.5cm","6.308cm","1cm",gddh);
    LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
    LODOP.ADD_PRINT_TEXT(247,"12.158cm","31.99mm",38,fjzfs);
    LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
    LODOP.ADD_PRINT_TEXT(247,"15.965cm","22.3mm",38,ftld);
    LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
    LODOP.ADD_PRINT_TEXT(285,"12.158cm","6.001cm",38,"/");
    LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
    LODOP.ADD_PRINT_TEXT(323,"12.184cm","60.01mm",38,fjhrq);
    LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    LODOP.ADD_PRINT_TEXT(359,"12.184cm","60.01mm","2.199cm","");
    LODOP.SET_PRINT_STYLEA(0,"FontSize",10);
    LODOP.ADD_PRINT_TEXT("1.341cm","15.372cm","27.99mm",38,"NO:" + frwdh);
    LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
    LODOP.ADD_PRINT_TEXT(441,"4.601cm","2.799cm",38,fdlrq);
    LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
    LODOP.ADD_PRINT_TEXT(441,"12.158cm",106,38,fczy);
    LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
}

/**
 * 打印任务单
 */
TrwdSbDlg.selectDefaultPrinterSubmit = function() {
    LODOP = getLodop();
    if (LODOP.CVERSION) {
        LODOP.On_Return = function (TaskID, Value) {
            if (Value >= 0) {
                Feng.success('选择成功!');
            } else {
                Feng.error('选择失败！');
            }
        }
        LODOP.SELECT_PRINTER();
    } else if (LODOP.SELECT_PRINTER() >= 0) {
        Feng.success('选择成功!');
    } else {
        Feng.error('选择失败！');
    }
}
TrwdSbDlg.printDesignSubmit = function() {
    TrwdSbDlg.createPrintPage();
    LODOP.PRINT_DESIGN();
}
TrwdSbDlg.previewSubmit = function() {
    TrwdSbDlg.createPrintPage();
    LODOP.PREVIEW();
}
TrwdSbDlg.printSubmit = function() {
    TrwdSbDlg.createPrintPage();
    // EPSON LQ-615KII ESC/P2
    if (LODOP.SET_PRINTER_INDEXA("Aisino SK-860")) {
        LODOP.PRINT();
    } else {
        Feng.error("打印机“Aisino SK-860”不存在");
    }

    // LODOP.PRINT_INIT("打印控件功能演示_Lodop功能_空白练习");
    // LODOP.ADD_PRINT_TEXT(65,148,570,39, fgcmc);
    // LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
    // LODOP.ADD_PRINT_TEXT(104,148,570,39,fjzbw);
    // LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
    // LODOP.ADD_PRINT_TEXT(143,148,253,39,fhtdw);
    // LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
    // LODOP.ADD_PRINT_TEXT(143,507,211,39,fjhsl);
    // LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
    // LODOP.ADD_PRINT_TEXT(183,507,211,39,ftpz);
    // LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
    // LODOP.ADD_PRINT_TEXT(182,148,253,39,fgcdz);
    // LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
    // LODOP.ADD_PRINT_TEXT(221,148,253,39,"/"); // 联系人
    // LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
    // LODOP.ADD_PRINT_TEXT(260,148,253,39,gddh); // 联系电话
    // LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
    // LODOP.ADD_PRINT_TEXT(299,148,253,39,"/"); // 是否加膨胀剂
    // LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
    // LODOP.ADD_PRINT_TEXT(338,87,314,128,"");
    // LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
    // LODOP.ADD_PRINT_TEXT(362,122,253,39,zlyq);
    // LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
    // LODOP.ADD_PRINT_TEXT(411,122,253,39,gddh); // 说明：联系电话
    // LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
    // LODOP.ADD_PRINT_TEXT(475,140,253,39,fdlrq); // 填单日期
    // LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
    // LODOP.ADD_PRINT_TEXT(222,507,74,39,fjzfs);
    // LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
    // LODOP.ADD_PRINT_TEXT(222,644,74,39,ftld);
    // LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
    // LODOP.ADD_PRINT_TEXT(261,507,211,39,"/"); // 碎石粒径
    // LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
    // LODOP.ADD_PRINT_TEXT(300,507,211,39,fjhrq);
    // LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
    // LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    // LODOP.ADD_PRINT_TEXT(339,404,314,128,"");
    // LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
    // LODOP.ADD_PRINT_TEXT(474,442,253,39,fczy); // 经办人
    // LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
    //LODOP.PREVIEW();
}

TrwdSbDlg.getAllFjzfs = function () {
    var fJzfs = $("#FJzfs").val();
    var param = {};
    if (fJzfs) {
        param["fjzfs"] = fJzfs;
    }
    $news.postX("/trwd_old/get_all_fjzfs", param, function (data) {
        console.log(data.data);
        if (data.code === 200) {
            var jzfss = data.data;
            $("#jzfs_ul").empty();
            for (var i in jzfss) {
                $('#jzfs_ul').append("<li>" + jzfss[i] + "</li>");
            }
            $('#FJzfs').focus(function () {
                $("#jzfs_ul").show();
                $("#gddh_ul").hide();
                $("#fjhrq_ul").hide();
                $("#zlyq_ul").hide();
            });
            // 移开焦点事件会导致点击事件失效，需要用setTimeout来避免事件失效
            $('#FJzfs').focusout(function () {
                setTimeout(function () {
                    $("#jzfs_ul").hide();
                }, 200);
            });
            $('.jzfs_ul').on('click', 'li', function() {
                $('#FJzfs').val($(this).text());
                $('#trwdSbForm').data("bootstrapValidator").resetForm();
                $("#jzfs_ul").hide();
            });
        } else {
            console.log(data.message)
        }
    });
};

TrwdSbDlg.getGddh = function () {
    var gddh = $("#gddh").val();
    if (!gddh) {
        return;
    }
    var param = {
        gddh: gddh
    };
    $news.postX("/trwd_old/get_gddh", param, function (data) {
        console.log(data.data);
        if (data.code === 200) {
            var gddhs = data.data;
            $("#gddh_ul").empty();
            for (var i in gddhs) {
                $("#gddh_ul").append("<li>" + gddhs[i] + "</li>");
            }
            $("#gddh").focus(function () {
                $("#gddh_ul").show();
                $("#jzfs_ul").hide();
                $("#fjhrq_ul").hide();
                $("#zlyq_ul").hide();
            });
            $("#gddh").focusout(function () {
                setTimeout(function () {
                    $("#gddh_ul").hide();
                }, 200);
            });

            $('#gddh_ul li').on('click', function() {
                $('#gddh').val($(this).text());
                $('#trwdSbForm').data("bootstrapValidator").resetForm();
                $("#gddh_ul").hide();
            });
        } else {
            console.log(data.message)
        }
    });
};

TrwdSbDlg.getFjhrq = function () {
    var fjhrq = $("#FJhrq").val();
    var param = {};
    if (fjhrq) {
        param["fjhrq"] = fjhrq;
    }
    $news.postX("/trwd_old/get_fjhrq", param, function (data) {
        console.log(data.data);
        if (data.code === 200) {
            var fjhrqs = data.data;
            $("#fjhrq_ul").empty();
            for (var i in fjhrqs) {
                $("#fjhrq_ul").append("<li>" + fjhrqs[i] + "</li>");
            }
            $("#FJhrq").focus(function () {
                $("#fjhrq_ul").show();
                $("#jzfs_ul").hide();
                $("#gddh_ul").hide();
                $("#zlyq_ul").hide();
            });
            $("#FJhrq").focusout(function () {
                setTimeout(function () {
                    $("#fjhrq_ul").hide();
                }, 200);
            });

            $('#fjhrq_ul li').on('click', function() {
                $('#FJhrq').val($(this).text());
                $('#trwdSbForm').data("bootstrapValidator").resetForm();
                $("#fjhrq_ul").hide();
            });
        } else {
            console.log(data.message)
        }
    });
};

TrwdSbDlg.getZlyq = function (temp) {
    $("#zlyq_ul").empty();
    $("#zlyq_ul").append("<li>/</li>");
    $("#zlyq_ul").append("<li>开盘鉴定</li>");
    $("#zlyq_ul").append("<li>开盘鉴定及试块</li>");
    if (temp) {
        $("#zlyq_ul").append("<li>" + temp + "</li>");
    }
    $("#zlyq").focus(function () {
        $("#zlyq_ul").show();
        $("#fjhrq_ul").hide();
        $("#jzfs_ul").hide();
        $("#gddh_ul").hide();
    });
    $("#zlyq").focusout(function () {
        setTimeout(function () {
            $("#zlyq_ul").hide();
        }, 200);
    });

    $('#zlyq_ul li').on('click', function() {
        $('#zlyq').val($(this).text());
        $('#trwdSbForm').data("bootstrapValidator").resetForm();
        $("#zlyq_ul").hide();
    });
};

function handleEvent(event) {
    if (event.keyCode !== 32) {
        console.log(event.keyCode)
        return;
    }
    console.log('空格键被按下了');
    if (this.stage === 'analyse') {
        TrwdSbDlg.addSubmit();
        return;
    } else if (this.stage === 'saveSubmit' && parent.jQuery('.layui-layer-btn0').length > 0) {
        var $confirmButton = parent.jQuery('.layui-layer-btn0');
        console.log($confirmButton.html());
        $confirmButton.click();
        return;
    }
    console.log("键盘事件结束")
}

$(function() {
    Feng.initValidator("trwdSbForm", TrwdSbDlg.validateFields);
    TrwdSbDlg.getAllFjzfs();

    $('#FJzfs').on('input', function() {
        TrwdSbDlg.getAllFjzfs();
    });

    TrwdSbDlg.getZlyq();
    // document.addEventListener('keydown', handleEvent);
    //初始化编辑器
    /*var E = window.wangEditor;
    var editor = new E('#editor');
    //editor.configs.menus = ['undo', 'redo'];
    editor.create();
    editor.txt.html("");*/
    //TrwdSbDlg.editor = editor;
});