/**
 * 任务单管理初始化
 */
var TrwdOld = {
    id: "TrwdOldTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    selectCheckbox: []
};

/**
 * 初始化表格的列
 */
TrwdOld.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true, width: 30},
        {title: '单号', field: 'frwdh', visible: true, align: 'center', valign: 'middle', width: 80},
        {title: '浇筑时间', field: 'fjhrq', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '施工单位', field: 'fhtdw', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '工程名称', field: 'fgcmc', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '浇筑部位', field: 'fjzbw', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '卸料方式', field: 'fjzfs', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '强度等级', field: 'ftpz', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '生产拌台', field: 'fscbt', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '状态', field: 'fzt', visible: true, align: 'center', valign: 'middle', width: 80},
        {title: '预计方量', field: 'fjhsl', visible: false, align: 'center', valign: 'middle', width: 100},
        {title: '任务性质', field: 'frwxz', visible: false, align: 'center', valign: 'middle', width: 100}
    ];
};

/**
 * 检查是否选中
 */
TrwdOld.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        TrwdOld.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加任务单
 */
TrwdOld.openAddTrwd = function () {
    var index = layer.open({
        type: 2,
        title: '添加任务单',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/trwd_old/trwd_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看任务单详情
 */
TrwdOld.openTrwdDetail = function () {
    if (this.check()) {
        console.log('选择的任务单：', TrwdOld.seItem);
        var index = layer.open({
            type: 2,
            title: '任务单详情',
            area: ['800px', '380px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/trwd_old/trwd_update/' + TrwdOld.seItem.frwdh
        });
        layer.full(index);
        this.layerIndex = index;
    }
};

/**
 * 查询任务单列表
 */
TrwdOld.search = function () {
    var queryData = {};
    queryData['gcmc'] = $("#gcmc").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['rwdh'] = $("#rwdh").val();
    queryData['scbt'] = $("#scbt").val();
    queryData['zt'] = $("#zt").val();
    TrwdOld.table.refresh({query: queryData});
    TrwdOld.selectCheckbox = [];
};

/**
 * 导出接单日报表
 */
TrwdOld.downloadTodayReport = function () {
    var operation = function(){
        var url = "/trwd_old/download_today_report" + "?jstime=" + new Date().getTime();
        if($("#gcmc").val() != '') url = url + "&gcmc=" + $("#gcmc").val();
        if($("#beginTime").val() != '') url = url + "&beginTime=" + $("#beginTime").val();
        if($("#endTime").val() != '') url = url + "&endTime=" + $("#endTime").val();
        if($("#rwdh").val() != '') url = url + "&rwdh=" + $("#rwdh").val();
        if($("#zt").val() != '') url = url + "&zt=" + $("#zt").val();
        if($("#scbt").val() != '') url = url + "&scbt=" + $("#scbt").val();
        url = url + "&excelType=03";
        $news.download(url);
    };

    $news.confirm("是否导出指定日期的日报表?", operation);
};

$(function () {
    $("#beginTime").val(new Date().format("yyyy-MM-dd"));
    $("#endTime").val(new Date().format("yyyy-MM-dd"));
    var defaultColunms = TrwdOld.initColumn();
    var table = new BSTable(TrwdOld.id, "/trwd_old/list", defaultColunms);
    table.setPaginationType("client");
    table.setQueryParams({
        scbt : $("#scbt").val(),
        zt: $("#zt").val(),
        beginTime:$("#beginTime").val(),
        endTime:$("#endTime").val()
    })
    TrwdOld.table = table.init(function (r, e) {
        TrwdOld.selectCheckbox.push(r.Frwdh);
    }, function (r, e) {
        TrwdOld.selectCheckbox = TrwdOld.selectCheckbox.filter(function (item) {
            return item != r.Frwdh
        });
    }, function (rs) {
        for(var o in rs) {
            TrwdOld.selectCheckbox.push(rs[o].Frwdh);
        }
    }, function (rs) {
        for(var o in rs) {
            TrwdOld.selectCheckbox = TrwdOld.selectCheckbox.filter(function (item) {
                return item != rs[o].Frwdh
            });
        }
    });
});