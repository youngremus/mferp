/**
 * 初始化任务单详情对话框
 */
var TrwdInfoDlg = {
    trwdInfoData : {}
};

/**
 * 清除数据
 */
TrwdInfoDlg.clearData = function() {
    this.trwdInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TrwdInfoDlg.set = function(key, val) {
    this.trwdInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TrwdInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
TrwdInfoDlg.close = function() {
    parent.layer.close(window.parent.Trwd.layerIndex);
}

/**
 * 收集数据
 */
TrwdInfoDlg.collectData = function() {
    this
    .set('FRwdh')
    .set('FHtbh')
    .set('FRwxz')
    .set('FRwly')
    .set('FZt')
    .set('FHtdw')
    .set('FGcmc')
    .set('FGcjb')
    .set('FGclb')
    .set('FJzbw')
    .set('FJzfs')
    .set('FBclb')
    .set('FGcdz')
    .set('FGls')
    .set('FJhrq')
    .set('FTpz')
    .set('FTld')
    .set('FSnbh')
    .set('FSzgg')
    .set('FTbj')
    .set('FJhsl')
    .set('FScbt')
    .set('FWcsl')
    .set('FLjcs')
    .set('FXdrw')
    .set('FCzy')
    .set('FDlrq')
    .set('FSgpb')
    .set('FSyy1')
    .set('FSjpb')
    .set('FSyy2')
    .set('FZdpb')
    .set('FTjsj')
    .set('FTjr')
    .set('FKsrq')
    .set('FWcrq')
    .set('FXph1')
    .set('FXph2')
    .set('FKzsl')
    .set('FKzdj')
    .set('FKzje')
    .set('FQrTsl')
    .set('FQrTdj')
    .set('FQrTje')
    .set('FQrBssl')
    .set('FQrBsdj')
    .set('FQrBsje')
    .set('FQrKzsl')
    .set('FQrKzdj')
    .set('FQrKzje')
    .set('FQrZjje')
    .set('FQrBz')
    .set('FQrFzr')
    .set('FQrrq')
    .set('FTdj')
    .set('FTje')
    .set('FBssl')
    .set('FBsdj')
    .set('FBsje')
    .set('FZje')
    .set('FRwno')
    .set('FHtno')
    .set('FJbsj')
    .set('FClsjNo')
    .set('FJsdNo')
    .set('FBranchId')
    .set('FHdDateTime')
    .set('FHdOperator')
    .set('FZzl')
    .set('FByDateTime')
    .set('FByOperator')
    .set('FByBz')
    .set('fc1')
    .set('fc2')
    .set('fc3')
    .set('FOperator')
    .set('FTimeStamp')
    .set('FCheckSum')
    .set('FCheckKey')
    .set('FMpfsP')
    .set('FPzzt')
    .set('FPzrq')
    .set('FPzr')
    .set('FMemo')
    .set('FPhfh')
    .set('FPhfhsj')
    .set('FPhbNo')
    .set('FBSfs')
    .set('FShb')
    .set('FPhbId')
    .set('FRz')
    .set('FQrZzl')
    .set('FQrRz')
    .set('fa1')
    .set('fa2')
    .set('fa3')
    .set('fa4')
    .set('FSjb')
    .set('FSlPt')
    .set('FJnclKg')
    .set('FRvA')
    .set('FRvB')
    .set('FRvC')
    .set('FQvA')
    .set('FQvB')
    .set('FQvC')
    .set('FIv0')
    .set('Fwyzfs')
    .set('Fwyzcs')
    .set('FVersion')
    .set('FXpNo')
    .set('FXpKey')
    .set('FXgsj');
}

/**
 * 提交添加
 */
TrwdInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/trwd/add", function(data){
        Feng.success("添加成功!");
        window.parent.Trwd.table.refresh();
        TrwdInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.trwdInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
TrwdInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/trwd/update", function(data){
        Feng.success("修改成功!");
        window.parent.Trwd.table.refresh();
        TrwdInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.trwdInfoData);
    ajax.start();
}


$(function() {

});