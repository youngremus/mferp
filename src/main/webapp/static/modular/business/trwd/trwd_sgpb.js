/**
 * 初始化任务单详情对话框
 */
var TrwdSgpbDlg = {
    id: "recentRwdTable",	//表格id
    id1: "sjpbxxTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    table1: null,
    layerIndex: -1,
    currentSjpbxxList: [], //当前实际配比原材料
    selectRecentRwd: false, //是否选择最近任务单
    adjusting: false,       //是否正在调整配方
    tableScrollPosition1: null, // table1滚动位置
    tableSjpbxxListLog1: {},      //调方时实际配比信息历史记录
    tableSjpbxxListChangeTime: 0,  //实际配比信息变更次数
    currentPzggxxList: [],          //当前品种规格列表信息
    jcDifference: 0,                 //胶材差值
    templateInfoList: [],            //模板信息
    adjustRz: false                 //是否调整容重
};

TrwdSgpbDlg.initColumn1 = function () {
    return [
        {field: 'selectItem', radio: true, width: 30},
        {title: '序号', field: 'fid', visible: false, align: 'center', valign: 'middle', width: 80},
        {title: '单号', field: 'frwdh', visible: true, align: 'center', valign: 'middle', width: 80},
        {title: '部位', field: 'fjzbw', visible: true, align: 'center', valign: 'middle', width: 80},
        {title: '强度', field: 'ftpz', visible: true, align: 'center', valign: 'middle', width: 80},
        {title: '日期', field: 'ftzsj', visible: true, align: 'center', valign: 'middle', width: 120},
    ];
};

TrwdSgpbDlg.initColumn2 = function () {
    return [
        {field: 'selectItem', radio: true, width: 30},
        {title: '序号', field: 'fid', visible: false, width: 30},
        {title: '名称', field: 'fylmc', visible: true, align: 'center', valign: 'middle', width: 80},
        {title: '品种', field: 'fpzgg', visible: true, align: 'center', valign: 'middle', width: 80},
        {title: '用量', field: 'fsysl', visible: true, align: 'center', valign: 'middle', width: 80},
    ];
};

/**
 * 清除数据
 */
TrwdSgpbDlg.clearData = function() {
    this.trwdInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TrwdSgpbDlg.set = function(key, val) {
    this.trwdInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TrwdSgpbDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
TrwdSgpbDlg.close = function() {
    parent.layer.close(window.parent.Trwd.layerIndex);
}

/**
 * 收集数据
 */
TrwdSgpbDlg.collectData = function() {
    
};

/**
 * 提交添加
 */
TrwdSgpbDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/trwd/add", function(data){
        Feng.success("添加成功!");
        window.parent.Trwd.table.refresh();
        TrwdInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.trwdInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
TrwdSgpbDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/trwd/update", function(data){
        Feng.success("修改成功!");
        window.parent.Trwd.table.refresh();
        TrwdInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.trwdInfoData);
    ajax.start();
}

// 清空所有数据
var clearAllData = function() {
    TrwdSgpbDlg.currentSjpbxxList = [];
    TrwdSgpbDlg.selectRecentRwd = false;
    $("#Ftpz").val("");
    $("#Ftld").val("");
    $("#Fjzfs").val("");
    $("#Fszgg").val("");
    $("#Fgcmc").val("");
    $("#Fjzbw").val("");
    $("#Fscbt").val("");
    $("#FrwdhValue").val("");
    $("#FscbtValue").val("");
    $("#FphbnoValue").val("");
    $("#FsgpbValue").val("");
    $("#Fphbno").val("");
    $("#phbno").val("");
    $("#scbtCurrent").val("");
    $("#fx").val("");
    $("#newSl").val("");
    $("#oldSl").val("");
    $("#newCl").val("");
    $("#oldCl").val("");
    $("#newJc").val("");
    $("#oldJc").val("");
    $("#jcDifference").html("");
    $("#newRz").val("");
    $("#oldRz").val("");
    $("#rzDifference").html("");
    $("#ylmc").val("");
    $("#Fpz").val("");
    $("#pz").val("");
    $("#sgpb").val("");
    $("#sysl").val("");
    $("#fid").val("");
    $("#ysysl").val("");
};
// 初始化列表（最近任务单）
TrwdSgpbDlg.initTable = function(queryData) {
    var defaultColunms1 = TrwdSgpbDlg.initColumn1();
    var table1 = new BSTable(TrwdSgpbDlg.id, "/trwdphb/getRecentRwdList", defaultColunms1);
    table1.setQueryParams(queryData);
    table1.setPageSize(32);
    table1.setPaginationType("client");
    TrwdSgpbDlg.table = table1.init(function (r, e) {
        $(e).addClass("info");
        var fid = r.fid;
        //提交信息
        var ajax = new $ax(Feng.ctxPath + "/trwdphb/get_trwd_phb_ycl/" + String(fid), function(data){
            console.log("get_trwd_phb_ycl:", data);
            TrwdSgpbDlg.selectRecentRwd = true;
            for(var o in data) {
                data[o].fid = Number(o) + 1;
            }
            TrwdSgpbDlg.setRwdSjpbxx(data);
        }, function(data){

        });
        ajax.start();
    }, function (r, e) {
        $(e).removeClass("info");
    });
}
// 初始化列表（生产配比原材料）
TrwdSgpbDlg.initTable1 = function(queryData) {
    var defaultColunms2 = TrwdSgpbDlg.initColumn2();
    var table2 = new BSTable(TrwdSgpbDlg.id1, "/trwdphbycl/getSjpbxx", defaultColunms2);
    table2.setQueryParams(queryData);
    table2.setPageSize(32);
    table2.setPaginationType("client");
    TrwdSgpbDlg.table1 = table2.init(function (r, ele) {
        $("#ylmc").val(r.fylmc);
        $("#pz").val(r.fpzgg);
        $("#sgpb").val($("#FsgpbValue").val());
        $("#sysl").val(r.fsysl);
        $("#ysysl").val(r.fsysl);
        $("#fid").val(r.fid);
        TrwdSgpbDlg.initYclDownList(r.fylmc, r.fpzgg);
        TrwdSgpbDlg.tableScrollPosition1 = $("#"+TrwdSgpbDlg.id1).bootstrapTable('getScrollPosition');
        if((r.fylmc.indexOf("砂") != -1 || r.fylmc.indexOf("石") != -1) && $("#adjustRecipeBtn").text() == "调整完毕") {
            $("#addSandStone").show();
            $("#minusSandStone").show();
        } else {
            $("#addSandStone").hide();
            $("#minusSandStone").hide();
        }
    }, null, null, function () {
        $("#ylmc").val("");
        $("#pz").val("");
        $("#sgpb").val("");
        $("#sysl").val("");
        $("#ysysl").val("");
        $("#fid").val("");
    }, function (data) {
        var list = $("#"+TrwdSgpbDlg.id1).bootstrapTable('getData');
        //console.log("list:", list);
        TrwdSgpbDlg.currentSjpbxxList = list;
        TrwdSgpbDlg.calculate(list);
    });
}
// 初始化配合比编号下拉列表
TrwdSgpbDlg.initPhbDownList = function() {
    var ajax = new $ax(Feng.ctxPath + "/tphb/getDownList", function (data) {
        //console.log(data);
        for (var o in data) {
            $("#phbno").append("<option value='" + data[o].fphbh + "'>" +
                data[o].fbz + "(" + data[o].fphbno + ")</option>");
        }
        $("#phbno").val($("#FsgpbValue").val());
        $("#Fphbno").val($("#FphbnoValue").val());
    }, function (data) {
        console.log(data.responseJSON.message);
    });
    ajax.start();
};
//初始化配方模板
TrwdSgpbDlg.initTemplateInfoDownList = function() {
    var url = "/templateInfo/getDownList";
    $news.postX(url, {}, function (data) {
        console.log(data);
        TrwdSgpbDlg.templateInfoList = data;
        for (var o in data) {
            $("#templateInfo").append("<option value='" + data[o].templateNum + "'>" +
                data[o].templateName + "</option>");
        }
    });
};
// 初始化原材料列表
TrwdSgpbDlg.initYclDownList = function(ylmc, pzgg) {
    var ajax = new $ax(Feng.ctxPath + "/tycl/getDownList/" + ylmc, function (data) {
        //console.log('原材料：', data);
        $("#Fpz").html("");
        for (var o in data) {
            $("#Fpz").append("<option value='" + data[o].fpzgg + "'>" + data[o].fpzgg + "</option>");
        }
        $("#Fpz").val(pzgg);
    }, function (data) {
        console.log(data.responseJSON.message);
    });
    ajax.start();
}
//最近任务单
TrwdSgpbDlg.search = function () {
    var queryData = {};
    var rwdh = $("#Frwdh").val();
    queryData['scbt'] = $("#Fscbt").val();
    queryData['gcmc'] = $("#Fgcmc").val();
    queryData['phbno'] = $("#Fphbno").val();
    queryData['qb'] = $("#qb").prop('checked');
    if(rwdh== null || rwdh == undefined || rwdh == '') {
        Feng.error("请先选择任务单");
        return false;
    }
    if(queryData['scbt'] == null || queryData['scbt'] == undefined || queryData['scbt'] == '') {
        Feng.error("请先选择生产线");
        return false;
    }
    if(queryData['phbno'] == null || queryData['phbno'] == undefined || queryData['phbno'] == '') {
        Feng.error("请先选择施工配比");
        return false;
    }
    if(TrwdSgpbDlg.table == null) {
        TrwdSgpbDlg.initTable(queryData);
    } else {
        TrwdSgpbDlg.table.refresh({query: queryData});
    }
};
//任务单变化初始化所有
TrwdSgpbDlg.getAll = function() {
    // 清空所有表单数据
    clearAllData();
    var rwdh = $("#Frwdh").val();
    if(rwdh != null && rwdh != undefined && rwdh != '') {
        $("#otherRow").show();
    } else {
        $("#otherRow").hide();
    }
    if(rwdh == undefined || rwdh == '') {
        return false;
    }
    var ajax = new $ax(Feng.ctxPath + "/trwd/get_by_rwd/" + rwdh, function (data) {
        console.log(data);
        $("#Ftpz").val(data.ftpz);
        $("#Ftld").val(data.ftld);
        $("#Fjzfs").val(data.fjzfs);
        $("#Fszgg").val(data.fszgg);
        $("#Fgcmc").val(data.fgcmc);
        $("#Fjzbw").val(data.fjzbw);
        $("#Fscbt").val(data.fscbt);
        $("#FrwdhValue").val(data.frwdh);
        $("#FscbtValue").val(data.fscbt);
        $("#FphbnoValue").val(data.fphbno);
        $("#FsgpbValue").val(data.fsgpb);
        $("#Fphbno").val(data.fphbno);
        $("#phbno").val(data.fsgpb);
        $("#scbtCurrent").val(data.fscbt);
        if(isNumeric(data.fscbt) && data.fscbt != 0) {
            $("#fx").val(data.fscbt+"#线");
        }
        var rwdh = data.frwdh;
        var scbt = data.fscbt;
        if(scbt == null || scbt == undefined || scbt == '*' || scbt == '') {
            scbt = '0';
        }
        TrwdSgpbDlg.getRecentRwdList();
        TrwdSgpbDlg.getSjpbxx(rwdh, scbt);
    }, function (data) {
        console.log(data.responseJSON.message);
    });
    ajax.start();


}
//实际生产配比原材料
TrwdSgpbDlg.getSjpbxx = function(rwdh, scbt) {
    if(rwdh == null || rwdh == undefined || rwdh == '') {
        return;
    }
    if(scbt == null || scbt == undefined || scbt == '') {
        return;
    }
    var queryData = {
        "scbt": scbt,
        "rwdh": rwdh
    };
    if(TrwdSgpbDlg.table1 == null) {
        TrwdSgpbDlg.initTable1(queryData);
        TrwdSgpbDlg.selectRecentRwd = false;
    } else {
        TrwdSgpbDlg.table1.refresh({query: queryData});
    }
    TrwdSgpbDlg.currentSjpbxxList = $("#" + TrwdSgpbDlg.id1).bootstrapTable("getData");
    TrwdSgpbDlg.setRwdSjpbxx(TrwdSgpbDlg.currentSjpbxxList);
}
//初始化最近任务单列表
TrwdSgpbDlg.getRecentRwdList = function() {
    var scbt = $("#Fscbt").val();
    var gcmc = $("#Fgcmc").val();
    var phbno = $("#Fphbno").val();
    var qb = $("#qb").prop('checked');
    if(scbt == undefined || scbt == '' || scbt == "*") {
        return;
    }
    if(gcmc == undefined || gcmc == '') {
        return;
    }
    if(phbno == undefined || phbno == '') {
        return;
    }
    var queryData = {
        "scbt": scbt,
        "gcmc": gcmc,
        "phbno": phbno,
        "qb": qb
    };
    if(TrwdSgpbDlg.table == null) {
        TrwdSgpbDlg.initTable(queryData);
    } else {
        TrwdSgpbDlg.table.refresh({query: queryData});
    }
}
//加载配比信息
TrwdSgpbDlg.setRwdSjpbxx = function(data) {
    if(data == null || data == undefined || data == '') {
        return;
    }
    if(TrwdSgpbDlg.table1 == null) {
        TrwdSgpbDlg.initTable1(null);
    }
    $("#"+TrwdSgpbDlg.id1).bootstrapTable('load', data);
    TrwdSgpbDlg.currentSjpbxxList = data;
    TrwdSgpbDlg.calculate(data);
}
//计算数据
TrwdSgpbDlg.calculate = function(data) {
    console.log("data.length:", data.length);
    if(data.length > 0) {
        var rz = 0.00;//容重
        var pow = 0;//胶凝材料
        var agg = 0;//骨料
        var ad = 0.0;//外加剂
        var sl = 0.00;//砂率
        var cl = 0.0;//掺量
        var sand = 0;//砂
        for(var x in data) {
            var ycl = data[x];
            var sysl = Number(ycl.fsysl);
            var ylmc = ycl.fylmc;
            var pzgg = ycl.fpzgg;
            rz = Number(rz) + sysl;
            if(ylmc.indexOf("水泥") != -1 || ylmc.indexOf("矿粉")  != -1 || ylmc.indexOf("粉煤灰") != -1 ) {
                pow = Number(pow) + sysl;
            } else if(ylmc.indexOf("石") != -1 || ylmc.indexOf("瓜米") != -1) {
                agg = Number(agg) + sysl;
            } else if(ylmc.indexOf("外加剂") != -1 && pzgg != 'B1') {
                ad = Number(ad) + sysl;
            } else if(ylmc.indexOf("砂") != -1) {
                agg = Number(agg) + sysl;
                sand = Number(sand) + sysl;
            }
        }
        rz = rz.toFixed(2);
        pow = Math.round(pow);
        if(Number(agg) > 0) {
            sl = (Number(sand) / Number(agg) * 100).toFixed(2);
        } else {
            if(Number(sand) > 0) {
                sl = 100;
            } else {
                sl = 0;
            }
        }
        if(Number(pow) > 0) {
            cl = (Number(ad) / Number(pow) * 100).toFixed(2);
        } else {
            if(Number(ad) > 0) {
                cl = 100;
            } else {
                cl = 0;
            }
        }
        $("#newRz").val(rz);
        $("#newJc").val(pow);
        $("#newCl").val(cl);
        $("#newSl").val(sl);
        // 正在调方则原配方信息不变
        var txt = $("#adjustRecipeBtn").text();
        var newFsgpbValue = $("#newFsgpbValue").val();
        if(txt == "调整配比" || newFsgpbValue) {
            $("#oldRz").val(rz);
            $("#oldJc").val(pow);
            $("#oldCl").val(cl);
            $("#oldSl").val(sl);
            //console.log("aaaaa");
            TrwdSgpbDlg.tableSjpbxxListLog1 = JSON.parse(JSON.stringify(TrwdSgpbDlg.currentSjpbxxList));
        }
        $("#rzDifference").text((Number($("#newRz").val()) - Number($("#oldRz").val())).toFixed(2));
        $("#jcDifference").text((Number($("#newJc").val()) - Number($("#oldJc").val())).toFixed(0));
    } else {
        $("#newRz").val("0");
        $("#newJc").val("0");
        $("#newCl").val("0");
        $("#newSl").val("0");
        $("#ylmc").val("");
        $("#pz").val("");
        $("#sgpb").val("");
        $("#sysl").val("");
        $("#ysysl").val("");
        $("#fid").val("");
        $("#rzDifference").text((0 - Number($("#oldRz").val())).toFixed(2));
        $("#jcDifference").text((0 - Number($("#oldJc").val())).toFixed(0));
    }
}
//调整掺量
TrwdSgpbDlg.adjustCl = function(cl) {
    if(TrwdSgpbDlg.currentSjpbxxList == null) {
        Feng.error("没有可以调整掺量的配方");
        return false;
    }
    //console.log(TrwdSgpbDlg.currentSjpbxxList);
    $news.post("/trwdphbycl/adjust_cl/" + String(cl),
            JSON.stringify(TrwdSgpbDlg.currentSjpbxxList), function (data) {
            console.log("adjust_cl:", data);
            TrwdSgpbDlg.setRwdSjpbxx(data);
            $("#"+TrwdSgpbDlg.id1).bootstrapTable('scrollTo', TrwdSgpbDlg.tableScrollPosition1);
        });
    //提交信息
    /*$.ajax({
        type: "post",
        url: Feng.ctxPath + "/trwdphbycl/adjust_cl/" + String(cl),
        dataType: "json",
        async: false,
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(TrwdSgpbDlg.currentSjpbxxList),
        beforeSend: function(data) {

        },
        success: function(data) {
            console.log("adjust_cl:", data);
            TrwdSgpbDlg.setRwdSjpbxx(data);
            $("#"+TrwdSgpbDlg.id1).bootstrapTable('scrollTo', TrwdSgpbDlg.tableScrollPosition1);
        },
        error: function(data) {
            Feng.error(data);
        }
    });*/
}
//调整砂率
TrwdSgpbDlg.adjustSl = function(sl) {
    if(TrwdSgpbDlg.currentSjpbxxList == null) {
        Feng.error("没有可以调整砂率的配方");
        return false;
    }
    $news.post("/trwdphbycl/adjust_sl/" + String(sl),
        JSON.stringify(TrwdSgpbDlg.currentSjpbxxList), function(data) {
            TrwdSgpbDlg.setRwdSjpbxx(data);
            $("#"+TrwdSgpbDlg.id1).bootstrapTable('scrollTo', TrwdSgpbDlg.tableScrollPosition1);
        });
    //提交信息
    /*$.ajax({
        type: "post",
        url: Feng.ctxPath + "/trwdphbycl/adjust_sl/" + String(sl),
        dataType: "json",
        async: false,
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(TrwdSgpbDlg.currentSjpbxxList),
        beforeSend: function(data) {

        },
        success: function(data) {
            console.log("adjust_sl:", data);
            TrwdSgpbDlg.setRwdSjpbxx(data);
            $("#"+TrwdSgpbDlg.id1).bootstrapTable('scrollTo', TrwdSgpbDlg.tableScrollPosition1);
        },
        error: function(data) {
            Feng.error(data);
        }
    });*/
}
//开始调整配方
TrwdSgpbDlg.startAdjustRecipe = function() {
    var txt = $("#adjustRecipeBtn").text();
    if(txt == "调整配比") {
        TrwdSgpbDlg.tableScrollPosition1 = $("#" + TrwdSgpbDlg.id1).bootstrapTable('getScrollPosition');
        TrwdSgpbDlg.adjusting = true; //正在调方
        $("#adjustRecipeBtn").removeClass("btn-primary").addClass("btn-warning").text("调整完毕");
        $("#saveRecipeBtn").removeClass("btn-primary").addClass("btn-danger").text("删除");
        $("input[type='text']").prop("disabled", "disabled"); //不可切换任务单
        $("#recentRwdTable input[type='radio']").prop("disabled", "disabled"); //不可指定配方
        $("select").prop("disabled", "disabled");
        $("button").prop("disabled", "disabled");
        $("#adjustRecipeBtn").removeAttr("disabled");
        $("#saveRecipeBtn").removeAttr("disabled");
        $("#resetRecipeBtn").show().removeAttr("disabled");
        $("#useSgpbRecipeBtn").hide();
        $("#phbno").hide(); //可指定施工配比
        $("#Fpz").removeAttr("disabled"); //可修改原材料品种
        $("#sysl").removeAttr("disabled"); //可修改原材料用量
        $("#jcDifference").show();  //胶材差值
        $("#rzDifference").show();  //容重差值
        $("#frzbl").removeAttr("disabled"); //可调整容重
        $(".cl").removeAttr("disabled"); //可调整掺量
        $(".sl").removeAttr("disabled"); //可调整砂率
        $("#templateInfo").show().removeAttr("disabled");
        $("#useTemplateInfoBtn").show().removeAttr("disabled");
        $("#clearRecipeBtn").show().removeAttr("disabled");
        if($("#ylmc").val().indexOf("砂") != -1 || $("#ylmc").val().indexOf("石") != -1) {
            $("#addSandStone").show();
            $("#minusSandStone").show();
        } else {
            $("#addSandStone").hide();
            $("#minusSandStone").hide();
        }
    } else if(txt == "调整完毕") {
        var jcDifference = $("#jcDifference").text();
        if(isNumeric(jcDifference) && Number(jcDifference) < 0 && !TrwdSgpbDlg.adjustRz) {
            $news.info("胶材比原来胶材用量少！");
            return false;
        }
        TrwdSgpbDlg.adjusting = false; //调方完毕
        $("#adjustRecipeBtn").removeClass("btn-warning").addClass("btn-primary").text("调整配比");
        $("#saveRecipeBtn").removeClass("btn-danger").addClass("btn-primary").text("保存配比");
        $("#resetRecipeBtn").hide();
        $("#useSgpbRecipeBtn").show();
        $("#phbno").show(); //可指定施工配比
        $("input").removeAttr("disabled");
        $("select").removeAttr("disabled");
        $("button").removeAttr("disabled");
        $("#Fpz").prop("disabled", "disabled"); //可修改原材料品种
        $("#sysl").prop("disabled", "disabled"); //可修改原材料用量
        $("#frzbl").prop("disabled", "disabled"); //可调整容重
        $(".cl").prop("disabled", "disabled"); //可调整掺量
        $(".sl").prop("disabled", "disabled"); //可调整砂率
        $("#jcDifference").hide();
        $("#rzDifference").hide();
        $("#ysysl").val("");
        $("#fid").val("");
        $("#"+TrwdSgpbDlg.id1).bootstrapTable('uncheckAll');
        $("#templateInfo").hide();
        $("#useTemplateInfoBtn").hide();
        $("#clearRecipeBtn").hide();
        $("#addSandStone").hide();
        $("#minusSandStone").hide();
        if(TrwdSgpbDlg.adjustRz) {
            TrwdSgpbDlg.adjustRz = false;
        }
    }
}
//保存配方
TrwdSgpbDlg.saveRecipe = function() {
    var txt = $("#saveRecipeBtn").text();
    if(txt == "保存配比") {
        var newFsgpbValue = $("#newFsgpbValue").val();
        var newFphbnoValue = $("#newFphbnoValue").val();
        var FsgpbValue = $("#FsgpbValue").val();
        var useSgpb = (newFsgpbValue != null && newFsgpbValue != '') ? newFsgpbValue : FsgpbValue;
        var recipe = {
            data : TrwdSgpbDlg.currentSjpbxxList,
            Frwdh: $("#FrwdhValue").val(),
            Fsgpb: useSgpb,
            Fscbt: $("#FscbtValue").val(),
            Ftpz: $("#Ftpz").val()
        };
        var operation = function() {
            $news.post("/trwd/save_recipe", JSON.stringify(recipe), function (data) {
                if(newFsgpbValue != null && newFsgpbValue != '') {
                    $("#FsgpbValue").val(newFsgpbValue);
                    $("#FphbnoValue").val(newFphbnoValue);
                    $("#Fphbno").val(newFphbnoValue);
                    TrwdSgpbDlg.currentSjpbxxList = $("#"+TrwdSgpbDlg.id1).bootstrapTable('getData');
                }
                if(data.code == 200) {
                    $news.success("保存配比成功");
                } else {
                    $news.fail(data.message);
                }
            }, function(data) {
                $news.fail(data.message, 3);
            });
        };
        Feng.confirm("确定要保存当前配方?", operation);
    } else if(txt == "删除") {
        var data = TrwdSgpbDlg.currentSjpbxxList;
        if(data.length <= 1) {
            TrwdSgpbDlg.currentSjpbxxList = [];
            $("#"+TrwdSgpbDlg.id1).bootstrapTable('removeAll');
            TrwdSgpbDlg.calculate([]);
        } else {
            var fid = $("#fid").val();
            var nextIndex = null;
            if(!isNumeric(fid)) {
                Feng.error("请确定选中要删除的原材料");
                return false;
            }
            for(var o in data) {
                if(data[o].fid == fid) {
                    if(o != 0) nextIndex = o - 1;
                    break;
                }
            }
            if(!nextIndex) nextIndex = 0;
            TrwdSgpbDlg.currentSjpbxxList.forEach(function(item, index, arr) {
                if(item.fid == fid) {
                    arr.splice(index, 1);
                }
            });
            // TrwdSgpbDlg.currentSjpbxxList = data.filter(function (item) {
            //     return item.fid != fid;
            // });
            TrwdSgpbDlg.setRwdSjpbxx(TrwdSgpbDlg.currentSjpbxxList);
            $("#"+TrwdSgpbDlg.id1).bootstrapTable('scrollTo', TrwdSgpbDlg.tableScrollPosition1);
            $("#"+TrwdSgpbDlg.id1).bootstrapTable('uncheckAll');
            if(nextIndex != null && nextIndex != undefined) {
                $("#"+TrwdSgpbDlg.id1).bootstrapTable('check', nextIndex);
            }
        }
    }
}
//撤销修改
TrwdSgpbDlg.resetRecipe = function() {
    // setRwdSjpbxx只能用currentSjpbxxList，不能使用tableSjpbxxListLog1，否则会被修改到！！！
    TrwdSgpbDlg.currentSjpbxxList = JSON.parse(JSON.stringify(TrwdSgpbDlg.tableSjpbxxListLog1));
    TrwdSgpbDlg.setRwdSjpbxx(TrwdSgpbDlg.currentSjpbxxList);
    console.log("撤销：", JSON.stringify(TrwdSgpbDlg.tableSjpbxxListLog1));
    $("#phbno").val($("#FsgpbValue").val());
    $("#newFsgpbValue").val("");
    $("#newFphbnoValue").val("");
    //$("#" + TrwdSgpbDlg.id1).bootstrapTable("uncheckAll")
}
//清空
TrwdSgpbDlg.clearRecipe = function() {
    TrwdSgpbDlg.currentSjpbxxList = [];
    $("#"+TrwdSgpbDlg.id1).bootstrapTable('removeAll');
    TrwdSgpbDlg.calculate([]);
};
//使用模板
TrwdSgpbDlg.useTemplateInfo = function() {
    if(!TrwdSgpbDlg.templateInfoList || TrwdSgpbDlg.templateInfoList.length <=0) return;
    var tiList = TrwdSgpbDlg.templateInfoList;
    //console.log('tiList', tiList);
    var tempalteNum = $("#templateInfo").val();
    //console.log('tempalteNum', tempalteNum);
    if(!tempalteNum) return;
    var operation = function () {
        for(var i = 0; i < tiList.length; i++) {
            if(tiList[i].templateNum == tempalteNum) {
                var list = tiList[i].list;
                //console.log('list', list);
                var t = TrwdSgpbDlg.tableSjpbxxListLog1;
                if(list != null && list != undefined && list.length > 0) {
                    var tempList = [];
                    for(var j=0; j<list.length; j++) {
                        tempList.push({
                            fid: j+1,
                            fylmc: list[j].dataCnName,
                            fpzgg: list[j].dataName,
                            fsysl:0
                        });
                    }
                    //console.log('tempList', tempList);
                    if(TrwdSgpbDlg.table1 == null) {
                        TrwdSgpbDlg.initTable1(null);
                    }
                    $("#"+TrwdSgpbDlg.id1).bootstrapTable('load', tempList);
                    TrwdSgpbDlg.currentSjpbxxList = tempList;
                    TrwdSgpbDlg.tableSjpbxxListLog1 = t;
                    $("#"+TrwdSgpbDlg.id1).bootstrapTable('scrollTo', TrwdSgpbDlg.tableScrollPosition1);
                }
            }
        }
    };
    Feng.confirm("确定要使用模板开始调方?", operation);

};
//加载当前施工配比
TrwdSgpbDlg.getSgPbRecipe = function() {
    var fsgpbValue = $("#phbno").val();
    var fphbnoValue = $("#phbno option:selected").text();
    fphbnoValue = fphbnoValue.substring(fphbnoValue.indexOf("(") + 1, fphbnoValue.indexOf(")"));
    if(fsgpbValue == null || fsgpbValue == '') {
        $news.info("请先指定施工配比");
        return false;
    }
    var scbt = $("#scbtCurrent").val();
    if(scbt == null || scbt == '') {
        $news.info("请先选择生产拌台");
        return false;
    }
    var recipe = {
        rwdh: $("#FrwdhValue").val(),
        sgpb: fsgpbValue,
        scbt: scbt
    };
    var operation = function () {
        $news.postX("/trwd/get_sgpb_recipe", recipe, function (data) {
            //console.log(data.data);
            $("#newFsgpbValue").val(fsgpbValue);
            $("#newFphbnoValue").val(fphbnoValue);
            var t = TrwdSgpbDlg.tableSjpbxxListLog1;
            TrwdSgpbDlg.setRwdSjpbxx(data.data);
            TrwdSgpbDlg.tableSjpbxxListLog1 = t;
            $("#"+TrwdSgpbDlg.id1).bootstrapTable('scrollTo', TrwdSgpbDlg.tableScrollPosition1);
            $news.success("加载施工配比成功");
        });
    };
    Feng.confirm("确定要使用基本施工配比?", operation);
}
$(function() {
    var rwdh = $("#Frwdh").val();
    if(rwdh != null && rwdh != undefined && rwdh != '') {
        $("#otherRow").show();
    } else {
        $("#otherRow").hide();
    }
    // 初始化生产拌台
    $("#Fscbt").val($("#FscbtValue").val()).change(function () {
        var scbt = $(this).children('option:selected').val();
        if(scbt != '*') {
            $("#scbtCurrent").val(scbt).change();
        }
        $("#" + TrwdSgpbDlg.id).bootstrapTable('removeAll');
    });
    // 选择生产线
    $("#scbtCurrent").val($("#FscbtValue").val()).change(function () {
        var rwdh = $("#Frwdh").val();
        var scbt = $(this).children('option:selected').val();
        if(scbt != 0) {
            $("#Fscbt").val(scbt);
        }
        TrwdSgpbDlg.getSjpbxx(rwdh, scbt);
        TrwdSgpbDlg.selectRecentRwd = false;
        $("#" + TrwdSgpbDlg.id).bootstrapTable('removeAll');
    });
    // 初始化配合比列表
    TrwdSgpbDlg.initPhbDownList();
    TrwdSgpbDlg.getRecentRwdList();
    TrwdSgpbDlg.initTemplateInfoDownList();
    TrwdSgpbDlg.getSjpbxx($("#Frwdh").val(), $("#Fscbt").val());

    $("#Frwdh").keydown(function(event) {
        if (event.keyCode == "13") {
            TrwdSgpbDlg.getAll();
        }
    });
    // 修改用量
    $("#sysl").blur(function () {
        var sysl = $(this).val();
        var ysysl = $("#ysysl").val();
        var fid = $("#fid").val();
        if(fid == null || fid == undefined || fid == '') {
            return false;
        }
        if(!TrwdSgpbDlg.adjusting) {
            return false;
        }
        if(!isNumeric(sysl)) {
            Feng.error("请输入数值");
            $(this).val(ysysl);
            return false;
        }
        //console.log(fid);
        for(var o in TrwdSgpbDlg.currentSjpbxxList) {
            if(TrwdSgpbDlg.currentSjpbxxList[o].fid == fid) {
                TrwdSgpbDlg.currentSjpbxxList[o].fsysl = sysl;
                break;
            }
        }
        $("#"+TrwdSgpbDlg.id1).bootstrapTable('load', TrwdSgpbDlg.currentSjpbxxList);
        $("#"+TrwdSgpbDlg.id1).bootstrapTable('scrollTo', TrwdSgpbDlg.tableScrollPosition1);
        TrwdSgpbDlg.calculate(TrwdSgpbDlg.currentSjpbxxList);
        $("#ysysl").val(sysl);
        return false;
    }).focus(function () {
        //防止重新加载数据的抖动
        TrwdSgpbDlg.tableScrollPosition1 = $("#" + TrwdSgpbDlg.id1).bootstrapTable('getScrollPosition');
    });
    //防止重新加载数据的抖动
    $(".cl").mouseover(function () {
        TrwdSgpbDlg.tableScrollPosition1 = $("#" + TrwdSgpbDlg.id1).bootstrapTable('getScrollPosition');
    });
    //防止重新加载数据的抖动
    $(".sl").mouseover(function () {
        TrwdSgpbDlg.tableScrollPosition1 = $("#" + TrwdSgpbDlg.id1).bootstrapTable('getScrollPosition');
    });
    // 选择品种
    $("#Fpz").click(function () {
        TrwdSgpbDlg.tableScrollPosition1 = $("#" + TrwdSgpbDlg.id1).bootstrapTable('getScrollPosition');
    }).
    blur(function () {
        var pz = $("#Fpz").val();
        var fid = $("#fid").val();
        if(pz == null || pz == undefined || pz == '') {
            return false;
        }
        if(fid == null || fid == undefined || fid == '') {
            return false;
        }
        if(!TrwdSgpbDlg.adjusting) {
            return false;
        }
        for(var o in TrwdSgpbDlg.currentSjpbxxList) {
            if(TrwdSgpbDlg.currentSjpbxxList[o].fid == fid) {
                TrwdSgpbDlg.currentSjpbxxList[o].fpzgg = pz;
                break;
            }
        }
        $("#"+TrwdSgpbDlg.id1).bootstrapTable('load', TrwdSgpbDlg.currentSjpbxxList);
        $("#"+TrwdSgpbDlg.id1).bootstrapTable('scrollTo', TrwdSgpbDlg.tableScrollPosition1);
        return false;
    });

    // 修改用量
    $("#frzbl").blur(function () {
        var rzbl = $(this).val();
        if(rzbl == null || rzbl == undefined || rzbl == '') {
            return false;
        }
        if(rzbl < 0.99 || rzbl > 1.05) {
            $(this).val(1);
            return false;
        }
        if(!TrwdSgpbDlg.adjusting) {
            return false;
        }
        if(!isNumeric(rzbl)) {
            Feng.error("请输入数值");
            $(this).val(1);
            return false;
        }
        // setRwdSjpbxx只能用currentSjpbxxList，不能使用tableSjpbxxListLog1，否则会被修改到！！！
        TrwdSgpbDlg.currentSjpbxxList = JSON.parse(JSON.stringify(TrwdSgpbDlg.tableSjpbxxListLog1));
        var oldRz = Number($("#oldRz").val());
        var lastRz = (oldRz * rzbl).toFixed(2);
        var tempRz = 0;
        TrwdSgpbDlg.adjustRz = true;
        for(var o in TrwdSgpbDlg.currentSjpbxxList) {
            if("外加剂" == TrwdSgpbDlg.currentSjpbxxList[o].fylmc) {
                TrwdSgpbDlg.currentSjpbxxList[o].fsysl = TrwdSgpbDlg.currentSjpbxxList[o].fsysl * rzbl;
                TrwdSgpbDlg.currentSjpbxxList[o].fsysl = TrwdSgpbDlg.currentSjpbxxList[o].fsysl.toFixed(2);
            } else {
                TrwdSgpbDlg.currentSjpbxxList[o].fsysl = TrwdSgpbDlg.currentSjpbxxList[o].fsysl * rzbl;
                TrwdSgpbDlg.currentSjpbxxList[o].fsysl = TrwdSgpbDlg.currentSjpbxxList[o].fsysl.toFixed(0);
            }
            tempRz = Number(tempRz) + Number(TrwdSgpbDlg.currentSjpbxxList[o].fsysl);
        }
        tempRz = tempRz.toFixed(2);
        console.log('tempRz', tempRz);
        console.log('lastRz', lastRz);
        if(Number(tempRz) < Number(lastRz)) {
            for(var o in TrwdSgpbDlg.currentSjpbxxList) {
                if(TrwdSgpbDlg.currentSjpbxxList[o].fylmc.indexOf("砂") != -1) {
                    TrwdSgpbDlg.currentSjpbxxList[o].fsysl = Math.floor(Number(TrwdSgpbDlg.currentSjpbxxList[o].fsysl) + Number(lastRz) - Number(tempRz));
                    break;
                }
            }
        } else {
            for(var o in TrwdSgpbDlg.currentSjpbxxList) {
                if(TrwdSgpbDlg.currentSjpbxxList[o].fylmc.indexOf("砂") != -1) {
                    TrwdSgpbDlg.currentSjpbxxList[o].fsysl = Math.ceil(Number(TrwdSgpbDlg.currentSjpbxxList[o].fsysl) - Number(lastRz) + Number(tempRz));
                    break;
                }
            }
        }
        $("#"+TrwdSgpbDlg.id1).bootstrapTable('load', TrwdSgpbDlg.currentSjpbxxList);
        $("#"+TrwdSgpbDlg.id1).bootstrapTable('scrollTo', TrwdSgpbDlg.tableScrollPosition1);
        TrwdSgpbDlg.calculate(TrwdSgpbDlg.currentSjpbxxList);
        return false;
    }).focus(function () {
        //防止重新加载数据的抖动
        TrwdSgpbDlg.tableScrollPosition1 = $("#" + TrwdSgpbDlg.id1).bootstrapTable('getScrollPosition');
    });
    //加砂石
    $("#addSandStone").click(function () {
        var sysl = $("#sysl").val();
        $("#sysl").val(Number(sysl) + 5).focus().blur();
        console.log("sysl", $("#sysl").val());
    });
    //减砂石
    $("#minusSandStone").click(function () {
        var sysl = $("#sysl").val();
        if(sysl > 5) {
            $("#sysl").val(Number(sysl) - 5).focus().blur();
        }
        console.log("sysl", $("#sysl").val());
    });
});