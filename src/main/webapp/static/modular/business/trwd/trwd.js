/**
 * 任务单管理初始化
 */
var Trwd = {
    id: "TrwdTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    selectCheckbox: []
};

/**
 * 初始化表格的列
 */
Trwd.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true, width: 30},
        {title: '单号', field: 'Frwdh', visible: true, align: 'center', valign: 'middle', width: 80},
        {title: '计划日期', field: 'Fjhrq', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '施工单位', field: 'Fhtdw', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '工程名称', field: 'Fgcmc', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '施工部位', field: 'Fjzbw', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '浇筑方式', field: 'Fjzfs', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '砼品种', field: 'Ftpz', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '生产拌台', field: 'Fscbt', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '状态', field: 'Fzt', visible: true, align: 'center', valign: 'middle', width: 80},
        {title: '计划方量', field: 'Fjhsl', visible: false, align: 'center', valign: 'middle', width: 100},
        {title: '完成方量', field: 'Fwcsl', visible: false, align: 'center', valign: 'middle', width: 100},
        {title: '任务性质', field: 'Frwxz', visible: false, align: 'center', valign: 'middle', width: 100},
        {title: '任务单', field: 'Frwno', visible: false, align: 'center', valign: 'middle', width: 100}
    ];
};

/**
 * 检查是否选中
 */
Trwd.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Trwd.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加任务单
 */
Trwd.openAddTrwd = function () {
    var index = layer.open({
        type: 2,
        title: '添加任务单',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/trwd/trwd_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看任务单详情
 */
Trwd.openTrwdDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '任务单详情',
            area: ['800px', '380px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/trwd/trwd_update/' + Trwd.seItem.Frwdh
        });
        layer.full(index);
        this.layerIndex = index;
    }
};

Trwd.openTrwdSgpb = function () {
    if (this.check()) {
        if(this.layerIndex != null) layer.close(this.layerIndex);
        var index = parent.layer.open({
            type: 2,
            title: '核对配方',
            area: ['800px', '420px'], //宽高
            fix: true, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/trwd/trwd_sgpb/' + Trwd.seItem.Frwdh
        });
        parent.layer.full(index);
        this.layerIndex = index;
    }
    //MyCrontab.menuItem($("#sgpb"));
};
//全线调掺量
Trwd.batchModifyCl = function () {
    var cl = $("#cl").val();
    console.log("cl:", cl);
    if(isNumeric(cl) == false) {
        Feng.error("掺量需要填入数值");
        return false;
    }
    if(cl != '0.05' && cl != '0.1' && cl != '0.2' && cl != '-0.05' && cl != '-0.1' && cl != '-0.2') {
        Feng.error("掺量请填入规定数值（0.05、0.1、0.2、-0.05、-0.1、-0.2）");
        return false;
    }
    var operation = function(){
        $("#qxtcl").addClass("disabled");
        var ajax = new $ax(Feng.ctxPath + "/trwd/batch_modify_cl", function (data) {
            console.log(data);
            if(data.code != 200) {
                Feng.error("批量调整掺量失败!" + data.message + "!");
            } else {
                Feng.success("批量调整掺量成功(水单、砂浆单、非正在生产不参与调整)!");
            }
            $("#qxtcl").toggleClass("disabled");
        }, function (data) {
            $("#qxtcl").toggleClass("disabled");
            Feng.error("批量调整掺量失败!" + data.message + "!");
        });
        ajax.set("scbt", $("#scbt").val());
        ajax.set("cl", cl);
        ajax.start();
    };
    Feng.confirm("是否为指定生产线批量调整掺量?", operation);
};
//选单调掺量
Trwd.chooseToModifyCl = function () {
    var cl = $("#cl").val();
    console.log("cl:", cl);
    if(Trwd.selectCheckbox.length <= 0) {
        Feng.error("请选择任务单");
        return false;
    }
    if(isNumeric(cl) == false) {
        Feng.error("掺量需要填入数值");
        return false;
    }
    if(cl != '0.05' && cl != '0.1' && cl != '0.2' && cl != '-0.05' && cl != '-0.1' && cl != '-0.2') {
        Feng.error("掺量请填入规定数值（0.05、0.1、0.2、-0.05、-0.1、-0.2）");
        return false;
    }
    var operation = function(){
        $("#xdtcl").addClass("disabled");
        var ajax = new $ax(Feng.ctxPath + "/trwd/choose_to_modify_cl", function (data) {
            console.log(data);
            if(data.code != 200) {
                Feng.error("批量调整掺量失败!" + data.message + "!");
            } else {
                //Trwd.selectCheckbox = [];
                //$('#' + this.id).bootstrapTable('checkAll');
                //$('#' + this.id).bootstrapTable('uncheckAll');
                Feng.success("批量调整掺量成功(水单、砂浆单、非正在生产不参与调整)!");
            }
            $("#xdtcl").toggleClass("disabled");
        }, function (data) {
            $("#xdtcl").toggleClass("disabled");
            Feng.error("批量调整掺量失败!" + data.message + "!");
        });
        ajax.set("rwdhs", Trwd.selectCheckbox);
        ajax.set("scbt", $("#scbt").val());
        ajax.set("cl", cl);
        ajax.start();
    };
    Feng.confirm("是否为所有选择的任务单调整掺量?", operation);
};
//批量调砂率
Trwd.chooseToModifySl = function () {
    var sl = $("#sl").val();
    console.log("sl:", sl);
    if(Trwd.selectCheckbox.length <= 0) {
        Feng.error("请选择任务单");
        return false;
    }
    if(isNumeric(sl) == false) {
        Feng.error("砂率需要填入数值");
        return false;
    }
    if(sl != '1' && sl != '2' && sl != '-1' && sl != '-2') {
        Feng.error("砂率请填入规定数值（1、2、-1、-2）");
        return false;
    }
    var operation = function(){
        $("#xdtsl").addClass("disabled");
        var ajax = new $ax(Feng.ctxPath + "/trwd/choose_to_modify_sl", function (data) {
            console.log(data);
            if(data.code != 200) {
                Feng.error("批量调整砂率失败!" + data.message + "!");
            } else {
                //Trwd.selectCheckbox = [];
                //$('#' + this.id).bootstrapTable('uncheckAll');
                Feng.success("批量调整砂率成功(水单、砂浆单、非正在生产不参与调整)!");
            }
            $("#xdtsl").toggleClass("disabled");
        }, function (data) {
            Feng.error("批量调整砂率失败!" + data.message + "!");
            $("#xdtsl").toggleClass("disabled");
        });
        ajax.set("rwdhs", Trwd.selectCheckbox);
        ajax.set("scbt", $("#scbt").val());
        ajax.set("sl", sl);
        ajax.start();
    };
    Feng.confirm("是否为所有选择的任务单调整砂率?", operation);
};
//选单选线调容重
Trwd.chooseToModifyRzForaScbt = function () {
    var rzbl = $("#rzbl").val();
    console.log("rzbl:", rzbl);
    if(Trwd.selectCheckbox.length <= 0) {
        Feng.error("请选择任务单");
        return false;
    }
    if(isNumeric(rzbl) == false) {
        Feng.error("容重比例需要填入数值");
        return false;
    }
    if(Number(rzbl) < 0.99 || Number(rzbl) > 1.05) {
        Feng.error("容重比例必须在0.99与1.05之间");
        return false;
    }
    var operation = function(){
        $("#xdxxtrz").addClass("disabled");
        var ajax = new $ax(Feng.ctxPath + "/trwd/choose_to_modify_rz_one_scbt", function (data) {
            console.log(data);
            if(data.code != 200) {
                Feng.error("批量调整容重失败!" + data.message + "!");
            } else {
                Feng.success("批量调整容重成功(水单、砂浆单、非正在生产不参与调整)!");
            }
            $("#xdxxtrz").toggleClass("disabled");
        }, function (data) {
            Feng.error("批量调整容重失败!" + data.message + "!");
            $("#xdxxtrz").toggleClass("disabled");
        });
        ajax.set("rwdhs", Trwd.selectCheckbox);
        ajax.set("scbt", $("#scbt").val());
        ajax.set("rzbl", rzbl);
        ajax.start();
    };
    Feng.confirm("是否为所有选择的任务单调整容重?", operation);
};
//选单选线容重回退
Trwd.chooseToRzBackForaScbt = function () {
    if(Trwd.selectCheckbox.length <= 0) {
        Feng.error("请选择任务单");
        return false;
    }
    var operation = function(){
        $("#xdxxrzht").addClass("disabled");
        var ajax = new $ax(Feng.ctxPath + "/trwd/choose_to_rz_back_one_scbt", function (data) {
            console.log(data);
            if(data.code != 200) {
                Feng.error("批量容重回退失败!" + data.message + "!");
            } else {
                Feng.success("批量容重回退成功!");
            }
            $("#xdxxrzht").toggleClass("disabled");
        }, function (data) {
            Feng.error("批量调整容重失败!" + data.message + "!");
            $("#xdxxrzht").toggleClass("disabled");
        });
        ajax.set("rwdhs", Trwd.selectCheckbox);
        ajax.set("scbt", $("#scbt").val());
        ajax.start();
    };
    Feng.confirm("是否为所有选择的任务单回退容重?", operation);
};

//批量调容重
Trwd.chooseToModifyRzForAllScbt = function () {
    var rzbl = $("#rzbl").val();
    console.log("rzbl:", rzbl);
    if(Trwd.selectCheckbox.length <= 0) {
        Feng.error("请选择任务单");
        return false;
    }
    if(isNumeric(rzbl) == false) {
        Feng.error("容重比例需要填入数值");
        return false;
    }
    if(Number(rzbl) < 0.99 || Number(rzbl) > 1.05) {
        Feng.error("容重比例必须在0.99与1.05之间");
        return false;
    }
    var operation = function(){
        $("#xdtrz").addClass("disabled");
        var ajax = new $ax(Feng.ctxPath + "/trwd/choose_to_modify_rz_all_scbt", function (data) {
            console.log(data);
            if(data.code != 200) {
                Feng.error("批量调整容重失败!" + data.message + "!");
            } else {
                Feng.success("批量调整容重成功(水单、砂浆单、非正在生产不参与调整)!");
            }
            $("#xdtrz").toggleClass("disabled");
        }, function (data) {
            Feng.error("批量调整容重失败!" + data.message + "!");
            $("#xdtrz").toggleClass("disabled");
        });
        ajax.set("rwdhs", Trwd.selectCheckbox);
        ajax.set("rzbl", rzbl);
        ajax.start();
    };
    Feng.confirm("是否为所有选择的任务单调整容重?", operation);
};
//批量容重回退
Trwd.chooseToRzBackForAllScbt = function () {
    if(Trwd.selectCheckbox.length <= 0) {
        Feng.error("请选择任务单");
        return false;
    }
    var operation = function(){
        $("#xdrzht").addClass("disabled");
        var ajax = new $ax(Feng.ctxPath + "/trwd/choose_to_rz_back_all_scbt", function (data) {
            console.log(data);
            if(data.code != 200) {
                Feng.error("批量容重回退失败!" + data.message + "!");
            } else {
                Feng.success("批量容重回退成功!");
            }
            $("#xdrzht").toggleClass("disabled");
        }, function (data) {
            Feng.error("批量调整容重失败!" + data.message + "!");
            $("#xdrzht").toggleClass("disabled");
        });
        ajax.set("rwdhs", Trwd.selectCheckbox);
        ajax.start();
    };
    Feng.confirm("是否为所有选择的任务单回退容重?", operation);
};

//选单调砂
Trwd.chooseToModifySand = function () {
    var ss = $("#ss").val();
    var sandClass = $("#sandClass").val();
    console.log("ss:", ss);
    if(Trwd.selectCheckbox.length <= 0) {
        Feng.error("请选择任务单");
        return false;
    }
    if(!sandClass) {
        Feng.error("请选择砂品种");
        return false;
    }
    if(isNumeric(ss) == false) {
        Feng.error("砂石数值需要填入数值");
        return false;
    }
    var operation = function(){
        $("#xdtsa").addClass("disabled");
        var ajax = new $ax(Feng.ctxPath + "/trwd/choose_to_modify_sand", function (data) {
            console.log(data);
            if(data.code != 200) {
                Feng.error("选单调砂失败!" + data.message + "!");
            } else {
                Feng.success("选单调砂成功(水单、砂浆单、非正在生产不参与调整)!");
            }
            $("#xdtsa").toggleClass("disabled");
        }, function (data) {
            Feng.error("选单调砂失败!" + data.message + "!");
            $("#xdtsa").toggleClass("disabled");
        });
        ajax.set("rwdhs", Trwd.selectCheckbox);
        ajax.set("scbt", $("#scbt").val());
        ajax.set("sand", ss);
        ajax.set("sandClass", sandClass);
        ajax.start();
    };
    Feng.confirm("是否为所有选择的任务单调砂?", operation);
};
//选单调石
Trwd.chooseToModifyStone = function () {
    var ss = $("#ss").val();
    var stoneClass = $("#stoneClass").val();
    console.log("ss:", ss);
    if(Trwd.selectCheckbox.length <= 0) {
        Feng.error("请选择任务单");
        return false;
    }
    if(!stoneClass) {
        Feng.error("请选择碎石品种");
        return false;
    }
    if(isNumeric(ss) == false) {
        Feng.error("砂石数值需要填入数值");
        return false;
    }
    var operation = function(){
        $("#xdtst").addClass("disabled");
        var ajax = new $ax(Feng.ctxPath + "/trwd/choose_to_modify_stone", function (data) {
            console.log(data);
            if(data.code != 200) {
                Feng.error("选单调石失败!" + data.message + "!");
            } else {
                Feng.success("选单调石成功(水单、砂浆单、非正在生产不参与调整)!");
            }
            $("#xdtst").toggleClass("disabled");
        }, function (data) {
            Feng.error("选单调石失败!" + data.message + "!");
            $("#xdtst").toggleClass("disabled");
        });
        ajax.set("rwdhs", Trwd.selectCheckbox);
        ajax.set("scbt", $("#scbt").val());
        ajax.set("stone", ss);
        ajax.set("stoneClass", stoneClass);
        ajax.start();
    };
    Feng.confirm("是否为所有选择的任务单调石?", operation);
};
/**
 * 删除任务单
 */
Trwd.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/trwd/delete", function (data) {
            Feng.success("删除成功!");
            Trwd.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("trwdId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询任务单列表
 */
Trwd.search = function () {
    var queryData = {};
    queryData['gcmc'] = $("#gcmc").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['rwdh'] = $("#rwdh").val();
    queryData['scbt'] = $("#scbt").val();
    queryData['zt'] = $("#zt").val();
    Trwd.table.refresh({query: queryData});
    Trwd.selectCheckbox = [];
};

$(function () {
    var defaultColunms = Trwd.initColumn();
    var table = new BSTable(Trwd.id, "/trwd/list", defaultColunms);
    table.setPaginationType("client");
    table.setQueryParams({
        scbt : $("#scbt").val(),
        zt: $("#zt").val()
    })
    Trwd.table = table.init(function (r, e) {
        Trwd.selectCheckbox.push(r.Frwdh);
    }, function (r, e) {
        Trwd.selectCheckbox = Trwd.selectCheckbox.filter(function (item) {
            return item != r.Frwdh
        });
    }, function (rs) {
        for(var o in rs) {
            Trwd.selectCheckbox.push(rs[o].Frwdh);
        }
    }, function (rs) {
        for(var o in rs) {
            Trwd.selectCheckbox = Trwd.selectCheckbox.filter(function (item) {
                return item != rs[o].Frwdh
            });
        }
    });
    $("#sandClass").html(initDictToSelect("sand_class"));
    $("#sandClass").val("砂1");
    $("#stoneClass").html(initDictToSelect("stone_class"));
    $("#stoneClass").val("1-2");
});