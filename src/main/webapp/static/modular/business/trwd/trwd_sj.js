/**
 * 任务单管理初始化
 */
var Trwd = {
    id: "TrwdTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    selectCheckbox: []
};

/**
 * 初始化表格的列
 */
Trwd.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true, width: 30},
        {title: '单号', field: 'Frwdh', visible: true, align: 'center', valign: 'middle', width: 80},
        {title: '计划日期', field: 'Fjhrq', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '施工单位', field: 'Fhtdw', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '工程名称', field: 'Fgcmc', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '施工部位', field: 'Fjzbw', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '浇筑方式', field: 'Fjzfs', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '砼品种', field: 'Ftpz', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '生产拌台', field: 'Fscbt', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '状态', field: 'Fzt', visible: true, align: 'center', valign: 'middle', width: 80},
        {title: '计划方量', field: 'Fjhsl', visible: false, align: 'center', valign: 'middle', width: 100},
        {title: '完成方量', field: 'Fwcsl', visible: false, align: 'center', valign: 'middle', width: 100},
        {title: '任务性质', field: 'Frwxz', visible: false, align: 'center', valign: 'middle', width: 100},
        {title: '任务单', field: 'Frwno', visible: false, align: 'center', valign: 'middle', width: 100}
    ];
};

/**
 * 检查是否选中
 */
Trwd.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Trwd.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加任务单
 */
Trwd.openAddTrwd = function () {
    var index = layer.open({
        type: 2,
        title: '添加任务单',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/trwd/trwd_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看任务单详情
 */
Trwd.openTrwdDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '任务单详情',
            area: ['800px', '380px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/trwd/trwd_update/' + Trwd.seItem.Frwdh
        });
        layer.full(index);
        this.layerIndex = index;
    }
};

Trwd.openTrwdSgpb = function () {
    if (this.check()) {
        if(this.layerIndex != null) layer.close(this.layerIndex);
        var index = parent.layer.open({
            type: 2,
            title: '核对配方',
            area: ['800px', '420px'], //宽高
            fix: true, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/trwd/trwd_sgpb/' + Trwd.seItem.Frwdh
        });
        parent.layer.full(index);
        this.layerIndex = index;
    }
    //MyCrontab.menuItem($("#sgpb"));
};
//全线调掺量
Trwd.batchModifyCl = function () {
    var cl = $("#cl").val();
    console.log("cl:", cl);
    if(isNumeric(cl) == false) {
        Feng.error("掺量需要填入数值");
        return false;
    }
    if(cl != '0.05' && cl != '0.1' && cl != '0.2' && cl != '-0.05' && cl != '-0.1' && cl != '-0.2') {
        Feng.error("掺量请填入规定数值（0.05、0.1、0.2、-0.05、-0.1、-0.2）");
        return false;
    }
    var operation = function(){
        $("#qxtcl").addClass("disabled");
        var ajax = new $ax(Feng.ctxPath + "/trwd/batch_modify_cl", function (data) {
            console.log(data);
            if(data.code != 200) {
                Feng.error("批量调整掺量失败!" + data.message + "!");
            } else {
                Feng.success("批量调整掺量成功(水单、砂浆单、非正在生产不参与调整)!");
            }
            $("#qxtcl").toggleClass("disabled");
        }, function (data) {
            $("#qxtcl").toggleClass("disabled");
            Feng.error("批量调整掺量失败!" + data.message + "!");
        });
        ajax.set("scbt", $("#scbt").val());
        ajax.set("cl", cl);
        ajax.start();
    };
    Feng.confirm("是否为指定生产线批量调整掺量?", operation);
};
//选单调掺量
Trwd.chooseToModifyCl = function () {
    var cl = $("#cl").val();
    console.log("cl:", cl);
    if(Trwd.selectCheckbox.length <= 0) {
        Feng.error("请选择任务单");
        return false;
    }
    if(isNumeric(cl) == false) {
        Feng.error("掺量需要填入数值");
        return false;
    }
    if(cl != '0.05' && cl != '0.1' && cl != '0.2' && cl != '-0.05' && cl != '-0.1' && cl != '-0.2') {
        Feng.error("掺量请填入规定数值（0.05、0.1、0.2、-0.05、-0.1、-0.2）");
        return false;
    }
    var operation = function(){
        $("#xdtcl").addClass("disabled");
        var ajax = new $ax(Feng.ctxPath + "/trwd/choose_to_modify_cl", function (data) {
            console.log(data);
            if(data.code != 200) {
                Feng.error("批量调整掺量失败!" + data.message + "!");
            } else {
                //Trwd.selectCheckbox = [];
                //$('#' + this.id).bootstrapTable('checkAll');
                //$('#' + this.id).bootstrapTable('uncheckAll');
                Feng.success("批量调整掺量成功(水单、砂浆单、非正在生产不参与调整)!");
            }
            $("#xdtcl").toggleClass("disabled");
        }, function (data) {
            $("#xdtcl").toggleClass("disabled");
            Feng.error("批量调整掺量失败!" + data.message + "!");
        });
        ajax.set("rwdhs", Trwd.selectCheckbox);
        ajax.set("scbt", $("#scbt").val());
        ajax.set("cl", cl);
        ajax.start();
    };
    Feng.confirm("是否为所有选择的任务单调整掺量?", operation);
};
//批量调砂率
Trwd.chooseToModifySl = function () {
    var sl = $("#sl").val();
    console.log("sl:", sl);
    if(Trwd.selectCheckbox.length <= 0) {
        Feng.error("请选择任务单");
        return false;
    }
    if(isNumeric(sl) == false) {
        Feng.error("砂率需要填入数值");
        return false;
    }
    if(sl != '1' && sl != '2' && sl != '-1' && sl != '-2') {
        Feng.error("砂率请填入规定数值（1、2、-1、-2）");
        return false;
    }
    var operation = function(){
        $("#xdtsl").addClass("disabled");
        var ajax = new $ax(Feng.ctxPath + "/trwd/choose_to_modify_sl", function (data) {
            console.log(data);
            if(data.code != 200) {
                Feng.error("批量调整砂率失败!" + data.message + "!");
            } else {
                //Trwd.selectCheckbox = [];
                //$('#' + this.id).bootstrapTable('uncheckAll');
                Feng.success("批量调整砂率成功(水单、砂浆单、非正在生产不参与调整)!");
            }
            $("#xdtsl").toggleClass("disabled");
        }, function (data) {
            Feng.error("批量调整砂率失败!" + data.message + "!");
            $("#xdtsl").toggleClass("disabled");
        });
        ajax.set("rwdhs", Trwd.selectCheckbox);
        ajax.set("scbt", $("#scbt").val());
        ajax.set("sl", sl);
        ajax.start();
    };
    Feng.confirm("是否为所有选择的任务单调整砂率?", operation);
};

/**
 * 删除任务单
 */
Trwd.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/trwd/delete", function (data) {
            Feng.success("删除成功!");
            Trwd.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("trwdId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询任务单列表
 */
Trwd.search = function () {
    var queryData = {};
    queryData['gcmc'] = $("#gcmc").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['rwdh'] = $("#rwdh").val();
    queryData['scbt'] = $("#scbt").val();
    queryData['zt'] = $("#zt").val();
    Trwd.table.refresh({query: queryData});
    Trwd.selectCheckbox = [];
};

$(function () {
    var defaultColunms = Trwd.initColumn();
    var table = new BSTable(Trwd.id, "/trwd/list", defaultColunms);
    table.setPaginationType("client");
    table.setQueryParams({
        scbt : $("#scbt").val(),
        zt: $("#zt").val()
    })
    Trwd.table = table.init(function (r, e) {
        Trwd.selectCheckbox.push(r.Frwdh);
    }, function (r, e) {
        Trwd.selectCheckbox = Trwd.selectCheckbox.filter(function (item) {
            return item != r.Frwdh
        });
    }, function (rs) {
        for(var o in rs) {
            Trwd.selectCheckbox.push(rs[o].Frwdh);
        }
    }, function (rs) {
        for(var o in rs) {
            Trwd.selectCheckbox = Trwd.selectCheckbox.filter(function (item) {
                return item != rs[o].Frwdh
            });
        }
    });
});