/**
 * 任务单管理初始化
 */
var Trwd = {
    id: "TrwdTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    selectCheckbox: []
};

/**
 * 初始化表格的列
 */
Trwd.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true, width: 30},
        {title: '单号', field: 'Frwdh', visible: true, align: 'center', valign: 'middle', width: 80},
        {title: '计划日期', field: 'Fjhrq', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '施工单位', field: 'Fhtdw', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '工程名称', field: 'Fgcmc', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '施工部位', field: 'Fjzbw', visible: true, align: 'center', valign: 'middle', width: 120},
        {title: '浇筑方式', field: 'Fjzfs', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '砼品种', field: 'Ftpz', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '生产拌台', field: 'Fscbt', visible: true, align: 'center', valign: 'middle', width: 100},
        {title: '状态', field: 'Fzt', visible: true, align: 'center', valign: 'middle', width: 80},
        {title: '计划方量', field: 'Fjhsl', visible: false, align: 'center', valign: 'middle', width: 100},
        {title: '完成方量', field: 'Fwcsl', visible: false, align: 'center', valign: 'middle', width: 100},
        {title: '任务性质', field: 'Frwxz', visible: false, align: 'center', valign: 'middle', width: 100},
        {title: '任务单', field: 'Frwno', visible: false, align: 'center', valign: 'middle', width: 100}
    ];
};

/**
 * 检查是否选中
 */
Trwd.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Trwd.seItem = selected[0];
        return true;
    }
};

/**
 * 打开查看任务单详情
 */
Trwd.openTrwdDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '任务单详情',
            area: ['800px', '380px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/trwd/trwd_update/' + Trwd.seItem.Frwdh
        });
        layer.full(index);
        this.layerIndex = index;
    }
};

/**
 * 查询任务单列表
 */
Trwd.search = function () {
    var queryData = {};
    queryData['gcmc'] = $("#gcmc").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['rwdh'] = $("#rwdh").val();
    queryData['scbt'] = $("#scbt").val();
    queryData['zt'] = $("#zt").val();
    Trwd.table.refresh({query: queryData});
    Trwd.selectCheckbox = [];
};

/**
 * 导出对外任务单
 */
Trwd.exportTrwdZlMsgOut = function () {
    var operation = function(){
        var url = "/trwd/download_out" + "?jstime=" + new Date().getTime();
        if($("#gcmc").val() != '') url = url + "&gcmc=" + $("#gcmc").val();
        if($("#beginTime").val() != '') url = url + "&beginTime=" + $("#beginTime").val();
        if($("#endTime").val() != '') url = url + "&endTime=" + $("#endTime").val();
        if($("#rwdh").val() != '') url = url + "&rwdh=" + $("#rwdh").val();
        if($("#zt").val() != '') url = url + "&zt=" + $("#zt").val();
        if($("#scbt").val() != '') url = url + "&scbt=" + $("#scbt").val();
        url = url + "&excelType=03";
        $news.download(url);
    };

    $news.confirm("是否导出查询到的所有数据?", operation);
};

$(function () {
    $("#beginTime").val(new Date().format("yyyy-MM-dd"));
    $("#endTime").val(new Date().format("yyyy-MM-dd"));
    var defaultColunms = Trwd.initColumn();
    var table = new BSTable(Trwd.id, "/trwd/list", defaultColunms);
    table.setPaginationType("client");
    table.setQueryParams({
        scbt : $("#scbt").val(),
        zt: $("#zt").val(),
        beginTime:$("#beginTime").val(),
        endTime:$("#endTime").val()
    })
    Trwd.table = table.init(function (r, e) {
        Trwd.selectCheckbox.push(r.Frwdh);
    }, function (r, e) {
        Trwd.selectCheckbox = Trwd.selectCheckbox.filter(function (item) {
            return item != r.Frwdh
        });
    }, function (rs) {
        for(var o in rs) {
            Trwd.selectCheckbox.push(rs[o].Frwdh);
        }
    }, function (rs) {
        for(var o in rs) {
            Trwd.selectCheckbox = Trwd.selectCheckbox.filter(function (item) {
                return item != rs[o].Frwdh
            });
        }
    });

});