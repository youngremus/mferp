var icon_value = "";

/**
 * 菜单详情对话框
 */
var MenuInfoDlg = {
    menuInfoData: {},
    ztreeInstance: null,
    validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '菜单名称不能为空'
                }
            }
        },
        code: {
            validators: {
                notEmpty: {
                    message: '菜单编号不能为空'
                }
            }
        },
        pcodeName: {
            validators: {
                notEmpty: {
                    message: '父菜单不能为空'
                }
            }
        },
        url: {
            validators: {
                notEmpty: {
                    message: '请求地址不能为空'
                }
            }
        },
        num: {
            validators: {
                notEmpty: {
                    message: '序号不能为空'
                }
            }
        }
    }
};

/**
 * 清除数据
 */
MenuInfoDlg.clearData = function () {
    this.menuInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
MenuInfoDlg.set = function (key, value) {
    this.menuInfoData[key] = (typeof value == "undefined") ? $("#" + key).val() : value;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
MenuInfoDlg.get = function (key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
MenuInfoDlg.close = function () {
    parent.layer.close(window.parent.Menu.layerIndex);
}

/**
 * 收集数据
 */
MenuInfoDlg.collectData = function () {
    this.set('id').set('name').set('code').set('pcode').set('url').set('num').set('levels').set('icon').set("ismenu");
}

/**
 * 验证数据是否为空
 */
MenuInfoDlg.validate = function () {
    $('#menuInfoForm').data("bootstrapValidator").resetForm();
    $('#menuInfoForm').bootstrapValidator('validate');
    return $("#menuInfoForm").data('bootstrapValidator').isValid();
}

/**
 * 提交添加用户
 */
MenuInfoDlg.addSubmit = function () {

    this.clearData();
    this.collectData();

    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/menu/add", function (data) {
        Feng.success("添加成功!");
        window.parent.Menu.table.refresh();
        MenuInfoDlg.close();
    }, function (data) {
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.menuInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
MenuInfoDlg.editSubmit = function () {

    this.clearData();
    this.collectData();

    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/menu/edit", function (data) {
        Feng.success("修改成功!");
        window.parent.Menu.table.refresh();
        MenuInfoDlg.close();
    }, function (data) {
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.menuInfoData);
    ajax.start();
}

/**
 * 点击父级编号input框时
 */
MenuInfoDlg.onClickDept = function (e, treeId, treeNode) {
    $("#pcodeName").attr("value", MenuInfoDlg.ztreeInstance.getSelectedVal());
    $("#pcode").attr("value", treeNode.id);
};


/**
 * 显示父级菜单选择的树
 */
MenuInfoDlg.showMenuSelectTree = function () {
    Feng.showInputTree("pcodeName", "pcodeTreeDiv", 15, 34);
};

/**
 * 打开选择菜单图标页面
 */
MenuInfoDlg.chooseMenuIcon = function () {
    var index = layer.open({
        type: 2,
        title: '选择菜单图标',
        area: ['830px', '500px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/menu/menu_icon',
        end: function () {
        }
    });
    this.layerIndex = index;
    // var url = Feng.ctxPath + '/menu/menu_icon';
    // openLayerUrl(url, 850, 550);
}


// function openLayerUrl(url, width, height) {
//     parent.layer.open({
//         type: 2,
//         title: false,
//         closeBtn: false,
//         shadeClose: false,
//         shade: 0.6,
//         border: [0],
//         area: [width <= 0 ? "auto" : width + 'px', height <= 0 ? "auto" : height + 'px'],
//         content: url,
//     })
// }

function selectText(t) {
    if (document.selection) {
        var range = document.body.createTextRange();
        range.moveToElementText(t);
        range.select();
    } else if (window.getSelection) {
        var range = document.createRange();
        range.selectNode(t);
        window.getSelection().addRange(range);
    }
}

$(function () {

    Feng.initValidator("menuInfoForm", MenuInfoDlg.validateFields);

    var ztree = new $ZTree("pcodeTree", "/menu/selectMenuTreeList");
    ztree.bindOnClick(MenuInfoDlg.onClickDept);
    ztree.init();
    MenuInfoDlg.ztreeInstance = ztree;

    //初始化是否是菜单
    if($("#ismenuValue").val() == undefined){
        $("#ismenu").val(0);
    }else{
        $("#ismenu").val($("#ismenuValue").val());
    }

    /**
     * 菜单导航栏选择事件
     */
    $("#menu_choose_list").on("click","li",function(){
        var obj = $("#"+$(this).attr("role")),
            st = obj.offset().top;
        jQuery('body,html').animate({
            scrollTop: st
        }, 1000);
        selectText(this);
    });


});

/**
 * 选中菜单图标事件
 */
$(".icons-list span").on("click",function(){
    icon_value = $(this).text();
    var index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);//关闭当前页
    $("#icon",window.parent.document).val(icon_value);
});
