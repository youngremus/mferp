$(function() {
    var hsize = $(window).width() / 1920 * 16;
    $("html").css({
        fontSize: hsize
    });
    var username_ = localStorage.getItem("username");
    if(username_) {
        $("input[name='username']").val(username_);
    }
    var password_ = localStorage.getItem("password");
    if(password_) {
        $("input[name='password']").val(password_);
    }
    var remember_ = localStorage.getItem("remember");
    if(remember_) {
        $("input[name='remember']").prop('checked', 'checked');
    }
    $("#login").click(function () {
        var username = $("input[name='username']").val();
        var password = $("input[name='password']").val();
        var remember = $("input[name='remember']").prop('checked');
        var param = {
            username: username,
            password: password,
            remember: remember
        };
        $news.postX("/login_me", param, function (data) {
            if (data.code !== 200) {
                $news.fail(data.message);
                return false;
            }
            if (remember) {
                localStorage.setItem("username", username);
                localStorage.setItem("password", password);
                localStorage.setItem("remember", remember);
            } else {
                localStorage.removeItem("username");
                localStorage.removeItem("password");
                localStorage.removeItem("remember");
            }
            window.location = ctxPath + "/";
        });
    });

    $('#psw').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            var username = $("input[name='username']").val();
            var password = $("input[name='password']").val();
            var remember = $("input[name='remember']").prop('checked');
            var param = {
                username: username,
                password: password,
                remember: remember
            };
            $news.postX("/login_me", param, function (data) {
                if (data.code !== 200) {
                    $news.fail(data.message);
                    return false;
                }
                if (remember) {
                    localStorage.setItem("username", username);
                    localStorage.setItem("password", password);
                    localStorage.setItem("remember", remember);
                } else {
                    localStorage.removeItem("username");
                    localStorage.removeItem("password");
                    localStorage.removeItem("remember");
                }
                window.location = ctxPath + "/";
            });
        }
    });

})