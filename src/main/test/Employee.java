import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {

    private int id;
    private String name;
    private int age;
    private double salary;

    public static List<Employee> getEmployees() {
        List<Employee> list = new ArrayList<>();
        list.add(new Employee(1001, "A", 12, 1000.00));
        list.add(new Employee(1002, "B", 13, 2000.00));
        list.add(new Employee(1003, "C", 14, 3000.00));
        list.add(new Employee(1004, "D", 15, 4000.00));
        list.add(new Employee(1005, "E", 16, 5000.00));
        return list;
    }
}
