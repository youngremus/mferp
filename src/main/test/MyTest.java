import cn.stylefeng.mferp.core.util.DateUtil;
import org.junit.Test;

import java.text.ParseException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

public class MyTest {

    @Test
    public void test20240331() throws ParseException {
        String s = "2024-3-1 3:01:0";
        s = DateUtil.parseDateTimeStr(s);
        System.out.println(s);
        String s1 = "2024-3-1 3:01:00";
        //String dateFormat = DateUtil.getDateFormat(s1, "yyyy-M-d H:m:s", "yyyy-MM-dd HH:mm:ss");
        //System.out.println(dateFormat);
        String[] split = s1.split(" ");
        String date = split[0];
        String time = split.length == 2 ? split[1] : "00:00:00";
        System.out.println(DateUtil.getDateFormat(date, DateUtil.DATE_PATTERN1, "M月d日"));
        System.out.println(DateUtil.getDateFormat(time, DateUtil.TIME_PATTERN1, "HH:mm"));
    }

    @Test
    public void test20240324() {
        System.out.println(DateUtil.getCurrDate("yyyy年M月份"));
    }

    @Test
    public void test20240310() {
        //long l = ThreadLocalRandom.current().nextLong(4000000000000000000L, 999999999999999999L);
        //System.out.println("l:" + l);
        String str = "15159539255||||||15159539255|||||";
        String[] split = str.split("\\|");
        System.out.println(split[0]);
    }

    @Test
    public void test1() {
        String strength = "C15砂浆";
        System.out.println(strength.substring(0, 3));
        System.out.println(strength.substring(0, 3).length());
    }

    @Test
    public void test20240123() {
        List<Employee> list = Employee.getEmployees();
        boolean b = list.stream().allMatch(employee -> employee.getAge() > 12);
        System.out.println(b);

        boolean b1 = list.stream().anyMatch(employee -> employee.getSalary() > 4000.00);
        System.out.println(b1);

        boolean e = list.stream().noneMatch(employee -> employee.getName().contains("F"));
        System.out.println(e);

        Optional<Employee> first = list.stream().findFirst();
        System.out.println(first);

        Optional<Employee> first1 = list.parallelStream().findFirst();
        System.out.println(first1);

        Optional<Employee> any = list.stream().findAny();
        System.out.println(any);

        Optional<Employee> any1 = list.parallelStream().findAny();
        System.out.println(any1);

        Comparator<String> comparator = (s1, s2) ->s1.compareTo(s2);
        System.out.println(comparator.compare("ab", "ab"));

        Comparator<String> comparator1 = String::compareTo;
        System.out.println(comparator1.compare("ab", "bcd"));

        Comparator<String> comparator2 = Comparator.naturalOrder();
        System.out.println(comparator2.compare("ab", "ef"));
    }
}
