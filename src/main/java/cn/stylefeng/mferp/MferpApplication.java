/**
 * Copyright 2018-2020 stylefeng & fengshuonan (https://gitee.com/stylefeng)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.stylefeng.mferp;

import cn.stylefeng.roses.core.config.MybaitsPlusAutoConfiguration;
import cn.stylefeng.roses.core.config.WebAutoConfiguration;
import com.bstek.ureport.console.UReportServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;

/**
 * SpringBoot方式启动类
 *
 * @author yzr
 * @Date 2020/5/17 19:20
 */
@SpringBootApplication(exclude = {WebAutoConfiguration.class, MybaitsPlusAutoConfiguration.class})
//@Import({YmlConfigUtil.class})//启动时就读取数据库链接信息
//@EnableTransactionManagement
//@ImportResource("classpath:context.xml")
public class MferpApplication {

    private final static Logger logger = LoggerFactory.getLogger(MferpApplication.class);

    public static void main(String[] args) {
        logger.info("MferpApplication 开始启动>>>>>>>>");
        SpringApplication.run(MferpApplication.class, args);
        logger.info("MferpApplication 启动成功!");
    }

    @Bean
    public ServletRegistrationBean buildUReportServlet() {
        return new ServletRegistrationBean(new UReportServlet(), "/ureport/*");
    }
}
