package cn.stylefeng.mferp.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * 同步数据库配置信息
 */
@Configuration
@ConfigurationProperties(prefix = "sqlserver")
@PropertySource(value = {"classpath:application-jdbc.properties",
        "file:${spring.profiles.path}/application-jdbc.properties"}, ignoreResourceNotFound = true)
public class SqlServerJdbcProperties extends JdbcBasis {
}
