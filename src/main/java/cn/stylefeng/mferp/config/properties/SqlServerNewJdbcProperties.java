package cn.stylefeng.mferp.config.properties;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 调度数据库配置信息
 */
@Data
@NoArgsConstructor
public class SqlServerNewJdbcProperties {

    private String url;
    private String username;
    private String password;
    private String driverClassName;
}
