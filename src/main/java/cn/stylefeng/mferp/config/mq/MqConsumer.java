package cn.stylefeng.mferp.config.mq;

import cn.stylefeng.mferp.config.RedisUtils;
import cn.stylefeng.mferp.modular.business.model.Trwd;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * activemq 消费者
 * @author: yzr
 * @date: 2020/5/31
 */
//@Component
//@PropertySource(value = {"file:${spring.profiles.path}/application-jdbc.properties",
//        "classpath:application-jdbc.properties"}, ignoreResourceNotFound = true)
public class MqConsumer {
    private static Logger logger = LoggerFactory.getLogger(MqConsumer.class);

    private static JdbcTemplate sqlServerJdbcTemplate = null;

    @Value("${sqlserver.url}")
    private String url;
    @Value("${sqlserver.username}")
    private String username;
    @Value("${sqlserver.password}")
    private String password;
    @Value("${sqlserver.driver-class-name}")
    private String driverClassName;

    @Resource
    private RedisUtils redisUtils;

    @JmsListener(destination = "rwdSync")
    public void receiveTrwds(String message) {
        if(sqlServerJdbcTemplate == null) {
            logger.info("初始化sqlserver连接");
            DriverManagerDataSource dataSource = new DriverManagerDataSource();
            dataSource.setDriverClassName(driverClassName);
            dataSource.setUrl(url);
            dataSource.setUsername(username);
            dataSource.setPassword(password);
            sqlServerJdbcTemplate = new JdbcTemplate(dataSource);
        }
        message = message.replaceAll("\\\\", "").
                replaceAll("\\\\r", "").
                replaceAll("\\\\n", "");
        //logger.info("处理消息：{}", message);
        Trwd trwd = JSON.parseObject(message, Trwd.class);
        if(redisUtils.exist(trwd.getFrwno())) return;
        StringBuffer command = new StringBuffer();
        command.append("insert into trwd(FRwdh,Frwxz,Fzt,Fhtdw,Fgcmc,Fjzbw,Fjzfs,Fgcdz,Fgls, ");
        command.append(" Fjhrq,Ftpz,Ftld,Fszgg,Ftbj,Fjhsl,Fscbt,Fwcsl,Fljcs,Fdlrq,Fsgpb,Fphbno,Frwno ");
        command.append(") values (");
        command.append("'" + trwd.getFrwdh() + "',").append("'" + trwd.getFrwxz() + "',");
        command.append("'" + trwd.getFzt() + "',").append("'" + trwd.getFhtdw() + "',");
        command.append("'" + trwd.getFgcmc() + "',").append("'" + trwd.getFjzbw() + "',");
        command.append("'" + trwd.getFjzfs() + "',").append("'" + trwd.getFgcdz() + "',");
        command.append("'" + trwd.getFgls() + "',").append("'" + trwd.getFjhrq() + "',");
        command.append("'" + trwd.getFtpz() + "',").append("'" + trwd.getFtld() + "',");
        command.append("'" + trwd.getFszgg() + "',").append("'" + trwd.getFtbj() + "',");
        command.append("'" + trwd.getFjhsl() + "',").append("'" + trwd.getFscbt() + "',");
        command.append("'" + trwd.getFwcsl() + "',").append("'" + trwd.getFljcs() + "',");
        command.append("'" + trwd.getFdlrq() + "',").append("'" + trwd.getFsgpb() + "',");
        command.append("'" + trwd.getFphbno() + "',").append("'" + trwd.getFrwno() + "') ");
        command.append("ON DUPLICATE KEY UPDATE Frwdh='"+trwd.getFrwdh()+"'");
        sqlServerJdbcTemplate.execute(command.toString());

        redisUtils.set(trwd.getFrwno().toString(), "1");
    }
}
