package cn.stylefeng.mferp.config;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.print.attribute.standard.Destination;

/**
 * activemq 生产者
 * @author: yzr
 * @date: 2020/5/31
 */
//@Component
public class MqUtils {
    private static Logger logger = LoggerFactory.getLogger(MqUtils.class);
    @Resource
    private JmsTemplate jmsTemplate;

    /**
     * 发送队列模式消息
     * @param target 队列标识
     *
     */
    public void sendQueueMessage(String target, String message) {
        //Queue queue = new ActiveMQQueue(target);// 生成队列
        //jmsMessagingTemplate.convertAndSend(queue, message);// 往队列中放入消息
        jmsTemplate.send(target, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                TextMessage textMessage = session.createTextMessage();
                textMessage.setText(message);
                return textMessage;
            }
        });
        logger.info("队列加入消息：{}", message);
    }
}
