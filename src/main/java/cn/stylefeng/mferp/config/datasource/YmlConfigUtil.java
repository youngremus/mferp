package cn.stylefeng.mferp.config.datasource;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Map;

/**
 * 用于项目启动时读取数据库链接信息
 */
public class YmlConfigUtil implements ApplicationContextAware {

    private static ApplicationContext applicationContext = null;

    private static Map<String,String> propertiesMap =null;

    public YmlConfigUtil() {
    }

    //根据key（url、username、password、driver-class-name）获取配置信息
    public static String getConfigByKey(String key) throws JsonProcessingException {
        if (propertiesMap ==null){
            YmlConfig ymlConfig = applicationContext.getBean(YmlConfig.class);
            propertiesMap = ymlConfig.getDatasource();
            //System.out.println("测试："+ JSON.toJSONString(propertiesMap));
        }
        return propertiesMap.get(key);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if(YmlConfigUtil.applicationContext == null){
            YmlConfigUtil.applicationContext  = applicationContext;
        }
    }
}
