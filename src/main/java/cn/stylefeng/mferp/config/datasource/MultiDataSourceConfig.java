/**
 * Copyright 2018-2020 stylefeng & fengshuonan (sn93@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.stylefeng.mferp.config.datasource;

import cn.stylefeng.mferp.core.common.constant.DatasourceEnum;
import cn.stylefeng.roses.core.config.properties.DruidProperties;
import cn.stylefeng.roses.core.config.properties.MutiDataSourceProperties;
import cn.stylefeng.roses.core.datascope.DataScopeInterceptor;
import cn.stylefeng.roses.core.mutidatasource.DynamicDataSource;
import cn.stylefeng.roses.core.mutidatasource.aop.MultiSourceExAop;
import com.alibaba.druid.pool.DruidDataSource;
import com.baomidou.mybatisplus.plugins.OptimisticLockerInterceptor;
import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.sql.SQLException;
import java.util.HashMap;

/**
 * 多数据源配置<br/>
 * <p>
 * 注：由于引入多数据源，所以让spring事务的aop要在多数据源切换aop的后面
 *
 * @author stylefeng
 * @Date 2017/5/20 21:58
 */
@Configuration
@ConditionalOnProperty(prefix = "guns.muti-datasource", name = "open", havingValue = "true")
@EnableTransactionManagement(order = 2, proxyTargetClass = true)
@MapperScan(basePackages = {"cn.stylefeng.mferp.modular.*.dao", "cn.stylefeng.mferp.multi.mapper"})
public class MultiDataSourceConfig {

    /**
     * druid配置
     */
    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DruidProperties druidProperties() {
        return new DruidProperties();
    }

    /**
     * 多数据源配置
     */
    @Bean
    @ConfigurationProperties(prefix = "guns.muti-datasource")
    public MutiDataSourceProperties mutiDataSourceProperties() {
        return new MutiDataSourceProperties();
    }

    /**
     * 多数据源配置
     */
    @Bean
    @ConfigurationProperties(prefix = "guns.third-datasource")
    public ThridDataSourceProperties thridDataSourceProperties() {
        return new ThridDataSourceProperties();
    }

    /**
     * 多数据源配置
     */
    @Bean
    @ConfigurationProperties(prefix = "guns.four-datasource")
    public FourDataSourceProperties fourDataSourceProperties() {
        return new FourDataSourceProperties();
    }

    /**
     * 多数据源配置
     */
    @Bean
    @ConfigurationProperties(prefix = "guns.five-datasource")
    public FiveDataSourceProperties fiveDataSourceProperties() {
        return new FiveDataSourceProperties();
    }

    /**
     * 多数据源切换的aop
     */
    @Bean
    public MultiSourceExAop multiSourceExAop() {
        return new MultiSourceExAop();
    }

    /**
     * guns的数据源
     */
    private DruidDataSource dataSource(DruidProperties druidProperties) {
        DruidDataSource dataSource = new DruidDataSource();
        druidProperties.config(dataSource);
        return dataSource;
    }

    /**
     * 多数据源，第二个数据源
     */
    private DruidDataSource bizDataSource(DruidProperties druidProperties, MutiDataSourceProperties mutiDataSourceProperties) {
        DruidDataSource dataSource = new DruidDataSource();
        druidProperties.config(dataSource);
        mutiDataSourceProperties.config(dataSource);
        return dataSource;
    }

    /**
     * 多数据源，第三个数据源
     */
    private DruidDataSource outDataSource(DruidProperties druidProperties, ThridDataSourceProperties thridDataSourceProperties) {
        DruidDataSource dataSource = new DruidDataSource();
        druidProperties.config(dataSource);
        thridDataSourceProperties.config(dataSource);
        return dataSource;
    }

    /**
     * 多数据源，第四个数据源
     */
    private DruidDataSource dispatchDataSource(DruidProperties druidProperties, FourDataSourceProperties fourDataSourceProperties) {
        DruidDataSource dataSource = new DruidDataSource();
        druidProperties.config(dataSource);
        fourDataSourceProperties.config(dataSource);
        return dataSource;
    }

    /**
     * 多数据源，第五个数据源
     */
    private DruidDataSource zlDataSource(DruidProperties druidProperties, FiveDataSourceProperties fiveDataSourceProperties) {
        DruidDataSource dataSource = new DruidDataSource();
        druidProperties.config(dataSource);
        fiveDataSourceProperties.config(dataSource);
        return dataSource;
    }

    /**
     * 多数据源连接池配置
     */
    @Bean
    public DynamicDataSource mutiDataSource(DruidProperties druidProperties,
                                            MutiDataSourceProperties mutiDataSourceProperties,
                                            ThridDataSourceProperties thridDataSourceProperties,
                                            FourDataSourceProperties fourDataSourceProperties,
                                            FiveDataSourceProperties fiveDataSourceProperties) {

        DruidDataSource dataSourceGuns = dataSource(druidProperties);
        DruidDataSource bizDataSource = bizDataSource(druidProperties, mutiDataSourceProperties);
        DruidDataSource outDataSource = outDataSource(druidProperties, thridDataSourceProperties);
        DruidDataSource dispatchDataSource = dispatchDataSource(druidProperties, fourDataSourceProperties);
        DruidDataSource zlDataSource = zlDataSource(druidProperties, fiveDataSourceProperties);

        try {
            dataSourceGuns.init();
            bizDataSource.init();
            outDataSource.init();
            dispatchDataSource.init();
            zlDataSource.init();
        } catch (SQLException sql) {
            sql.printStackTrace();
        }

        DynamicDataSource dynamicDataSource = new DynamicDataSource();
        HashMap<Object, Object> hashMap = new HashMap<>(5);
        // 铭发ERP数据库
        hashMap.put(DatasourceEnum.DATA_SOURCE_GUNS, dataSourceGuns);
        // 生产mysql数据库（对内）
        hashMap.put(DatasourceEnum.DATA_SOURCE_BIZ, bizDataSource);
        // 资料mysql数据库（对外）
        hashMap.put(DatasourceEnum.DATA_SOURCE_OUT, outDataSource);
        // 调度SqlServer数据库（对内）
        hashMap.put(DatasourceEnum.DATA_SOURCE_DISPATCH, dispatchDataSource);
        // 资料SqlServer数据库（对内）
        hashMap.put(DatasourceEnum.DATA_SOURCE_ZL, zlDataSource);
        dynamicDataSource.setTargetDataSources(hashMap);
        dynamicDataSource.setDefaultTargetDataSource(dataSourceGuns);
        return dynamicDataSource;
    }

    /**
     * mybatis-plus分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

    /**
     * 数据范围mybatis插件
     */
    @Bean
    public DataScopeInterceptor dataScopeInterceptor() {
        return new DataScopeInterceptor();
    }

    /**
     * 乐观锁mybatis插件
     */
    @Bean
    public OptimisticLockerInterceptor optimisticLockerInterceptor() {
        return new OptimisticLockerInterceptor();
    }
}
