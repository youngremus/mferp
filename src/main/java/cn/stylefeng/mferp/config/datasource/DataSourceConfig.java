package cn.stylefeng.mferp.config.datasource;


import cn.stylefeng.mferp.config.properties.JdbcBasis;
import cn.stylefeng.mferp.config.properties.SqlServerNewJdbcProperties;
import cn.stylefeng.mferp.config.properties.SqlServerJdbcProperties;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * Created by Administrator on 2019/4/9.
 */
@Configuration
public class DataSourceConfig {
    private static Logger logger = LoggerFactory.getLogger(DataSourceConfig.class);

    @Bean
    public SqlServerJdbcProperties sqlServerJdbcProperties() {
        return new SqlServerJdbcProperties();
    }

    /**
     * 资料室开盘数据库
     * @param sqlServerJdbcProperties
     * @return
     */
    @Bean(name = "sqlServerJdbcTemplate")
    public JdbcTemplate getSqlServerJdbcTemplate(SqlServerJdbcProperties sqlServerJdbcProperties) {
        return getJdbcTemplate(sqlServerJdbcProperties);
    }

    @Bean
    @ConfigurationProperties(prefix = "sqlserver.ddjd")
    public SqlServerNewJdbcProperties sqlServerDispatchJdbcProperties() {
        return new SqlServerNewJdbcProperties();
    }

    /**
     * 资料室开盘数据库
     * @param sqlServerDispatchJdbcProperties
     * @return
     */
    @Bean(name = "sqlServerDispatchJdbcTemplate")
    public JdbcTemplate getSqlServerDispatchJdbcTemplate(SqlServerNewJdbcProperties sqlServerDispatchJdbcProperties) {
        return getJdbcTemplateNew(sqlServerDispatchJdbcProperties);
    }

    /** 根据数据库名、数据信息连接库（公共创建jdbc连接）
     * @param  dataBaseName  数据库名
     * @param  jdbcBasis 数据连接信息
     */
    public JdbcTemplate getJdbcTemplate(String dataBaseName, JdbcBasis jdbcBasis){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(jdbcBasis.getDriverClassName());
        dataSource.setUrl(jdbcBasis.getUrl() + dataBaseName);
        dataSource.setUsername(jdbcBasis.getUsername());
        dataSource.setPassword(jdbcBasis.getPassword());
        return new JdbcTemplate(dataSource);
    }

    public JdbcTemplate getJdbcTemplate(JdbcBasis jdbcBasis) {
        logger.info("初始化JdbcTemplate");
        logger.info("jdbcBasis:{}", JSON.toJSONString(jdbcBasis));
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(jdbcBasis.getDriverClassName());
        dataSource.setUrl(jdbcBasis.getUrl());
        dataSource.setUsername(jdbcBasis.getUsername());
        dataSource.setPassword(jdbcBasis.getPassword());
        return new JdbcTemplate(dataSource);
    }

    public JdbcTemplate getJdbcTemplateNew(SqlServerNewJdbcProperties sqlServerDispatchJdbcProperties) {
        logger.info("初始化JdbcTemplate");
        logger.info("sqlServerDispatchJdbcProperties:{}", JSON.toJSONString(sqlServerDispatchJdbcProperties));
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(sqlServerDispatchJdbcProperties.getDriverClassName());
        dataSource.setUrl(sqlServerDispatchJdbcProperties.getUrl());
        dataSource.setUsername(sqlServerDispatchJdbcProperties.getUsername());
        dataSource.setPassword(sqlServerDispatchJdbcProperties.getPassword());
        return new JdbcTemplate(dataSource);
    }


}
