package cn.stylefeng.mferp.config.datasource;

import java.sql.*;

public class ConUtil {
    public static Connection getConnection(){
        Connection conn=null;

        try {
            String url="jdbc:oracle:thin:@10.41.2.235:2019:sjhjyg01";
            String user="qzjl";
            String password="qjl.fqi.sirc";

            Class.forName("oracle.jdbc.driver.OracleDriver");//加载数据驱动
            conn = DriverManager.getConnection(url, user, password);// 连接数据库

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("加载数据库驱动失败");
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("连接数据库失败");
        }
        return conn;
    }

    public static Connection getConnection(String url, String user, String password, String className){
        Connection conn=null;

        try {
            Class.forName(className);//加载数据驱动
            conn = DriverManager.getConnection(url, user, password);// 连接数据库
            System.out.println("连接数据库成功");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("加载数据库驱动失败");
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("连接数据库失败");
        }
        return conn;
    }

    public static void close(Connection conn, PreparedStatement ps, ResultSet rs){
        try {
            if(rs!=null){
                rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            if(ps!=null){
                ps.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            if(conn!=null){
                conn.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
