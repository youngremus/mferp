package cn.stylefeng.mferp.core.excel;

import java.io.File;
import java.io.IOException;

public class ExeUtil {

    public static void exec(String exePath) {
        Runtime runtime = Runtime.getRuntime();
        try {
            runtime.exec(exePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void openDirectory(String filePath) {
        File file = new File(filePath);
        try {
            Runtime.getRuntime().exec(
                    "rundll32 SHELL32.DLL,ShellExec_RunDLL "
                            + "Explorer.exe /select," + file.getAbsolutePath());
        }  catch (IOException e) {
            e.printStackTrace();
        }
    }

}
