package cn.stylefeng.mferp.core.util;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * <p>
 * 日期操作工具类
 * </p>
 * @version 1.0
 * @author lxc
 */
public class DateUtil {
	private static final String DEFAULT_CONVERT_PATTERN = "yyyyMMddHHmmssSSS";
	public static final String DATETIME_PATTERN19 = "yyyy-MM-dd HH:mm:ss";
	public static final String DATETIME_PATTERN18 = "yyyy-M-dd HH:mm:ss";
	public static final String DATE_PATTERN = "yyyy-MM-dd";
	public static final String DATE_PATTERN1 = "yyyy-M-d";
	public static final String TIME_PATTERN = "HH:mm:ss";
	public static final String TIME_PATTERN1 = "H:m:s";

	/**
	 * 
	 * 格式化日期
	 * @param sdate
	 *            日期字符串
	 * @param format
	 *            要格式化的日期格式
	 * @return 格式化后的日期字符串
	 */
	public static String format(String sdate, String format) {
		Date date = new Date();
		SimpleDateFormat df = new SimpleDateFormat(format);
		try {
			date = df.parse(sdate);
		} catch (ParseException ex) {
			ex.printStackTrace();
		}
		return df.format(date);
	}

	/**
	 *
	 * 一个日期是否是指定的日期格式
	 * @param dateStr
	 *
	 *            日期字符串
	 * @param pattern
	 *            验证的日期格式
	 * @return 是否是指定的日期格式
	 */
	public static boolean isValidDate(String dateStr, String pattern) {
		SimpleDateFormat df = new SimpleDateFormat(pattern);
		try {
			df.setLenient(false);
			df.parse(dateStr);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

	/**
	 * 将字符串按指定的格式转换为日期类型
	 * @param str
	 *            日期字符串
	 * @param format
	 *            指定格式
	 * @return 格式化后的日期对象
	 */
	public static Date strToDate(String str, String format) {
		SimpleDateFormat dtFormat = null;
		try {
			dtFormat = new SimpleDateFormat(format);
			return dtFormat.parse(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 对一个日期进行偏移
	 * @param date
	 *            日期
	 * @param offset
	 *            偏移两
	 * @return 偏移后的日期
	 */
	public static Date addDayByDate(Date date, int offset) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int day = cal.get(Calendar.DAY_OF_YEAR);
		cal.set(Calendar.DAY_OF_YEAR, day + offset);
		return cal.getTime();
	}

	/**
	 * 
	 * 将日期格式化为<字符串类型>
	 * @param date
	 * @param dateFormat
	 *            日期格式
	 * @return 当前日期<字符串类型>
	 */
	public static String dataToStr(Date date, String dateFormat) {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		return sdf.format(date);
	}

	/**
	 * 得到当前日期<字符串类型>
	 * @param dateFormat
	 *            日期格式
	 * @return 当前日期<字符串类型>
	 */
	public static String getCurrDate(String dateFormat) {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
		return sdf.format(new Date());
	}

	
	/**
	 * 得到当前日期<字符串类型>
	 *
	 * @return 当前日期<字符串类型>
	 */
	public static String getCurrDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
		return sdf.format(new Date());
	}
	
	/**
	 * 得到当前日期<java.util.Date类型>
	 * @param dateFormat
	 *            日期格式
	 * @return 当前日期<java.util.Date类型>
	 */
	public static Date getCurrentDate(String dateFormat) {
		return strToDate(getCurrDate(dateFormat), dateFormat);
	}

	/**
	 * 将一个日期转换为指定格式的日期类型
	 * @param date
	 *            要转换的日期
	 * @param dateFormat
	 *            日期格式
	 * @return 转换后的日期对象
	 */
	public static Date formatDate(Date date, String dateFormat) {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		return strToDate(sdf.format(date), dateFormat);
	}

	/**
	 * 对格式为20080101类型的字符串进行日期格式化
	 * @param dateStr
	 *            要格式化的字符串
	 * @param formatChar
	 *            连接字符
	 * @param dateFormat
	 *            日期格式
	 * @return 格式后的日期字符串
	 */
	public static String format(String dateStr, String formatChar, String dateFormat) {
		try {
			dateStr = dateStr.substring(0, 4) + formatChar
			+ dateStr.substring(4, 6) + formatChar
			+ dateStr.substring(6, 8);
			return format(dateStr, dateFormat);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 对格式为20080101类型的字符串进行日期格式化
	 * @param dateStr
	 *            要格式化的字符串
	 * @param formatChar
	 *            连接字符
	 * @param dateFormat
	 *            日期格式
	 * @return 格式后的日期对象
	 */
	public static Date formatDate(String dateStr, String formatChar,String dateFormat) {
		try {
			dateStr = dateStr.substring(0, 4) + formatChar
			+ dateStr.substring(4, 6) + formatChar
			+ dateStr.substring(6, 8);
			return strToDate(dateStr, dateFormat);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 获得某一个月份的第一天
	 * @param date
	 * @return
	 */
	public static Date getFirstDayByMonth(Date date) {
		GregorianCalendar gc = (GregorianCalendar) Calendar.getInstance();
		gc.setTime(date);
		gc.set(Calendar.DAY_OF_MONTH, 1);
		return formatDate(gc.getTime(), "yyyy-MM-dd");
	}

	/**
	 * 获得某一个月份的最后一天
	 * @param date
	 * @return
	 */
	public static Date getLastDayByMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DATE, 1);// 设为当前月的1号
		cal.add(Calendar.MONTH, 1);// 加一个月，变为下月的1号
		cal.add(Calendar.DATE, -1);// 减去一天，变为当月最后一天
		return formatDate(cal.getTime(), "yyyy-MM-dd");
	}

	/**
	 * 获得指定日期的年份
	 * @param date
	 * @return
	 */
	public static int getYearByDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.YEAR);
	}

	/**
	 * 获得指定日期的月份
	 * @param date
	 * @return
	 */
	public static int getMonthByDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.MONTH);
	}

	/**
	 * 获得指定日期的所在月份当前的天数
	 * @param date
	 * @return
	 */
	public static int getDayInMonthByDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * 获得当前传入日期的上一个月份的当前日期
	 * @param date
	 * @return
	 */
	public static Date getPreviousDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, -1);
		return DateUtil.formatDate(cal.getTime(), "yyyy-MM-dd");
	}

	/**
	 * 获得当前传入日期的下一个月份的当前日期
	 * @param date
	 * @return
	 */
	public static Date getLastMonthDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, 1);
		return DateUtil.formatDate(cal.getTime(), "yyyy-MM-dd");
	}

	/**
	 * 计算两个日期相差的月数（具体细分到天数的差别）
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static int getDiffMonths(Date date1, Date date2) {
		int iMonth = 0;
		int flag = 0;
		try {
			Calendar objCalendarDate1 = Calendar.getInstance();
			objCalendarDate1.setTime(date1);
			Calendar objCalendarDate2 = Calendar.getInstance();
			objCalendarDate2.setTime(date2);
			if (objCalendarDate2.equals(objCalendarDate1)){
				return 0;
			}
			if (objCalendarDate1.after(objCalendarDate2)) {
				Calendar temp = objCalendarDate1;
				objCalendarDate1 = objCalendarDate2;
				objCalendarDate2 = temp;
			}
			if (objCalendarDate2.get(Calendar.DAY_OF_MONTH) < objCalendarDate1.get(Calendar.DAY_OF_MONTH)){
				flag = 1;
			}

			if (objCalendarDate2.get(Calendar.YEAR) > objCalendarDate1.get(Calendar.YEAR)){
				iMonth = ((objCalendarDate2.get(Calendar.YEAR) - objCalendarDate1.get(Calendar.YEAR))
						* 12 + objCalendarDate2.get(Calendar.MONTH) - flag)
						- objCalendarDate1.get(Calendar.MONTH);
			}
			else{
				iMonth = objCalendarDate2.get(Calendar.MONTH)- objCalendarDate1.get(Calendar.MONTH) - flag;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return iMonth;
	}

	/**
	 * 
	 * 计算两个日期月数的差别，不计算详细到天数的差别
	 * 
	 * 日期大的为参数一，结果为正数，反之为负数
	 * 
	 * @return
	 */
	public static int getDiffMonth(Date date1, Date date2) {
		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTime(date1);
		Calendar calendar2 = Calendar.getInstance();
		calendar2.setTime(date2);
		int diffyaer = calendar1.get(Calendar.YEAR)
				- calendar2.get(Calendar.YEAR);
		int diffmonth = calendar1.get(Calendar.MONTH)
				- calendar2.get(Calendar.MONTH);
		return diffyaer * 12 + diffmonth;
	}
	
	/**
	 * 
	 * 计算两个时间相差的秒数
	 * 
	 * @return
	 */
	public static long getDiffTime(String date1,String date2) {
		// 格式化时间
		SimpleDateFormat d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			// 这个的除以1000得到秒，相应的60000得到分，3600000得到小时
            long result = ((d.parse(date1).getTime() - d.parse(date2) .getTime()) / 1000);
            return result;
		} catch (ParseException e) {  
            e.printStackTrace();  
        }
		return 0;
	}

	/**
	 * 获取当前时间
	 * @return
	 */
	public static String getCurrentDate(){
		return DateUtil.getCurrDate("yyyy-MM-dd");
	}

	/**
	 * 获取当前时间
	 * @return
	 */
	public static Date getCurrentTime(){
		return DateUtil.getCurrentDate("yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 获取时间序列
	 * @return
	 */
	public static String getDateSerial() {
		return DateUtil.getCurrDate("yyyyMMddHHmmss");
	}

	public static Date getDate(String datestr, String inFmt) throws ParseException {
		if (null != datestr && !"".equals(datestr.trim())) {
			if (null == inFmt || "".equals(inFmt.trim())) {
				inFmt = "yyyyMMdd";
			}

			Date inDate;
			SimpleDateFormat inDateFormat = new SimpleDateFormat(inFmt);
			inDateFormat.setLenient(true);
			inDate = inDateFormat.parse(datestr);
			return inDate;
		} else {
			return null;
		}
	}

	public static String getDateFormat(Date date, String outFmt) {
		if (null == date) {
			return null;
		} else {
			if (null == outFmt || "".equals(outFmt.trim())) {
				outFmt = "yyyyMMdd";
			}

			String retu = null;
			SimpleDateFormat dateFormat = null;

			try {
				dateFormat = new SimpleDateFormat(outFmt);
			} catch (IllegalArgumentException var5) {
				dateFormat = new SimpleDateFormat("yyyyMMdd");
			}

			dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+8"));
			retu = dateFormat.format(date);
			dateFormat = null;
			return retu;
		}
	}

	public static String getDateFormat(String dateStr, String inFmt, String outFmt) throws ParseException {
		if (null != dateStr && !"".equals(dateStr.trim())) {
			if (null != inFmt && !"".equals(inFmt.trim())) {
				if (null == outFmt || "".equals(outFmt.trim())) {
					outFmt = "yyyyMMdd";
				}

				Date inDate = getDate(dateStr, inFmt);
				if (null == inDate) {
					return dateStr;
				} else {
					return getDateFormat(inDate, outFmt);
				}
			} else {
				return dateStr;
			}
		} else {
			return dateStr;
		}
	}

	/**
	 * yyyy-M-d H:m:s 转成 yyyy-MM-dd HH:mm:ss格式
	 * 相当于 DateUtil.getDateFormat(s, "yyyy-M-d H:m:s", "yyyy-MM-dd HH:mm:ss")
	 * @param dateTimeStr
	 * @return
	 */
	public static String parseDateTimeStr(String dateTimeStr) {
		if (StringUtils.isBlank(dateTimeStr)) {
			return "";
		}
		String[] split = dateTimeStr.split(" ");
		String date = split[0];
		String time = split.length == 2 ? split[1] : "00:00:00";
		try {
			return getDateFormat(date, DATE_PATTERN1, DATE_PATTERN) + " "
					+ getDateFormat(time, TIME_PATTERN1, TIME_PATTERN);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dateTimeStr;
	}
	
	public static void main(String[] args){
		String nowDate = DateUtil.getCurrDate("yyyy-MM-dd HH:mm:ss");
		String testDate = "2015-05-19 09:50:10";
		System.out.println(getDiffTime(nowDate,testDate));
	}
	
	
}

