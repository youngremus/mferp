package cn.stylefeng.mferp.core.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Description:
 *
 * @author lxc
 * @date 2018/12/19.15:03
 */
public class ToolsUtil {

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    private static final AtomicInteger atomicInteger = new AtomicInteger(10);

    /**
     * 生成问题工单编号
     */
    public synchronized static  String getProbleNumber() {
        atomicInteger.getAndIncrement();
        String date = simpleDateFormat.format(new Date());
        int i = atomicInteger.get();
        return date + i;
    }

}
