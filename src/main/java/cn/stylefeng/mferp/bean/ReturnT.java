package cn.stylefeng.mferp.bean;

import java.io.Serializable;

/**
 * common return
 * @author xuxueli 2015-12-4 16:32:31
 * @param <T>
 */
public class ReturnT<T> implements Serializable {
	public static final long serialVersionUID = 42L;

	public static final int SUCCESS_CODE = 200;
	public static final int FAIL_CODE = 500;

	public static final ReturnT<String> SUCCESS = new ReturnT<String>(null);
	public static final ReturnT<String> FAIL = new ReturnT<String>(FAIL_CODE, null);

	private int code;
	private String msg;
	private T content;

	public ReturnT(){}
	public ReturnT(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}
	public ReturnT(T content) {
		this.code = SUCCESS_CODE;
		this.content = content;
	}

	public static ReturnT<String> responseFailed(int code, String msg) {
		return new ReturnT<>(code, msg);
	}

	public static ReturnT<String> responseFailed(AppResult appResult) {
		return new ReturnT<>(appResult.getCode(), appResult.getMessage());
	}

	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public T getContent() {
		return content;
	}
	public void setContent(T content) {
		this.content = content;
	}

	public static boolean isFail(ReturnT returnT) {
		return returnT == null || FAIL_CODE == returnT.getCode();
	}

	public static boolean isSuccess(ReturnT returnT) {
		return returnT != null && SUCCESS_CODE == returnT.getCode();
	}

	@Override
	public String toString() {
		return "ReturnT [code=" + code + ", msg=" + msg + ", content=" + content + "]";
	}

}
