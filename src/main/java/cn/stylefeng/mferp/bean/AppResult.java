package cn.stylefeng.mferp.bean;

/**
 * @author yzr
 */
public enum AppResult {

    /**
     * 通用返回消息
     */
    RET_COMM_SUCCESS(0, "操作成功"),
    RET_COMM_INVALID(400, "登录失效[400]"),
    RET_COMM_UNAUTHORIZED(401, "认证失败[401]"),
    RET_COMM_ACCESS_DENIED(403, "禁止访问[403]"),
    RET_COMM_NOT_FOUND(404, "请求的资源不存在[404]"),
    RET_COMM_TIME_OUT(408, "请求超时[408]"),
    RET_COMM_VERIFY_CODE_ERROR(409, "验证码错误[409]"),
    RET_COMM_SERVER_ERROR(500, "服务内部错误[500]"),
    RET_COMM_USER_EXISTS(501, "用户已经存在[501]"),
    RET_COMM_USER_INVALID(10000, "登录已失效，请重新进入小程序[10000]"),
    RET_COMM_PARAM_NOT_FOUND(10002, "缺少参数[10002]"),
    RET_COMM_PARAM_EXCEPTION(390201, "参数异常[10002]"),
    RET_COMM_OPERATION_FAIL(10003, "操作失败[10003]"),
    RET_COMM_NOT_EXISTS(10004, "操作的数据不存在[10004]"),
    RET_COMM_USING(10005, "操作的数据在使用中[10005]"),
    RET_COMM_USER_NOT_EXISTS(10006, "用户不存在[10006]"),
    RET_COMM_OLD_PASSWORD_NOT_MATCH(10007, "旧密码不正确[10007]"),
    RET_COMM_FORBIDDEN(10009, "未授权访问[10009]"),
    RET_COMM_USERNAME_USED(10010, "用户名已被使用[10010]"),
    RET_COMM_USER_IN_USING(10011, "用户使用中，不能删除[10011]"),
    RET_COMM_ROLE_IN_USING(10012, "角色使用中，不能删除[10012]"),
    RET_COMM_SAME_NAME_EXISTS(10013, "同名数据已经存在[10013]"),
    RET_COMM_USER_ORG_IS_NULL(10014, "用户未归属公司[10014]"),
    RET_COMM_RENTPLANINFO_EXIST_FAIL(10015, "该招租项目中还存在招租信息，不允许被删除[10015]"),
    RET_COMM_VALIDATE_SIGN_FAIL(10016, "验签失败[10016]"),

    ;
    /**
     * 返回编码
     */
    private final int code;

    /**
     * 返回错误信息
     */
    private final String message;

    AppResult(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }
}
