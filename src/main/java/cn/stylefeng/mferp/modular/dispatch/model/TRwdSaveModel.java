package cn.stylefeng.mferp.modular.dispatch.model;

import cn.stylefeng.mferp.core.common.validator.AddGroup;
import cn.stylefeng.mferp.core.common.validator.UpdateGroup;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 任务单
 * </p>
 *
 * @author yzr
 * @since 2020-05-27
 */
@Data
public class TRwdSaveModel {

    private static final long serialVersionUID = 1L;

    /**
     * 任务单号
     */
    @NotNull(message = "任务单号不能为空", groups = {UpdateGroup.class})
    private Integer Frwdh;
    /**
     * 合同编号
     */
    private String Fhtbh;
    /**
     * 任务性质
     */
    private String Frwxz;
    /**
     * 状态
     */
    private String Fzt;
    /**
     * 施工单位
     */
    @NotBlank(message = "施工单位不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String Fhtdw;
    /**
     * 工程名称
     */
    @NotBlank(message = "工程名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String Fgcmc;
    /**
     * 施工部位
     */
    @NotBlank(message = "浇筑部位不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String Fjzbw;
    /**
     * 泵送
     */
    @NotBlank(message = "卸料方式不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String Fjzfs;
    /**
     * 施工地点
     */
    @NotBlank(message = "工地地址不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String Fgcdz;
    /**
     * 运距
     */
    private Double Fgls;
    /**
     * 计划日期
     */
    @NotBlank(message = "浇筑时间不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String Fjhrq;
    /**
     * 砼品种
     */
    @NotBlank(message = "强度等级不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String Ftpz;
    /**
     * 坍落度
     */
    @NotBlank(message = "塌落度不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String Ftld;
    /**
     * 水泥品种
     */
    private String Fsnbh;
    /**
     * 石子规格
     */
    private String Fszgg;
    /**
     * 备注|渗料规格|外加剂规格|抗渗等级|其他要求|施工单位联系人|工程编号|营销部门联系人|监督号码|优先采用的仓库编号|监控系统工地号|质检员
     * 13599156937|/|||||13599156937|||||
     */
    private String Ftbj;
    /**
     * 计划方量
     */
    @NotNull(message = "预计方量不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private Double Fjhsl;
    /**
     * 生产拌台
     */
    private String Fscbt;
    /**
     * 完成方量/砼数量
     */
    private Double Fwcsl;
    /**
     * 累计车数
     */
    private Integer Fljcs;
    /**
     * 操作员
     */
    private String Fczy;
    /**
     * 登录日期
     */
    private String Fdlrq;
    /**
     * 施工配合比号
     */
    private Integer Fsgpb;
    /**
     * 砂浆配合号
     */
    private Integer Fsjpb = 0;

    private String Frwno;
    private String Fhtno;

    /**
     * 配合比号
     */
    private String Fphbno;

    /**
     * 工地电话
     */
    @NotBlank(message = "工地电话不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String gddh;
    /**
     * 资料要求
     */
    @NotBlank(message = "资料要求不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String zlyq;
}
