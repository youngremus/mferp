package cn.stylefeng.mferp.modular.dispatch.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentFontStyle;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import lombok.Data;

/**
 * <p>
 * 接单日报表导出数据用
 * </p>
 *
 * @author yzr
 * @since 2021-05-13
 */

@Data
// 头背景设置成红色 IndexedColors.RED.getIndex()
//@HeadStyle(fillPatternType = FillPatternType.SOLID_FOREGROUND, fillForegroundColor = 10)
// 头字体设置成20
@HeadFontStyle(fontHeightInPoints = 11)
// 内容的背景设置成绿色 IndexedColors.GREEN.getIndex()
//@ContentStyle(fillPatternType = FillPatternType.SOLID_FOREGROUND, fillForegroundColor = 17)
// 内容字体设置成20
@ContentFontStyle(fontHeightInPoints = 11)
public class TRwdTodayReportModel {

    @ColumnWidth(12)
    @ExcelProperty("日期")
    private String rq;

    /**
     * 任务单号
     */
    @ColumnWidth(12)
    @ExcelProperty("单号")
    private Integer frwdh;

    @ColumnWidth(12)
    @ExcelProperty("时间")
    private String sj;
    /**
     * 工程名称
     */
    @ColumnWidth(20)
    @ExcelProperty("工程名称")
    private String fgcmc;
    /**
     * 施工单位
     */
    @ColumnWidth(20)
    @ExcelProperty("施工单位")
    private String fhtdw;
    /**
     * 工程地址
     */
    @ColumnWidth(20)
    @ExcelProperty("工程地址")
    private String fgcdz;
    /**
     * 浇筑部位
     */
    @ColumnWidth(20)
    @ExcelProperty("浇筑部位")
    private String fjzbw;
    /**
     * 计划方量
     */
    @ColumnWidth(12)
    @ExcelProperty("方量")
    private Double fjhsl;
    /**
     * 砼品种
     */
    @ColumnWidth(12)
    @ExcelProperty("标号")
    private String ftpz;
    /**
     * 泵送
     */
    @ColumnWidth(12)
    @ExcelProperty("泵/否")
    private String fjzfs;
    /**
     * 联系电话
     */
    @ColumnWidth(12)
    @ExcelProperty("联系电话")
    private String gddh;
    /**
     * 备注
     */
    @ColumnWidth(12)
    @ExcelProperty("备注")
    private String zlyq;

}
