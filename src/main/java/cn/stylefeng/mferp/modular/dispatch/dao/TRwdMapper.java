package cn.stylefeng.mferp.modular.dispatch.dao;

import cn.stylefeng.mferp.modular.business.model.Trwd;
import cn.stylefeng.mferp.modular.dispatch.model.TRwd;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 任务单 Mapper 接口
 * </p>
 *
 * @author yzr
 * @since 2020-05-27
 */
public interface TRwdMapper extends BaseMapper<TRwd> {

    /**
     * 按任务单号查询
     */
    TRwd selectByRwdh(@Param("rwdh") String rwdh);

    List<Map<String, Object>> selectTrwds(@Param("beginTime") String beginTime, @Param("endTime") String endTime, @Param("gcmc") String gcmc, @Param("rwdh") String rwdh, @Param("scbt") String scbt, @Param("zt") String zt);

    List<TRwd> selectTrwdList(@Param("beginTime") String beginTime, @Param("endTime") String endTime);
}
