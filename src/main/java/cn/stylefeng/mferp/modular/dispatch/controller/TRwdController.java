package cn.stylefeng.mferp.modular.dispatch.controller;

import cn.stylefeng.mferp.core.common.validator.AddGroup;
import cn.stylefeng.mferp.core.common.validator.UpdateGroup;
import cn.stylefeng.mferp.core.log.LogObjectHolder;
import cn.stylefeng.mferp.core.util.DateUtil;
import cn.stylefeng.mferp.modular.business.model.Trwd;
import cn.stylefeng.mferp.modular.business.warpper.TrwdWarpper;
import cn.stylefeng.mferp.modular.dispatch.model.TRwd;
import cn.stylefeng.mferp.modular.dispatch.model.TRwdSaveModel;
import cn.stylefeng.mferp.modular.dispatch.service.ITRwdService;
import cn.stylefeng.roses.core.reqres.response.SuccessResponseData;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 任务单控制器
 *
 * @author yzr
 * @Date 2024-03-16 16:44:57
 */
@Controller
@RequestMapping("/trwd_old")
public class TRwdController {

    @Autowired
    private ITRwdService itRwdService;

    private String PREFIX = "/business/trwd/";

    /**
     * 跳转到任务单首页(调度接单)
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "trwd_old.html";
    }

    /**
     * 跳转到修改任务单
     */
    @RequestMapping("/trwd_update/{trwdId}")
    public String trwdUpdate(@PathVariable Integer trwdId, Model model) {
        TRwd trwd = itRwdService.selectByRwdh(String.valueOf(trwdId));
        model.addAttribute("item", trwd);
        LogObjectHolder.me().set(trwd);
        return PREFIX + "trwd_old_edit.html";
    }

    /**
     * 获取任务单列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(@RequestParam(required = false) String beginTime,
                       @RequestParam(required = false) String endTime,
                       @RequestParam(required = false) String gcmc,
                       @RequestParam(required = false) String rwdh,
                       @RequestParam(required = false) String scbt,
                       @RequestParam(required = false) String zt) {
        return itRwdService.selectTrwds(beginTime, endTime, gcmc, rwdh, scbt, zt);
    }

    /**
     * 根据任务单号查找任务单
     */
    @RequestMapping(value = "/get_trwd_by_frwdh/{rwdh}")
    @ResponseBody
    public Object getTrwdByFrwdh(@PathVariable("rwdh") String rwdh) {
        return itRwdService.getTrwdByFrwdh(rwdh);
    }

    /**
     * 查找上一个任务单
     */
    @RequestMapping(value = "/get_last_trwd_by_frwdh/{rwdh}")
    @ResponseBody
    public Object getLastTrwdByFrwdh(@PathVariable("rwdh") String rwdh) {
        return itRwdService.getLastTrwdByFrwdh(rwdh);
    }

    /**
     * 查找下一个任务单
     */
    @RequestMapping(value = "/get_next_trwd_by_frwdh/{rwdh}")
    @ResponseBody
    public Object getNextTrwdByFrwdh(@PathVariable("rwdh") String rwdh) {
        return itRwdService.getNextTrwdByFrwdh(rwdh);
    }

    /**
     * 自动识别任务单
     */
    @RequestMapping(value = "/analyse_content")
    @ResponseBody
    public Object analyseContent(@RequestParam String content) {
        return itRwdService.analyseContent(content);
    }

    /**
     * 新增任务单
     */
    @RequestMapping(value = "/save_trwd")
    @ResponseBody
    public Object saveTrwd(@RequestBody @Validated(AddGroup.class) TRwdSaveModel data) {
        return itRwdService.saveTRwd(data);
    }

    /**
     * 修改任务单
     */
    @RequestMapping(value = "/update_trwd")
    @ResponseBody
    public Object updateTrwd(@RequestBody @Validated(UpdateGroup.class) TRwdSaveModel data) {
        return itRwdService.updateTRwd(data);
    }

    /**
     * 获取任务单列表
     */
    @RequestMapping(value = "/get_trwd_list_today")
    @ResponseBody
    public Object getTrwdListToday() {
        return itRwdService.getTrwdListToday();
    }

    @GetMapping("/download_today_report")
    public void downloadTodayReport(HttpServletResponse response,
                                    @RequestParam(required = false) String beginTime,
                                    @RequestParam(required = false) String endTime,
                                    @RequestParam(required = false) String excelType) throws IOException {
        itRwdService.downloadTodayReport(response, beginTime, endTime, excelType);
    }

    /**
     * 获取浇筑方式提示
     */
    @RequestMapping(value = "/get_all_fjzfs")
    @ResponseBody
    public Object getAllFjzfs(@RequestParam(required = false) String fjzfs) {
//        List<String> list = new ArrayList<>();
//        list.add("泵送");
//        list.add("非泵");
//        list.add("62米天泵");
//        list.add("32米地泵");
//        list.add("自卸");
//        list.add("机械");
//        return new SuccessResponseData(list);
        return itRwdService.getAllFjzfs(fjzfs);
    }

    /**
     * 获取浇筑方式提示
     */
    @RequestMapping(value = "/get_gddh")
    @ResponseBody
    public Object getGddh(@RequestParam String gddh) {
//        List<String> list = new ArrayList<>();
//        list.add(gddh);
//        list.add(gddh + "泵送");
//        list.add(gddh + "非泵");
//        list.add(gddh + "62米天泵");
//        list.add(gddh + "32米地泵");
//        list.add(gddh + "自卸");
//        list.add(gddh + "机械");
//        return new SuccessResponseData(list);
        return itRwdService.getGddh(gddh);
    }

    /**
     * 获取计划日期提示
     */
    @RequestMapping(value = "/get_fjhrq")
    @ResponseBody
    public Object getFjhrq(@RequestParam(required = false) String fjhrq) {
//        List<String> list = new ArrayList<>();
//        String currentDate = StringUtils.isNotBlank(fjhrq) ? fjhrq : DateUtil.getCurrentDate();
//        for (int i = 1; i <= 23; i++) {
//            list.add(currentDate + " " + String.format("%02d", i) + ":00:00");
//        }
//        return new SuccessResponseData(list);
        return itRwdService.getFjhrq(fjhrq);
    }

}
