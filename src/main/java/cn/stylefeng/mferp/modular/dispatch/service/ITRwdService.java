package cn.stylefeng.mferp.modular.dispatch.service;

import cn.stylefeng.mferp.core.common.constant.DatasourceEnum;
import cn.stylefeng.mferp.modular.dispatch.model.TRwd;
import cn.stylefeng.mferp.modular.dispatch.model.TRwdSaveModel;
import cn.stylefeng.roses.core.mutidatasource.annotion.DataSource;
import com.baomidou.mybatisplus.service.IService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>
 * 任务单 服务类
 * </p>
 *
 * @author yzr
 * @since 2020-05-27
 */
public interface ITRwdService extends IService<TRwd> {

    Object selectTrwds(String beginTime, String endTime, String gcmc, String rwdh, String scbt, String zt);

    /**
     * 按任务单号查询
     */
    TRwd selectByRwdh(String rwdh);

    /**
     * 按单号查询任务单，用于任务单识别
     * @param rwdh
     * @return
     */
    Object getTrwdByFrwdh(String rwdh);

    /**
     * 按单号查询上一个任务单，用于任务单识别
     * @param rwdh
     * @return
     */
    Object getLastTrwdByFrwdh(String rwdh);

    /**
     * 按单号查询下一个任务单，用于任务单识别
     * @param rwdh
     * @return
     */
    Object getNextTrwdByFrwdh(String rwdh);

    @DataSource(name = DatasourceEnum.DATA_SOURCE_DISPATCH)
    Object getTrwdList(String gcmc, String rwdh);

    /**
     * 今天新建的任务单
     * @return
     */
    Object getTrwdListToday();

    /**
     * 保存任务单：SQLserver
     * @param data
     * @return
     */
    Object saveTRwd(TRwdSaveModel data);

    /**
     * 识别任务单
     * @param content
     * @return
     */
    Object analyseContent(String content);

    /**
     * 修改任务单：SQLserver
     * @param data
     * @return
     */
    Object updateTRwd(TRwdSaveModel data);

    void downloadTodayReport(HttpServletResponse response, String beginTime, String endTime, String excelType) throws IOException;

    Object getAllFjzfs(String fjzfs);

    Object getGddh(String gddh);

    Object getFjhrq(String fjhrq);
}
