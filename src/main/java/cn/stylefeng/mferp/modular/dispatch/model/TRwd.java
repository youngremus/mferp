package cn.stylefeng.mferp.modular.dispatch.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;

@TableName("TRwd")
@Data
public class TRwd extends Model<TRwd> {
    private static final long serialVersionUID = 1L;

    @TableId(value = "Frwdh", type = IdType.AUTO)
    private Integer Frwdh;
    private String Fhtbh;
    private String Frwxz;
    private String Frwly;
    private String Fzt;
    private String Fhtdw;
    private String Fgcmc;
    private String Fgcjb;
    private String Fgclb;
    private String Fjzbw;
    private String Fjzfs;
    private String Fbclb;
    private String Fgcdz;
    private Double Fgls;
    private String Fjhrq;
    private String Ftpz;
    private String Ftld;
    private String Fsnbh;
    private String Fszgg;
    private String Ftbj;
    private Double Fjhsl;
    private String Fscbt;
    private Double Fwcsl;
    private Integer Fljcs;
    private String Fxdrw;
    private String Fczy;
    private String Fdlrq;
    private Integer Fsgpb;
    private String Fsyy1;
    private String Fsjpb;
    private String Fsyy2;
    private String Fzdpb;
    private String Ftjsj;
    private String Ftjr;
    private String Fksrq;
    private String Fwcrq;
    private String Fxph1;
    private String Fxph2;
    private String Fkzsl;
    private String Fkzdj;
    private String Fkzje;
    private String Fqrtsl;
    private String Fqrtdj;
    private String Fqrtje;
    private String Fqrbssl;
    private String Fqrbsdj;
    private String Fqrbsje;
    private String Fqrkzsl;
    private String Fqrkzdj;
    private String Fqrkzje;
    private String Fqrzjje;
    private String Fqrbz;
    private String Fqrfzr;
    private String Fqrrq;
    private String Ftdj;
    private String Ftje;
    private String Fbssl;
    private String Fbsdj;
    private String Fbsje;
    private String Fzje;
    private String Frwno;
    private String Fhtno;
    private String Fjbsj;
    private String Fclsjno;
    private String Fjsdno;
    private String Fbranchid;
    private String Fhddatetime;
    private String Fhdoperator;
    private String Fzzl;
    private String Fbydatetime;
    private String Fbyoperator;
    private String Fbybz;
    private String Fc1;
    private String Fc2;
    private String Fc3;
    private String Foperator;
    private String Ftimestamp;
    private String Fchecksum;
    private String Fcheckkey;
    private String Fphbno;
    private String Fphbid;
    private String Frz;
    private String Fqrzzl;
    private String Fqrrz;
    private String Fa1;
    private String Fa2;
    private String Fa3;
    private String Fa4;
    private String Fsjb;
    private String Fslpt;
    private String Fjnclkg;
    private String Frva;
    private String Frvb;
    private String Frvc;
    private String Fqva;
    private String Fqvb;
    private String Fqvc;
    private String Fiv0;
    private String Fjhsm;
    private String Fssyq;
    private String Ffhsl;
    private String Fsfcfsc;
    private String Fsftz;
    private String Fbg;
    private String Fgg;
    private String fqc;
    private String ftd;
    private String Ffhcs;
    private String Fqrcs;
    private String Fmpfsp;
    private String Fpzzt;
    private String Fpzrq;
    private String Fpzr;
    private String Fmemo;
    private String Fphfh;
    private String Fphfhsj;
    private String Fbsfs;
    private String Fshb;
    private String Fpbfs;
    private String Ftno;
    private String Fqtfl;
    private String Fqtcs;
    private String Sfcq;
    private String Fdc;
    private String Ftid;
    private String Fxpno;
    private String Fversion;
    private Long Fxpkey;

    /**
     * 工地电话
     */
    @TableField(exist = false)
    private String gddh;
    /**
     * 资料要求
     */
    @TableField(exist = false)
    private String zlyq;
    @Override
    protected Serializable pkVal() {
        return null;
    }
}
