package cn.stylefeng.mferp.modular.dispatch.constant;

public class Constants {
    public final static String[] AM_STR_ARRAY = {"凌晨", "早上", "上午"};
    public final static String[] PM_STR_ARRAY = {"中午", "午后", "下午", "晚上"};
    public final static String[] TIME_STR_ARRAY = {"时", "点", "点钟", "小时"};
    public final static String YEAR_STR = "年";
    public final static String MONTH_STR = "月";
    public final static String DAY_STR = "日";
    public final static String HOUR_STR = "时";
    // 浇筑部位
    public final static String[] JZBW_STR_ARRAY = {"浇筑部位", "施工部位", "浇注部位"};
    // 砼品种
    public final static String[] TPZ_STR_ARRAY = {"强度等级", "砼等级", "标号", "强度", "砼品种"};
    // 计划方量
    public final static String[] JHSL_STR_ARRAY = {"预计方量", "数量", "计划方量", "方量"};
    public final static String[] JHSL_UNIT_STR_ARRAY = {"立方", "方", "m³"};
    // 计划日期
    public final static String[] JHRQ_STR_ARRAY = {"浇筑时间", "浇注时间", "计划日期", "计划时间"};
    // 浇筑方式
    public final static String[] JZFS_STR_ARRAY = {"卸料方式", "浇筑方式", "浇注方式", "施工方式"};
    // 工地电话
    public final static String[] GDDH_STR_ARRAY = {"工地电话", "联系电话", "现场联系人"};
    // 工程地址
    public final static String[] GCDZ_STR_ARRAY = {"工地地址", "工程地址", "浇筑地址", "浇注地址", "工地地点", "工程地点", "浇筑地点", "浇注地点"};
    // 资料要求
    public final static String[] ZLYQ_STR_ARRAY = {"资料要求", "备注"};
    public final static String ZLYQ_STR_KPJD = "开盘鉴定";
    public final static String ZLYQ_STR_SJ = "是否送检";
    public final static String ZLYQ_STR_SK = "试块";
    // 塌落度
    public final static String[] TLD_STR_ARRAY = {"塌落度", "坍落度"};
    //非泵
    public final static String[] TLD_STR_ARRAY1 = {"非泵", "自卸斗车", "自卸"};
    //泵送
    public final static String[] TLD_STR_ARRAY2 = {"泵送", "天泵", "地泵", "汽车泵"};
    public final static String[] TLD_STR_ARRAY3 = {"水单", "润管剂", "砂浆"};
    public final static String DEFAULT_NULL_VALUE = "/";
}
