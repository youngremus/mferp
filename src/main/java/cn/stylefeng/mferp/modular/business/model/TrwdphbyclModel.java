package cn.stylefeng.mferp.modular.business.model;

import java.text.Collator;
import java.util.Comparator;
import java.util.List;

/**
 * 任务单配合比原材料实体
 * 用于核对配方界面
 *
 * @author: yzr
 * @date: 2020/5/31
 */
public class TrwdphbyclModel {

    private Integer Fid;
    /**
     * 任务单号
     */
    private Integer Frwdh;
    /**
     * 类别
     */
    private Integer Fpblb;
    /**
     * 材料名称
     */
    private String Fylmc;
    /**
     * 材料规格
     */
    private String Fpzgg;
    /**
     * 用量
     */
    private Double Fsysl;
    /**
     * 仓位
     */
    private Integer Fplcw;
    /**
     * 仓库编号
     */
    private String Fckno;
    /**
     * 含水率
     */
    private Double Fhsl;

    private String Fsgpb;

    private Integer Fbtid;

    public Integer getFid() {
        return Fid;
    }

    public void setFid(Integer fid) {
        Fid = fid;
    }

    public Integer getFrwdh() {
        return Frwdh;
    }

    public void setFrwdh(Integer frwdh) {
        Frwdh = frwdh;
    }

    public Integer getFpblb() {
        return Fpblb;
    }

    public void setFpblb(Integer fpblb) {
        Fpblb = fpblb;
    }

    public String getFylmc() {
        return Fylmc;
    }

    public void setFylmc(String fylmc) {
        Fylmc = fylmc;
    }

    public String getFpzgg() {
        return Fpzgg;
    }

    public void setFpzgg(String fpzgg) {
        Fpzgg = fpzgg;
    }

    public Double getFsysl() {
        return Fsysl;
    }

    public void setFsysl(Double fsysl) {
        Fsysl = fsysl;
    }

    public Integer getFplcw() {
        return Fplcw;
    }

    public void setFplcw(Integer fplcw) {
        Fplcw = fplcw;
    }

    public String getFckno() {
        return Fckno;
    }

    public void setFckno(String fckno) {
        Fckno = fckno;
    }

    public Double getFhsl() {
        return Fhsl;
    }

    public void setFhsl(Double fhsl) {
        Fhsl = fhsl;
    }

    public String getFsgpb() {
        return Fsgpb;
    }

    public void setFsgpb(String fsgpb) {
        Fsgpb = fsgpb;
    }

    public Integer getFbtid() {
        return Fbtid;
    }

    public void setFbtid(Integer fbtid) {
        Fbtid = fbtid;
    }

    /**
     * 任务单配方原材料按原材料名称排序
     * @param list
     * @return
     */
    public static List<TrwdphbyclModel> sortByYlmc(List<TrwdphbyclModel> list) {
        String[] key = new String[] {"水泥", "矿粉", "粉煤灰", "砂", "石", "瓜米", "水", "外加剂"};
        if(list != null && list.size() > 1) {
            for(String val : key) {
                list.sort(new Comparator<TrwdphbyclModel>() {
                    @Override
                    public int compare(TrwdphbyclModel o1, TrwdphbyclModel o2) {
                        String ylmc1 = o1.getFylmc();
                        String ylmc2 = o2.getFylmc();
                        if(ylmc1.contains(val) && !ylmc2.contains(val)) {
                            return 1;
                        } else if(!ylmc1.contains(val) && ylmc2.contains(val)) {
                            return -1;
                        } else {
                            return 0;
                        }
                    }
                });
            }
        }
        return list;
    }

    /**
     * 任务单配方原材料按品种规格排序
     * @param list
     * @return
     */
    public static List<TrwdphbyclModel> sortByPzgg(List<TrwdphbyclModel> list) {
        String[] key = new String[] {"C", "K", "F", "砂", "1-3", "1-2", "0-5", "水", "A"};
        if(list != null && list.size() > 1) {
            for(String val : key) {
                list.sort(new Comparator<TrwdphbyclModel>() {
                    @Override
                    public int compare(TrwdphbyclModel o1, TrwdphbyclModel o2) {
                        String pzgg1 = o1.getFpzgg();
                        String pzgg2 = o2.getFpzgg();
                        if(pzgg1.contains(val) && !pzgg2.contains(val)) {
                            return 1;
                        } else if(!pzgg1.contains(val) && pzgg2.contains(val)) {
                            return -1;
                        } else {
                            return 0;
                        }
                    }
                });
            }
        }
        return list;
    }
}
