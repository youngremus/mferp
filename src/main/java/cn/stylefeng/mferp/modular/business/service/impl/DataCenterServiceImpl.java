package cn.stylefeng.mferp.modular.business.service.impl;

import cn.stylefeng.mferp.bean.ReturnT;
import cn.stylefeng.mferp.modular.business.service.IDataCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yzr
 * @since 2020-05-18
 */
@Service
public class DataCenterServiceImpl implements IDataCenterService {

    @Value("${yzr.excel.export_path}")
    private String excelExportPath;

    /**
     * 查询生产记录
     */
    @Override
    public void getTjlb() {

    }

    /**
     * export table to excel
     *
     * @param request
     * @param id        DBinfo id
     * @param tableName table's name
     * @param sheetName
     * @return
     */
    @Override
    public ReturnT<String> exportToFile(HttpServletRequest request, Long id, String tableName, String sheetName) {
        return null;
    }

    @Override
    public ReturnT<List<Map<String, Object>>> searchTableFields(Long id, String tableName) {
        return null;
    }

    @Override
    public Map<String, Object> pageList(Long id, String tableName, int start, int length) {
        return null;
    }

    /**
     * open file
     *
     * @param filePath
     * @return
     */
    @Override
    public ReturnT<String> open(String filePath) {
        return null;
    }

    /**
     * @param filePath
     * @return
     */
    @Override
    public ReturnT<String> openDirectory(String filePath) {
        return null;
    }
}
