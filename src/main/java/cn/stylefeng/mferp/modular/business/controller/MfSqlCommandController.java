package cn.stylefeng.mferp.modular.business.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import cn.stylefeng.mferp.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import cn.stylefeng.mferp.modular.business.model.MfSqlCommand;
import cn.stylefeng.mferp.modular.business.service.IMfSqlCommandService;

/**
 * 命令信息控制器
 *
 * @author fengshuonan
 * @Date 2020-06-09 07:44:25
 */
@Controller
@RequestMapping("/mfSqlCommand")
public class MfSqlCommandController extends BaseController {

    private String PREFIX = "/business/mfSqlCommand/";

    @Autowired
    private IMfSqlCommandService mfSqlCommandService;

    /**
     * 跳转到命令信息首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "mfSqlCommand.html";
    }

    /**
     * 跳转到添加命令信息
     */
    @RequestMapping("/mfSqlCommand_add")
    public String mfSqlCommandAdd() {
        return PREFIX + "mfSqlCommand_add.html";
    }

    /**
     * 跳转到修改命令信息
     */
    @RequestMapping("/mfSqlCommand_update/{mfSqlCommandId}")
    public String mfSqlCommandUpdate(@PathVariable Integer mfSqlCommandId, Model model) {
        MfSqlCommand mfSqlCommand = mfSqlCommandService.selectById(mfSqlCommandId);
        model.addAttribute("item",mfSqlCommand);
        LogObjectHolder.me().set(mfSqlCommand);
        return PREFIX + "mfSqlCommand_edit.html";
    }

    /**
     * 获取命令信息列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return mfSqlCommandService.selectList(null);
    }

    /**
     * 新增命令信息
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(MfSqlCommand mfSqlCommand) {
        mfSqlCommandService.insert(mfSqlCommand);
        return SUCCESS_TIP;
    }

    /**
     * 删除命令信息
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer mfSqlCommandId) {
        mfSqlCommandService.deleteById(mfSqlCommandId);
        return SUCCESS_TIP;
    }

    /**
     * 修改命令信息
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(MfSqlCommand mfSqlCommand) {
        mfSqlCommandService.updateById(mfSqlCommand);
        return SUCCESS_TIP;
    }

    /**
     * 命令信息详情
     */
    @RequestMapping(value = "/detail/{mfSqlCommandId}")
    @ResponseBody
    public Object detail(@PathVariable("mfSqlCommandId") Integer mfSqlCommandId) {
        return mfSqlCommandService.selectById(mfSqlCommandId);
    }
}
