package cn.stylefeng.mferp.modular.business.service.impl;

import cn.stylefeng.mferp.modular.business.model.Tphbycl;
import cn.stylefeng.mferp.modular.business.dao.TphbyclMapper;
import cn.stylefeng.mferp.modular.business.service.ITphbyclService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yzr
 * @since 2020-06-14
 */
@Service
public class TphbyclServiceImpl extends ServiceImpl<TphbyclMapper, Tphbycl> implements ITphbyclService {

}
