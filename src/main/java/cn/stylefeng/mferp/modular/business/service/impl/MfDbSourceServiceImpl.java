package cn.stylefeng.mferp.modular.business.service.impl;

import cn.stylefeng.mferp.modular.business.model.MfDbSource;
import cn.stylefeng.mferp.modular.business.dao.MfDbSourceMapper;
import cn.stylefeng.mferp.modular.business.service.IMfDbSourceService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yzr
 * @since 2020-06-09
 */
@Service
public class MfDbSourceServiceImpl extends ServiceImpl<MfDbSourceMapper, MfDbSource> implements IMfDbSourceService {

}
