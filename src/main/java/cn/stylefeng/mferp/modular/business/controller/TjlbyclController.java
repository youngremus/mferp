package cn.stylefeng.mferp.modular.business.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import cn.stylefeng.mferp.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import cn.stylefeng.mferp.modular.business.model.Tjlbycl;
import cn.stylefeng.mferp.modular.business.service.ITjlbyclService;

/**
 * 生产记录原材料控制器
 *
 * @author fengshuonan
 * @Date 2020-05-27 23:27:58
 */
@Controller
@RequestMapping("/tjlbycl")
public class TjlbyclController extends BaseController {

    private String PREFIX = "/business/tjlbycl/";

    @Autowired
    private ITjlbyclService tjlbyclService;

    /**
     * 跳转到生产记录原材料首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "tjlbycl.html";
    }

    /**
     * 跳转到添加生产记录原材料
     */
    @RequestMapping("/tjlbycl_add")
    public String tjlbyclAdd() {
        return PREFIX + "tjlbycl_add.html";
    }

    /**
     * 跳转到修改生产记录原材料
     */
    @RequestMapping("/tjlbycl_update/{tjlbyclId}")
    public String tjlbyclUpdate(@PathVariable Integer tjlbyclId, Model model) {
        Tjlbycl tjlbycl = tjlbyclService.selectById(tjlbyclId);
        model.addAttribute("item",tjlbycl);
        LogObjectHolder.me().set(tjlbycl);
        return PREFIX + "tjlbycl_edit.html";
    }

    /**
     * 获取生产记录原材料列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return tjlbyclService.selectList(null);
    }

    /**
     * 新增生产记录原材料
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Tjlbycl tjlbycl) {
        tjlbyclService.insert(tjlbycl);
        return SUCCESS_TIP;
    }

    /**
     * 删除生产记录原材料
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer tjlbyclId) {
        tjlbyclService.deleteById(tjlbyclId);
        return SUCCESS_TIP;
    }

    /**
     * 修改生产记录原材料
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Tjlbycl tjlbycl) {
        tjlbyclService.updateById(tjlbycl);
        return SUCCESS_TIP;
    }

    /**
     * 生产记录原材料详情
     */
    @RequestMapping(value = "/detail/{tjlbyclId}")
    @ResponseBody
    public Object detail(@PathVariable("tjlbyclId") Integer tjlbyclId) {
        return tjlbyclService.selectById(tjlbyclId);
    }
}
