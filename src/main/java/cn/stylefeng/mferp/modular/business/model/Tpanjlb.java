package cn.stylefeng.mferp.modular.business.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * <p>
 * 每盘生产信息
 * </p>
 *
 * @author yzr
 * @since 2020-05-27
 */
@TableName("tpanjlb")
public class Tpanjlb extends Model<Tpanjlb> {

    private static final long serialVersionUID = 1L;

    /**
     * 小票号
     */
    private Integer Fno;
    /**
     * 状态
     */
    private String Fzt;
    /**
     * 盘号
     */
    private Integer Fpanno;
    /**
     * 生产日期
     */
    private String Fscrq;
    /**
     * 生产拌台
     */
    private String Fscbt;
    private String Fdcsj;
    private String Fccsj;
    /**
     * 任务单号
     */
    private Integer Frwdh;
    private Integer Fhtbh;
    private Integer Fkhbh;
    private String Fhtdw;
    private String Fgcmc;
    private String Fgcdz;
    private String Fjzbw;
    private String Fjzfs;
    private Double Fgls;
    private String Ftpz;
    private String Ftld;
    private Integer Fsgpb;
    private Integer Fsjpb;
    private String Fshch;
    private String Fsjxm;
    private Integer Fbcps;
    private Double Fbcfs;
    private Double Fzzl;
    private Double Fbsfs;
    private Double Fljfs;
    private Integer fljcs;
    private String Fccqf;
    private String Fyhqs;
    private String Fczy;
    private String Fbz;
    private String Frwno;
    private String Fhtno;
    private String Fgpfs;
    private String Fspsj;
    private Integer Fjbsj;
    private String Fbranchid;
    private String Fhddatetime;
    private String Fhdoperator;
    private Integer Fmzkg;
    private Integer Fpzkg;
    private Integer Fjzkg;
    private Integer frz;
    private Double Fflnum;
    private String Fclsj;
    private String Fsby;
    private String Fjby;
    private String Fgpsj;
    private String Frecid;
    private String Fphbno;
    private Integer Fglsid;
    private String Ftlb;
    private Double Fbcfsc;
    private Double Fbcfsm;
    private String Fpcbid;
    private String Fphbsj;
    private Double Fbpfs;
    /**
     * 开始配料时间
     */
    private String Fksplsj;
    /**
     * 配料结束时间
     */
    private String Fpljssj;
    /**
     * 开始计量时间：第一次投料时间
     */
    private String Fksjlsj;
    /**
     * 开始搅拌时间
     */
    private String Fksjbsj;
    /**
     * 搅拌机开门时间
     */
    private String Fjbjkmsj;
    /**
     * 搅拌机关门时间
     */
    private String Fjbjgmsj;
    /**
     * 工程编号
     */
    private String Fgcbh;
    /**
     * 监督号码
     */
    private String Fjdhm;
    /**
     * 上传状态
     */
    private Integer fsczt;
    private Integer Fxpno;

    public Integer getFno() {
        return Fno;
    }

    public void setFno(Integer fno) {
        Fno = fno;
    }

    public String getFzt() {
        return Fzt;
    }

    public void setFzt(String fzt) {
        Fzt = fzt;
    }

    public Integer getFpanno() {
        return Fpanno;
    }

    public void setFpanno(Integer fpanno) {
        Fpanno = fpanno;
    }

    public String getFscrq() {
        return Fscrq;
    }

    public void setFscrq(String fscrq) {
        Fscrq = fscrq;
    }

    public String getFscbt() {
        return Fscbt;
    }

    public void setFscbt(String fscbt) {
        Fscbt = fscbt;
    }

    public String getFdcsj() {
        return Fdcsj;
    }

    public void setFdcsj(String fdcsj) {
        Fdcsj = fdcsj;
    }

    public String getFccsj() {
        return Fccsj;
    }

    public void setFccsj(String fccsj) {
        Fccsj = fccsj;
    }

    public Integer getFrwdh() {
        return Frwdh;
    }

    public void setFrwdh(Integer frwdh) {
        Frwdh = frwdh;
    }

    public Integer getFhtbh() {
        return Fhtbh;
    }

    public void setFhtbh(Integer fhtbh) {
        Fhtbh = fhtbh;
    }

    public Integer getFkhbh() {
        return Fkhbh;
    }

    public void setFkhbh(Integer fkhbh) {
        Fkhbh = fkhbh;
    }

    public String getFhtdw() {
        return Fhtdw;
    }

    public void setFhtdw(String fhtdw) {
        Fhtdw = fhtdw;
    }

    public String getFgcmc() {
        return Fgcmc;
    }

    public void setFgcmc(String fgcmc) {
        Fgcmc = fgcmc;
    }

    public String getFgcdz() {
        return Fgcdz;
    }

    public void setFgcdz(String fgcdz) {
        Fgcdz = fgcdz;
    }

    public String getFjzbw() {
        return Fjzbw;
    }

    public void setFjzbw(String fjzbw) {
        Fjzbw = fjzbw;
    }

    public String getFjzfs() {
        return Fjzfs;
    }

    public void setFjzfs(String fjzfs) {
        Fjzfs = fjzfs;
    }

    public Double getFgls() {
        return Fgls;
    }

    public void setFgls(Double fgls) {
        Fgls = fgls;
    }

    public String getFtpz() {
        return Ftpz;
    }

    public void setFtpz(String ftpz) {
        Ftpz = ftpz;
    }

    public String getFtld() {
        return Ftld;
    }

    public void setFtld(String ftld) {
        Ftld = ftld;
    }

    public Integer getFsgpb() {
        return Fsgpb;
    }

    public void setFsgpb(Integer fsgpb) {
        Fsgpb = fsgpb;
    }

    public Integer getFsjpb() {
        return Fsjpb;
    }

    public void setFsjpb(Integer fsjpb) {
        Fsjpb = fsjpb;
    }

    public String getFshch() {
        return Fshch;
    }

    public void setFshch(String fshch) {
        Fshch = fshch;
    }

    public String getFsjxm() {
        return Fsjxm;
    }

    public void setFsjxm(String fsjxm) {
        Fsjxm = fsjxm;
    }

    public Integer getFbcps() {
        return Fbcps;
    }

    public void setFbcps(Integer fbcps) {
        Fbcps = fbcps;
    }

    public Double getFbcfs() {
        return Fbcfs;
    }

    public void setFbcfs(Double fbcfs) {
        Fbcfs = fbcfs;
    }

    public Double getFzzl() {
        return Fzzl;
    }

    public void setFzzl(Double fzzl) {
        Fzzl = fzzl;
    }

    public Double getFbsfs() {
        return Fbsfs;
    }

    public void setFbsfs(Double fbsfs) {
        Fbsfs = fbsfs;
    }

    public Double getFljfs() {
        return Fljfs;
    }

    public void setFljfs(Double fljfs) {
        Fljfs = fljfs;
    }

    public Integer getFljcs() {
        return fljcs;
    }

    public void setFljcs(Integer fljcs) {
        this.fljcs = fljcs;
    }

    public String getFccqf() {
        return Fccqf;
    }

    public void setFccqf(String fccqf) {
        Fccqf = fccqf;
    }

    public String getFyhqs() {
        return Fyhqs;
    }

    public void setFyhqs(String fyhqs) {
        Fyhqs = fyhqs;
    }

    public String getFczy() {
        return Fczy;
    }

    public void setFczy(String fczy) {
        Fczy = fczy;
    }

    public String getFbz() {
        return Fbz;
    }

    public void setFbz(String fbz) {
        Fbz = fbz;
    }

    public String getFrwno() {
        return Frwno;
    }

    public void setFrwno(String frwno) {
        Frwno = frwno;
    }

    public String getFhtno() {
        return Fhtno;
    }

    public void setFhtno(String fhtno) {
        Fhtno = fhtno;
    }

    public String getFgpfs() {
        return Fgpfs;
    }

    public void setFgpfs(String fgpfs) {
        Fgpfs = fgpfs;
    }

    public String getFspsj() {
        return Fspsj;
    }

    public void setFspsj(String fspsj) {
        Fspsj = fspsj;
    }

    public Integer getFjbsj() {
        return Fjbsj;
    }

    public void setFjbsj(Integer fjbsj) {
        Fjbsj = fjbsj;
    }

    public String getFbranchid() {
        return Fbranchid;
    }

    public void setFbranchid(String fbranchid) {
        Fbranchid = fbranchid;
    }

    public String getFhddatetime() {
        return Fhddatetime;
    }

    public void setFhddatetime(String fhddatetime) {
        Fhddatetime = fhddatetime;
    }

    public String getFhdoperator() {
        return Fhdoperator;
    }

    public void setFhdoperator(String fhdoperator) {
        Fhdoperator = fhdoperator;
    }

    public Integer getFmzkg() {
        return Fmzkg;
    }

    public void setFmzkg(Integer fmzkg) {
        Fmzkg = fmzkg;
    }

    public Integer getFpzkg() {
        return Fpzkg;
    }

    public void setFpzkg(Integer fpzkg) {
        Fpzkg = fpzkg;
    }

    public Integer getFjzkg() {
        return Fjzkg;
    }

    public void setFjzkg(Integer fjzkg) {
        Fjzkg = fjzkg;
    }

    public Integer getFrz() {
        return frz;
    }

    public void setFrz(Integer frz) {
        this.frz = frz;
    }

    public Double getFflnum() {
        return Fflnum;
    }

    public void setFflnum(Double fflnum) {
        Fflnum = fflnum;
    }

    public String getFclsj() {
        return Fclsj;
    }

    public void setFclsj(String fclsj) {
        Fclsj = fclsj;
    }

    public String getFsby() {
        return Fsby;
    }

    public void setFsby(String fsby) {
        Fsby = fsby;
    }

    public String getFjby() {
        return Fjby;
    }

    public void setFjby(String fjby) {
        Fjby = fjby;
    }

    public String getFgpsj() {
        return Fgpsj;
    }

    public void setFgpsj(String fgpsj) {
        Fgpsj = fgpsj;
    }

    public String getFrecid() {
        return Frecid;
    }

    public void setFrecid(String frecid) {
        Frecid = frecid;
    }

    public String getFphbno() {
        return Fphbno;
    }

    public void setFphbno(String fphbno) {
        Fphbno = fphbno;
    }

    public Integer getFglsid() {
        return Fglsid;
    }

    public void setFglsid(Integer fglsid) {
        Fglsid = fglsid;
    }

    public String getFtlb() {
        return Ftlb;
    }

    public void setFtlb(String ftlb) {
        Ftlb = ftlb;
    }

    public Double getFbcfsc() {
        return Fbcfsc;
    }

    public void setFbcfsc(Double fbcfsc) {
        Fbcfsc = fbcfsc;
    }

    public Double getFbcfsm() {
        return Fbcfsm;
    }

    public void setFbcfsm(Double fbcfsm) {
        Fbcfsm = fbcfsm;
    }

    public String getFpcbid() {
        return Fpcbid;
    }

    public void setFpcbid(String fpcbid) {
        Fpcbid = fpcbid;
    }

    public String getFphbsj() {
        return Fphbsj;
    }

    public void setFphbsj(String fphbsj) {
        Fphbsj = fphbsj;
    }

    public Double getFbpfs() {
        return Fbpfs;
    }

    public void setFbpfs(Double fbpfs) {
        Fbpfs = fbpfs;
    }

    public String getFksplsj() {
        return Fksplsj;
    }

    public void setFksplsj(String fksplsj) {
        Fksplsj = fksplsj;
    }

    public String getFpljssj() {
        return Fpljssj;
    }

    public void setFpljssj(String fpljssj) {
        Fpljssj = fpljssj;
    }

    public String getFksjlsj() {
        return Fksjlsj;
    }

    public void setFksjlsj(String fksjlsj) {
        Fksjlsj = fksjlsj;
    }

    public String getFksjbsj() {
        return Fksjbsj;
    }

    public void setFksjbsj(String fksjbsj) {
        Fksjbsj = fksjbsj;
    }

    public String getFjbjkmsj() {
        return Fjbjkmsj;
    }

    public void setFjbjkmsj(String fjbjkmsj) {
        Fjbjkmsj = fjbjkmsj;
    }

    public String getFjbjgmsj() {
        return Fjbjgmsj;
    }

    public void setFjbjgmsj(String fjbjgmsj) {
        Fjbjgmsj = fjbjgmsj;
    }

    public String getFgcbh() {
        return Fgcbh;
    }

    public void setFgcbh(String fgcbh) {
        Fgcbh = fgcbh;
    }

    public String getFjdhm() {
        return Fjdhm;
    }

    public void setFjdhm(String fjdhm) {
        Fjdhm = fjdhm;
    }

    public Integer getFsczt() {
        return fsczt;
    }

    public void setFsczt(Integer fsczt) {
        this.fsczt = fsczt;
    }

    public Integer getFxpno() {
        return Fxpno;
    }

    public void setFxpno(Integer fxpno) {
        Fxpno = fxpno;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }
}
