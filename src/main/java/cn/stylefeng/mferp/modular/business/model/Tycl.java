package cn.stylefeng.mferp.modular.business.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 原材料
 * </p>
 *
 * @author yzr
 * @since 2020-06-07
 */
@TableName("tycl")
public class Tycl extends Model<Tycl> {

    private static final long serialVersionUID = 1L;

    private String Fylmc;
    private String Fpzgg;
    private String Flb;
    /**
     * 原材料ID
     */
    private Integer Fyclid;
    /**
     * 最大量
     */
    private Double Fzdyl;
    /**
     * 最小量
     */
    private Double Fzxyl;
    private Integer Fplcw;
    /**
     * 状态
     */
    private String Fzt;
    /**
     * 备注
     */
    private String Fbz;
    private Double Ffdz;
    /**
     * 排列顺序
     */
    @TableField("FC1")
    private Integer fc1;
    @TableField("FC2")
    private Integer fc2;
    @TableField("FC3")
    private Integer fc3;
    private String Foperator;
    private String Ftimestamp;
    private Integer Fchecksum;
    private Integer Fcheckkey;
    private String Fylmcex;
    private String Fpzggex;
    @TableField("FA1")
    private String fa1;
    @TableField("FA2")
    private String fa2;
    /**
     * 原材料编码
     */
    @TableField("FA3")
    private String fa3;
    @TableField("FA4")
    private String fa4;
    private Integer Fseqa;
    private Integer Fseqb;
    private Integer Fseqc;
    private Integer Fxpno;
    private Date Fversion;
    private Double Fmaxratio;

    public String getFylmc() {
        return Fylmc;
    }

    public void setFylmc(String fylmc) {
        Fylmc = fylmc;
    }

    public String getFpzgg() {
        return Fpzgg;
    }

    public void setFpzgg(String fpzgg) {
        Fpzgg = fpzgg;
    }

    public String getFlb() {
        return Flb;
    }

    public void setFlb(String flb) {
        Flb = flb;
    }

    public Integer getFyclid() {
        return Fyclid;
    }

    public void setFyclid(Integer fyclid) {
        Fyclid = fyclid;
    }

    public Double getFzdyl() {
        return Fzdyl;
    }

    public void setFzdyl(Double fzdyl) {
        Fzdyl = fzdyl;
    }

    public Double getFzxyl() {
        return Fzxyl;
    }

    public void setFzxyl(Double fzxyl) {
        Fzxyl = fzxyl;
    }

    public Integer getFplcw() {
        return Fplcw;
    }

    public void setFplcw(Integer fplcw) {
        Fplcw = fplcw;
    }

    public String getFzt() {
        return Fzt;
    }

    public void setFzt(String fzt) {
        Fzt = fzt;
    }

    public String getFbz() {
        return Fbz;
    }

    public void setFbz(String fbz) {
        Fbz = fbz;
    }

    public Double getFfdz() {
        return Ffdz;
    }

    public void setFfdz(Double ffdz) {
        Ffdz = ffdz;
    }

    public Integer getFc1() {
        return fc1;
    }

    public void setFc1(Integer fc1) {
        this.fc1 = fc1;
    }

    public Integer getFc2() {
        return fc2;
    }

    public void setFc2(Integer fc2) {
        this.fc2 = fc2;
    }

    public Integer getFc3() {
        return fc3;
    }

    public void setFc3(Integer fc3) {
        this.fc3 = fc3;
    }

    public String getFoperator() {
        return Foperator;
    }

    public void setFoperator(String foperator) {
        Foperator = foperator;
    }

    public String getFtimestamp() {
        return Ftimestamp;
    }

    public void setFtimestamp(String ftimestamp) {
        Ftimestamp = ftimestamp;
    }

    public Integer getFchecksum() {
        return Fchecksum;
    }

    public void setFchecksum(Integer fchecksum) {
        Fchecksum = fchecksum;
    }

    public Integer getFcheckkey() {
        return Fcheckkey;
    }

    public void setFcheckkey(Integer fcheckkey) {
        Fcheckkey = fcheckkey;
    }

    public String getFylmcex() {
        return Fylmcex;
    }

    public void setFylmcex(String fylmcex) {
        Fylmcex = fylmcex;
    }

    public String getFpzggex() {
        return Fpzggex;
    }

    public void setFpzggex(String fpzggex) {
        Fpzggex = fpzggex;
    }

    public String getFa1() {
        return fa1;
    }

    public void setFa1(String fa1) {
        this.fa1 = fa1;
    }

    public String getFa2() {
        return fa2;
    }

    public void setFa2(String fa2) {
        this.fa2 = fa2;
    }

    public String getFa3() {
        return fa3;
    }

    public void setFa3(String fa3) {
        this.fa3 = fa3;
    }

    public String getFa4() {
        return fa4;
    }

    public void setFa4(String fa4) {
        this.fa4 = fa4;
    }

    public Integer getFseqa() {
        return Fseqa;
    }

    public void setFseqa(Integer fseqa) {
        Fseqa = fseqa;
    }

    public Integer getFseqb() {
        return Fseqb;
    }

    public void setFseqb(Integer fseqb) {
        Fseqb = fseqb;
    }

    public Integer getFseqc() {
        return Fseqc;
    }

    public void setFseqc(Integer fseqc) {
        Fseqc = fseqc;
    }

    public Integer getFxpno() {
        return Fxpno;
    }

    public void setFxpno(Integer fxpno) {
        Fxpno = fxpno;
    }

    public Date getFversion() {
        return Fversion;
    }

    public void setFversion(Date fversion) {
        Fversion = fversion;
    }

    public Double getFmaxratio() {
        return Fmaxratio;
    }

    public void setFmaxratio(Double fmaxratio) {
        Fmaxratio = fmaxratio;
    }

    @Override
    protected Serializable pkVal() {
        return this.Fylmc;
    }

    @Override
    public String toString() {
        return "Tycl{" +
        ", FYlmc=" + Fylmc +
        ", FPzgg=" + Fpzgg +
        ", FLb=" + Flb +
        ", FYclid=" + Fyclid +
        ", FZdyl=" + Fzdyl +
        ", FZxyl=" + Fzxyl +
        ", FPlcw=" + Fplcw +
        ", FZt=" + Fzt +
        ", FBz=" + Fbz +
        ", FFdz=" + Ffdz +
        ", fc1=" + fc1 +
        ", fc2=" + fc2 +
        ", fc3=" + fc3 +
        ", FOperator=" + Foperator +
        ", FTimeStamp=" + Ftimestamp +
        ", FCheckSum=" + Fchecksum +
        ", FCheckKey=" + Fcheckkey +
        ", FYlmcEx=" + Fylmcex +
        ", FPzggEx=" + Fpzggex +
        ", fa1=" + fa1 +
        ", fa2=" + fa2 +
        ", fa3=" + fa3 +
        ", fa4=" + fa4 +
        ", FSeqA=" + Fseqa +
        ", FSeqB=" + Fseqb +
        ", FSeqC=" + Fseqc +
        ", FXpNo=" + Fxpno +
        ", FVersion=" + Fversion +
        ", FMaxRatio=" + Fmaxratio +
        "}";
    }
}
