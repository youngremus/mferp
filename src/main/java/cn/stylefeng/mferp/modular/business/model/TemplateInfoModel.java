package cn.stylefeng.mferp.modular.business.model;

import lombok.Data;

import java.util.List;

@Data
public class TemplateInfoModel {
    private String templateName;
    private String templateType;
    private String templateNum;
    private String isDefault;
    private String enabledStatus;
    private String remark;
    private List<TyclTransfer> list;
}
