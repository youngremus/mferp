package cn.stylefeng.mferp.modular.business.dao;

import cn.stylefeng.mferp.modular.business.model.MfDbSource;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yzr
 * @since 2020-06-09
 */
public interface MfDbSourceMapper extends BaseMapper<MfDbSource> {

}
