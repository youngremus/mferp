/**
 * Copyright 2018-2020 stylefeng & fengshuonan (https://gitee.com/stylefeng)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.stylefeng.mferp.modular.business.warpper;

import cn.stylefeng.mferp.core.common.constant.factory.ConstantFactory;
import cn.stylefeng.mferp.modular.business.model.TemplateInfo;
import cn.stylefeng.mferp.modular.system.model.Dict;
import cn.stylefeng.roses.core.base.warpper.BaseControllerWrapper;
import cn.stylefeng.roses.core.util.ToolUtil;
import cn.stylefeng.roses.kernel.model.page.PageResult;
import com.baomidou.mybatisplus.plugins.Page;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 任务单的包装类
 *
 * @author fengshuonan
 * @date 2017年2月13日 下午10:47:03
 */
public class TemplateInfoWarpper extends BaseControllerWrapper {

    public TemplateInfoWarpper(Map<String, Object> single) {
        super(single);
    }

    public TemplateInfoWarpper(List<Map<String, Object>> multi) {
        super(multi);
    }

    public TemplateInfoWarpper(Page<Map<String, Object>> page) {
        super(page);
    }

    public TemplateInfoWarpper(PageResult<Map<String, Object>> pageResult) {
        super(pageResult);
    }

    @Override
    protected void wrapTheMap(Map<String, Object> map) {

    }

    public static List<TemplateInfo> wrap(List<TemplateInfo> list) {
        List<Dict> templateTypes = ConstantFactory.me().selectListByParentCode("template_type");
        List<Dict> isDefaults = ConstantFactory.me().selectListByParentCode("is_default");
        List<Dict> enabledStatus = ConstantFactory.me().selectListByParentCode("sys_state");

        for (int i =0; i < list.size(); i++) {
            TemplateInfo info = list.get(i);
            String templateType = info.getTemplateType();
            String isDefault = info.getIsDefault();
            String status = info.getEnabledStatus();
            if (ToolUtil.isEmpty(templateType)) {
                info.setTemplateType("--");
            } else {
                for(Dict dict : templateTypes) {
                    if(templateType.equals(dict.getCode())) {
                        info.setTemplateType(dict.getName());
                        break;
                    }
                }
            }
            if (ToolUtil.isEmpty(isDefault)) {
                info.setIsDefault("--");
            } else {
                for(Dict dict : isDefaults) {
                    if(isDefault.equals(dict.getCode())) {
                        info.setIsDefault(dict.getName());
                        break;
                    }
                }
            }
            if (ToolUtil.isEmpty(status)) {
                info.setEnabledStatus("--");
            } else {
                for(Dict dict : enabledStatus) {
                    if(status.equals(dict.getCode())) {
                        info.setEnabledStatus(dict.getName());
                        break;
                    }
                }
            }
        }
        return list;
    }
}
