package cn.stylefeng.mferp.modular.business.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.*;
import lombok.Data;
import org.apache.poi.ss.usermodel.FillPatternType;

/**
 * 资料室导出数据用
 * @author: yzr
 * @date: 2020/5/23
 */
@Data
// 头背景设置成红色 IndexedColors.RED.getIndex()
//@HeadStyle(fillPatternType = FillPatternType.SOLID_FOREGROUND, fillForegroundColor = 10)
// 头字体设置成20
@HeadFontStyle(fontHeightInPoints = 11)
// 内容的背景设置成绿色 IndexedColors.GREEN.getIndex()
//@ContentStyle(fillPatternType = FillPatternType.SOLID_FOREGROUND, fillForegroundColor = 17)
// 内容字体设置成20
@ContentFontStyle(fontHeightInPoints = 11)
public class TjlbModel {
    @ColumnWidth(12)
    @ExcelProperty("生产日期")
    private String fscrq;
    @ColumnWidth(20)
    @ExcelProperty("工程名称")
    private String fgcmc;
    @ColumnWidth(20)
    @ExcelProperty("施工部位")
    private String fjzbw;
    @ColumnWidth(18)
    @ExcelProperty("施工方式")
    private String fjzfs;
    @ColumnWidth(12)
    @ExcelProperty("强度等级")
    private String ftpz;
    @ColumnWidth(14)
    @ExcelProperty("配合比编号")
    private String fphbno;
    @ColumnWidth(12)
    @ExcelProperty("完成时间")
    private String fccsj;
    @ColumnWidth(10)
    @ExcelProperty("车号")
    private String fshch;
    @ColumnWidth(10)
    @ExcelProperty("方量")
    private String fbcfs;
    @ColumnWidth(10)
    @ExcelProperty("盘数")
    private String fbcps;
    @ColumnWidth(12)
    @ExcelProperty("任务单号")
    private String frwno;
    @ColumnWidth(20)
    @ExcelProperty("施工单位")
    private String fhtdw;
    @ColumnWidth(12)
    @ExcelProperty("联系电话")
    private String fbz;
    @ColumnWidth(12)
    @ExcelProperty("小票号")
    private String fno;
    @ColumnWidth(12)
    @ExcelProperty("流水号")
    private String ftbj;
}
