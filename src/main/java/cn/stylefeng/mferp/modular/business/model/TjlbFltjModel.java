package cn.stylefeng.mferp.modular.business.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentFontStyle;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import lombok.Data;

/**
 * 方量统计实体
 * 用于生产记录方量统计
 * 属性名称要小写
 * @author yzr
 */
@Data
@HeadFontStyle(fontHeightInPoints = 11)
@ContentFontStyle(fontHeightInPoints = 11)
public class TjlbFltjModel {
    @ColumnWidth(12)
    @ExcelProperty("日期")
    private String fscrq;
    @ColumnWidth(12)
    @ExcelProperty("生产拌台")
    private String fscbt;
    @ColumnWidth(12)
    @ExcelProperty("总方量")
    private String zfl;
    @ColumnWidth(12)
    @ExcelProperty("首车时间")
    private String fccsjMin;
    @ColumnWidth(12)
    @ExcelProperty("尾车时间")
    private String fccsjMax;


}
