package cn.stylefeng.mferp.modular.business.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentFontStyle;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import lombok.Data;

/**
 * 资料室导出数据用
 * @author: yzr
 * @date: 2020/5/23
 */
@Data
@HeadFontStyle(fontHeightInPoints = 11)
@ContentFontStyle(fontHeightInPoints = 11)
public class OffcutLogModel {

    @ColumnWidth(12)
    @ExcelProperty("车号")
    private String carNumber;
    @ColumnWidth(12)
    @ExcelProperty("剩料类型")
    private String offcutType;
    @ColumnWidth(12)
    @ExcelProperty("剩料方量")
    private String num;
    @ColumnWidth(12)
    @ExcelProperty("备注")
    private String remark;
    @ColumnWidth(12)
    @ExcelProperty("原小票")
    private String originNo;
    @ColumnWidth(12)
    @ExcelProperty("原单号")
    private String originOrderNo;
    @ColumnWidth(20)
    @ExcelProperty("原工地")
    private String originProjectName;
    @ColumnWidth(20)
    @ExcelProperty("原部位")
    private String originPourPosition;
    @ColumnWidth(12)
    @ExcelProperty("原品种")
    private String originConcreteType;
    @ColumnWidth(12)
    @ExcelProperty("原日期")
    private String originProductionDate;
    @ColumnWidth(12)
    @ExcelProperty("原时间")
    private String originProductionTime;
    @ColumnWidth(18)
    @ExcelProperty("原施工方法")
    private String originConstructionMethod;
    @ColumnWidth(12)
    @ExcelProperty("现小票")
    private String currentNo;
    @ColumnWidth(12)
    @ExcelProperty("现单号")
    private String currentOrderNo;
    @ColumnWidth(20)
    @ExcelProperty("现工地")
    private String currentProjectName;
    @ColumnWidth(20)
    @ExcelProperty("现部位")
    private String currentPourPosition;
    @ColumnWidth(12)
    @ExcelProperty("现品种")
    private String currentConcreteType;
    @ColumnWidth(12)
    @ExcelProperty("现日期")
    private String currentProductionDate;
    @ColumnWidth(12)
    @ExcelProperty("现时间")
    private String currentProductionTime;
    @ColumnWidth(18)
    @ExcelProperty("现施工方法")
    private String currentConstructionMethod;
}
