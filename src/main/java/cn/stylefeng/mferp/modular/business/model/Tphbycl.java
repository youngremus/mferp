package cn.stylefeng.mferp.modular.business.model;

import java.util.Date;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yzr
 * @since 2020-05-31
 */
@TableName("tphbycl")
public class Tphbycl extends Model<Tphbycl> {

    private static final long serialVersionUID = 1L;

    /**
     * 配合比号
     */
    private Integer fphbh;
    /**
     * 原材料名称
     */
    private String fylmc;
    /**
     * 品种规格
     */
    private String fpzgg;
    /**
     * 用量（kg）
     */
    private Double fsysl;
    /**
     * 仓位
     */
    private Integer fplcw;
    private Double fndpt;
    private Double fclpt;
    /**
     * 仓库编号
     */
    private String fckno;
    /**
     * 含水率
     */
    private Double fhsl;
    private Long fxpkey;
    private Date fversion;
    private Integer fxpno;

    public Integer getFphbh() {
        return fphbh;
    }

    public void setFphbh(Integer fphbh) {
        this.fphbh = fphbh;
    }

    public String getFylmc() {
        return fylmc;
    }

    public void setFylmc(String fylmc) {
        this.fylmc = fylmc;
    }

    public String getFpzgg() {
        return fpzgg;
    }

    public void setFpzgg(String fpzgg) {
        this.fpzgg = fpzgg;
    }

    public Double getFsysl() {
        return fsysl;
    }

    public void setFsysl(Double fsysl) {
        this.fsysl = fsysl;
    }

    public Integer getFplcw() {
        return fplcw;
    }

    public void setFplcw(Integer fplcw) {
        this.fplcw = fplcw;
    }

    public Double getFndpt() {
        return fndpt;
    }

    public void setFndpt(Double fndpt) {
        this.fndpt = fndpt;
    }

    public Double getFclpt() {
        return fclpt;
    }

    public void setFclpt(Double fclpt) {
        this.fclpt = fclpt;
    }

    public String getFckno() {
        return fckno;
    }

    public void setFckno(String fckno) {
        this.fckno = fckno;
    }

    public Double getFhsl() {
        return fhsl;
    }

    public void setFhsl(Double fhsl) {
        this.fhsl = fhsl;
    }

    public Long getFxpkey() {
        return fxpkey;
    }

    public void setFxpkey(Long fxpkey) {
        this.fxpkey = fxpkey;
    }

    public Date getFversion() {
        return fversion;
    }

    public void setFversion(Date fversion) {
        this.fversion = fversion;
    }

    public Integer getFxpno() {
        return fxpno;
    }

    public void setFxpno(Integer fxpno) {
        this.fxpno = fxpno;
    }

    @Override
    public String toString() {
        return "Tphbycl{" +
                "fphbh=" + fphbh +
                ", fylmc='" + fylmc + '\'' +
                ", fpzgg='" + fpzgg + '\'' +
                ", fsysl=" + fsysl +
                ", fplcw=" + fplcw +
                ", fndpt=" + fndpt +
                ", fclpt=" + fclpt +
                ", fckno='" + fckno + '\'' +
                ", fhsl=" + fhsl +
                ", fxpkey=" + fxpkey +
                ", fversion=" + fversion +
                ", fxpno=" + fxpno +
                '}';
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }
}
