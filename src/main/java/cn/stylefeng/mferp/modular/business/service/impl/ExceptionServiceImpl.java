package cn.stylefeng.mferp.modular.business.service.impl;

import cn.stylefeng.mferp.modular.business.model.Exception;
import cn.stylefeng.mferp.modular.business.dao.ExceptionMapper;
import cn.stylefeng.mferp.modular.business.service.IExceptionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 异常信息 服务实现类
 * </p>
 *
 * @author yzr
 * @since 2021-03-23
 */
@Service
public class ExceptionServiceImpl extends ServiceImpl<ExceptionMapper, Exception> implements IExceptionService {

}
