package cn.stylefeng.mferp.modular.business.service;

import cn.stylefeng.mferp.modular.business.model.TemplateInfo;
import cn.stylefeng.mferp.modular.business.model.TemplateInfoModel;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 模板信息 服务类
 * </p>
 *
 * @author yzr
 * @since 2020-06-28
 */
public interface ITemplateInfoService extends IService<TemplateInfo> {

    /**
     * 新增模板信息
     */
    void addtemplateInfo(TemplateInfoModel templateInfo);

    /**
     * 修改模板信息
     */
    void updateTemplateInfo(TemplateInfoModel templateInfo);

    /**
     * 获取模板列表
     */
    List<TemplateInfoModel> selectDownList();
}
