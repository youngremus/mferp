package cn.stylefeng.mferp.modular.business.service;

import cn.stylefeng.mferp.bean.ReturnT;
import cn.stylefeng.mferp.modular.business.model.SqlCommand;
import com.baomidou.mybatisplus.service.IService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yzr
 * @since 2020-05-18
 */
public interface IDataCenterService {

    /**
     * 查询生产记录
     */
    void getTjlb();

    /**
     * export table to excel
     * @param id DBinfo id
     * @param tableName table's name
     * @return
     */
    ReturnT<String> exportToFile(HttpServletRequest request, Long id, String tableName, String sheetName);

    ReturnT<List<Map<String, Object>>> searchTableFields(Long id, String tableName);

    Map<String, Object> pageList(Long id, String tableName, int start, int length);

    /**
     * open file
     * @param filePath
     * @return
     */
    ReturnT<String> open(String filePath);

    /**
     *
     * @param filePath
     * @return
     */
    ReturnT<String> openDirectory(String filePath);
}
