package cn.stylefeng.mferp.modular.business.model;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 模板数据信息
 * </p>
 *
 * @author yzr
 * @since 2020-06-28
 */
@TableName("template_data_info")
public class TemplateDataInfo extends Model<TemplateDataInfo> {

    private static final long serialVersionUID = 1L;

    /**
     * 序号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 数据名称
     */
    @TableField("data_name")
    private String dataName;
    /**
     * 数据类型
     */
    @TableField("data_type")
    private String dataType;
    /**
     * 数据信息
     */
    @TableField("data_value")
    private String dataValue;
    /**
     * 模板编号
     */
    @TableField("template_num")
    private String templateNum;
    /**
     * 中文名称
     */
    @TableField("data_cn_name")
    private String dataCnName;
    /**
     * 排序
     */
    private Integer sort;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDataName() {
        return dataName;
    }

    public void setDataName(String dataName) {
        this.dataName = dataName;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDataValue() {
        return dataValue;
    }

    public void setDataValue(String dataValue) {
        this.dataValue = dataValue;
    }

    public String getTemplateNum() {
        return templateNum;
    }

    public void setTemplateNum(String templateNum) {
        this.templateNum = templateNum;
    }

    public String getDataCnName() {
        return dataCnName;
    }

    public void setDataCnName(String dataCnName) {
        this.dataCnName = dataCnName;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TemplateDataInfo{" +
        ", id=" + id +
        ", dataName=" + dataName +
        ", dataType=" + dataType +
        ", dataValue=" + dataValue +
        ", templateNum=" + templateNum +
        ", dataCnName=" + dataCnName +
        ", sort=" + sort +
        "}";
    }
}
