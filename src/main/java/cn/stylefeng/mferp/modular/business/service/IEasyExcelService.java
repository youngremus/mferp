package cn.stylefeng.mferp.modular.business.service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yzr
 * @since 2020-06-27
 */
public interface IEasyExcelService {
    void download(HttpServletResponse response, List list, Class head, String name, String excelType) throws IOException;
}
