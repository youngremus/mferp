package cn.stylefeng.mferp.modular.business.service;

import cn.stylefeng.mferp.modular.business.model.BulkDensityStandard;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yzr
 * @since 2020-07-26
 */
public interface IBulkDensityStandardService extends IService<BulkDensityStandard> {

}
