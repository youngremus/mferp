package cn.stylefeng.mferp.modular.business.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 异常信息
 * </p>
 *
 * @author yzr
 * @since 2021-03-23
 */
@TableName("sys_exception")
public class Exception extends Model<Exception> {

    private static final long serialVersionUID = 1L;

    /**
     * 序号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 备注
     */
    private String memo;
    /**
     * ip地址
     */
    private String ip;
    /**
     * 类名
     */
    @TableField("class_name")
    private String className;
    /**
     * 方法名
     */
    @TableField("method_name")
    private String methodName;
    /**
     * 异常类型
     */
    @TableField("exception_type")
    private String exceptionType;
    /**
     * 异常详情
     */
    @TableField("exception_detail")
    private String exceptionDetail;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getExceptionType() {
        return exceptionType;
    }

    public void setExceptionType(String exceptionType) {
        this.exceptionType = exceptionType;
    }

    public String getExceptionDetail() {
        return exceptionDetail;
    }

    public void setExceptionDetail(String exceptionDetail) {
        this.exceptionDetail = exceptionDetail;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Exception{" +
        ", id=" + id +
        ", memo=" + memo +
        ", ip=" + ip +
        ", className=" + className +
        ", methodName=" + methodName +
        ", exceptionType=" + exceptionType +
        ", exceptionDetail=" + exceptionDetail +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
