package cn.stylefeng.mferp.modular.business.controller;

import cn.stylefeng.mferp.modular.business.listener.UploadTphbListener;
import cn.stylefeng.mferp.modular.business.model.TphbModel;
import cn.stylefeng.mferp.modular.business.warpper.TphbWarpper;
import cn.stylefeng.roses.core.base.controller.BaseController;
import com.alibaba.excel.EasyExcel;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import cn.stylefeng.mferp.core.log.LogObjectHolder;
import cn.stylefeng.mferp.modular.business.model.Tphb;
import cn.stylefeng.mferp.modular.business.service.ITphbService;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 生产配合比控制器
 *
 * @author fengshuonan
 * @Date 2020-05-31 20:45:48
 */
@Controller
@RequestMapping("/tphb")
public class TphbController extends BaseController {

    private String PREFIX = "/business/tphb/";

    @Autowired
    private ITphbService tphbService;

    /**
     * 跳转到生产配合比首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "tphb.html";
    }

    /**
     * 跳转到添加生产配合比
     */
    @RequestMapping("/tphb_add")
    public String tphbAdd() {
        return PREFIX + "tphb_add.html";
    }

    /**
     * 跳转到修改生产配合比
     */
    @RequestMapping("/tphb_update/{tphbId}")
    public String tphbUpdate(@PathVariable Integer tphbId, Model model) {
        Tphb tphb = tphbService.selectById(tphbId);
        model.addAttribute("item",tphb);
        LogObjectHolder.me().set(tphb);
        return PREFIX + "tphb_edit.html";
    }

    /**
     * 获取生产配合比列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return tphbService.selectTphbs();
    }

    /**
     * 获取生产配合比列表
     */
    @RequestMapping(value = "/getDownList")
    @ResponseBody
    public Object getDownList(String condition) {
        List<Tphb> list = tphbService.selectDownList();
        return list;
    }

    /**
     * 新增生产配合比
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Tphb tphb) {
        tphbService.insert(tphb);
        return SUCCESS_TIP;
    }

    /**
     * 删除生产配合比
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer tphbId) {
        tphbService.deleteById(tphbId);
        return SUCCESS_TIP;
    }

    /**
     * 修改生产配合比
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Tphb tphb) {
        tphbService.updateById(tphb);
        return SUCCESS_TIP;
    }

    /**
     * 生产配合比详情
     */
    @RequestMapping(value = "/detail/{tphbId}")
    @ResponseBody
    public Object detail(@PathVariable("tphbId") Integer tphbId) {
        return tphbService.selectById(tphbId);
    }

    @PostMapping("/import")
    @ResponseBody
    public Object upload(MultipartFile file) throws IOException {
        EasyExcel.read(file.getInputStream(), TphbModel.class, new UploadTphbListener(tphbService)).sheet().doRead();
        return SUCCESS_TIP;
    }
}
