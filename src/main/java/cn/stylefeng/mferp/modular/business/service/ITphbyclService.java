package cn.stylefeng.mferp.modular.business.service;

import cn.stylefeng.mferp.modular.business.model.Tphbycl;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yzr
 * @since 2020-06-14
 */
public interface ITphbyclService extends IService<Tphbycl> {

}
