package cn.stylefeng.mferp.modular.business.dao;

import cn.stylefeng.mferp.modular.business.model.Exception;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 异常信息 Mapper 接口
 * </p>
 *
 * @author yzr
 * @since 2021-03-23
 */
public interface ExceptionMapper extends BaseMapper<Exception> {

}
