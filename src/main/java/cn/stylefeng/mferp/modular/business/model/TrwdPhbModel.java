package cn.stylefeng.mferp.modular.business.model;

/**
 * @author: yzr
 * @date: 2020/5/31
 */
public class TrwdPhbModel {
    private static final long serialVersionUID = 1L;

    private Integer Fid;

    private String Frwdh;

    private String Ftzsj;

    private String Ftpz;

    private String Fjzbw;

    public Integer getFid() {
        return Fid;
    }

    public void setFid(Integer Fid) {
        this.Fid = Fid;
    }

    public String getFrwdh() {
        return Frwdh;
    }

    public void setFrwdh(String frwdh) {
        Frwdh = frwdh;
    }

    public String getFtzsj() {
        return Ftzsj;
    }

    public void setFtzsj(String ftzsj) {
        Ftzsj = ftzsj;
    }

    public String getFtpz() {
        return Ftpz;
    }

    public void setFtpz(String ftpz) {
        Ftpz = ftpz;
    }

    public String getFjzbw() {
        return Fjzbw;
    }

    public void setFjzbw(String fjzbw) {
        Fjzbw = fjzbw;
    }
}
