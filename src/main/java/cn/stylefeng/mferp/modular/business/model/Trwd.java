package cn.stylefeng.mferp.modular.business.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 任务单
 * </p>
 *
 * @author yzr
 * @since 2020-05-27
 */
@TableName("trwd")
public class Trwd extends Model<Trwd> {

    private static final long serialVersionUID = 1L;

    /**
     * 任务单号
     */
    @TableId(value = "Frwdh", type = IdType.AUTO)
    private Integer Frwdh;
    /**
     * 合同编号
     */
    private String Fhtbh;
    /**
     * 任务性质
     */
    private String Frwxz;
    /**
     * 状态
     */
    private String Fzt;
    /**
     * 单位名称
     */
    private String Fhtdw;
    /**
     * 工程名称
     */
    private String Fgcmc;
    /**
     * 施工部位
     */
    private String Fjzbw;
    /**
     * 泵送
     */
    private String Fjzfs;
    /**
     * 施工地点
     */
    private String Fgcdz;
    /**
     * 运距
     */
    private Double Fgls;
    /**
     * 计划日期
     */
    private String Fjhrq;
    /**
     * 砼品种
     */
    private String Ftpz;
    /**
     * 坍落度
     */
    private String Ftld;
    /**
     * 水泥品种
     */
    private String Fsnbh;
    /**
     * 石子规格
     */
    private String Fszgg;
    /**
     * 备注|渗料规格|外加剂规格|抗渗等级|其他要求|施工单位联系人|工程编号|营销部门联系人|监督号码|优先采用的仓库编号|监控系统工地号|质检员
     * 13599156937|/|||||13599156937|||||
     */
    private String Ftbj;
    /**
     * 计划方量
     */
    private Double Fjhsl;
    /**
     * 生产拌台
     */
    private String Fscbt;
    /**
     * 完成方量/砼数量
     */
    private Double Fwcsl;
    /**
     * 累计车数
     */
    private Integer Fljcs;
    /**
     * 操作员
     */
    private String Fczy;
    /**
     * 登录日期
     */
    private String Fdlrq;
    /**
     * 施工配合比号
     */
    private Integer Fsgpb;
    /**
     * 砂浆配合号
     */
    private Integer Fsjpb = 0;
    private Integer Fxph1 = 0;
    private Integer Fxph2 = 0;
    private Double Fkzsl = 0d;
    private Double Fkzdj = 0d;
    private Double Fkzje = 0d;
    private Double Fqrtsl = 0d;
    private Double Fqrtdj = 0d;
    private Double Fqrtje = 0d;
    private Double Fqrbssl = 0d;
    private Double Fqrbsdj = 0d;
    private Double Fqrbsje = 0d;
    private Double Fqrkzsl = 0d;
    private Double Fqrkzdj = 0d;
    private Double Fqrkzje = 0d;
    private Double Fqrzjje = 0d;
    private Double Ftdj = 0d;
    private Double Ftje = 0d;
    private Double Fbssl = 0d;
    private Double Fbsdj = 0d;
    private Double Fbsje = 0d;
    private Double Fzje = 0d;

    private String Frwno;
    private String Fhtno;

    /**
     * 配合比号
     */
    private String Fphbno;
    private Integer Fjbsj = 0;
    private Integer Fclsjno = 0;
    private Double Fzzl = 0d;
    private Integer Fc1 = 0;
    private Integer Fc2 = 0;
    private Integer Fc3 = 0;
    private Integer Fchecksum = 0;
    private Integer Fcheckkey = 0;
    private Integer Fmpfsp = 0;
    private String Frz = "0";
    private Double Fqrzzl = 0d;
    private Double Frva = 0d;
    private Double Frvb = 0d;
    private Double Frvc = 0d;
    private Double Fqva = 0d;
    private Double Fqvb = 0d;
    private Double Fqvc = 0d;
    private Integer Fiv0 = 0;
    private Double Fwyzfs = 0d;
    private Integer Fwyzcs = 0;
    private Date Fversion = new Date();
    private Integer Fxpno = 0;
    /**
     * 5146262355334334200
     */
    private Long Fxpkey;
    private Date Fxgsj = new Date();
    private Date updatetime = new Date();

    public Integer getFrwdh() {
        return Frwdh;
    }

    public void setFrwdh(Integer frwdh) {
        Frwdh = frwdh;
    }

    public String getFhtbh() {
        return Fhtbh;
    }

    public void setFhtbh(String fhtbh) {
        Fhtbh = fhtbh;
    }

    public String getFrwxz() {
        return Frwxz;
    }

    public void setFrwxz(String frwxz) {
        Frwxz = frwxz;
    }

    public String getFzt() {
        return Fzt;
    }

    public void setFzt(String fzt) {
        Fzt = fzt;
    }

    public String getFhtdw() {
        return Fhtdw;
    }

    public void setFhtdw(String fhtdw) {
        Fhtdw = fhtdw;
    }

    public String getFgcmc() {
        return Fgcmc;
    }

    public void setFgcmc(String fgcmc) {
        Fgcmc = fgcmc;
    }

    public String getFjzbw() {
        return Fjzbw;
    }

    public void setFjzbw(String fjzbw) {
        Fjzbw = fjzbw;
    }

    public String getFjzfs() {
        return Fjzfs;
    }

    public void setFjzfs(String fjzfs) {
        Fjzfs = fjzfs;
    }

    public String getFgcdz() {
        return Fgcdz;
    }

    public void setFgcdz(String fgcdz) {
        Fgcdz = fgcdz;
    }

    public Double getFgls() {
        return Fgls;
    }

    public void setFgls(Double fgls) {
        Fgls = fgls;
    }

    public String getFjhrq() {
        return Fjhrq;
    }

    public void setFjhrq(String fjhrq) {
        Fjhrq = fjhrq;
    }

    public String getFtpz() {
        return Ftpz;
    }

    public void setFtpz(String ftpz) {
        Ftpz = ftpz;
    }

    public String getFtld() {
        return Ftld;
    }

    public void setFtld(String ftld) {
        Ftld = ftld;
    }

    public String getFsnbh() {
        return Fsnbh;
    }

    public void setFsnbh(String Fsnbh) {
        this.Fsnbh = Fsnbh;
    }

    public String getFszgg() {
        return Fszgg;
    }

    public void setFszgg(String fszgg) {
        Fszgg = fszgg;
    }

    public String getFtbj() {
        return Ftbj;
    }

    public void setFtbj(String ftbj) {
        Ftbj = ftbj;
    }

    public Double getFjhsl() {
        return Fjhsl;
    }

    public void setFjhsl(Double fjhsl) {
        Fjhsl = fjhsl;
    }

    public String getFscbt() {
        return Fscbt;
    }

    public void setFscbt(String fscbt) {
        Fscbt = fscbt;
    }

    public Double getFwcsl() {
        return Fwcsl;
    }

    public void setFwcsl(Double fwcsl) {
        Fwcsl = fwcsl;
    }

    public Integer getFljcs() {
        return Fljcs;
    }

    public void setFljcs(Integer fljcs) {
        Fljcs = fljcs;
    }

    public String getFczy() {
        return Fczy;
    }

    public void setFczy(String fczy) {
        Fczy = fczy;
    }

    public String getFdlrq() {
        return Fdlrq;
    }

    public void setFdlrq(String fdlrq) {
        Fdlrq = fdlrq;
    }

    public Integer getFsgpb() {
        return Fsgpb;
    }

    public void setFsgpb(Integer fsgpb) {
        Fsgpb = fsgpb;
    }

    public Integer getFsjpb() {
        return Fsjpb;
    }

    public void setFsjpb(Integer fsjpb) {
        Fsjpb = fsjpb;
    }

    public String getFrwno() {
        return Frwno;
    }

    public void setFrwno(String frwno) {
        Frwno = frwno;
    }

    public String getFphbno() {
        return Fphbno;
    }

    public void setFphbno(String fphbno) {
        Fphbno = fphbno;
    }

    public Integer getFxph1() {
        return Fxph1;
    }

    public void setFxph1(Integer fxph1) {
        Fxph1 = fxph1;
    }

    public Integer getFxph2() {
        return Fxph2;
    }

    public void setFxph2(Integer fxph2) {
        Fxph2 = fxph2;
    }

    public Double getFkzsl() {
        return Fkzsl;
    }

    public void setFkzsl(Double fkzsl) {
        Fkzsl = fkzsl;
    }

    public Double getFkzdj() {
        return Fkzdj;
    }

    public void setFkzdj(Double fkzdj) {
        Fkzdj = fkzdj;
    }

    public Double getFkzje() {
        return Fkzje;
    }

    public void setFkzje(Double fkzje) {
        Fkzje = fkzje;
    }

    public Double getFqrtsl() {
        return Fqrtsl;
    }

    public void setFqrtsl(Double fqrtsl) {
        Fqrtsl = fqrtsl;
    }

    public Double getFqrtdj() {
        return Fqrtdj;
    }

    public void setFqrtdj(Double fqrtdj) {
        Fqrtdj = fqrtdj;
    }

    public Double getFqrtje() {
        return Fqrtje;
    }

    public void setFqrtje(Double fqrtje) {
        Fqrtje = fqrtje;
    }

    public Double getFqrbssl() {
        return Fqrbssl;
    }

    public void setFqrbssl(Double fqrbssl) {
        Fqrbssl = fqrbssl;
    }

    public Double getFqrbsdj() {
        return Fqrbsdj;
    }

    public void setFqrbsdj(Double fqrbsdj) {
        Fqrbsdj = fqrbsdj;
    }

    public Double getFqrbsje() {
        return Fqrbsje;
    }

    public void setFqrbsje(Double fqrbsje) {
        Fqrbsje = fqrbsje;
    }

    public Double getFqrkzsl() {
        return Fqrkzsl;
    }

    public void setFqrkzsl(Double fqrkzsl) {
        Fqrkzsl = fqrkzsl;
    }

    public Double getFqrkzdj() {
        return Fqrkzdj;
    }

    public void setFqrkzdj(Double fqrkzdj) {
        Fqrkzdj = fqrkzdj;
    }

    public Double getFqrkzje() {
        return Fqrkzje;
    }

    public void setFqrkzje(Double fqrkzje) {
        Fqrkzje = fqrkzje;
    }

    public Double getFqrzjje() {
        return Fqrzjje;
    }

    public void setFqrzjje(Double fqrzjje) {
        Fqrzjje = fqrzjje;
    }

    public Double getFtdj() {
        return Ftdj;
    }

    public void setFtdj(Double ftdj) {
        Ftdj = ftdj;
    }

    public Double getFtje() {
        return Ftje;
    }

    public void setFtje(Double ftje) {
        Ftje = ftje;
    }

    public Double getFbssl() {
        return Fbssl;
    }

    public void setFbssl(Double fbssl) {
        Fbssl = fbssl;
    }

    public Double getFbsdj() {
        return Fbsdj;
    }

    public void setFbsdj(Double fbsdj) {
        Fbsdj = fbsdj;
    }

    public Double getFbsje() {
        return Fbsje;
    }

    public void setFbsje(Double fbsje) {
        Fbsje = fbsje;
    }

    public Double getFzje() {
        return Fzje;
    }

    public void setFzje(Double fzje) {
        Fzje = fzje;
    }

    public String getFhtno() {
        return Fhtno;
    }

    public void setFhtno(String fhtno) {
        Fhtno = fhtno;
    }

    public Integer getFjbsj() {
        return Fjbsj;
    }

    public void setFjbsj(Integer fjbsj) {
        Fjbsj = fjbsj;
    }

    public Integer getFclsjno() {
        return Fclsjno;
    }

    public void setFclsjno(Integer fclsjno) {
        Fclsjno = fclsjno;
    }

    public Double getFzzl() {
        return Fzzl;
    }

    public void setFzzl(Double fzzl) {
        Fzzl = fzzl;
    }

    public Integer getFc1() {
        return Fc1;
    }

    public void setFc1(Integer fc1) {
        Fc1 = fc1;
    }

    public Integer getFc2() {
        return Fc2;
    }

    public void setFc2(Integer fc2) {
        Fc2 = fc2;
    }

    public Integer getFc3() {
        return Fc3;
    }

    public void setFc3(Integer fc3) {
        Fc3 = fc3;
    }

    public Integer getFchecksum() {
        return Fchecksum;
    }

    public void setFchecksum(Integer fchecksum) {
        Fchecksum = fchecksum;
    }

    public Integer getFcheckkey() {
        return Fcheckkey;
    }

    public void setFcheckkey(Integer fcheckkey) {
        Fcheckkey = fcheckkey;
    }

    public Integer getFmpfsp() {
        return Fmpfsp;
    }

    public void setFmpfsp(Integer fmpfsp) {
        Fmpfsp = fmpfsp;
    }

    public String getFrz() {
        return Frz;
    }

    public void setFrz(String frz) {
        Frz = frz;
    }

    public Double getFqrzzl() {
        return Fqrzzl;
    }

    public void setFqrzzl(Double fqrzzl) {
        Fqrzzl = fqrzzl;
    }

    public Double getFrva() {
        return Frva;
    }

    public void setFrva(Double frva) {
        Frva = frva;
    }

    public Double getFrvb() {
        return Frvb;
    }

    public void setFrvb(Double frvb) {
        Frvb = frvb;
    }

    public Double getFrvc() {
        return Frvc;
    }

    public void setFrvc(Double frvc) {
        Frvc = frvc;
    }

    public Double getFqva() {
        return Fqva;
    }

    public void setFqva(Double fqva) {
        Fqva = fqva;
    }

    public Double getFqvb() {
        return Fqvb;
    }

    public void setFqvb(Double fqvb) {
        Fqvb = fqvb;
    }

    public Double getFqvc() {
        return Fqvc;
    }

    public void setFqvc(Double fqvc) {
        Fqvc = fqvc;
    }

    public Integer getFiv0() {
        return Fiv0;
    }

    public void setFiv0(Integer fiv0) {
        Fiv0 = fiv0;
    }

    public Double getFwyzfs() {
        return Fwyzfs;
    }

    public void setFwyzfs(Double fwyzfs) {
        Fwyzfs = fwyzfs;
    }

    public Integer getFwyzcs() {
        return Fwyzcs;
    }

    public void setFwyzcs(Integer fwyzcs) {
        Fwyzcs = fwyzcs;
    }

    public Date getFversion() {
        return Fversion;
    }

    public void setFversion(Date fversion) {
        Fversion = fversion;
    }

    public Integer getFxpno() {
        return Fxpno;
    }

    public void setFxpno(Integer fxpno) {
        Fxpno = fxpno;
    }

    public Long getFxpkey() {
        return Fxpkey;
    }

    public void setFxpkey(Long fxpkey) {
        Fxpkey = fxpkey;
    }

    public Date getFxgsj() {
        return Fxgsj;
    }

    public void setFxgsj(Date fxgsj) {
        Fxgsj = fxgsj;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    @Override
    public String toString() {
        return "Trwd{" +
        ", FRwdh=" + Frwdh +
        ", FRwxz=" + Frwxz +
        ", FZt=" + Fzt +
        ", FHtdw=" + Fhtdw +
        ", FGcmc=" + Fgcmc +
        ", FJzbw=" + Fjzbw +
        ", FJzfs=" + Fjzfs +
        ", FGcdz=" + Fgcdz +
        ", FGls=" + Fgls +
        ", FJhrq=" + Fjhrq +
        ", FTpz=" + Ftpz +
        ", FTld=" + Ftld +
        ", FSnbh=" + Fsnbh +
        ", FSzgg=" + Fszgg +
        ", FTbj=" + Ftbj +
        ", FJhsl=" + Fjhsl +
        ", FScbt=" + Fscbt +
        ", FWcsl=" + Fwcsl +
        ", FLjcs=" + Fljcs +
        ", FCzy=" + Fczy +
        ", FDlrq=" + Fdlrq +
        ", FSgpb=" + Fsgpb +
        ", FRwno=" + Frwno +
        ", FPhbNo=" + Fphbno +
        "}";
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }
}
