package cn.stylefeng.mferp.modular.business.service.impl;

import cn.stylefeng.mferp.modular.business.model.BulkDensityStandard;
import cn.stylefeng.mferp.modular.business.dao.BulkDensityStandardMapper;
import cn.stylefeng.mferp.modular.business.service.IBulkDensityStandardService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yzr
 * @since 2020-07-26
 */
@Service
public class BulkDensityStandardServiceImpl extends ServiceImpl<BulkDensityStandardMapper, BulkDensityStandard> implements IBulkDensityStandardService {

}
