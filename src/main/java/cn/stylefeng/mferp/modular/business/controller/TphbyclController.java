package cn.stylefeng.mferp.modular.business.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import cn.stylefeng.mferp.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import cn.stylefeng.mferp.modular.business.model.Tphbycl;
import cn.stylefeng.mferp.modular.business.service.ITphbyclService;

/**
 * 控制器
 *
 * @author fengshuonan
 * @Date 2020-06-14 19:49:56
 */
@Controller
@RequestMapping("/tphbycl")
public class TphbyclController extends BaseController {

    private String PREFIX = "/business/tphbycl/";

    @Autowired
    private ITphbyclService tphbyclService;

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "tphbycl.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/tphbycl_add")
    public String tphbyclAdd() {
        return PREFIX + "tphbycl_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/tphbycl_update/{tphbyclId}")
    public String tphbyclUpdate(@PathVariable Integer tphbyclId, Model model) {
        Tphbycl tphbycl = tphbyclService.selectById(tphbyclId);
        model.addAttribute("item",tphbycl);
        LogObjectHolder.me().set(tphbycl);
        return PREFIX + "tphbycl_edit.html";
    }

    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return tphbyclService.selectList(null);
    }

    /**
     * 新增
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Tphbycl tphbycl) {
        tphbyclService.insert(tphbycl);
        return SUCCESS_TIP;
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer tphbyclId) {
        tphbyclService.deleteById(tphbyclId);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Tphbycl tphbycl) {
        tphbyclService.updateById(tphbycl);
        return SUCCESS_TIP;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{tphbyclId}")
    @ResponseBody
    public Object detail(@PathVariable("tphbyclId") Integer tphbyclId) {
        return tphbyclService.selectById(tphbyclId);
    }
}
