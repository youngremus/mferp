package cn.stylefeng.mferp.modular.business.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 任务单
 * </p>
 *
 * @author yzr
 * @since 2020-05-27
 */
@Data
public class TrwdSaveModel {

    private static final long serialVersionUID = 1L;

    /**
     * 任务单号
     */
    private Integer Frwdh;
    /**
     * 合同编号
     */
    private String Fhtbh;
    /**
     * 任务性质
     */
    private String Frwxz;
    /**
     * 状态
     */
    private String Fzt;
    /**
     * 单位名称
     */
    private String Fhtdw;
    /**
     * 工程名称
     */
    private String Fgcmc;
    /**
     * 施工部位
     */
    private String Fjzbw;
    /**
     * 泵送
     */
    private String Fjzfs;
    /**
     * 施工地点
     */
    private String Fgcdz;
    /**
     * 运距
     */
    private Double Fgls;
    /**
     * 计划日期
     */
    private String Fjhrq;
    /**
     * 砼品种
     */
    private String Ftpz;
    /**
     * 坍落度
     */
    private String Ftld;
    /**
     * 水泥品种
     */
    private String Fsnbh;
    /**
     * 石子规格
     */
    private String Fszgg;
    /**
     * 备注|渗料规格|外加剂规格|抗渗等级|其他要求|施工单位联系人|工程编号|营销部门联系人|监督号码|优先采用的仓库编号|监控系统工地号|质检员
     * 13599156937|/|||||13599156937|||||
     */
    private String Ftbj;
    /**
     * 计划方量
     */
    private Double Fjhsl;
    /**
     * 生产拌台
     */
    private String Fscbt;
    /**
     * 完成方量/砼数量
     */
    private Double Fwcsl;
    /**
     * 累计车数
     */
    private Integer Fljcs;
    /**
     * 操作员
     */
    private String Fczy;
    /**
     * 登录日期
     */
    private String Fdlrq;
    /**
     * 施工配合比号
     */
    private Integer Fsgpb;
    /**
     * 砂浆配合号
     */
    private Integer Fsjpb = 0;

    private String Frwno;
    private String Fhtno;

    /**
     * 配合比号
     */
    private String Fphbno;

    /**
     * 工地电话
     */
    private String gddh;
    /**
     * 资料要求
     */
    private String zlyq;
}
