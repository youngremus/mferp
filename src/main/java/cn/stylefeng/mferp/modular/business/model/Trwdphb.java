package cn.stylefeng.mferp.modular.business.model;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 任务单施工配比
 * 任务单配方调整记录表
 * </p>
 *
 * @author yzr
 * @since 2020-05-27
 */
@TableName("trwdphb")
public class Trwdphb extends Model<Trwdphb> {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "Fid", type = IdType.AUTO)
    private Integer Fid;
    /**
     * 任务单号
     */
    private Integer Frwdh;
    /**
     * 调整时间
     */
    private String Ftzsj;
    /**
     * 类别
     */
    private String Fpblx;
    /**
     * 调整前配比编号
     */
    private Integer Fpbh1;
    /**
     * 调整后配比编号
     */
    private Integer Fpbh2;
    private Integer Fxpbh;
    /**
     * 累计车数
     */
    private Integer Fljcs;
    /**
     * 操作员
     */
    private String Fczy;
    /**
     * 备注
     */
    private String Fbz;
    private Integer Fno;
    /*试验员*/
    private String Fsyy;

    private String Fbz1;

    public Integer getFid() {
        return Fid;
    }

    public void setFid(Integer fid) {
        Fid = fid;
    }

    public Integer getFrwdh() {
        return Frwdh;
    }

    public void setFrwdh(Integer frwdh) {
        Frwdh = frwdh;
    }

    public String getFtzsj() {
        return Ftzsj;
    }

    public void setFtzsj(String ftzsj) {
        Ftzsj = ftzsj;
    }

    public String getFpblx() {
        return Fpblx;
    }

    public void setFpblx(String fpblx) {
        Fpblx = fpblx;
    }

    public Integer getFpbh1() {
        return Fpbh1;
    }

    public void setFpbh1(Integer fpbh1) {
        Fpbh1 = fpbh1;
    }

    public Integer getFpbh2() {
        return Fpbh2;
    }

    public void setFpbh2(Integer fpbh2) {
        Fpbh2 = fpbh2;
    }

    public Integer getFxpbh() {
        return Fxpbh;
    }

    public void setFxpbh(Integer fxpbh) {
        Fxpbh = fxpbh;
    }

    public Integer getFljcs() {
        return Fljcs;
    }

    public void setFljcs(Integer fljcs) {
        Fljcs = fljcs;
    }

    public String getFczy() {
        return Fczy;
    }

    public void setFczy(String fczy) {
        Fczy = fczy;
    }

    public String getFbz() {
        return Fbz;
    }

    public void setFbz(String fbz) {
        Fbz = fbz;
    }

    public Integer getFno() {
        return Fno;
    }

    public void setFno(Integer fno) {
        Fno = fno;
    }

    public String getFsyy() {
        return Fsyy;
    }

    public void setFsyy(String fsyy) {
        Fsyy = fsyy;
    }

    @Override
    protected Serializable pkVal() {
        return this.Fid;
    }

    @Override
    public String toString() {
        return "Trwdphb{" +
                "Fid=" + Fid +
                ", Frwdh=" + Frwdh +
                ", Ftzsj='" + Ftzsj + '\'' +
                ", Fpblx='" + Fpblx + '\'' +
                ", Fpbh1=" + Fpbh1 +
                ", Fpbh2=" + Fpbh2 +
                ", Fxpbh=" + Fxpbh +
                ", Fljcs=" + Fljcs +
                ", Fczy='" + Fczy + '\'' +
                ", Fsyy='" + Fsyy + '\'' +
                ", Fbz='" + Fbz + '\'' +
                ", Fno=" + Fno +
                '}';
    }

    public String getFbz1() {
        return Fbz1;
    }

    public void setFbz1(String fbz1) {
        Fbz1 = fbz1;
    }
}
