package cn.stylefeng.mferp.modular.business.controller;

import cn.stylefeng.mferp.modular.business.model.SqlCommand;
import cn.stylefeng.mferp.modular.business.service.IDataCenterService;
import cn.stylefeng.roses.core.base.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author: yzr
 * @date: 2020/5/18
 */
@Controller
@RequestMapping("/dataCenter")
public class DataCenterController extends BaseController {
    private String PREFIX = "/business/dataCenter/";

    @Autowired
    private IDataCenterService dataCenterService;

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "dataCenter.html";
    }

    /**
     * 查询生产记录
     */
    @RequestMapping(value = "/getTjlb")
    @ResponseBody
    public Object getTjlb() {
        dataCenterService.getTjlb();
        return SUCCESS_TIP;
    }
}
