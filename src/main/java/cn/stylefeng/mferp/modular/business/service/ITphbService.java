package cn.stylefeng.mferp.modular.business.service;

import cn.stylefeng.mferp.modular.business.model.Tphb;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yzr
 * @since 2020-05-31
 */
public interface ITphbService extends IService<Tphb> {

    List<Tphb> selectDownList();

    /**
     * 获取生产配合比列表
     * @return
     */
    List<Tphb> selectTphbs();
}
