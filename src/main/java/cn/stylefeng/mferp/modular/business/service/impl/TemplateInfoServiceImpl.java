package cn.stylefeng.mferp.modular.business.service.impl;

import cn.stylefeng.mferp.core.util.DateUtil;
import cn.stylefeng.mferp.core.util.ReflectUtil;
import cn.stylefeng.mferp.modular.business.model.*;
import cn.stylefeng.mferp.modular.business.dao.TemplateInfoMapper;
import cn.stylefeng.mferp.modular.business.service.ITemplateDataInfoService;
import cn.stylefeng.mferp.modular.business.service.ITemplateInfoService;
import cn.stylefeng.mferp.modular.business.service.ITyclService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 模板信息 服务实现类
 * </p>
 *
 * @author yzr
 * @since 2020-06-28
 */
@Service
public class TemplateInfoServiceImpl extends ServiceImpl<TemplateInfoMapper, TemplateInfo> implements ITemplateInfoService {

    @Autowired
    private ITyclService tyclService;

    @Autowired
    private ITemplateDataInfoService templateDataInfoService;

    @Autowired
    private TemplateInfoMapper templateInfoMapper;

    /**
     * 新增模板信息
     *
     * @param templateInfo
     */
    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void addtemplateInfo(TemplateInfoModel templateInfo) {
        if(StringUtils.isBlank(templateInfo.getTemplateNum())) {
            templateInfo.setTemplateNum("tp"+ DateUtil.getDateSerial());
        }
        if(StringUtils.isBlank(templateInfo.getEnabledStatus())) {
            templateInfo.setEnabledStatus("1");
        }
        if(StringUtils.isBlank(templateInfo.getIsDefault())) {
            templateInfo.setIsDefault("1");
        }
        TemplateInfo info = new TemplateInfo();
        ReflectUtil.copyValue(templateInfo, info);
        info.setCreateTime(DateUtil.getCurrentTime());
        insert(info);
        List<TyclTransfer> list = templateInfo.getList();
        Integer sort = 1;
        for(TyclTransfer tt : list) {
            String value  = tt.getValue();
            Tycl tycl = tyclService.getTyclById(Integer.valueOf(value));
            if(tycl == null) continue;
            TemplateDataInfo dataInfo = new TemplateDataInfo();
            dataInfo.setDataCnName(tycl.getFylmc());
            dataInfo.setDataName(tycl.getFpzgg());
            dataInfo.setDataValue(String.valueOf(tycl.getFyclid()));
            dataInfo.setDataType(tycl.getFlb());
            dataInfo.setTemplateNum(templateInfo.getTemplateNum());
            dataInfo.setSort(sort);
            templateDataInfoService.insert(dataInfo);
            sort++;
        }
    }

    /**
     * 修改模板信息
     */
    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void updateTemplateInfo(TemplateInfoModel templateInfo) {
        TemplateInfo info = new TemplateInfo();
        ReflectUtil.copyValue(templateInfo, info);
        info.setCreateTime(null);
        info.setTemplateNum(null);
        info.setUpdateTime(DateUtil.getCurrentTime());
        Wrapper<TemplateInfo> wrapper = new EntityWrapper<>();
        wrapper.eq("template_num", templateInfo.getTemplateNum());
        update(info, wrapper);

        Wrapper<TemplateDataInfo> wrapper1 = new EntityWrapper<>();
        wrapper1.eq("template_num", templateInfo.getTemplateNum());
        templateDataInfoService.delete(wrapper1);
        List<TyclTransfer> list = templateInfo.getList();
        Integer sort = 1;
        for(TyclTransfer tt : list) {
            String value  = tt.getValue();
            Tycl tycl = tyclService.getTyclById(Integer.valueOf(value));
            if(tycl == null) continue;
            TemplateDataInfo dataInfo = new TemplateDataInfo();
            dataInfo.setDataCnName(tycl.getFylmc());
            dataInfo.setDataName(tycl.getFpzgg());
            dataInfo.setDataValue(String.valueOf(tycl.getFyclid()));
            dataInfo.setDataType(tycl.getFlb());
            dataInfo.setTemplateNum(templateInfo.getTemplateNum());
            dataInfo.setSort(sort);
            templateDataInfoService.insert(dataInfo);
            sort++;
        }
    }

    /**
     * 获取模板列表
     */
    @Override
    public List<TemplateInfoModel> selectDownList() {
        return templateInfoMapper.selectDownList();
    }
}
