package cn.stylefeng.mferp.modular.business.service.impl;

import cn.stylefeng.mferp.modular.business.model.MfSqlCommand;
import cn.stylefeng.mferp.modular.business.dao.MfSqlCommandMapper;
import cn.stylefeng.mferp.modular.business.service.IMfSqlCommandService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yzr
 * @since 2020-06-09
 */
@Service
public class MfSqlCommandServiceImpl extends ServiceImpl<MfSqlCommandMapper, MfSqlCommand> implements IMfSqlCommandService {

}
