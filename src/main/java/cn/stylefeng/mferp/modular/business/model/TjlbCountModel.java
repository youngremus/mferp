package cn.stylefeng.mferp.modular.business.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentFontStyle;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import lombok.Data;

/**
 * 资料室导出数据用
 * @author: yzr
 * @date: 2020/5/23
 */
@Data
// 头背景设置成红色 IndexedColors.RED.getIndex()
//@HeadStyle(fillPatternType = FillPatternType.SOLID_FOREGROUND, fillForegroundColor = 10)
// 头字体设置成20
@HeadFontStyle(fontHeightInPoints = 11)
// 内容的背景设置成绿色 IndexedColors.GREEN.getIndex()
//@ContentStyle(fillPatternType = FillPatternType.SOLID_FOREGROUND, fillForegroundColor = 17)
// 内容字体设置成20
@ContentFontStyle(fontHeightInPoints = 11)
public class TjlbCountModel {
    @ColumnWidth(12)
    @ExcelProperty("生产日期")
    private String fscrq;
    @ColumnWidth(14)
    @ExcelProperty("任务单号")
    private String frwdh;
    @ColumnWidth(25)
    @ExcelProperty("工程名称")
    private String fgcmc;
    @ColumnWidth(20)
    @ExcelProperty("施工单位")
    private String fhtdw;
    @ColumnWidth(25)
    @ExcelProperty("施工部位")
    private String fjzbw;
    @ColumnWidth(15)
    @ExcelProperty("施工方式")
    private String fjzfs;
    @ColumnWidth(15)
    @ExcelProperty("强度等级")
    private String ftpz;
    @ColumnWidth(15)
    @ExcelProperty("配合比编号")
    private String fphbno;
    @ColumnWidth(10)
    @ExcelProperty("方量")
    private String fbcfs;
    @ColumnWidth(10)
    @ExcelProperty("总盘数")
    private String fbcps;
    @ColumnWidth(10)
    @ExcelProperty("首盘方量")
    private String fbz;
    @ColumnWidth(10)
    @ExcelProperty("首盘车号")
    private String fshch;
    @ColumnWidth(10)
    @ExcelProperty("首盘时间")
    private String fccsj;
    @ColumnWidth(12)
    @ExcelProperty("流水号")
    private String ftbj;
}
