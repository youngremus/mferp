package cn.stylefeng.mferp.modular.business.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import cn.stylefeng.mferp.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import cn.stylefeng.mferp.modular.business.model.TemplateDataInfo;
import cn.stylefeng.mferp.modular.business.service.ITemplateDataInfoService;

/**
 * 模板数据信息控制器
 *
 * @author fengshuonan
 * @Date 2020-06-28 22:29:28
 */
@Controller
@RequestMapping("/templateDataInfo")
public class TemplateDataInfoController extends BaseController {

    private String PREFIX = "/business/templateDataInfo/";

    @Autowired
    private ITemplateDataInfoService templateDataInfoService;

    /**
     * 跳转到模板数据信息首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "templateDataInfo.html";
    }

    /**
     * 跳转到添加模板数据信息
     */
    @RequestMapping("/templateDataInfo_add")
    public String templateDataInfoAdd() {
        return PREFIX + "templateDataInfo_add.html";
    }

    /**
     * 跳转到修改模板数据信息
     */
    @RequestMapping("/templateDataInfo_update/{templateDataInfoId}")
    public String templateDataInfoUpdate(@PathVariable Integer templateDataInfoId, Model model) {
        TemplateDataInfo templateDataInfo = templateDataInfoService.selectById(templateDataInfoId);
        model.addAttribute("item",templateDataInfo);
        LogObjectHolder.me().set(templateDataInfo);
        return PREFIX + "templateDataInfo_edit.html";
    }

    /**
     * 获取模板数据信息列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return templateDataInfoService.selectList(null);
    }

    /**
     * 新增模板数据信息
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(TemplateDataInfo templateDataInfo) {
        templateDataInfoService.insert(templateDataInfo);
        return SUCCESS_TIP;
    }

    /**
     * 删除模板数据信息
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer templateDataInfoId) {
        templateDataInfoService.deleteById(templateDataInfoId);
        return SUCCESS_TIP;
    }

    /**
     * 修改模板数据信息
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(TemplateDataInfo templateDataInfo) {
        templateDataInfoService.updateById(templateDataInfo);
        return SUCCESS_TIP;
    }

    /**
     * 模板数据信息详情
     */
    @RequestMapping(value = "/detail/{templateDataInfoId}")
    @ResponseBody
    public Object detail(@PathVariable("templateDataInfoId") Integer templateDataInfoId) {
        return templateDataInfoService.selectById(templateDataInfoId);
    }
}
