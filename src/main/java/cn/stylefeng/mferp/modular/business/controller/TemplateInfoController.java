package cn.stylefeng.mferp.modular.business.controller;

import cn.stylefeng.mferp.core.util.DateUtil;
import cn.stylefeng.mferp.core.util.ReflectUtil;
import cn.stylefeng.mferp.modular.business.constant.Constants;
import cn.stylefeng.mferp.modular.business.dao.TyclMapper;
import cn.stylefeng.mferp.modular.business.model.*;
import cn.stylefeng.mferp.modular.business.service.ITemplateDataInfoService;
import cn.stylefeng.mferp.modular.business.service.ITyclService;
import cn.stylefeng.mferp.modular.business.warpper.TemplateInfoWarpper;
import cn.stylefeng.roses.core.base.controller.BaseController;
import cn.stylefeng.roses.core.reqres.response.ErrorResponseData;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import io.swagger.models.auth.In;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import cn.stylefeng.mferp.core.log.LogObjectHolder;
import cn.stylefeng.mferp.modular.business.service.ITemplateInfoService;

import java.util.ArrayList;
import java.util.List;

/**
 * 模板信息控制器
 *
 * @author fengshuonan
 * @Date 2020-06-28 22:28:53
 */
@Controller
@RequestMapping("/templateInfo")
public class TemplateInfoController extends BaseController {

    private String PREFIX = "/business/templateInfo/";

    @Autowired
    private ITemplateInfoService templateInfoService;

    @Autowired
    private ITyclService tyclService;

    @Autowired
    private ITemplateDataInfoService templateDataInfoService;

    /**
     * 跳转到模板信息首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "templateInfo.html";
    }

    /**
     * 跳转到添加模板信息
     */
    @RequestMapping("/templateInfo_add")
    public String templateInfoAdd() {
        return PREFIX + "templateInfo_add.html";
    }

    /**
     * 跳转到修改模板信息
     */
    @RequestMapping("/templateInfo_update/{templateInfoId}")
    public String templateInfoUpdate(@PathVariable Integer templateInfoId, Model model) {
        TemplateInfo templateInfo = templateInfoService.selectById(templateInfoId);
        String templateNum = templateInfo.getTemplateNum();
        Wrapper<TemplateDataInfo> wrapper = new EntityWrapper<>();
        wrapper.eq("template_num", templateNum);
        List<String> orderByList = new ArrayList<>();
        orderByList.add("sort");
        wrapper.orderAsc(orderByList);
        List<TemplateDataInfo> list = templateDataInfoService.selectList(wrapper);
        StringBuilder builder = new StringBuilder();
        for(TemplateDataInfo info : list) {
            builder.append(String.valueOf(info.getDataValue()) + ",");
        }
        model.addAttribute("item",templateInfo);
        model.addAttribute("chooseItem", builder.deleteCharAt(builder.length()-1));
        LogObjectHolder.me().set(templateInfo);
        return PREFIX + "templateInfo_edit.html";
    }

    /**
     * 获取模板信息列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Wrapper<TemplateInfo> wrapper = new EntityWrapper<>();
        wrapper.like("template_num", condition);
        List<TemplateInfo> list = templateInfoService.selectList(wrapper);
        return TemplateInfoWarpper.wrap(list);
    }

    /**
     * 新增模板信息
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(@RequestBody TemplateInfoModel templateInfo) {
        if(templateInfo == null || StringUtils.isBlank(templateInfo.getTemplateName()) ||
                (templateInfo.getList() == null || templateInfo.getList().size() <= 0)) {
            return new ErrorResponseData(Constants.BUSINESS_TEMPLATEINFO_INCOMPLETE_DATA);
        }
        templateInfoService.addtemplateInfo(templateInfo);
        return SUCCESS_TIP;
    }

    /**
     * 删除模板信息
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer templateInfoId) {
        TemplateInfo templateInfo = templateInfoService.selectById(templateInfoId);
        if(templateInfoService.deleteById(templateInfoId)) {
            Wrapper<TemplateDataInfo> warpper = new EntityWrapper<>();
            warpper.eq("template_num", templateInfo.getTemplateNum());
            templateDataInfoService.delete(warpper);
            return SUCCESS_TIP;
        }
        return new ErrorResponseData(Constants.BUSINESS_TEMPLATEDATAINFO_DELETE_FAILED);
    }

    /**
     * 修改模板信息
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(@RequestBody TemplateInfoModel templateInfo) {
        if(templateInfo == null || StringUtils.isBlank(templateInfo.getTemplateName()) ||
                (templateInfo.getList() == null || templateInfo.getList().size() <= 0)) {
            return new ErrorResponseData(Constants.BUSINESS_TEMPLATEINFO_INCOMPLETE_DATA);
        }
        templateInfoService.updateTemplateInfo(templateInfo);
        return SUCCESS_TIP;
    }

    /**
     * 模板信息详情
     */
    @RequestMapping(value = "/detail/{templateInfoId}")
    @ResponseBody
    public Object detail(@PathVariable("templateInfoId") Integer templateInfoId) {
        return templateInfoService.selectById(templateInfoId);
    }

    /**
     * 获取模板列表
     */
    @RequestMapping(value = "/getDownList")
    @ResponseBody
    public Object getDownList(String condition) {
        List<TemplateInfoModel> list = templateInfoService.selectDownList();
        return list;
    }
}
