package cn.stylefeng.mferp.modular.business.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yzr
 * @since 2020-07-26
 */
@TableName("bulk_density_standard")
public class BulkDensityStandard extends Model<BulkDensityStandard> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 最小容重
     */
    @TableField("min_bulk_density")
    private BigDecimal minBulkDensity;
    /**
     * 最大容重
     */
    @TableField("max_bulk_density")
    private BigDecimal maxBulkDensity;
    /**
     * 强度值
     */
    @TableField("strength_value")
    private Integer strengthValue;
    /**
     * 强度等级
     */
    private String strength;
    /**
     * 是否默认
     */
    @TableField("is_default")
    private String isDefault;
    /**
     * 是否启用
     */
    @TableField("enabled_status")
    private String enabledStatus;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private String createTime;
    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getMinBulkDensity() {
        return minBulkDensity;
    }

    public void setMinBulkDensity(BigDecimal minBulkDensity) {
        this.minBulkDensity = minBulkDensity;
    }

    public BigDecimal getMaxBulkDensity() {
        return maxBulkDensity;
    }

    public void setMaxBulkDensity(BigDecimal maxBulkDensity) {
        this.maxBulkDensity = maxBulkDensity;
    }

    public Integer getStrengthValue() {
        return strengthValue;
    }

    public void setStrengthValue(Integer strengthValue) {
        this.strengthValue = strengthValue;
    }

    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

    public String getEnabledStatus() {
        return enabledStatus;
    }

    public void setEnabledStatus(String enabledStatus) {
        this.enabledStatus = enabledStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "BulkDensityStandard{" +
        ", id=" + id +
        ", minBulkDensity=" + minBulkDensity +
        ", maxBulkDensity=" + maxBulkDensity +
        ", strengthValue=" + strengthValue +
        ", strength=" + strength +
        ", isDefault=" + isDefault +
        ", enabledStatus=" + enabledStatus +
        ", remark=" + remark +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
