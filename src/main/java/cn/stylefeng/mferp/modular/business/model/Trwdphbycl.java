package cn.stylefeng.mferp.modular.business.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 任务单施工配比原材料
 * 当前任务单在所在生产线的配方记录
 * </p>
 *
 * @author yzr
 * @since 2020-05-27
 */
@TableName("trwdphbycl")
public class Trwdphbycl extends Model<Trwdphbycl> {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "Fid", type = IdType.AUTO)
    private Integer Fid;
    /**
     * 任务单号
     */
    private Integer Frwdh;
    /**
     * 类别
     */
    private Integer Fpblb;
    /**
     * 材料名称
     */
    private String Fylmc;
    /**
     * 材料规格
     */
    private String Fpzgg;
    /**
     * 用量
     */
    private Double Fsysl;
    /**
     * 生产线
     */
    private Integer Fbtid;
    /**
     * 仓位
     */
    private Integer Fplcw;
    private Double FndPt;
    private Double FclPt;
    private Double Faddnum;
    private Double Fpbsl;
    private Double Fsl;
    @TableField("Fpb_Num")
    private Double FpbNum;
    /**
     * 仓库编号
     */
    private String Fckno;
    /**
     * 含水率
     */
    private Double Fhsl;
    private Long FxpKey;
    private String Fversion;
    private Float FBigSandAdd;
    private Float FProValue;
    private Float FSandRate;
    private Float FStoneRate;
    private Float FWaterRate;
    private Integer FOrder;

    public Integer getFid() {
        return Fid;
    }

    public void setFid(Integer fid) {
        Fid = fid;
    }

    public Integer getFrwdh() {
        return Frwdh;
    }

    public void setFrwdh(Integer frwdh) {
        Frwdh = frwdh;
    }

    public Integer getFpblb() {
        return Fpblb;
    }

    public void setFpblb(Integer fpblb) {
        Fpblb = fpblb;
    }

    public String getFylmc() {
        return Fylmc;
    }

    public void setFylmc(String fylmc) {
        Fylmc = fylmc;
    }

    public String getFpzgg() {
        return Fpzgg;
    }

    public void setFpzgg(String fpzgg) {
        Fpzgg = fpzgg;
    }

    public Double getFsysl() {
        return Fsysl;
    }

    public void setFsysl(Double fsysl) {
        Fsysl = fsysl;
    }

    public Integer getFplcw() {
        return Fplcw;
    }

    public void setFplcw(Integer fplcw) {
        Fplcw = fplcw;
    }

    public Double getFndPt() {
        return FndPt;
    }

    public void setFndPt(Double fndPt) {
        FndPt = fndPt;
    }

    public Double getFclPt() {
        return FclPt;
    }

    public void setFclPt(Double fclPt) {
        FclPt = fclPt;
    }

    public Double getFaddnum() {
        return Faddnum;
    }

    public void setFaddnum(Double faddnum) {
        Faddnum = faddnum;
    }

    public Double getFpbsl() {
        return Fpbsl;
    }

    public void setFpbsl(Double fpbsl) {
        Fpbsl = fpbsl;
    }

    public Double getFsl() {
        return Fsl;
    }

    public void setFsl(Double fsl) {
        Fsl = fsl;
    }

    public Double getFpbNum() {
        return FpbNum;
    }

    public void setFpbNum(Double fpbNum) {
        FpbNum = fpbNum;
    }

    public Integer getFbtid() {
        return Fbtid;
    }

    public void setFbtid(Integer fbtid) {
        Fbtid = fbtid;
    }

    public String getFckno() {
        return Fckno;
    }

    public void setFckno(String fckno) {
        Fckno = fckno;
    }

    public Double getFhsl() {
        return Fhsl;
    }

    public void setFhsl(Double fhsl) {
        Fhsl = fhsl;
    }

    public Long getFxpKey() {
        return FxpKey;
    }

    public void setFxpKey(Long fxpKey) {
        FxpKey = fxpKey;
    }

    public String getFversion() {
        return Fversion;
    }

    public void setFversion(String fversion) {
        Fversion = fversion;
    }

    public Float getFBigSandAdd() {
        return FBigSandAdd;
    }

    public void setFBigSandAdd(Float FBigSandAdd) {
        this.FBigSandAdd = FBigSandAdd;
    }

    public Float getFProValue() {
        return FProValue;
    }

    public void setFProValue(Float FProValue) {
        this.FProValue = FProValue;
    }

    public Float getFSandRate() {
        return FSandRate;
    }

    public void setFSandRate(Float FSandRate) {
        this.FSandRate = FSandRate;
    }

    public Float getFStoneRate() {
        return FStoneRate;
    }

    public void setFStoneRate(Float FStoneRate) {
        this.FStoneRate = FStoneRate;
    }

    public Float getFWaterRate() {
        return FWaterRate;
    }

    public void setFWaterRate(Float FWaterRate) {
        this.FWaterRate = FWaterRate;
    }

    public Integer getFOrder() {
        return FOrder;
    }

    public void setFOrder(Integer FOrder) {
        this.FOrder = FOrder;
    }

    @Override
    protected Serializable pkVal() {
        return this.Fid;
    }

    @Override
    public String toString() {
        return "Trwdphbycl{" +
                "Fid=" + Fid +
                ", Frwdh=" + Frwdh +
                ", Fpblb=" + Fpblb +
                ", Fylmc='" + Fylmc + '\'' +
                ", Fpzgg='" + Fpzgg + '\'' +
                ", Fsysl=" + Fsysl +
                ", Fplcw=" + Fplcw +
                ", FndPt=" + FndPt +
                ", FclPt=" + FclPt +
                ", Faddnum=" + Faddnum +
                ", Fpbsl=" + Fpbsl +
                ", Fsl=" + Fsl +
                ", FpbNum=" + FpbNum +
                ", FbtId=" + Fbtid +
                ", Fckno='" + Fckno + '\'' +
                ", Fhsl=" + Fhsl +
                ", FxpKey=" + FxpKey +
                ", Fversion=" + Fversion +
                ", FBigSandAdd=" + FBigSandAdd +
                ", FProValue=" + FProValue +
                ", FSandRate=" + FSandRate +
                ", FStoneRate=" + FStoneRate +
                ", FWaterRate=" + FWaterRate +
                ", FOrder=" + FOrder +
                '}';
    }
}
