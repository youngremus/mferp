package cn.stylefeng.mferp.modular.business.controller;

import cn.stylefeng.mferp.config.RedisUtils;
import cn.stylefeng.mferp.core.common.constant.cache.Cache;
import cn.stylefeng.mferp.core.common.constant.cache.CacheKey;
import cn.stylefeng.mferp.core.util.DateUtil;
import cn.stylefeng.mferp.modular.business.constant.Constants;
import cn.stylefeng.mferp.modular.business.warpper.BulkDensityStandardWarpper;
import cn.stylefeng.roses.core.base.controller.BaseController;
import cn.stylefeng.roses.core.reqres.response.ErrorResponseData;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import cn.stylefeng.mferp.core.log.LogObjectHolder;
import cn.stylefeng.mferp.modular.business.model.BulkDensityStandard;
import cn.stylefeng.mferp.modular.business.service.IBulkDensityStandardService;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 容重标准控制器
 *
 * @author fengshuonan
 * @Date 2020-07-26 16:59:02
 */
@Controller
@RequestMapping("/bulkDensityStandard")
public class BulkDensityStandardController extends BaseController {

    private String PREFIX = "/business/bulkDensityStandard/";

    @Autowired
    private IBulkDensityStandardService bulkDensityStandardService;

    /**
     * 跳转到容重标准首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "bulkDensityStandard.html";
    }

    /**
     * 跳转到添加容重标准
     */
    @RequestMapping("/bulkDensityStandard_add")
    public String bulkDensityStandardAdd() {
        return PREFIX + "bulkDensityStandard_add.html";
    }

    /**
     * 跳转到修改容重标准
     */
    @RequestMapping("/bulkDensityStandard_update/{bulkDensityStandardId}")
    public String bulkDensityStandardUpdate(@PathVariable Integer bulkDensityStandardId, Model model) {
        BulkDensityStandard bulkDensityStandard = bulkDensityStandardService.selectById(bulkDensityStandardId);
        model.addAttribute("item",bulkDensityStandard);
        LogObjectHolder.me().set(bulkDensityStandard);
        return PREFIX + "bulkDensityStandard_edit.html";
    }

    /**
     * 获取容重标准列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Wrapper<BulkDensityStandard> wrapper = new EntityWrapper<>();
        wrapper.like("strength", condition);
        List<BulkDensityStandard> bulkDensityStandards = bulkDensityStandardService.selectList(wrapper);
        return BulkDensityStandardWarpper.wrap(bulkDensityStandards);
    }

    /**
     * 新增容重标准
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(@RequestBody BulkDensityStandard bulkDensityStandard) {
        if(bulkDensityStandard == null || bulkDensityStandard.getStrengthValue() == null ||
                bulkDensityStandard.getMinBulkDensity() == null || bulkDensityStandard.getMaxBulkDensity() == null) {
            return new ErrorResponseData(Constants.BUSINESS_BULK_DENSITY_STANDARD_INCOMPLETE_DATA);
        }
        bulkDensityStandard.setCreateTime(DateUtil.getCurrDate());
        bulkDensityStandardService.insert(bulkDensityStandard);
        return SUCCESS_TIP;
    }

    /**
     * 删除容重标准
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    @CacheEvict(value = Cache.CONSTANT, allEntries = true)
    public Object delete(@RequestParam Integer bulkDensityStandardId) {
        bulkDensityStandardService.deleteById(bulkDensityStandardId);
        return SUCCESS_TIP;
    }

    /**
     * 修改容重标准
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    @CacheEvict(value = Cache.CONSTANT, key = "'" + CacheKey.STRENGTH_CODE + "'+#bulkDensityStandard.strength")
    public Object update(@RequestBody BulkDensityStandard bulkDensityStandard) {
        if(bulkDensityStandard == null || bulkDensityStandard.getId() == null ||
                bulkDensityStandard.getStrengthValue() == null ||
                bulkDensityStandard.getMinBulkDensity() == null ||
                bulkDensityStandard.getMaxBulkDensity() == null) {
            return new ErrorResponseData(Constants.BUSINESS_BULK_DENSITY_STANDARD_INCOMPLETE_DATA);
        }
        bulkDensityStandard.setUpdateTime(DateUtil.getCurrentTime());
        bulkDensityStandardService.updateById(bulkDensityStandard);
        return SUCCESS_TIP;
    }

    /**
     * 容重标准详情
     */
    @RequestMapping(value = "/detail/{bulkDensityStandardId}")
    @ResponseBody
    public Object detail(@PathVariable("bulkDensityStandardId") Integer bulkDensityStandardId) {
        return bulkDensityStandardService.selectById(bulkDensityStandardId);
    }
}
