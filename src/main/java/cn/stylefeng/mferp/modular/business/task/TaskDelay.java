package cn.stylefeng.mferp.modular.business.task;

/**
 * Description:
 *
 * @author lxc
 * @date 2019/1/20.10:50
 */
import java.util.ArrayList;
import java.util.List;

/**
 * 延迟任务机
 * @author wangqiqi
 * @Date 2018年05月23日
 */
public class TaskDelay {
    /** 实例 */
    private static final TaskDelay INSTANCE = new TaskDelay();
    /** 任务机所有的任务 */
    private volatile static List<ApplicationTask> TASKS = new ArrayList<ApplicationTask>();
    /**
     * 私有构造方法
     */
    private TaskDelay() {

    }
    /**
     * 获取实例
     * @return {@link TaskMachine}
     */
    public static TaskDelay getInstance() {
        return INSTANCE;
    }
    /**
     * 添加任务
     * @param task {@link ApplicationTask}
     */
    public TaskDelay addTask(ApplicationTask task) {
        if (TASKS.indexOf(task) == -1) {
            TASKS.add(task);
        }
        return INSTANCE;
    }
    /**
     * 执行任务
     * @param task {@link ApplicationTask}
     */
    public void startTask() {
        for (ApplicationTask task : TASKS) {
            if (task == null) {
                continue;
            }
            task.execute();
        }
    }

    /**
     * 应用任务
     */
    public static interface ApplicationTask {
        /**
         * 任务执行
         */
        void execute();
    }
}
