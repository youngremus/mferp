package cn.stylefeng.mferp.modular.business.dao;

import cn.stylefeng.mferp.modular.business.model.TemplateInfo;
import cn.stylefeng.mferp.modular.business.model.TemplateInfoModel;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 模板信息 Mapper 接口
 * </p>
 *
 * @author yzr
 * @since 2020-06-28
 */
public interface TemplateInfoMapper extends BaseMapper<TemplateInfo> {

    /**
     * 获取模板列表
     */
    List<TemplateInfoModel> selectDownList();
}
