package cn.stylefeng.mferp.modular.business.dao;

import cn.stylefeng.mferp.modular.business.model.Tphbycl;
import cn.stylefeng.mferp.modular.business.model.TrwdphbyclModel;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yzr
 * @since 2020-05-31
 */
public interface TphbyclMapper extends BaseMapper<Tphbycl> {

    /**
     * 获取指定施工配比
     */
    List<TrwdphbyclModel> getSgpbRecipe(@Param("sgpb") String sgpb);
}
