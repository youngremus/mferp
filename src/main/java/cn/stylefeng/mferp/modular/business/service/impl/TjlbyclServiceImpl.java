package cn.stylefeng.mferp.modular.business.service.impl;

import cn.stylefeng.mferp.modular.business.model.Tjlbycl;
import cn.stylefeng.mferp.modular.business.dao.TjlbyclMapper;
import cn.stylefeng.mferp.modular.business.service.ITjlbyclService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 生产记录原材料 服务实现类
 * </p>
 *
 * @author yzr
 * @since 2020-05-27
 */
@Service
public class TjlbyclServiceImpl extends ServiceImpl<TjlbyclMapper, Tjlbycl> implements ITjlbyclService {

}
