package cn.stylefeng.mferp.modular.business.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentFontStyle;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import lombok.Data;

/**
 * 日统计实体
 * 用于生产记录日统计
 * 属性名称要小写
 * @author yzr
 */
@Data
@HeadFontStyle(fontHeightInPoints = 11)
@ContentFontStyle(fontHeightInPoints = 11)
public class TjlbRtjModel {
    @ColumnWidth(12)
    @ExcelProperty("任务单号")
    private String frwno;
    @ColumnWidth(12)
    @ExcelProperty("日期")
    private String fscrq;
    @ColumnWidth(12)
    @ExcelProperty("工程名称")
    private String fgcmc;
    @ColumnWidth(12)
    @ExcelProperty("浇筑部位")
    private String fjzbw;
    @ColumnWidth(12)
    @ExcelProperty("砼品种")
    private String ftpz;
    @ColumnWidth(12)
    @ExcelProperty("施工方法")
    private String fjzfs;
    @ColumnWidth(12)
    @ExcelProperty("当天累计方量")
    private String fbcfs;


}
