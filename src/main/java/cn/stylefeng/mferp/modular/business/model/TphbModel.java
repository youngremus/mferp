package cn.stylefeng.mferp.modular.business.model;

import cn.stylefeng.mferp.core.util.DateUtil;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentFontStyle;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 导入生产配方
 * </p>
 *
 * @author yzr
 * @since 2020-05-31
 */
@Data
@HeadFontStyle(fontHeightInPoints = 11)
@ContentFontStyle(fontHeightInPoints = 11)
public class TphbModel {
    private String fzt = "尚未使用";
    @ColumnWidth(12)
    @ExcelProperty("强度等级")
    private String ftpz;
    @ColumnWidth(12)
    @ExcelProperty("配比号")
    private String fbz;
    @ColumnWidth(12)
    @ExcelProperty("样品编号")
    private String fphbno;
    private String fczy = "杨进程";
    @ColumnWidth(12)
    @ExcelProperty("容重")
    private String frz;
    private String fdlrq = DateUtil.getCurrDate("yyyy-MM-dd");
    private String fsnpz = "C1";
    private String ftld = "150±30";
    @ColumnWidth(12)
    @ExcelProperty("水")
    private BigDecimal water;
    @ColumnWidth(12)
    @ExcelProperty("水泥")
    private BigDecimal cement;
    @ColumnWidth(12)
    @ExcelProperty("砂")
    private BigDecimal sand;
    @ColumnWidth(12)
    @ExcelProperty("石")
    private BigDecimal stone;
}
