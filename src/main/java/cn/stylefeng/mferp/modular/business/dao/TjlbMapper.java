package cn.stylefeng.mferp.modular.business.dao;

import cn.stylefeng.mferp.modular.business.model.Tjlb;
import cn.stylefeng.mferp.modular.business.model.TjlbFltjModel;
import cn.stylefeng.mferp.modular.business.model.TjlbHctjModel;
import cn.stylefeng.mferp.modular.business.model.TjlbSubModel;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yzr
 * @since 2020-05-19
 */
public interface TjlbMapper extends BaseMapper<Tjlb> {

    /**
     * 根据条件查询
     */
    List<Map<String, Object>> selectTjlbs(@Param("beginTime") String beginTime,@Param("endTime") String endTime,@Param("gcmc") String gcmc);
    List<Map<String, Object>> selectTjlbsByTime(@Param("beginTime") String beginTime,@Param("endTime") String endTime,@Param("gcmc") String gcmc,
                                          @Param("outBeginTime") String outBeginTime, @Param("outEndTime") String outEndTime,
                                          @Param("inBeginTime") String inBeginTime,@Param("inEndTime") String inEndTime);
    List<Tjlb> selectTjlbList(@Param("beginTime") String beginTime,@Param("endTime") String endTime,@Param("gcmc") String gcmc);

    List<Tjlb> selectTjlbListByTime(@Param("beginTime") String beginTime,@Param("endTime") String endTime,@Param("gcmc") String gcmc,
                                    @Param("outBeginTime") String outBeginTime, @Param("outEndTime") String outEndTime,
                                    @Param("inBeginTime") String inBeginTime,@Param("inEndTime") String inEndTime);

    List<Tjlb> selectTjlbCountByTime(@Param("beginTime") String beginTime,@Param("endTime") String endTime,@Param("gcmc") String gcmc,
                                     @Param("outBeginTime") String outBeginTime, @Param("outEndTime") String outEndTime,
                                     @Param("inBeginTime") String inBeginTime,@Param("inEndTime") String inEndTime);

    List<Tjlb> selectTjlbCountByTimeOut(@Param("beginTime") String beginTime,@Param("endTime") String endTime,@Param("gcmc") String gcmc,
                                     @Param("outBeginTime") String outBeginTime, @Param("outEndTime") String outEndTime,
                                     @Param("inBeginTime") String inBeginTime,@Param("inEndTime") String inEndTime);

    /**
     * 根据选中项查询
     */
    List<Tjlb> selectTjlbListByItems(@Param("selectItems") List<String> selectItems);

    /**
     * 查询砂浆生产记录
     */
    List<Map<String, Object>> selectTjlbSjs(@Param("beginTime") String beginTime,@Param("endTime") String endTime,@Param("gcmc") String gcmc);

    /**
     * 砂浆记录查询
     */
    List<Tjlb> selectTjlbSjList(@Param("beginTime") String beginTime,@Param("endTime") String endTime,@Param("gcmc") String gcmc);

    /**
     * 日统计
     */
    List<Tjlb> countByDay(@Param("start") String start, @Param("end") String end);

    /**
     * 送检专用
     */
    List<TjlbSubModel> submission(@Param("start") String start, @Param("end") String end);

    /**
     * 获取回弹生产记录列表
     */
    List<Map<String, Object>> selectTjlbHtList(@Param("beginTime") String beginTime, @Param("endTime") String endTime,
                                               @Param("gcmc") String gcmc, @Param("jzbw") String jzbw, @Param("rwdh") String rwdh,
                                               @Param("scbt") String scbt, @Param("tpz") String tpz);

    /**
     * 耗材统计
     */
    List<TjlbHctjModel> consumableMaterialCount(@Param("start") String start, @Param("end") String end);

    /**
     * 获取方量统计记录列表
     */
    List<TjlbFltjModel> selectTjlbFltjMonths(@Param("beginTime") String beginTime, @Param("endTime") String endTime,
                                             @Param("gcmc") String gcmc, @Param("jzbw") String jzbw, @Param("rwdh") String rwdh,
                                             @Param("scbt") String scbt, @Param("tpz") String tpz);
    /**
     * 获取方量统计记录列表
     */
    List<TjlbFltjModel> selectTjlbFltjDays(@Param("beginTime") String beginTime, @Param("endTime") String endTime,
                                             @Param("gcmc") String gcmc, @Param("jzbw") String jzbw, @Param("rwdh") String rwdh,
                                             @Param("scbt") String scbt, @Param("tpz") String tpz);
    /**
     * 获取方量统计记录列表
     */
    List<TjlbFltjModel> selectTjlbFltjYears(@Param("beginTime") String beginTime, @Param("endTime") String endTime,
                                             @Param("gcmc") String gcmc, @Param("jzbw") String jzbw, @Param("rwdh") String rwdh,
                                             @Param("scbt") String scbt, @Param("tpz") String tpz);

}
