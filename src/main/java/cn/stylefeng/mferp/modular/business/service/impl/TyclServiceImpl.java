package cn.stylefeng.mferp.modular.business.service.impl;

import cn.stylefeng.mferp.core.common.constant.DatasourceEnum;
import cn.stylefeng.mferp.modular.business.dao.TyclMapper;
import cn.stylefeng.mferp.modular.business.model.Tycl;
import cn.stylefeng.mferp.modular.business.model.TyclTransfer;
import cn.stylefeng.mferp.modular.business.service.ITyclService;
import cn.stylefeng.roses.core.mutidatasource.annotion.DataSource;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 原材料 服务实现类
 * </p>
 *
 * @author yzr
 * @since 2020-06-07
 */
@Service
public class TyclServiceImpl extends ServiceImpl<TyclMapper, Tycl> implements ITyclService {

    @Autowired
    private TyclMapper tyclMapper;
    /**
     * 获取原材料列表
     *
     * @return
     */
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    @Override
    public List<Tycl> selectDownList(String ylmc) {
        return tyclMapper.selectDownList(ylmc);
    }

    /**
     * 获取原材料穿梭列表
     */
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    @Override
    public List<TyclTransfer> getYclTransferList() {
        return tyclMapper.getYclTransferList();
    }

    @Override
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    public Tycl getTyclById(Integer id) {
        List<Tycl> list = tyclMapper.selectListByYclid(id);
        return list == null || list.size() <= 0 ? null : list.get(0);
    }
}
