package cn.stylefeng.mferp.modular.business.constant;

/**
 * 提示信息
 * @author yzr
 */
public class Constants {
    public static String BUSINESS_TRWD_PARAM_INVALID = "参数无效";
    public static String BUSINESS_TRWD_RWD_NOT_FOUND = "任务单信息未找到";
    public static String BUSINESS_OFFCUTLOG_INCOMPLETE_DATA = "剩料记录数据不完整";
    public static String BUSINESS_OFFCUTLOG_EXISTS = "剩料记录已经存在";

    public static String BUSINESS_TEMPLATEINFO_INCOMPLETE_DATA = "模板数据不完整";
    public static String BUSINESS_TEMPLATEDATAINFO_DELETE_FAILED = "模板数据删除失败";

    public static String BUSINESS_BULK_DENSITY_STANDARD_INCOMPLETE_DATA = "容重标准的信息未填写不完整";
    public static String BUSINESS_BULK_DENSITY_STANDARD_DENSITY_IS_OVER = "容重不在标准范围内";
}
