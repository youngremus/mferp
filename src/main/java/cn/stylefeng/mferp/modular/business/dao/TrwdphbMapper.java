package cn.stylefeng.mferp.modular.business.dao;

import cn.stylefeng.mferp.modular.business.model.TrwdPhbModel;
import cn.stylefeng.mferp.modular.business.model.Trwdphb;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 任务单施工配比 Mapper 接口
 * </p>
 *
 * @author yzr
 * @since 2020-05-27
 */
public interface TrwdphbMapper extends BaseMapper<Trwdphb> {

    /**
     * 获取最近任务单调整记录
     *
     * @param gcmc
     * @param scbt
     * @param phbno
     * @return
     */
    List<TrwdPhbModel> getRecentRwdList(@Param("gcmc") String gcmc,
                                        @Param("scbt") String scbt,
                                        @Param("phbno") String phbno);

    /**
     * 获取最近任务单全部调整记录
     *
     * @param gcmc
     * @param scbt
     * @param phbno
     * @return
     */
    List<TrwdPhbModel> getAllRecentRwdList(@Param("gcmc") String gcmc,
                                        @Param("scbt") String scbt,
                                        @Param("phbno") String phbno);

    /**
     * 查询列表
     */
    List<Map<String, Object>> getTrwdphbs(@Param("fscbt") String fscbt, @Param("frwdh") String frwdh);

    /**
     * 查询上一次调方日志
     */
    List<Map<String, Object>> getLastBz(@Param("fscbt") String fscbt, @Param("frwdh") String frwdh);
}
