package cn.stylefeng.mferp.modular.business.task1;

import cn.stylefeng.mferp.config.MqUtils;
import cn.stylefeng.mferp.config.RedisUtils;
import cn.stylefeng.mferp.core.util.DateUtil;
import cn.stylefeng.mferp.modular.business.model.Trwd;
import cn.stylefeng.mferp.modular.business.service.ITrwdService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.quartz.QuartzJobBean;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HiJob extends QuartzJobBean {
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    //@Resource
    //private RedisUtils redisUtils;

    //@Resource
    //private MqUtils mqUtils;

    //@Resource
    //private ITrwdService trwdService;

    //@Value("${yzr.switch.trwdSync}")
    //private boolean trwdSync;

    //@Value("${yzr.switch.trwdSyncJustThreeDay}")
    //private boolean trwdSyncJustThreeDay;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
//        if(!trwdSync) return;
//        logger.info("开始定时任务HiJob=========>");
//        // 读取Mysql数据库任务单
//        Wrapper<Trwd> wrapper = new EntityWrapper<>();
//        if(trwdSyncJustThreeDay) {
//            wrapper.gt("Fjhrq", DateUtil.dataToStr(DateUtil.addDayByDate(new Date(), -3), "YYYY-MM-dd HH:mm:ss"));
//        }
//        List<String> orderByList = new ArrayList<>();
//        orderByList.add("Frwdh");
//        wrapper.orderAsc(orderByList);
//        List<Trwd> list = trwdService.selectList(wrapper);
//
//        if(list != null && !list.isEmpty()) {
//            for(Trwd trwd : list) {
//                if(!redisUtils.exist(trwd.getFrwno().toString())) {
//                    // 推送mq
//                    mqUtils.sendQueueMessage("rwdSync", JSON.toJSONString(trwd));
//                }
//            }
//        }
    }
}