package cn.stylefeng.mferp.modular.business.controller;

import cn.stylefeng.mferp.core.log.LogObjectHolder;
import cn.stylefeng.mferp.modular.business.model.TrwdPhbModel;
import cn.stylefeng.mferp.modular.business.model.Trwdphb;
import cn.stylefeng.mferp.modular.business.model.TrwdphbyclModel;
import cn.stylefeng.mferp.modular.business.service.ITrwdphbService;
import cn.stylefeng.roses.core.base.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.Collator;
import java.util.Comparator;
import java.util.List;

/**
 * 任务单施工配比控制器
 *
 * @author fengshuonan
 * @Date 2020-05-27 23:26:31
 */
@Controller
@RequestMapping("/trwdphb")
public class TrwdphbController extends BaseController {

    private String PREFIX = "/business/trwdphb/";

    @Autowired
    private ITrwdphbService trwdphbService;

    /**
     * 跳转到任务单施工配比首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "trwdphb.html";
    }

    /**
     * 跳转到添加任务单施工配比
     */
    @RequestMapping("/trwdphb_add")
    public String trwdphbAdd() {
        return PREFIX + "trwdphb_add.html";
    }

    /**
     * 跳转到修改任务单施工配比
     */
    @RequestMapping("/trwdphb_update/{trwdphbId}")
    public String trwdphbUpdate(@PathVariable Integer trwdphbId, Model model) {
        Trwdphb trwdphb = trwdphbService.selectById(trwdphbId);
        model.addAttribute("item",trwdphb);
        LogObjectHolder.me().set(trwdphb);
        return PREFIX + "trwdphb_edit.html";
    }

    /**
     * 获取任务单施工配比列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(@RequestParam(name = "fscbt", required = false) String fscbt,
                       @RequestParam(name = "frwdh", required = false) String frwdh) {
        return trwdphbService.getTrwdphbs(fscbt, frwdh);
    }


    /**
     * 获取最近任务单
     */
    @RequestMapping(value = "/getRecentRwdList")
    @ResponseBody
    public Object getRecentRwdList(@RequestParam("gcmc") String gcmc,
                                   @RequestParam("scbt") String scbt,
                                   @RequestParam("phbno") String phbno,
                                   @RequestParam("qb") boolean qb) {
        List<TrwdPhbModel> list = null;
        if(qb) {
            list = trwdphbService.getAllRecentRwdList(gcmc, scbt, phbno);
        } else {
            list = trwdphbService.getRecentRwdList(gcmc, scbt, phbno);
        }
        return list;
    }

    /**
     * 新增任务单施工配比
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Trwdphb trwdphb) {
        trwdphbService.insert(trwdphb);
        return SUCCESS_TIP;
    }

    /**
     * 删除任务单施工配比
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer trwdphbId) {
        trwdphbService.deleteById(trwdphbId);
        return SUCCESS_TIP;
    }

    /**
     * 修改任务单施工配比
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Trwdphb trwdphb) {
        trwdphbService.updateById(trwdphb);
        return SUCCESS_TIP;
    }

    /**
     * 任务单施工配比详情
     */
    @RequestMapping(value = "/detail/{trwdphbId}")
    @ResponseBody
    public Object detail(@PathVariable("trwdphbId") Integer trwdphbId) {
        return trwdphbService.selectById(trwdphbId);
    }

    /**
     * 任务单施工配比详情
     */
    @RequestMapping(value = "/get_trwd_phb_ycl/{fid}")
    @ResponseBody
    public Object getDetail(@PathVariable("fid") Integer fid) {
        List<TrwdphbyclModel> list = trwdphbService.getTrwdphbyclByFid(fid);
        // 排序
        list = TrwdphbyclModel.sortByPzgg(list);
        return list;
    }
}
