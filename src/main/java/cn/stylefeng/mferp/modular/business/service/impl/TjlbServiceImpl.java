package cn.stylefeng.mferp.modular.business.service.impl;

import cn.stylefeng.mferp.core.common.constant.DatasourceEnum;
import cn.stylefeng.mferp.modular.business.dao.TjlbMapper;
import cn.stylefeng.mferp.modular.business.model.*;
import cn.stylefeng.mferp.modular.business.service.ITjlbService;
import cn.stylefeng.roses.core.mutidatasource.annotion.DataSource;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yzr
 * @since 2020-05-19
 */
@Service
public class TjlbServiceImpl extends ServiceImpl<TjlbMapper, Tjlb> implements ITjlbService {

    @Autowired
    private TjlbMapper tjlbMapper;

    @Override
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    public List<Map<String, Object>> selectTjlbs(String beginTime, String endTime, String gcmc) {
        return tjlbMapper.selectTjlbs(beginTime, endTime, gcmc);
    }

    @Override
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    public List<Map<String, Object>> selectTjlbs(String beginTime, String endTime, String gcmc, String outBeginTime, String outEndTime, String inBeginTime, String inEndTime) {
        return tjlbMapper.selectTjlbsByTime(beginTime, endTime, gcmc, outBeginTime, outEndTime, inBeginTime, inEndTime);
    }

    @Override
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    public List<Tjlb> selectTjlbList(String beginTime, String endTime, String gcmc) {
        return tjlbMapper.selectTjlbList(beginTime, endTime, gcmc);
    }

    @Override
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    public List<Tjlb> selectTjlbList(String beginTime, String endTime, String gcmc, String outBeginTime, String outEndTime, String inBeginTime, String inEndTime) {
        return tjlbMapper.selectTjlbListByTime(beginTime, endTime, gcmc, outBeginTime, outEndTime, inBeginTime, inEndTime);
    }

    @Override
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    public List<Tjlb> selectTjlbCount(String beginTime, String endTime, String gcmc, String outBeginTime, String outEndTime, String inBeginTime, String inEndTime) {
        return tjlbMapper.selectTjlbCountByTime(beginTime, endTime, gcmc, outBeginTime, outEndTime, inBeginTime, inEndTime);
    }

    @Override
    @DataSource(name = DatasourceEnum.DATA_SOURCE_OUT)
    public List<Tjlb> selectTjlbListOut(String beginTime, String endTime, String gcmc) {
        return tjlbMapper.selectTjlbList(beginTime, endTime, gcmc);
    }

    @Override
    @DataSource(name = DatasourceEnum.DATA_SOURCE_OUT)
    public List<Tjlb> selectTjlbListOut(String beginTime, String endTime, String gcmc, String outBeginTime, String outEndTime, String inBeginTime, String inEndTime) {
        return tjlbMapper.selectTjlbListByTime(beginTime, endTime, gcmc, outBeginTime, outEndTime, inBeginTime, inEndTime);
    }

    @Override
    @DataSource(name = DatasourceEnum.DATA_SOURCE_OUT)
    public List<Tjlb> selectTjlbCountOut(String beginTime, String endTime, String gcmc, String outBeginTime, String outEndTime, String inBeginTime, String inEndTime) {
        return tjlbMapper.selectTjlbCountByTimeOut(beginTime, endTime, gcmc, outBeginTime, outEndTime, inBeginTime, inEndTime);
    }

    @Override
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    public List<Tjlb> selectTjlbListByItems(List<String> selectItems) {
        return tjlbMapper.selectTjlbListByItems(selectItems);
    }

    /**
     * 查询生产记录
     *
     * @param no
     * @return
     */
    @Override
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    public Tjlb getTjlb(String no) {
        Wrapper<Tjlb> wrapper = new EntityWrapper<>();
        wrapper.eq("fno", no);
        List<Tjlb> list = tjlbMapper.selectList(wrapper);
        if(list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }

    /**
     * 查询砂浆生产记录
     *
     * @param beginTime
     * @param endTime
     * @param gcmc
     * @return
     */
    @Override
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    public List<Map<String, Object>> selectTjlbSjs(String beginTime, String endTime, String gcmc) {
        return tjlbMapper.selectTjlbSjs(beginTime, endTime, gcmc);
    }

    /**
     * 砂浆记录查询
     *
     * @param beginTime
     * @param endTime
     * @param gcmc
     * @return
     */
    @Override
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    public List<Tjlb> selectTjlbSjList(String beginTime, String endTime, String gcmc) {
        return tjlbMapper.selectTjlbSjList(beginTime, endTime, gcmc);
    }

    /**
     * 日统计
     */
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    @Override
    public List<Tjlb> countByDay(String start, String end) {
        return tjlbMapper.countByDay(start, end);
    }

    /**
     * 送检专用
     */
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    @Override
    public List<TjlbSubModel> submission(String start, String end) {
        return tjlbMapper.submission(start, end);
    }

    /**
     * 获取回弹生产记录列表
     */
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    @Override
    public List<Map<String, Object>> selectTjlbHts(String beginTime, String endTime, String gcmc, String jzbw,
                                                   String rwdh, String scbt, String tpz) {
        return tjlbMapper.selectTjlbHtList(beginTime, endTime, gcmc, jzbw, rwdh, scbt, tpz);
    }

    /**
     * 生产记录详情
     */
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    @Override
    public Tjlb selectByFno(Integer fno) {
        return tjlbMapper.selectById(fno);
    }

    /**
     * 获取方量统计记录列表
     */
    @Override
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    public List<TjlbFltjModel> selectTjlbFltjMonths(String beginTime, String endTime, String gcmc, String jzbw, String rwdh, String scbt, String tpz) {
        return tjlbMapper.selectTjlbFltjMonths(beginTime, endTime, gcmc, jzbw, rwdh, scbt, tpz);
    }
    @Override
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    public List<TjlbFltjModel> selectTjlbFltjDays(String beginTime, String endTime, String gcmc, String jzbw, String rwdh, String scbt, String tpz) {
        return tjlbMapper.selectTjlbFltjDays(beginTime, endTime, gcmc, jzbw, rwdh, scbt, tpz);
    }
    @Override
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    public List<TjlbFltjModel> selectTjlbFltjYears(String beginTime, String endTime, String gcmc, String jzbw, String rwdh, String scbt, String tpz) {
        return tjlbMapper.selectTjlbFltjYears(beginTime, endTime, gcmc, jzbw, rwdh, scbt, tpz);
    }

    /**
     * 耗材统计
     */
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    @Override
    public List<TjlbHctjModel> consumableMaterialCount(String start, String end) {
        return tjlbMapper.consumableMaterialCount(start, end);
    }

    /**
     * 对外耗材统计
     */
    @DataSource(name = DatasourceEnum.DATA_SOURCE_OUT)
    @Override
    public List<TjlbHctjModel> consumableMaterialCountOut(String start, String end) {
        return tjlbMapper.consumableMaterialCount(start, end);
    }
}
