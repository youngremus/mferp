package cn.stylefeng.mferp.modular.business.service;

import cn.stylefeng.mferp.modular.business.model.Trwdphbycl;
import cn.stylefeng.mferp.modular.business.model.TrwdphbyclModel;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 任务单施工配比原材料 服务类
 * </p>
 *
 * @author yzr
 * @since 2020-05-27
 */
public interface ITrwdphbyclService extends IService<Trwdphbycl> {

    /**
     * 查询该任务单的所有配比信息
     * @param rwdh 任务单号
     * @return 返回配合比原材料列表
     */
    List<TrwdphbyclModel> getTrwdphbYclListByFrwdh(String rwdh, String scbt);

    /**
     * 单个配合比原材料信息调整掺量
     * @param ycls 配合比原材料列表
     * @param cl 掺量
     * @return 返回调整完毕的配合比原材料列表
     */
    List<TrwdphbyclModel> adjustCl(List<TrwdphbyclModel> ycls, double cl);

    /**
     * 单个配合比原材料信息调整砂率
     * @param ycls 配合比原材料列表
     * @param sl 砂率
     * @return 返回调整完毕的配合比原材料列表
     */
    List<TrwdphbyclModel> adjustSl(List<TrwdphbyclModel> ycls, double sl);
}
