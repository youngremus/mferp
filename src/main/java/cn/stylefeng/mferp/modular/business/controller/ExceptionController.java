package cn.stylefeng.mferp.modular.business.controller;

import cn.stylefeng.mferp.core.log.LogObjectHolder;
import cn.stylefeng.roses.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import cn.stylefeng.mferp.modular.business.model.Exception;
import cn.stylefeng.mferp.modular.business.service.IExceptionService;

/**
 * 异常信息控制器
 *
 * @author fengshuonan
 * @Date 2021-03-23 22:34:35
 */
@Controller
@RequestMapping("/exception")
public class ExceptionController extends BaseController {

    private String PREFIX = "/business/exception/";

    @Autowired
    private IExceptionService exceptionService;

    /**
     * 跳转到异常信息首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "exception.html";
    }

    /**
     * 跳转到添加异常信息
     */
    @RequestMapping("/exception_add")
    public String exceptionAdd() {
        return PREFIX + "exception_add.html";
    }

    /**
     * 跳转到修改异常信息
     */
    @RequestMapping("/exception_update/{exceptionId}")
    public String exceptionUpdate(@PathVariable Integer exceptionId, Model model) {
        Exception exception = exceptionService.selectById(exceptionId);
        model.addAttribute("item",exception);
        LogObjectHolder.me().set(exception);
        return PREFIX + "exception_edit.html";
    }

    /**
     * 获取异常信息列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return exceptionService.selectList(null);
    }

    /**
     * 新增异常信息
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Exception exception) {
        exceptionService.insert(exception);
        return SUCCESS_TIP;
    }

    /**
     * 删除异常信息
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer exceptionId) {
        exceptionService.deleteById(exceptionId);
        return SUCCESS_TIP;
    }

    /**
     * 修改异常信息
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Exception exception) {
        exceptionService.updateById(exception);
        return SUCCESS_TIP;
    }

    /**
     * 异常信息详情
     */
    @RequestMapping(value = "/detail/{exceptionId}")
    @ResponseBody
    public Object detail(@PathVariable("exceptionId") Integer exceptionId) {
        return exceptionService.selectById(exceptionId);
    }
}
