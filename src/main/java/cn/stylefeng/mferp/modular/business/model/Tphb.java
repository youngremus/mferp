package cn.stylefeng.mferp.modular.business.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yzr
 * @since 2020-05-31
 */
@TableName("tphb")
public class Tphb extends Model<Tphb> {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "fphbh", type = IdType.AUTO)
    private Integer fphbh;
    /**
     * 状态
     */
    private String fzt;
    /**
     * 砼品种
     */
    private String ftpz;
    /**
     * 用途
     */
    private String fyt;
    /**
     * 坍落度
     */
    private String ftld;
    /**
     * 水泥品种
     */
    private String fsnpz;
    /**
     * 石子规格
     */
    private String fszgg;
    /**
     * 砼标记
     */
    private String ftbj;
    /**
     * 龄期
     */
    private String ftlq;
    /**
     * 备注
     */
    private String fbz;
    /**
     * 试验
     */
    private String fsy;
    /**
     * 审核
     */
    private String fsh;
    /**
     * 技术负责
     */
    private String fjsfz;
    /**
     * 操作员
     */
    private String fczy;
    /**
     * 登录日期
     */
    private String fdlrq;
    /**
     * 搅拌时间
     */
    private Integer fjbsj;
    /**
     * 出砼门控制参数
     */
    private Integer fclsjno;
    /**
     * 配合比号
     */
    private String fphbno;
    private String fphbnoa;
    private String fphbnob;
    /**
     * 功率曲线标识号
     */
    private String fphbnoc;
    private String fqddj;
    private String fksdj;
    private String fkddj;
    private String fkzdj;
    private String fqtdj;
    private String fcd;
    private String fcnsj;
    private String fznsj;
    private String fnjsj;
    @TableField("fc1")
    private Integer fc1;
    @TableField("fc2")
    private Integer fc2;
    @TableField("fc3")
    private Integer fc3;
    private String foperator;
    private String ftimestamp;
    private Integer fchecksum;
    private Integer fcheckkey;
    @TableField("fa1")
    private String fa1;
    @TableField("fa2")
    private String fa2;
    @TableField("fa3")
    private String fa3;
    @TableField("fa4")
    private String fa4;
    private String frz;
    private String fsjb;
    private String fslpt;
    private String fjnclkg;
    private Integer fiv0;
    private String fbsfs;
    private Date fversion;
    private Integer fxpno;
    private Long fxpkey;


    public Integer getFphbh() {
        return fphbh;
    }

    public void setFphbh(Integer fphbh) {
        this.fphbh = fphbh;
    }

    public String getFzt() {
        return fzt;
    }

    public void setFzt(String fzt) {
        this.fzt = fzt;
    }

    public String getFtpz() {
        return ftpz;
    }

    public void setFtpz(String ftpz) {
        this.ftpz = ftpz;
    }

    public String getFyt() {
        return fyt;
    }

    public void setFyt(String fyt) {
        this.fyt = fyt;
    }

    public String getFtld() {
        return ftld;
    }

    public void setFtld(String ftld) {
        this.ftld = ftld;
    }

    public String getFsnpz() {
        return fsnpz;
    }

    public void setFsnpz(String fsnpz) {
        this.fsnpz = fsnpz;
    }

    public String getFszgg() {
        return fszgg;
    }

    public void setFszgg(String fszgg) {
        this.fszgg = fszgg;
    }

    public String getFtbj() {
        return ftbj;
    }

    public void setFtbj(String ftbj) {
        this.ftbj = ftbj;
    }

    public String getFtlq() {
        return ftlq;
    }

    public void setFtlq(String ftlq) {
        this.ftlq = ftlq;
    }

    public String getFbz() {
        return fbz;
    }

    public void setFbz(String fbz) {
        this.fbz = fbz;
    }

    public String getFsy() {
        return fsy;
    }

    public void setFsy(String fsy) {
        this.fsy = fsy;
    }

    public String getFsh() {
        return fsh;
    }

    public void setFsh(String fsh) {
        this.fsh = fsh;
    }

    public String getFjsfz() {
        return fjsfz;
    }

    public void setFjsfz(String fjsfz) {
        this.fjsfz = fjsfz;
    }

    public String getFczy() {
        return fczy;
    }

    public void setFczy(String fczy) {
        this.fczy = fczy;
    }

    public String getFdlrq() {
        return fdlrq;
    }

    public void setFdlrq(String fdlrq) {
        this.fdlrq = fdlrq;
    }

    public Integer getFjbsj() {
        return fjbsj;
    }

    public void setFjbsj(Integer fjbsj) {
        this.fjbsj = fjbsj;
    }

    public Integer getFclsjno() {
        return fclsjno;
    }

    public void setFclsjno(Integer fclsjno) {
        this.fclsjno = fclsjno;
    }

    public String getFphbno() {
        return fphbno;
    }

    public void setFphbno(String fphbno) {
        this.fphbno = fphbno;
    }

    public String getFphbnoa() {
        return fphbnoa;
    }

    public void setFphbnoa(String fphbnoa) {
        this.fphbnoa = fphbnoa;
    }

    public String getFphbnob() {
        return fphbnob;
    }

    public void setFphbnob(String fphbnob) {
        this.fphbnob = fphbnob;
    }

    public String getFphbnoc() {
        return fphbnoc;
    }

    public void setFphbnoc(String fphbnoc) {
        this.fphbnoc = fphbnoc;
    }

    public String getFqddj() {
        return fqddj;
    }

    public void setFqddj(String fqddj) {
        this.fqddj = fqddj;
    }

    public String getFksdj() {
        return fksdj;
    }

    public void setFksdj(String fksdj) {
        this.fksdj = fksdj;
    }

    public String getFkddj() {
        return fkddj;
    }

    public void setFkddj(String fkddj) {
        this.fkddj = fkddj;
    }

    public String getFkzdj() {
        return fkzdj;
    }

    public void setFkzdj(String fkzdj) {
        this.fkzdj = fkzdj;
    }

    public String getFqtdj() {
        return fqtdj;
    }

    public void setFqtdj(String fqtdj) {
        this.fqtdj = fqtdj;
    }

    public String getFcd() {
        return fcd;
    }

    public void setFcd(String fcd) {
        this.fcd = fcd;
    }

    public String getFcnsj() {
        return fcnsj;
    }

    public void setFcnsj(String fcnsj) {
        this.fcnsj = fcnsj;
    }

    public String getFznsj() {
        return fznsj;
    }

    public void setFznsj(String fznsj) {
        this.fznsj = fznsj;
    }

    public String getFnjsj() {
        return fnjsj;
    }

    public void setFnjsj(String fnjsj) {
        this.fnjsj = fnjsj;
    }

    public Integer getFc1() {
        return fc1;
    }

    public void setFc1(Integer fc1) {
        this.fc1 = fc1;
    }

    public Integer getFc2() {
        return fc2;
    }

    public void setFc2(Integer fc2) {
        this.fc2 = fc2;
    }

    public Integer getFc3() {
        return fc3;
    }

    public void setFc3(Integer fc3) {
        this.fc3 = fc3;
    }

    public String getFoperator() {
        return foperator;
    }

    public void setFoperator(String foperator) {
        this.foperator = foperator;
    }

    public String getFtimestamp() {
        return ftimestamp;
    }

    public void setFtimestamp(String ftimestamp) {
        this.ftimestamp = ftimestamp;
    }

    public Integer getFchecksum() {
        return fchecksum;
    }

    public void setFchecksum(Integer fchecksum) {
        this.fchecksum = fchecksum;
    }

    public Integer getFcheckkey() {
        return fcheckkey;
    }

    public void setFcheckkey(Integer fcheckkey) {
        this.fcheckkey = fcheckkey;
    }

    public String getFa1() {
        return fa1;
    }

    public void setFa1(String fa1) {
        this.fa1 = fa1;
    }

    public String getFa2() {
        return fa2;
    }

    public void setFa2(String fa2) {
        this.fa2 = fa2;
    }

    public String getFa3() {
        return fa3;
    }

    public void setFa3(String fa3) {
        this.fa3 = fa3;
    }

    public String getFa4() {
        return fa4;
    }

    public void setFa4(String fa4) {
        this.fa4 = fa4;
    }

    public String getFrz() {
        return frz;
    }

    public void setFrz(String frz) {
        this.frz = frz;
    }

    public String getFsjb() {
        return fsjb;
    }

    public void setFsjb(String fsjb) {
        this.fsjb = fsjb;
    }

    public String getFslpt() {
        return fslpt;
    }

    public void setFslpt(String fslpt) {
        this.fslpt = fslpt;
    }

    public String getFjnclkg() {
        return fjnclkg;
    }

    public void setFjnclkg(String fjnclkg) {
        this.fjnclkg = fjnclkg;
    }

    public Integer getFiv0() {
        return fiv0;
    }

    public void setFiv0(Integer fiv0) {
        this.fiv0 = fiv0;
    }

    public String getFbsfs() {
        return fbsfs;
    }

    public void setFbsfs(String fbsfs) {
        this.fbsfs = fbsfs;
    }

    public Date getFversion() {
        return fversion;
    }

    public void setFversion(Date fversion) {
        this.fversion = fversion;
    }

    public Integer getFxpno() {
        return fxpno;
    }

    public void setFxpno(Integer fxpno) {
        this.fxpno = fxpno;
    }

    public Long getFxpkey() {
        return fxpkey;
    }

    public void setFxpkey(Long fxpkey) {
        this.fxpkey = fxpkey;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }
}
