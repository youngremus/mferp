package cn.stylefeng.mferp.modular.business.service;

import cn.stylefeng.mferp.modular.business.model.OffcutLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 剩料记录表 服务类
 * </p>
 *
 * @author yzr
 * @since 2020-06-09
 */
public interface IOffcutLogService extends IService<OffcutLog> {

}
