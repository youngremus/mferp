package cn.stylefeng.mferp.modular.business.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yzr
 * @since 2020-05-18
 */
@TableName("mf_sql_command")
public class SqlCommand extends Model<SqlCommand> {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;
    /**
     * 数据源编号
     */
    @TableField("db_source_id")
    private Integer dbSourceId;
    /**
     * 数据源名称
     */
    @TableField("db_source_name")
    private String dbSourceName;
    /**
     * 命令文本
     */
    @TableField("cmd_text")
    private String cmdText;
    /**
     * 命令类型
     */
    @TableField("cmd_type")
    private String cmdType;
    /**
     * 启用状态
     */
    @TableField("enabled_status")
    private String enabledStatus;
    /**
     * 是否显示
     */
    @TableField("is_show")
    private String isShow;
    /**
     * 命令名称
     */
    @TableField("cmd_name")
    private String cmdName;
    /**
     * 命令中文名称
     */
    @TableField("cmd_cname")
    private String cmdCname;
    /**
     * 验证密码
     */
    @TableField("verify_pwd")
    private String verifyPwd;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getDbSourceId() {
        return dbSourceId;
    }

    public void setDbSourceId(Integer dbSourceId) {
        this.dbSourceId = dbSourceId;
    }

    public String getDbSourceName() {
        return dbSourceName;
    }

    public void setDbSourceName(String dbSourceName) {
        this.dbSourceName = dbSourceName;
    }

    public String getCmdText() {
        return cmdText;
    }

    public void setCmdText(String cmdText) {
        this.cmdText = cmdText;
    }

    public String getCmdType() {
        return cmdType;
    }

    public void setCmdType(String cmdType) {
        this.cmdType = cmdType;
    }

    public String getEnabledStatus() {
        return enabledStatus;
    }

    public void setEnabledStatus(String enabledStatus) {
        this.enabledStatus = enabledStatus;
    }

    public String getIsShow() {
        return isShow;
    }

    public void setIsShow(String isShow) {
        this.isShow = isShow;
    }

    public String getCmdName() {
        return cmdName;
    }

    public void setCmdName(String cmdName) {
        this.cmdName = cmdName;
    }

    public String getCmdCname() {
        return cmdCname;
    }

    public void setCmdCname(String cmdCname) {
        this.cmdCname = cmdCname;
    }

    public String getVerifyPwd() {
        return verifyPwd;
    }

    public void setVerifyPwd(String verifyPwd) {
        this.verifyPwd = verifyPwd;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SqlCommand{" +
        ", id=" + id +
        ", remark=" + remark +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        ", dbSourceId=" + dbSourceId +
        ", dbSourceName=" + dbSourceName +
        ", cmdText=" + cmdText +
        ", cmdType=" + cmdType +
        ", enabledStatus=" + enabledStatus +
        ", isShow=" + isShow +
        ", cmdName=" + cmdName +
        ", cmdCname=" + cmdCname +
        ", verifyPwd=" + verifyPwd +
        "}";
    }
}
