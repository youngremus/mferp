package cn.stylefeng.mferp.modular.business.service;

import cn.stylefeng.mferp.core.common.constant.DatasourceEnum;
import cn.stylefeng.mferp.modular.business.model.*;
import cn.stylefeng.roses.core.mutidatasource.annotion.DataSource;
import com.baomidou.mybatisplus.service.IService;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yzr
 * @since 2020-05-19
 */
public interface ITjlbService extends IService<Tjlb> {

    List<Map<String, Object>> selectTjlbs(String beginTime, String endTime, String gcmc);
    List<Map<String, Object>> selectTjlbs(String beginTime, String endTime, String gcmc, String outBeginTime, String outEndTime, String inBeginTime, String inEndTime);
    List<Tjlb> selectTjlbList(String beginTime, String endTime, String gcmc);
    /**
     * 对内生产记录查询
     * @param beginTime 起始生产日期
     * @param endTime 末尾生产日期
     * @param gcmc 工程名称
     * @param outBeginTime 起始出厂时间
     * @param outEndTime 末尾出厂时间
     * @param inBeginTime 起始进厂时间
     * @param inEndTime 末尾进厂时间
     * @return
     */
    List<Tjlb> selectTjlbList(String beginTime, String endTime, String gcmc,
                              String outBeginTime, String outEndTime, String inBeginTime, String inEndTime);
    /**
     * 对外生产记录统计（按生产日期、工程名称、浇筑部位汇总方数）
     * @param beginTime 起始生产日期
     * @param endTime 末尾生产日期
     * @param gcmc 工程名称
     * @param outBeginTime 起始出厂时间
     * @param outEndTime 末尾出厂时间
     * @param inBeginTime 起始进厂时间
     * @param inEndTime 末尾进厂时间
     * @return
     */
    List<Tjlb> selectTjlbCount(String beginTime, String endTime, String gcmc,
                               String outBeginTime, String outEndTime, String inBeginTime, String inEndTime);
    List<Tjlb> selectTjlbListOut(String beginTime, String endTime, String gcmc);
    List<Tjlb> selectTjlbListOut(String beginTime, String endTime, String gcmc, String outBeginTime, String outEndTime, String inBeginTime, String inEndTime);
    List<Tjlb> selectTjlbCountOut(String beginTime, String endTime, String gcmc, String outBeginTime, String outEndTime, String inBeginTime, String inEndTime);

    List<Tjlb> selectTjlbListByItems(List<String> selectItems);

    /**
     * 查询生产记录
     */
    Tjlb getTjlb(String no);

    /**
     * 查询砂浆生产记录
     */
    List<Map<String, Object>> selectTjlbSjs(String beginTime, String endTime, String gcmc);

    /**
     * 砂浆记录查询
     */
    List<Tjlb> selectTjlbSjList(String beginTime, String endTime, String gcmc);

    /**
     * 按日期范围日统计
     */
    List<Tjlb> countByDay(String start, String end);

    /**
     * 送检专用
     */
    List<TjlbSubModel> submission(String start, String end);

    /**
     * 获取回弹生产记录列表
     */
    List<Map<String, Object>> selectTjlbHts(String beginTime, String endTime, String gcmc, String jzbw,
                                            String rwdh, String scbt, String tpz);

    /**
     * 生产记录详情
     */
    Tjlb selectByFno(Integer fno);

    /**
     * 耗材统计
     */
    List<TjlbHctjModel> consumableMaterialCount(String start, String end);
    /**
     * 对外耗材统计
     */
    List<TjlbHctjModel> consumableMaterialCountOut(String start, String end);

    /**
     * 获取方量统计记录列表
     */
    List<TjlbFltjModel> selectTjlbFltjMonths(String beginTime, String endTime, String gcmc,
                                             String jzbw, String rwdh, String scbt, String tpz);

    /**
     * 获取方量统计记录列表
     */
    List<TjlbFltjModel> selectTjlbFltjDays(String beginTime, String endTime, String gcmc,
                                             String jzbw, String rwdh, String scbt, String tpz);

    /**
     * 获取方量统计记录列表
     */
    List<TjlbFltjModel> selectTjlbFltjYears(String beginTime, String endTime, String gcmc,
                                             String jzbw, String rwdh, String scbt, String tpz);

}
