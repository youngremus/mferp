package cn.stylefeng.mferp.modular.business.controller;

import cn.stylefeng.mferp.core.log.LogObjectHolder;
import cn.stylefeng.mferp.modular.business.model.Tycl;
import cn.stylefeng.mferp.modular.business.model.TyclTransfer;
import cn.stylefeng.mferp.modular.business.service.ITyclService;
import cn.stylefeng.roses.core.base.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 控制器
 *
 * @author fengshuonan
 * @Date 2020-06-07 21:06:40
 */
@Controller
@RequestMapping("/tycl")
public class TyclController extends BaseController {

    private String PREFIX = "/business/tycl/";

    @Autowired
    private ITyclService tyclService;

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "tycl.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/tycl_add")
    public String tyclAdd() {
        return PREFIX + "tycl_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/tycl_update/{tyclId}")
    public String tyclUpdate(@PathVariable Integer tyclId, Model model) {
        Tycl tycl = tyclService.selectById(tyclId);
        model.addAttribute("item",tycl);
        LogObjectHolder.me().set(tycl);
        return PREFIX + "tycl_edit.html";
    }

    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return tyclService.selectList(null);
    }

    /**
     * 新增
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Tycl tycl) {
        tyclService.insert(tycl);
        return SUCCESS_TIP;
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer tyclId) {
        tyclService.deleteById(tyclId);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Tycl tycl) {
        tyclService.updateById(tycl);
        return SUCCESS_TIP;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{tyclId}")
    @ResponseBody
    public Object detail(@PathVariable("tyclId") Integer tyclId) {
        return tyclService.selectById(tyclId);
    }

    /**
     * 获取原材料列表
     */
    @RequestMapping(value = "/getDownList/{ylmc}")
    @ResponseBody
    public Object getDownList(@PathVariable(name = "ylmc") String ylmc) {
        List<Tycl> list = tyclService.selectDownList(ylmc);
        return list;
    }

    /**
     * 获取原材料穿梭列表
     */
    @RequestMapping(value = "/getYclTransferList")
    @ResponseBody
    public Object getYclTransferList() {
        List<TyclTransfer> list = tyclService.getYclTransferList();
        return list;
    }
}
