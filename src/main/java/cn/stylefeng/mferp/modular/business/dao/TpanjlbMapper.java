package cn.stylefeng.mferp.modular.business.dao;

import cn.stylefeng.mferp.modular.business.model.Tpanjlb;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 每盘生产信息 Mapper 接口
 * </p>
 *
 * @author yzr
 * @since 2020-05-27
 */
public interface TpanjlbMapper extends BaseMapper<Tpanjlb> {

}
