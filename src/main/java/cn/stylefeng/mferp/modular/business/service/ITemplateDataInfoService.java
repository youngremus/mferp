package cn.stylefeng.mferp.modular.business.service;

import cn.stylefeng.mferp.modular.business.model.TemplateDataInfo;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 模板数据信息 服务类
 * </p>
 *
 * @author yzr
 * @since 2020-06-28
 */
public interface ITemplateDataInfoService extends IService<TemplateDataInfo> {

}
