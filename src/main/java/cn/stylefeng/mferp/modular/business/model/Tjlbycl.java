package cn.stylefeng.mferp.modular.business.model;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 生产记录原材料
 * </p>
 *
 * @author yzr
 * @since 2020-05-27
 */
@TableName("tjlbycl")
public class Tjlbycl extends Model<Tjlbycl> {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "fid", type = IdType.AUTO)
    private Integer fid;
    /**
     * 小票号
     */
    private Integer Fno;
    /**
     * 盘号
     */
    private Integer Fpanno;
    /**
     * 原料编号
     */
    private String Fbh;
    /**
     * 原料名称
     */
    private String Fylmc;
    /**
     * 品种规格
     */
    private String Fpzgg;
    /**
     * 配比数量
     */
    private Double Fpbsl;
    /**
     * 实用数量
     */
    private Double Fsysl;
    /**
     * 含水量
     */
    private Double Fhsl;
    /**
     * 管理用量
     */
    private Double Fglyl;

    public Integer getFid() {
        return fid;
    }

    public void setFid(Integer fid) {
        this.fid = fid;
    }

    public Integer getFno() {
        return Fno;
    }

    public void setFno(Integer fno) {
        Fno = fno;
    }

    public Integer getFpanno() {
        return Fpanno;
    }

    public void setFpanno(Integer fpanno) {
        Fpanno = fpanno;
    }

    public String getFbh() {
        return Fbh;
    }

    public void setFbh(String fbh) {
        Fbh = fbh;
    }

    public String getFylmc() {
        return Fylmc;
    }

    public void setFylmc(String fylmc) {
        Fylmc = fylmc;
    }

    public String getFpzgg() {
        return Fpzgg;
    }

    public void setFpzgg(String fpzgg) {
        Fpzgg = fpzgg;
    }

    public Double getFpbsl() {
        return Fpbsl;
    }

    public void setFpbsl(Double fpbsl) {
        Fpbsl = fpbsl;
    }

    public Double getFsysl() {
        return Fsysl;
    }

    public void setFsysl(Double fsysl) {
        Fsysl = fsysl;
    }

    public Double getFhsl() {
        return Fhsl;
    }

    public void setFhsl(Double fhsl) {
        Fhsl = fhsl;
    }

    public Double getFglyl() {
        return Fglyl;
    }

    public void setFglyl(Double fglyl) {
        Fglyl = fglyl;
    }

    @Override
    protected Serializable pkVal() {
        return this.fid;
    }

}
