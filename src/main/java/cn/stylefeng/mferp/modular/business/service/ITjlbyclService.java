package cn.stylefeng.mferp.modular.business.service;

import cn.stylefeng.mferp.modular.business.model.Tjlbycl;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 生产记录原材料 服务类
 * </p>
 *
 * @author yzr
 * @since 2020-05-27
 */
public interface ITjlbyclService extends IService<Tjlbycl> {

}
