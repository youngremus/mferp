package cn.stylefeng.mferp.modular.business.controller;

import cn.stylefeng.mferp.modular.business.model.TrwdPhbModel;
import cn.stylefeng.mferp.modular.business.model.TrwdphbyclModel;
import cn.stylefeng.roses.core.base.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import cn.stylefeng.mferp.core.log.LogObjectHolder;
import cn.stylefeng.mferp.modular.business.model.Trwdphbycl;
import cn.stylefeng.mferp.modular.business.service.ITrwdphbyclService;

import java.text.Collator;
import java.util.Comparator;
import java.util.List;

/**
 * 任务单施工配比原材料控制器
 *
 * @author fengshuonan
 * @Date 2020-05-27 23:26:51
 */
@Controller
@RequestMapping("/trwdphbycl")
public class TrwdphbyclController extends BaseController {
    private static Logger logger = LoggerFactory.getLogger(TrwdphbyclController.class);

    private String PREFIX = "/business/trwdphbycl/";

    @Autowired
    private ITrwdphbyclService trwdphbyclService;

    /**
     * 跳转到任务单施工配比原材料首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "trwdphbycl.html";
    }

    /**
     * 跳转到添加任务单施工配比原材料
     */
    @RequestMapping("/trwdphbycl_add")
    public String trwdphbyclAdd() {
        return PREFIX + "trwdphbycl_add.html";
    }

    /**
     * 跳转到修改任务单施工配比原材料
     */
    @RequestMapping("/trwdphbycl_update/{trwdphbyclId}")
    public String trwdphbyclUpdate(@PathVariable Integer trwdphbyclId, Model model) {
        Trwdphbycl trwdphbycl = trwdphbyclService.selectById(trwdphbyclId);
        model.addAttribute("item",trwdphbycl);
        LogObjectHolder.me().set(trwdphbycl);
        return PREFIX + "trwdphbycl_edit.html";
    }

    /**
     * 获取任务单施工配比原材料列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return trwdphbyclService.selectList(null);
    }


    /**
     * 获取任务单实际配比信息
     */
    @RequestMapping(value = "/getSjpbxx")
    @ResponseBody
    public Object getRecentRwdList(@RequestParam("rwdh") String rwdh,
                                   @RequestParam(name = "scbt", required = false) String scbt) {
        logger.info("getRecentRwdList rwdh:{}, scbt:{}", rwdh, scbt);
        return trwdphbyclService.getTrwdphbYclListByFrwdh(rwdh, scbt);
    }

    /**
     * 新增任务单施工配比原材料
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Trwdphbycl trwdphbycl) {
        trwdphbyclService.insert(trwdphbycl);
        return SUCCESS_TIP;
    }

    /**
     * 删除任务单施工配比原材料
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer trwdphbyclId) {
        trwdphbyclService.deleteById(trwdphbyclId);
        return SUCCESS_TIP;
    }

    /**
     * 修改任务单施工配比原材料
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Trwdphbycl trwdphbycl) {
        trwdphbyclService.updateById(trwdphbycl);
        return SUCCESS_TIP;
    }

    /**
     * 任务单施工配比原材料详情
     */
    @RequestMapping(value = "/detail/{trwdphbyclId}")
    @ResponseBody
    public Object detail(@PathVariable("trwdphbyclId") Integer trwdphbyclId) {
        return trwdphbyclService.selectById(trwdphbyclId);
    }

    /**
     * 单个配合比原材料信息调整掺量
     */
    @RequestMapping(value = "/adjust_cl/{cl}")
    @ResponseBody
    public Object adjustCl(@RequestBody List<TrwdphbyclModel> data, @PathVariable(name = "cl") String cl) {
        logger.info("adjustCl 调整掺量:{}", cl);
        double cls = 0;
        if(cl.startsWith("x")) {
            cls = -1 * Double.valueOf(cl.replace("x",""))/100;
        } else {
            cls = Double.valueOf(cl)/100;
        }
        List<TrwdphbyclModel> list = trwdphbyclService.adjustCl(data, cls);
        list = TrwdphbyclModel.sortByPzgg(list);
        return list;
    }

    /**
     * 单个配合比原材料信息调整砂率
     */
    @RequestMapping(value = "/adjust_sl/{sl}")
    @ResponseBody
    public Object adjustSl(@RequestBody List<TrwdphbyclModel> data, @PathVariable(name = "sl") String sl) {
        logger.info("adjustSl 调整掺量:{}", sl);
        double sls = 0;
        if(sl.startsWith("x")) {
            sls = -1 * Double.valueOf(sl.replace("x",""));
        } else {
            sls = Double.valueOf(sl);
        }
        // 排序
        List<TrwdphbyclModel> list = trwdphbyclService.adjustSl(data, sls);
        list = TrwdphbyclModel.sortByPzgg(list);
        return list;
    }
}
