package cn.stylefeng.mferp.modular.business.dao;

import cn.stylefeng.mferp.modular.business.model.Trwd;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 任务单 Mapper 接口
 * </p>
 *
 * @author yzr
 * @since 2020-05-27
 */
public interface TrwdMapper extends BaseMapper<Trwd> {

    /**
     * 获取任务单列表
     */
    List<Map<String,Object>> selectTrwds(@Param("beginTime") String beginTime,@Param("endTime") String endTime,
                                         @Param("gcmc") String gcmc,@Param("rwdh") String rwdh,
                                         @Param("scbt") String scbt,@Param("zt") String zt);

    List<Trwd> selectTrwdList(@Param("beginTime") String beginTime,@Param("endTime") String endTime,
                              @Param("gcmc") String gcmc,@Param("rwdh") String rwdh,
                              @Param("scbt") String scbt,@Param("zt") String zt);

    /**
     * 按任务单号查询
     */
    Trwd selectByRwdh(@Param("rwdh") String rwdh);

    /**
     * 根据生产拌台查询
     */
    List<Trwd> selectCheckRwdByScbt(@Param("scbt") String scbt);
}
