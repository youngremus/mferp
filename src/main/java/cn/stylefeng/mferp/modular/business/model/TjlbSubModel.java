package cn.stylefeng.mferp.modular.business.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentFontStyle;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import lombok.Data;

@Data
@HeadFontStyle(fontHeightInPoints = 11)
@ContentFontStyle(fontHeightInPoints = 11)
public class TjlbSubModel {
    @ColumnWidth(12)
    @ExcelProperty("单号")
    private String frwdh;
    @ColumnWidth(12)
    @ExcelProperty("日期")
    private String fscrq;
    @ColumnWidth(12)
    @ExcelProperty("工程名称")
    private String fgcmc;
    @ColumnWidth(12)
    @ExcelProperty("施工单位")
    private String fhtdw;
    @ColumnWidth(12)
    @ExcelProperty("施工部位")
    private String fjzbw;
    @ColumnWidth(12)
    @ExcelProperty("强度等级")
    private String ftpz;
    @ColumnWidth(12)
    @ExcelProperty("施工方式")
    private String fjzfs;
    @ColumnWidth(12)
    @ExcelProperty("方量")
    private String fbcfs;
    @ColumnWidth(12)
    @ExcelProperty("样品编号")
    private String fypbh;
    @ColumnWidth(12)
    @ExcelProperty("配合比编号")
    private String sybh;
    @ColumnWidth(12)
    @ExcelProperty("流水号")
    private String lsh;
}
