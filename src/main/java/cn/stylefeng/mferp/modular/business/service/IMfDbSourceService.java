package cn.stylefeng.mferp.modular.business.service;

import cn.stylefeng.mferp.modular.business.model.MfDbSource;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yzr
 * @since 2020-06-09
 */
public interface IMfDbSourceService extends IService<MfDbSource> {

}
