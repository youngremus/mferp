package cn.stylefeng.mferp.modular.business.dao;

import cn.stylefeng.mferp.modular.business.model.OffcutLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 剩料记录表 Mapper 接口
 * </p>
 *
 * @author yzr
 * @since 2020-06-09
 */
public interface OffcutLogMapper extends BaseMapper<OffcutLog> {

}
