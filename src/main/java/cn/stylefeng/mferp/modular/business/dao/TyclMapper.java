package cn.stylefeng.mferp.modular.business.dao;

import cn.stylefeng.mferp.modular.business.model.Tycl;
import cn.stylefeng.mferp.modular.business.model.TyclTransfer;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * <p>
 * 原材料 Mapper 接口
 * </p>
 *
 * @author yzr
 * @since 2020-06-07
 */
public interface TyclMapper extends BaseMapper<Tycl> {

    /**
     * 获取原材料列表
     */
    List<Tycl> selectDownList(@Param("ylmc") String ylmc);

    /**
     * 获取原材料穿梭列表
     */
    List<TyclTransfer> getYclTransferList();

    List<Tycl> selectListByYclid(@Param("id") Integer id);
}
