package cn.stylefeng.mferp.modular.business.service.impl;

import cn.stylefeng.mferp.core.common.constant.DatasourceEnum;
import cn.stylefeng.mferp.modular.business.dao.TrwdphbMapper;
import cn.stylefeng.mferp.modular.business.model.TrwdPhbModel;
import cn.stylefeng.mferp.modular.business.model.Trwdphb;
import cn.stylefeng.mferp.modular.business.model.TrwdphbyclModel;
import cn.stylefeng.mferp.modular.business.service.ITrwdphbService;
import cn.stylefeng.roses.core.mutidatasource.annotion.DataSource;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.Collator;
import java.util.*;

/**
 * <p>
 * 任务单施工配比 服务实现类
 * </p>
 *
 * @author yzr
 * @since 2020-05-27
 */
@Service
public class TrwdphbServiceImpl extends ServiceImpl<TrwdphbMapper, Trwdphb> implements ITrwdphbService {

    @Autowired
    private TrwdphbMapper trwdphbMapper;

    /**
     * 获取最近任务单调整记录
     *
     * @param gcmc
     * @param scbt
     * @param phbno
     * @return
     */
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    @Override
    public List<TrwdPhbModel> getRecentRwdList(String gcmc, String scbt, String phbno) {
        return trwdphbMapper.getRecentRwdList(gcmc, scbt, phbno);
    }

    /**
     * 获取最近任务单全部调整记录
     *
     * @param gcmc
     * @param scbt
     * @param phbno
     * @return
     */
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    @Override
    public List<TrwdPhbModel> getAllRecentRwdList(String gcmc, String scbt, String phbno) {
        return trwdphbMapper.getAllRecentRwdList(gcmc, scbt, phbno);
    }

    /**
     * 任务单施工配比详情
     *
     * @param fid
     * @return
     */
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    @Override
    public List<TrwdphbyclModel> getTrwdphbyclByFid(Integer fid) {
        Trwdphb obj = trwdphbMapper.selectById(fid);
        String fbtid = obj.getFpblx();
        Integer scbt = 0;
        if(StringUtils.isNotBlank(fbtid) && !"施工".equals(fbtid)) {
            scbt = Integer.valueOf(fbtid);
        }
        Integer frwdh = obj.getFrwdh();
        String bz = obj.getFbz();
        if(StringUtils.isNotBlank(bz)) {
            List<TrwdphbyclModel> list = new ArrayList<>();
            bz = bz.substring(1, bz.length()-1);
            bz = bz.replaceAll("\\)=", "|");
            bz = bz.replaceAll("\\(", "|");
            List<String> temp = Arrays.asList( bz.split("}\\{"));
            for(String t : temp) {
                String[] arrayTemp = t.split("\\|");
                TrwdphbyclModel model = new TrwdphbyclModel();
                model.setFylmc(arrayTemp[0]);
                model.setFpzgg(arrayTemp[1]);
                model.setFsysl(Double.valueOf(arrayTemp[2]));
                model.setFbtid(scbt);
                model.setFrwdh(frwdh);
                model.setFpblb(0);
                model.setFplcw(0);
                list.add(model);
            }
            if(list != null && list.size() > 1) {
                list = TrwdphbyclModel.sortByPzgg(list);
                /*list.sort(new Comparator<TrwdphbyclModel>() {
                    @Override
                    public int compare(TrwdphbyclModel o1, TrwdphbyclModel o2) {
                        Comparator comparator = Collator.getInstance(java.util.Locale.CHINA);
                        return comparator.compare(o1.getFylmc(), o2.getFylmc());
                    }
                });*/
            }
            return list;
        }
        return null;
    }

    /**
     * 查询列表
     *
     * @param Fscbt
     * @param Frwdh
     * @return
     */
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    @Override
    public List<Map<String, Object>> getTrwdphbs(String Fscbt, String Frwdh) {
        return trwdphbMapper.getTrwdphbs(Fscbt, Frwdh);
    }

}
