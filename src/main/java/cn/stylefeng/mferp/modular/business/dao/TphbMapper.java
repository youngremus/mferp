package cn.stylefeng.mferp.modular.business.dao;

import cn.stylefeng.mferp.modular.business.model.Tphb;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yzr
 * @since 2020-05-31
 */
public interface TphbMapper extends BaseMapper<Tphb> {

    List<Tphb> selectDownList();

}
