package cn.stylefeng.mferp.modular.business.model;

public class MrwdModel {
    private String Frwdh;

    private String Fsybh;

    private String Ftbj;

    private String Fypbh;

    public String getFrwdh() {
        return Frwdh;
    }

    public void setFrwdh(String frwdh) {
        Frwdh = frwdh;
    }

    public String getFsybh() {
        return Fsybh;
    }

    public void setFsybh(String fsybh) {
        Fsybh = fsybh;
    }

    public String getFtbj() {
        return Ftbj;
    }

    public void setFtbj(String ftbj) {
        Ftbj = ftbj;
    }

    public String getFypbh() {
        return Fypbh;
    }

    public void setFypbh(String fypbh) {
        Fypbh = fypbh;
    }
}
