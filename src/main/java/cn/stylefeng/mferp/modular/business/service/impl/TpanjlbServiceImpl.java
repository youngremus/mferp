package cn.stylefeng.mferp.modular.business.service.impl;

import cn.stylefeng.mferp.modular.business.model.Tpanjlb;
import cn.stylefeng.mferp.modular.business.dao.TpanjlbMapper;
import cn.stylefeng.mferp.modular.business.service.ITpanjlbService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 每盘生产信息 服务实现类
 * </p>
 *
 * @author yzr
 * @since 2020-05-27
 */
@Service
public class TpanjlbServiceImpl extends ServiceImpl<TpanjlbMapper, Tpanjlb> implements ITpanjlbService {

}
