package cn.stylefeng.mferp.modular.business.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 剩料记录表
 * </p>
 *
 * @author yzr
 * @since 2020-06-09
 */
@TableName("offcut_log")
public class OffcutLog extends Model<OffcutLog> {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 原小票号
     */
    @TableField("origin_no")
    private String originNo;
    /**
     * 原单号
     */
    @TableField("origin_order_no")
    private String originOrderNo;
    /**
     * 原砼品种
     */
    @TableField("origin_concrete_type")
    private String originConcreteType;
    /**
     * 原工程名称
     */
    @TableField("origin_project_name")
    private String originProjectName;
    /**
     * 原浇筑部位
     */
    @TableField("origin_pour_position")
    private String originPourPosition;
    /**
     * 原出厂日期
     */
    @TableField("origin_production_date")
    private String originProductionDate;
    /**
     * 原出厂时间
     */
    @TableField("origin_production_time")
    private String originProductionTime;
    /**
     * 原施工方法
     */
    @TableField("origin_construction_method")
    private String originConstructionMethod;
    /**
     * 车号
     */
    @TableField("car_number")
    private String carNumber;
    /**
     * 当前小票号
     */
    @TableField("current_no")
    private String currentNo;
    /**
     * 当前单号
     */
    @TableField("current_order_no")
    private String currentOrderNo;
    /**
     * 当前砼品种
     */
    @TableField("current_concrete_type")
    private String currentConcreteType;
    /**
     * 当前工程名称
     */
    @TableField("current_project_name")
    private String currentProjectName;
    /**
     * 当前浇筑部位
     */
    @TableField("current_pour_position")
    private String currentPourPosition;
    /**
     * 当前出厂日期
     */
    @TableField("current_production_date")
    private String currentProductionDate;
    /**
     * 当前出厂时间
     */
    @TableField("current_production_time")
    private String currentProductionTime;
    /**
     * 当前施工方法
     */
    @TableField("current_construction_method")
    private String currentConstructionMethod;
    /**
     * 剩料类型
     */
    @TableField("offcut_type")
    private String offcutType;
    /**
     * 备注
     */
    private String remark;

    /**
     * 剩料方数
     */
    private String num;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOriginNo() {
        return originNo;
    }

    public void setOriginNo(String originNo) {
        this.originNo = originNo;
    }

    public String getOriginOrderNo() {
        return originOrderNo;
    }

    public void setOriginOrderNo(String originOrderNo) {
        this.originOrderNo = originOrderNo;
    }

    public String getOriginConcreteType() {
        return originConcreteType;
    }

    public void setOriginConcreteType(String originConcreteType) {
        this.originConcreteType = originConcreteType;
    }

    public String getOriginProjectName() {
        return originProjectName;
    }

    public void setOriginProjectName(String originProjectName) {
        this.originProjectName = originProjectName;
    }

    public String getOriginPourPosition() {
        return originPourPosition;
    }

    public void setOriginPourPosition(String originPourPosition) {
        this.originPourPosition = originPourPosition;
    }

    public String getOriginProductionDate() {
        return originProductionDate;
    }

    public void setOriginProductionDate(String originProductionDate) {
        this.originProductionDate = originProductionDate;
    }

    public String getOriginProductionTime() {
        return originProductionTime;
    }

    public void setOriginProductionTime(String originProductionTime) {
        this.originProductionTime = originProductionTime;
    }

    public String getOriginConstructionMethod() {
        return originConstructionMethod;
    }

    public void setOriginConstructionMethod(String originConstructionMethod) {
        this.originConstructionMethod = originConstructionMethod;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getCurrentNo() {
        return currentNo;
    }

    public void setCurrentNo(String currentNo) {
        this.currentNo = currentNo;
    }

    public String getCurrentOrderNo() {
        return currentOrderNo;
    }

    public void setCurrentOrderNo(String currentOrderNo) {
        this.currentOrderNo = currentOrderNo;
    }

    public String getCurrentConcreteType() {
        return currentConcreteType;
    }

    public void setCurrentConcreteType(String currentConcreteType) {
        this.currentConcreteType = currentConcreteType;
    }

    public String getCurrentProjectName() {
        return currentProjectName;
    }

    public void setCurrentProjectName(String currentProjectName) {
        this.currentProjectName = currentProjectName;
    }

    public String getCurrentPourPosition() {
        return currentPourPosition;
    }

    public void setCurrentPourPosition(String currentPourPosition) {
        this.currentPourPosition = currentPourPosition;
    }

    public String getCurrentProductionDate() {
        return currentProductionDate;
    }

    public void setCurrentProductionDate(String currentProductionDate) {
        this.currentProductionDate = currentProductionDate;
    }

    public String getCurrentProductionTime() {
        return currentProductionTime;
    }

    public void setCurrentProductionTime(String currentProductionTime) {
        this.currentProductionTime = currentProductionTime;
    }

    public String getCurrentConstructionMethod() {
        return currentConstructionMethod;
    }

    public void setCurrentConstructionMethod(String currentConstructionMethod) {
        this.currentConstructionMethod = currentConstructionMethod;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getOffcutType() {
        return offcutType;
    }

    public void setOffcutType(String offcutType) {
        this.offcutType = offcutType;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "OffcutLog{" +
        ", id=" + id +
        ", originNo=" + originNo +
        ", originOrderNo=" + originOrderNo +
        ", originConcreteType=" + originConcreteType +
        ", originProjectName=" + originProjectName +
        ", originPourPosition=" + originPourPosition +
        ", originProductionDate=" + originProductionDate +
        ", originProductionTime=" + originProductionTime +
        ", originConstructionMethod=" + originConstructionMethod +
        ", carNumber=" + carNumber +
        ", currentNo=" + currentNo +
        ", currentOrderNo=" + currentOrderNo +
        ", currentConcreteType=" + currentConcreteType +
        ", currentProjectName=" + currentProjectName +
        ", currentPourPosition=" + currentPourPosition +
        ", currentProductionDate=" + currentProductionDate +
        ", currentProductionTime=" + currentProductionTime +
        ", currentConstructionMethod=" + currentConstructionMethod +
        ", remark=" + remark +
        ", num=" + num +
        ", offcutType=" + offcutType +
        "}";
    }
}
