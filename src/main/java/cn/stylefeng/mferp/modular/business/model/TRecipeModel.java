package cn.stylefeng.mferp.modular.business.model;

import java.util.List;

/**
 * 保存配合比
 */
public class TRecipeModel {
    private List<TrwdphbyclModel> data;
    private String Frwdh;
    private String Fsgpb;
    private String Fscbt;
    private String Ftpz;

    public List<TrwdphbyclModel> getData() {
        return data;
    }

    public void setData(List<TrwdphbyclModel> data) {
        this.data = data;
    }

    public String getFrwdh() {
        return Frwdh;
    }

    public void setFrwdh(String frwdh) {
        Frwdh = frwdh;
    }

    public String getFsgpb() {
        return Fsgpb;
    }

    public void setFsgpb(String fsgpb) {
        Fsgpb = fsgpb;
    }

    public String getFscbt() {
        return Fscbt;
    }

    public void setFscbt(String fscbt) {
        Fscbt = fscbt;
    }

    public String getFtpz() {
        return Ftpz;
    }

    public void setFtpz(String ftpz) {
        Ftpz = ftpz;
    }
}
