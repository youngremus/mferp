package cn.stylefeng.mferp.modular.business.warpper;

import cn.stylefeng.mferp.core.common.constant.factory.ConstantFactory;
import cn.stylefeng.mferp.modular.business.model.BulkDensityStandard;
import cn.stylefeng.mferp.modular.business.model.TemplateInfo;
import cn.stylefeng.mferp.modular.system.model.Dict;
import cn.stylefeng.roses.core.base.warpper.BaseControllerWrapper;
import cn.stylefeng.roses.core.util.ToolUtil;
import cn.stylefeng.roses.kernel.model.page.PageResult;
import com.baomidou.mybatisplus.plugins.Page;

import java.util.List;
import java.util.Map;

public class BulkDensityStandardWarpper extends BaseControllerWrapper {

    public BulkDensityStandardWarpper(Map<String, Object> single) {
        super(single);
    }

    public BulkDensityStandardWarpper(List<Map<String, Object>> multi) {
        super(multi);
    }

    public BulkDensityStandardWarpper(Page<Map<String, Object>> page) {
        super(page);
    }

    public BulkDensityStandardWarpper(PageResult<Map<String, Object>> pageResult) {
        super(pageResult);
    }

    @Override
    protected void wrapTheMap(Map<String, Object> map) {

    }

    public static List<BulkDensityStandard> wrap(List<BulkDensityStandard> list) {
        List<Dict> isDefaults = ConstantFactory.me().selectListByParentCode("is_default");
        List<Dict> enabledStatus = ConstantFactory.me().selectListByParentCode("sys_state");

        for (int i =0; i < list.size(); i++) {
            BulkDensityStandard density = list.get(i);
            String isDefault = density.getIsDefault();
            String status = density.getEnabledStatus();
            if (ToolUtil.isEmpty(isDefault)) {
                density.setIsDefault("--");
            } else {
                for(Dict dict : isDefaults) {
                    if(isDefault.equals(dict.getCode())) {
                        density.setIsDefault(dict.getName());
                        break;
                    }
                }
            }
            if (ToolUtil.isEmpty(status)) {
                density.setEnabledStatus("--");
            } else {
                for(Dict dict : enabledStatus) {
                    if(status.equals(dict.getCode())) {
                        density.setEnabledStatus(dict.getName());
                        break;
                    }
                }
            }
        }
        return list;
    }
}
