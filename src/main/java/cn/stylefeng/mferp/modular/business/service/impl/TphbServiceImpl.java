package cn.stylefeng.mferp.modular.business.service.impl;

import cn.stylefeng.mferp.core.common.constant.DatasourceEnum;
import cn.stylefeng.mferp.modular.business.model.Tphb;
import cn.stylefeng.mferp.modular.business.dao.TphbMapper;
import cn.stylefeng.mferp.modular.business.service.ITphbService;
import cn.stylefeng.roses.core.mutidatasource.annotion.DataSource;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yzr
 * @since 2020-05-31
 */
@Service
public class TphbServiceImpl extends ServiceImpl<TphbMapper, Tphb> implements ITphbService {

    @Autowired
    private TphbMapper tphbMapper;

    @Override
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    public List<Tphb> selectDownList() {
        return tphbMapper.selectDownList();
    }

    /**
     * 获取生产配合比列表
     * @return
     */
    @DataSource(name = DatasourceEnum.DATA_SOURCE_BIZ)
    @Override
    public List<Tphb> selectTphbs() {
        return tphbMapper.selectList(null);
    }
}
