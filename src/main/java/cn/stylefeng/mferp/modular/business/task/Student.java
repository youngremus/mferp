package cn.stylefeng.mferp.modular.business.task;

/**
 * Description:
 *
 * @author lxc
 * @date 2019/1/20.10:50
 */
import java.io.Serializable;

public class Student implements Serializable {
    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;
    /**延迟任务属性start()必须序列化*/
    /**自增id主键*/
    private Long id;
    /**延迟时间*/
    private String delayTime;
    /**延迟任务名称，项目延迟任务唯一*/
    private String taskName;
    /**添加任务时间毫秒值*/
    private Long addTimeMillis;
    /**延迟任务属性end*/

    private String name;
    @Override
    public String toString() {
        return "Student [id=" + id + ", delayTime=" + delayTime + ", taskName=" + taskName + ", addTimeMillis="
                + addTimeMillis + ", name=" + name + "]";
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDelayTime() {
        return delayTime;
    }

    public void setDelayTime(String delayTime) {
        this.delayTime = delayTime;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getAddTimeMillis() {
        return addTimeMillis;
    }

    public void setAddTimeMillis(Long addTimeMillis) {
        this.addTimeMillis = addTimeMillis;
    }



}
