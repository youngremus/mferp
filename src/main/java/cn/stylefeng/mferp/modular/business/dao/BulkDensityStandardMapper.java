package cn.stylefeng.mferp.modular.business.dao;

import cn.stylefeng.mferp.modular.business.model.BulkDensityStandard;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yzr
 * @since 2020-07-26
 */
public interface BulkDensityStandardMapper extends BaseMapper<BulkDensityStandard> {

}
