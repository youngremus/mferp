package cn.stylefeng.mferp.modular.business.service.impl;

import cn.stylefeng.mferp.modular.business.model.TemplateDataInfo;
import cn.stylefeng.mferp.modular.business.dao.TemplateDataInfoMapper;
import cn.stylefeng.mferp.modular.business.service.ITemplateDataInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 模板数据信息 服务实现类
 * </p>
 *
 * @author yzr
 * @since 2020-06-28
 */
@Service
public class TemplateDataInfoServiceImpl extends ServiceImpl<TemplateDataInfoMapper, TemplateDataInfo> implements ITemplateDataInfoService {

}
