package cn.stylefeng.mferp.modular.business.dao;

import cn.stylefeng.mferp.modular.business.model.Trwdphb;
import cn.stylefeng.mferp.modular.business.model.Trwdphbycl;
import cn.stylefeng.mferp.modular.business.model.TrwdphbyclModel;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 任务单施工配比原材料 Mapper 接口
 * </p>
 *
 * @author yzr
 * @since 2020-05-27
 */
public interface TrwdphbyclMapper extends BaseMapper<Trwdphbycl> {

    /**
     * 查询该任务单的所有配比信息
     *
     * @param rwdh
     * @param scbt
     * @return
     */
    List<TrwdphbyclModel> getTrwdphbYclListByFrwdh(@Param("rwdh") String rwdh, @Param("scbt") String scbt);

    /**
     * 删除该任务单的所有配比信息
     * @param rwdh
     * @param scbt
     */
    void deleteTrwdphbYclByFrwdhAndScbt(@Param("rwdh") String rwdh, @Param("scbt") String scbt);
}
