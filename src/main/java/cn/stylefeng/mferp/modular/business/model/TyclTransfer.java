package cn.stylefeng.mferp.modular.business.model;

import lombok.Data;

@Data
public class TyclTransfer {
    private String value;
    private String title;
    private String dataName;
    private String dataType;
    private String dataCnName;
    private String dataValue;
}
