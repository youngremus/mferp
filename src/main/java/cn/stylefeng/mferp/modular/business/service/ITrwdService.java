package cn.stylefeng.mferp.modular.business.service;

import cn.stylefeng.mferp.core.common.constant.DatasourceEnum;
import cn.stylefeng.mferp.modular.business.model.TRecipeModel;
import cn.stylefeng.mferp.modular.business.model.Trwd;
import cn.stylefeng.mferp.modular.business.model.TrwdSaveModel;
import cn.stylefeng.mferp.modular.business.model.TrwdphbyclModel;
import cn.stylefeng.roses.core.mutidatasource.annotion.DataSource;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 任务单 服务类
 * </p>
 *
 * @author yzr
 * @since 2020-05-27
 */
public interface ITrwdService extends IService<Trwd> {

    /**
     * 获取任务单列表
     */
    List<Map<String,Object>> selectTrwds(String beginTime, String endTime, String gcmc,
                                         String rwdh, String scbt, String zt);

    List<Trwd> selectTrwdListOut(String beginTime, String endTime, String zt, String gcmc, String scbt, String rwdh);

    /**
     * 按任务单号查询
     */
    Trwd selectByRwdh(String rwdh);

    /**
     * 根据生产拌台查询
     */
    List<Trwd> selectCheckRwdByScbt(String scbt);

    /**
     * 调整掺量：添加调整记录、更新配合比信息
     */
    void adjustCl(Trwd trwd, String scbt, double cl);

    /**
     * 添加配合比调整日志
     */
    boolean addPhbLog(Trwd trwd, String scbt);

    /**
     * 为新施工配比添加配合比调整日志
     */
    boolean addPhbLogForNewSgpb(Trwd trwd, String scbt, String oldSgpb);

    /**
     * 根据任务单号查询
     */
    List<Trwd> selectCheckRwdByRwdhs(List<String> rwdhs);

    /**
     * 调整砂率：添加调整记录、更新配合比信息
     */
    void adjustSl(Trwd trwd, String scbt, double sl);

    /**
     * 调整容重：添加调整记录、更新配合比信息
     */
    List<TrwdphbyclModel> adjustRz(Trwd trwd, String scbt, double rzbl);

    /**
     * 单个配合比原材料信息保存配方
     */
    Object saveRecipe(TRecipeModel recipe);

    /**
     * 获取指定施工配比
     */
    List<TrwdphbyclModel> getSgpbRecipe(String sgpb);

    /**
     * 检查容重是否在标准范围内
     * @param list 配方信息
     * @param strength 强度等级 例如C15
     * @return
     */
    boolean checkBulkDensity(List<TrwdphbyclModel> list, String strength);

    void updateTrwdphbycls(List<TrwdphbyclModel> list);

    /**
     * 调整容重时添加配合比调整日志
     */
    boolean addPhbLogForAdjustRz(Trwd trwd, String scbt);

    /**
     * 容重回退
     */
    boolean rzBack(Trwd trwd, String scbt);

    /**
     * 调砂
     */
    void adjustSand(Trwd trwd, String scbt, double sand, String sandClass);

    /**
     * 调石
     */
    void adjustStone(Trwd trwd, String scbt, double stone, String stoneClass);

    Object saveTrwd(TrwdSaveModel data);
}
