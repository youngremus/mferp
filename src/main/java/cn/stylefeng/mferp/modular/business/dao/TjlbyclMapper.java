package cn.stylefeng.mferp.modular.business.dao;

import cn.stylefeng.mferp.modular.business.model.Tjlbycl;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 生产记录原材料 Mapper 接口
 * </p>
 *
 * @author yzr
 * @since 2020-05-27
 */
public interface TjlbyclMapper extends BaseMapper<Tjlbycl> {

}
