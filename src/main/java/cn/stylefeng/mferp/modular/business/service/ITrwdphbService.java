package cn.stylefeng.mferp.modular.business.service;

import cn.stylefeng.mferp.modular.business.model.TrwdPhbModel;
import cn.stylefeng.mferp.modular.business.model.Trwdphb;
import cn.stylefeng.mferp.modular.business.model.TrwdphbyclModel;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 任务单施工配比 服务类
 * </p>
 *
 * @author yzr
 * @since 2020-05-27
 */
public interface ITrwdphbService extends IService<Trwdphb> {

    /**
     * 获取最近任务单调整记录
     *
     * @param Fgcmc
     * @param Fscbt
     * @param Fphbno
     * @return
     */
    List<TrwdPhbModel> getRecentRwdList(String Fgcmc, String Fscbt, String Fphbno);

    /**
     * 获取最近任务单全部调整记录
     *
     * @param Fgcmc
     * @param Fscbt
     * @param Fphbno
     * @return
     */
    List<TrwdPhbModel> getAllRecentRwdList(String Fgcmc, String Fscbt, String Fphbno);

    /**
     * 任务单施工配比详情
     * @param fid
     * @return
     */
    List<TrwdphbyclModel> getTrwdphbyclByFid(Integer fid);

    /**
     * 查询列表
     * @param Fscbt
     * @param Frwdh
     * @return
     */
    List<Map<String,Object>> getTrwdphbs(String Fscbt, String Frwdh);
}
