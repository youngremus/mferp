package cn.stylefeng.mferp.modular.business.dao;

import cn.stylefeng.mferp.modular.business.model.TemplateDataInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 模板数据信息 Mapper 接口
 * </p>
 *
 * @author yzr
 * @since 2020-06-28
 */
public interface TemplateDataInfoMapper extends BaseMapper<TemplateDataInfo> {

}
