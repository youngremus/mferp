package cn.stylefeng.mferp.modular.business.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentFontStyle;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import lombok.Data;

/**
 * 耗材统计实体
 * 用于生产记录耗材统计
 * 属性名称要小写
 * @author yzr
 */
@Data
@HeadFontStyle(fontHeightInPoints = 11)
@ContentFontStyle(fontHeightInPoints = 11)
public class TjlbHctjModel {
    @ColumnWidth(12)
    @ExcelProperty("日期")
    private String fscrq;
    @ColumnWidth(12)
    @ExcelProperty("原料品种")
    private String fpzgg;
    @ColumnWidth(12)
    @ExcelProperty("耗材总量")
    private String zl;


}
