/**
 * Copyright 2018-2020 stylefeng & fengshuonan (https://gitee.com/stylefeng)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.stylefeng.mferp.modular.business.warpper;

import cn.stylefeng.mferp.core.common.constant.factory.ConstantFactory;
import cn.stylefeng.mferp.modular.business.model.OffcutLog;
import cn.stylefeng.mferp.modular.system.model.Dict;
import cn.stylefeng.roses.core.base.warpper.BaseControllerWrapper;
import cn.stylefeng.roses.core.util.ToolUtil;
import cn.stylefeng.roses.kernel.model.page.PageResult;
import com.baomidou.mybatisplus.plugins.Page;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 生产记录的包装类
 *
 * @author fengshuonan
 * @date 2017年2月13日 下午10:47:03
 */
public class OffcutLogWarpper extends BaseControllerWrapper {

    public OffcutLogWarpper(Map<String, Object> single) {
        super(single);
    }

    public OffcutLogWarpper(List<Map<String, Object>> multi) {
        super(multi);
    }

    public OffcutLogWarpper(Page<Map<String, Object>> page) {
        super(page);
    }

    public OffcutLogWarpper(PageResult<Map<String, Object>> pageResult) {
        super(pageResult);
    }

    @Override
    protected void wrapTheMap(Map<String, Object> map) {

    }

    public static List<OffcutLog> wrap(List<OffcutLog> list) {
        List<OffcutLog> newList = new ArrayList<>();

        List<Dict> offcutTypes = ConstantFactory.me().selectListByParentCode("offcutType");
        for (OffcutLog log : list) {
            String offcutType = log.getOffcutType();

            if (ToolUtil.isEmpty(offcutType)) {
                log.setOffcutType("--");
            } else {
                for(Dict dict : offcutTypes) {
                    if(offcutType.equals(dict.getCode())) {
                        log.setOffcutType(dict.getName());
                        break;
                    }
                }

            }
            newList.add(log);
        }
        return newList;
    }

}
