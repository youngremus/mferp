package cn.stylefeng.mferp.modular.business.task;

/**
 * Description:
 *
 * @author lxc
 * @date 2019/1/20.10:51
 */
public class Test {
    public static void main(String[] args) {

        //Tomcat容器启动初始化start
        TaskQueueDaemonThread taskQueueDaemonThread=TaskQueueDaemonThread.getInstance();
        taskQueueDaemonThread.init();
        //Tomcat容器启动初始化end

        //任务添加start
        /*for(int i=1;i<100000;i++){
            Student s=new Student();
            s.setId((long)i);
            //任务名称，同一类型任务名字相同
            s.setTaskName("sms");
            //任务延迟时间
            s.setDelayTime(i*2+"00");
            //当前时间ms值
            s.setAddTimeMillis(System.currentTimeMillis());

            //任务数据
            s.setName("李四"+i);
            taskQueueDaemonThread.put(Long.parseLong(s.getDelayTime()),s);
        }
        System.out.println("添加完成");*/
        Student s=new Student();
        s.setId((long)2);
        //任务名称，同一类型任务名字相同
        s.setTaskName("sms");
        //任务延迟时间
        s.setDelayTime(10+"000");
        //当前时间ms值
        s.setAddTimeMillis(System.currentTimeMillis());

        //任务数据
        s.setName("Survey"+10);
        taskQueueDaemonThread.put(Long.parseLong(s.getDelayTime()),s);
        //任务添加end
    }
}
