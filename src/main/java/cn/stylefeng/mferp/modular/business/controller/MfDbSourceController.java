package cn.stylefeng.mferp.modular.business.controller;

import cn.stylefeng.mferp.core.util.DateUtil;
import cn.stylefeng.roses.core.base.controller.BaseController;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import cn.stylefeng.mferp.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import cn.stylefeng.mferp.modular.business.model.MfDbSource;
import cn.stylefeng.mferp.modular.business.service.IMfDbSourceService;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * 数据源信息控制器
 *
 * @author fengshuonan
 * @Date 2020-06-09 07:44:50
 */
@Controller
@RequestMapping("/mfDbSource")
public class MfDbSourceController extends BaseController {

    private String PREFIX = "/business/mfDbSource/";

    @Autowired
    private IMfDbSourceService mfDbSourceService;

    /**
     * 跳转到数据源信息首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "mfDbSource.html";
    }

    /**
     * 跳转到添加数据源信息
     */
    @RequestMapping("/mfDbSource_add")
    public String mfDbSourceAdd() {
        return PREFIX + "mfDbSource_add.html";
    }

    /**
     * 跳转到修改数据源信息
     */
    @RequestMapping("/mfDbSource_update/{mfDbSourceId}")
    public String mfDbSourceUpdate(@PathVariable Integer mfDbSourceId, Model model) {
        MfDbSource mfDbSource = mfDbSourceService.selectById(mfDbSourceId);
        model.addAttribute("item", mfDbSource);
        LogObjectHolder.me().set(mfDbSource);
        return PREFIX + "mfDbSource_edit.html";
    }

    /**
     * 获取数据源信息列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return mfDbSourceService.selectList(null);
    }

    /**
     * 新增数据源信息
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(MfDbSource mfDbSource) {
        mfDbSource.setCreateTime(new Date());
        mfDbSourceService.insert(mfDbSource);
        return SUCCESS_TIP;
    }

    /**
     * 删除数据源信息
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer mfDbSourceId) {
        mfDbSourceService.deleteById(mfDbSourceId);
        return SUCCESS_TIP;
    }

    /**
     * 修改数据源信息
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(MfDbSource mfDbSource) {
        mfDbSource.setUpdateTime(new Date());
        mfDbSourceService.updateById(mfDbSource);
        return SUCCESS_TIP;
    }

    /**
     * 数据源信息详情
     */
    @RequestMapping(value = "/detail/{mfDbSourceId}")
    @ResponseBody
    public Object detail(@PathVariable("mfDbSourceId") Integer mfDbSourceId) {
        return mfDbSourceService.selectById(mfDbSourceId);
    }
}
