package cn.stylefeng.mferp.modular.business.service;

import cn.stylefeng.mferp.modular.business.model.Tpanjlb;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 每盘生产信息 服务类
 * </p>
 *
 * @author yzr
 * @since 2020-05-27
 */
public interface ITpanjlbService extends IService<Tpanjlb> {

}
