package cn.stylefeng.mferp.modular.business.service;

import cn.stylefeng.mferp.modular.business.model.Tycl;
import cn.stylefeng.mferp.modular.business.model.TyclTransfer;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 原材料 服务类
 * </p>
 *
 * @author yzr
 * @since 2020-06-07
 */
public interface ITyclService extends IService<Tycl> {

    /**
     * 获取原材料列表
     */
    List<Tycl> selectDownList(String ylmc);

    /**
     * 获取原材料穿梭列表
     */
    List<TyclTransfer> getYclTransferList();

    Tycl getTyclById(Integer id);
}
