package cn.stylefeng.mferp.modular.business.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yzr
 * @since 2020-05-19
 */
@TableName("tjlb")
public class Tjlb extends Model<Tjlb> {

    private static final long serialVersionUID = 1L;

    /**
     * 小票编号
     */
    @TableId(value = "Fno", type = IdType.NONE)
    private String Fno;
    /**
     * 状态
     */
    private String Fzt;
    /**
     * 生产日期
     */
    private String Fscrq;
    /**
     * 生产拌台
     */
    private String Fscbt;
    private String Fdcsj;
    private String Fccsj;
    /**
     * 任务单号
     */
    private String Frwdh;

    private String Frwno;
    /**
     * 合同编号
     */
    private String Fhtbh;

    private Integer Fkhbh;
    /**
     * 单位名称
     */
    private String Fhtdw;
    /**
     * 工程名称
     */
    private String Fgcmc;
    /**
     * 工程地址
     */
    private String Fgcdz;
    /**
     * 施工部位
     */
    private String Fjzbw;
    /**
     * 施工方法
     */
    private String Fjzfs;
    /**
     * 公里数
     */
    private Double Fgls;
    /**
     * 砼品种
     */
    private String Ftpz;
    /**
     * 坍落度
     */
    private String Ftld;
    /**
     * 配合比号
     */
    private String Fsgpb;
    /**
     * 砂浆配比
     */
    private String Fsjpb;
    /**
     * 车号
     */
    private String Fshch;
    /**
     * 司机
     */
    private String Fsjxm;
    /**
     * 盘数
     */
    private String Fbcps;
    /**
     * 本车方数
     */
    private String Fbcfs;
    /**
     * 总重量
     */
    private String Fzzl;
    /**
     * 泵送数量
     */
    private String Fbsfs;
    /**
     * 累计方数
     */
    private String Fljfs;
    /**
     * 累计车数
     */
    private String Fljcs;
    /**
     * 出厂签发
     */
    private String Fccqf;
    /**
     * 操作员
     */
    private String Fczy;

    /**
     * 配合比号
     */
    private String Fphbno;
    /**
     * 备注
     */
    private String Fbz;

    @Override
    public String toString() {
        return "Tjlb{" +
        ", FNo=" + Fno +
        ", FZt=" + Fzt +
        ", FScrq=" + Fscrq +
        ", FScbt=" + Fscbt +
        ", FDcsj=" + Fdcsj +
        ", FCcsj=" + Fccsj +
        ", FRwdh=" + Frwdh +
        ", FHtbh=" + Fhtbh +
        ", FKhbh=" + Fkhbh +
        ", FHtdw=" + Fhtdw +
        ", FGcmc=" + Fgcmc +
        ", FGcdz=" + Fgcdz +
        ", FJzbw=" + Fjzbw +
        ", FJzfs=" + Fjzfs +
        ", FGls=" + Fgls +
        ", FTpz=" + Ftpz +
        ", FTld=" + Ftld +
        ", FSgpb=" + Fsgpb +
        ", FSjpb=" + Fsjpb +
        ", FShch=" + Fshch +
        ", FSjxm=" + Fsjxm +
        ", FBcps=" + Fbcps +
        ", FBcfs=" + Fbcfs +
        ", FZzl=" + Fzzl +
        ", FBsfs=" + Fbsfs +
        ", FLjfs=" + Fljfs +
        ", FLjcs=" + Fljcs +
        ", FCcqf=" + Fccqf +
        ", FCzy=" + Fczy +
        ", FBz=" + Fbz +
        ", FPhbNo=" + Fphbno +
        "}";
    }

    @Override
    protected Serializable pkVal() {
        return getFno();
    }

    public String getFno() {
        return Fno;
    }

    public void setFno(String fno) {
        Fno = fno;
    }

    public String getFzt() {
        return Fzt;
    }

    public void setFzt(String fzt) {
        Fzt = fzt;
    }

    public String getFscrq() {
        return Fscrq;
    }

    public void setFscrq(String fscrq) {
        Fscrq = fscrq;
    }

    public String getFscbt() {
        return Fscbt;
    }

    public void setFscbt(String fscbt) {
        Fscbt = fscbt;
    }

    public String getFdcsj() {
        return Fdcsj;
    }

    public void setFdcsj(String fdcsj) {
        Fdcsj = fdcsj;
    }

    public String getFccsj() {
        return Fccsj;
    }

    public void setFccsj(String fccsj) {
        Fccsj = fccsj;
    }

    public String getFrwdh() {
        return Frwdh;
    }

    public void setFrwdh(String frwdh) {
        Frwdh = frwdh;
    }

    public String getFhtbh() {
        return Fhtbh;
    }

    public void setFhtbh(String fhtbh) {
        Fhtbh = fhtbh;
    }

    public Integer getFkhbh() {
        return Fkhbh;
    }

    public void setFkhbh(Integer fkhbh) {
        Fkhbh = fkhbh;
    }

    public String getFhtdw() {
        return Fhtdw;
    }

    public void setFhtdw(String fhtdw) {
        Fhtdw = fhtdw;
    }

    public String getFgcmc() {
        return Fgcmc;
    }

    public void setFgcmc(String fgcmc) {
        Fgcmc = fgcmc;
    }

    public String getFgcdz() {
        return Fgcdz;
    }

    public void setFgcdz(String fgcdz) {
        Fgcdz = fgcdz;
    }

    public String getFjzbw() {
        return Fjzbw;
    }

    public void setFjzbw(String fjzbw) {
        Fjzbw = fjzbw;
    }

    public String getFjzfs() {
        return Fjzfs;
    }

    public void setFjzfs(String fjzfs) {
        Fjzfs = fjzfs;
    }

    public Double getFgls() {
        return Fgls;
    }

    public void setFgls(Double fgls) {
        Fgls = fgls;
    }

    public String getFtpz() {
        return Ftpz;
    }

    public void setFtpz(String ftpz) {
        Ftpz = ftpz;
    }

    public String getFtld() {
        return Ftld;
    }

    public void setFtld(String ftld) {
        Ftld = ftld;
    }

    public String getFsgpb() {
        return Fsgpb;
    }

    public void setFsgpb(String fsgpb) {
        Fsgpb = fsgpb;
    }

    public String getFsjpb() {
        return Fsjpb;
    }

    public void setFsjpb(String fsjpb) {
        Fsjpb = fsjpb;
    }

    public String getFshch() {
        return Fshch;
    }

    public void setFshch(String fshch) {
        Fshch = fshch;
    }

    public String getFsjxm() {
        return Fsjxm;
    }

    public void setFsjxm(String fsjxm) {
        Fsjxm = fsjxm;
    }

    public String getFbcps() {
        return Fbcps;
    }

    public void setFbcps(String fbcps) {
        Fbcps = fbcps;
    }

    public String getFbcfs() {
        return Fbcfs;
    }

    public void setFbcfs(String fbcfs) {
        Fbcfs = fbcfs;
    }

    public String getFzzl() {
        return Fzzl;
    }

    public void setFzzl(String fzzl) {
        Fzzl = fzzl;
    }

    public String getFbsfs() {
        return Fbsfs;
    }

    public void setFbsfs(String fbsfs) {
        Fbsfs = fbsfs;
    }

    public String getFljfs() {
        return Fljfs;
    }

    public void setFljfs(String fljfs) {
        Fljfs = fljfs;
    }

    public String getFljcs() {
        return Fljcs;
    }

    public void setFljcs(String fljcs) {
        Fljcs = fljcs;
    }

    public String getFccqf() {
        return Fccqf;
    }

    public void setFccqf(String fccqf) {
        Fccqf = fccqf;
    }

    public String getFczy() {
        return Fczy;
    }

    public void setFczy(String fczy) {
        Fczy = fczy;
    }

    public String getFphbno() {
        return Fphbno;
    }

    public void setFphbno(String fphbno) {
        Fphbno = fphbno;
    }

    public String getFbz() {
        return Fbz;
    }

    public void setFbz(String fbz) {
        Fbz = fbz;
    }

    public String getFrwno() {
        return Frwno;
    }

    public void setFrwno(String frwno) {
        Frwno = frwno;
    }
}
