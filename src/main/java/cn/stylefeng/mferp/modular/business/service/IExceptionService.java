package cn.stylefeng.mferp.modular.business.service;

import cn.stylefeng.mferp.modular.business.model.Exception;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 异常信息 服务类
 * </p>
 *
 * @author yzr
 * @since 2021-03-23
 */
public interface IExceptionService extends IService<Exception> {

}
