package cn.stylefeng.mferp.modular.business.service.impl;

import cn.stylefeng.mferp.modular.business.model.OffcutLog;
import cn.stylefeng.mferp.modular.business.dao.OffcutLogMapper;
import cn.stylefeng.mferp.modular.business.service.IOffcutLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 剩料记录表 服务实现类
 * </p>
 *
 * @author yzr
 * @since 2020-06-09
 */
@Service
public class OffcutLogServiceImpl extends ServiceImpl<OffcutLogMapper, OffcutLog> implements IOffcutLogService {

}
