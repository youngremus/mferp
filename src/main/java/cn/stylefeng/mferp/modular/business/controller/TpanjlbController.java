package cn.stylefeng.mferp.modular.business.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import cn.stylefeng.mferp.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import cn.stylefeng.mferp.modular.business.model.Tpanjlb;
import cn.stylefeng.mferp.modular.business.service.ITpanjlbService;

/**
 * 每盘生产信息控制器
 *
 * @author fengshuonan
 * @Date 2020-05-27 23:27:30
 */
@Controller
@RequestMapping("/tpanjlb")
public class TpanjlbController extends BaseController {

    private String PREFIX = "/business/tpanjlb/";

    @Autowired
    private ITpanjlbService tpanjlbService;

    /**
     * 跳转到每盘生产信息首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "tpanjlb.html";
    }

    /**
     * 跳转到添加每盘生产信息
     */
    @RequestMapping("/tpanjlb_add")
    public String tpanjlbAdd() {
        return PREFIX + "tpanjlb_add.html";
    }

    /**
     * 跳转到修改每盘生产信息
     */
    @RequestMapping("/tpanjlb_update/{tpanjlbId}")
    public String tpanjlbUpdate(@PathVariable Integer tpanjlbId, Model model) {
        Tpanjlb tpanjlb = tpanjlbService.selectById(tpanjlbId);
        model.addAttribute("item",tpanjlb);
        LogObjectHolder.me().set(tpanjlb);
        return PREFIX + "tpanjlb_edit.html";
    }

    /**
     * 获取每盘生产信息列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return tpanjlbService.selectList(null);
    }

    /**
     * 新增每盘生产信息
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Tpanjlb tpanjlb) {
        tpanjlbService.insert(tpanjlb);
        return SUCCESS_TIP;
    }

    /**
     * 删除每盘生产信息
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer tpanjlbId) {
        tpanjlbService.deleteById(tpanjlbId);
        return SUCCESS_TIP;
    }

    /**
     * 修改每盘生产信息
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Tpanjlb tpanjlb) {
        tpanjlbService.updateById(tpanjlb);
        return SUCCESS_TIP;
    }

    /**
     * 每盘生产信息详情
     */
    @RequestMapping(value = "/detail/{tpanjlbId}")
    @ResponseBody
    public Object detail(@PathVariable("tpanjlbId") Integer tpanjlbId) {
        return tpanjlbService.selectById(tpanjlbId);
    }
}
