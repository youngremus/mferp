package cn.stylefeng.mferp.modular.business.job;

import cn.stylefeng.mferp.config.MqUtils;
import cn.stylefeng.mferp.config.RedisUtils;
import cn.stylefeng.mferp.modular.business.model.Trwd;
import cn.stylefeng.mferp.modular.business.service.ITrwdService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Description: 从配置文件加载任务信息
 * @Author: lxc
 * @Date: Created in 2018/12/17
 */

//@Component
//@PropertySource(value = "classpath:application-jdbc.properties")
public class ScheduledTask {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Resource
    private RedisUtils redisUtils;

    //@Resource
    //private MqUtils mqUtils;

    //@Resource
    //private ITrwdService trwdService;

    //@Value("${yzr.switch.trwdSync}")
    //private boolean trwdSync;

    @Scheduled(fixedDelayString = "2000")
    public void getTask1(){
        System.out.println("【任务1,从配置文件加载任务信息，当前时间：】" + dateFormat.format(new Date()));
    }

    @Scheduled(cron = "${cron}")
    public void getTask2() {
//        if(!trwdSync) return;
//        System.out.println("【任务2,从配置文件加载任务信息，当前时间：】" + dateFormat.format(new Date()));
        // 读取Mysql数据库任务单
//        Wrapper<Trwd> wrapper = new EntityWrapper<>();
//        List<String> orderByList = new ArrayList<>();
//        orderByList.add("Frwdh");
//        wrapper.orderAsc(orderByList);
//        List<Trwd> list = trwdService.selectList(wrapper);
//
//        if(list != null && !list.isEmpty()) {
//            for(Trwd trwd : list) {
//                if(redisUtils.exist(trwd.getFrwdh().toString())) {
//                    // 推送mq
//                    mqUtils.sendQueueMessage("rwdSync", JSON.toJSONString(trwd));
//                }
//            }
//        }
    }

}
