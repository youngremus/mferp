package cn.stylefeng.mferp.modular.business.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentFontStyle;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import lombok.Data;

/**
 * 资料室导出砂浆数据用
 * @author: yzr
 * @date: 2020/5/23
 */
@Data
// 头背景设置成红色 IndexedColors.RED.getIndex()
//@HeadStyle(fillPatternType = FillPatternType.SOLID_FOREGROUND, fillForegroundColor = 10)
// 头字体设置成20
@HeadFontStyle(fontHeightInPoints = 11)
// 内容的背景设置成绿色 IndexedColors.GREEN.getIndex()
//@ContentStyle(fillPatternType = FillPatternType.SOLID_FOREGROUND, fillForegroundColor = 17)
// 内容字体设置成20
@ContentFontStyle(fontHeightInPoints = 11)
public class TjlbSjModel {
    @ColumnWidth(12)
    @ExcelProperty("日期")
    private String fscrq;
    @ColumnWidth(30)
    @ExcelProperty("工地名称")
    private String fgcmc;
    @ColumnWidth(30)
    @ExcelProperty("部位")
    private String fjzbw;
    @ColumnWidth(12)
    @ExcelProperty("强度等级")
    private String ftpz;
    @ColumnWidth(12)
    @ExcelProperty("出厂时间")
    private String fccsj;
    @ColumnWidth(10)
    @ExcelProperty("车号")
    private String fshch;
    @ColumnWidth(10)
    @ExcelProperty("方量")
    private String fbcfs;
    @ColumnWidth(14)
    @ExcelProperty("代码")
    private String fphbno;
    @ColumnWidth(12)
    @ExcelProperty("任务单号")
    private String frwno;
}
