package cn.stylefeng.mferp.modular.business.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentFontStyle;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 资料室导出数据用
 * </p>
 *
 * @author yzr
 * @since 2021-05-13
 */

@Data
// 头背景设置成红色 IndexedColors.RED.getIndex()
//@HeadStyle(fillPatternType = FillPatternType.SOLID_FOREGROUND, fillForegroundColor = 10)
// 头字体设置成20
@HeadFontStyle(fontHeightInPoints = 11)
// 内容的背景设置成绿色 IndexedColors.GREEN.getIndex()
//@ContentStyle(fillPatternType = FillPatternType.SOLID_FOREGROUND, fillForegroundColor = 17)
// 内容字体设置成20
@ContentFontStyle(fontHeightInPoints = 11)
public class TrwdModel {

    /**
     * 任务单号
     */
    @ColumnWidth(12)
    @ExcelProperty("任务单号")
    private Integer frwdh;
    /**
     * 生产拌台
     */
    @ColumnWidth(12)
    @ExcelProperty("生产拌台")
    private String fscbt;
    /**
     * 单位名称
     */
    @ColumnWidth(20)
    @ExcelProperty("单位名称")
    private String fhtdw;
    /**
     * 工程名称
     */
    @ColumnWidth(20)
    @ExcelProperty("工程名称")
    private String fgcmc;
    /**
     * 施工部位
     */
    @ColumnWidth(20)
    @ExcelProperty("施工部位")
    private String fjzbw;
    /**
     * 计划方量
     */
    @ColumnWidth(12)
    @ExcelProperty("计划方量")
    private Double fjhsl;
    /**
     * 泵送
     */
    @ColumnWidth(12)
    @ExcelProperty("浇筑方式")
    private String fjzfs;
    /**
     * 砼品种
     */
    @ColumnWidth(12)
    @ExcelProperty("砼品种")
    private String ftpz;
    /**
     * 坍落度
     */
    @ColumnWidth(12)
    @ExcelProperty("坍落度")
    private String ftld;
    /**
     * 状态
     */
    @ColumnWidth(12)
    @ExcelProperty("状态")
    private String fzt;
    /**
     * 计划日期
     */
    @ColumnWidth(12)
    @ExcelProperty("计划日期")
    private String fjhrq;

}
