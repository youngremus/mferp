package cn.stylefeng.mferp.socketio;

import lombok.*;

/**
 * @author hxy
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PushMessage {
    private String loginUserNum;
    private String content;
}