package cn.stylefeng.mferp.socketio;

import com.corundumstudio.socketio.SocketIOClient;

import java.util.Map;

/**
 * @author hxy
 */
public interface SocketIOService {

    /**
     * 推送的事件
     */
    public static final String PUSH_EVENT = "push_event";

    /**
     * 推送成功
     */
    public static final String TRANSFER_SUCCESS = "transfer_success";

    /**
     * 推送失败
     */
    public static final String TRANSFER_FAIL = "transfer_fail";

    /**
     * getorder
     */
    public static final String GET_ORDER = "get_order";

    /**
     * 启动服务
     * @throws Exception
     */
    void start() throws Exception;

    /**
     * 停止服务
     */
    void stop();

    /**
     * 断开连接
     * @param loginUserNum
     */
    void disconnect(String loginUserNum);

    /**
     * 推送信息
     * @param pushMessage
     */
    void pushMessageToUser(PushMessage pushMessage);

    /**
     * 消息推送
     * @param loginUserNum
     * @param content
     * @param event
     */
    public void pushMessageToUser(String loginUserNum, String content, String event);

    /**
     * map
     * @return
     */
    public Map<String, SocketIOClient> getMaps();
}
