package cn.stylefeng.mferp.socketio.remind;

/**
 * @author yzr
 */
public interface RemindService {
    /**
     * 启动服务
     * @throws Exception 异常
     */
    //void start() throws Exception;

    /**
     * 停止服务
     */
    //void stop();

    /**
     * 断开连接
     * @param username 用户名
     */
    //void disconnect(String username);

    /**
     * 推送信息
     * @param remindMessage 提醒信息
     */
    //void pushMessageToUser(RemindMessage remindMessage);

    /**
     * 消息推送
     * @param username 用户名
     * @param content 信息内容
     * @param event 监听事件名
     */
    //void pushMessageToUser(String username, String content, String event);

}
