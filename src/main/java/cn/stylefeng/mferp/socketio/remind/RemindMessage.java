package cn.stylefeng.mferp.socketio.remind;

import lombok.*;

/**
 * @author yzr
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RemindMessage {
    private String username;
    private String content;

}
